package com.cargowala;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.cargowala.Models.FcmToken;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/17/2016.
 */
public class Login extends FragmentActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {

    MyTextView forgot_pass;
    LinearLayout register,login;
    MyEditText loginemail,loginpassword;
    CallbackManager callbackManager;
    AccessToken accessToken;
    ImageView fb_login,google_login;
    SweetAlertDialog pDialog;
    String input_type;
    GoogleApiClient mGoogleApiClient;
    int RC_SIGN_IN = 69;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    boolean wasGoogleLoggedIn;
    String fcmToken="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login);

        Log.e("onCreate", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        try {
            fcmToken = pref.getString("fcmToken","");
            if(fcmToken.equalsIgnoreCase("") || fcmToken.isEmpty() || fcmToken == null){
                if(FirebaseInstanceId.getInstance().getToken() != null){
                    fcmToken=FirebaseInstanceId.getInstance().getToken();
                    Log.e("fcmToken", "XX");
                }else {
                    Log.e("getToken", "null");
                }
            }else {
                Log.e("fcmToken", "preff!null");
            }
        } catch (Exception e) {
            fcmToken="";
            Log.e("fcmToken", "Exception");
        }
        Log.e("fcmToken", fcmToken);
        callbackManager = CallbackManager.Factory.create();


        Intent intent = getIntent();
        wasGoogleLoggedIn = intent.getExtras().getBoolean("wasGoogleLoggedIn");


        Log.d("1 ","abc "+pref.getString("userFirstName","NA"));
        Log.d("1 ","abc "+pref.getString("userLastName","NA"));
        Log.d("1 ","abc "+pref.getString("userEmail","NA"));
        Log.d("1 ","abc "+pref.getString("userImage","NA"));
        Log.d("1 ","abc "+pref.getString("logged_in_with","NA"));
        Log.d("1 ","abc "+pref.getBoolean("isLoggedIn", false));


        Log.d("123 ","abc ");




        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        accessToken = loginResult.getAccessToken();


                        GraphRequest request = GraphRequest.newMeRequest(
                                accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        // Application code

                                        try {




                                       /*     pDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                                            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                                            pDialog.setTitleText("Loading");
                                            pDialog.setCancelable(false);
                                            pDialog.show();

                                            makeLoginReqFb(object);*/

                                            Log.e("fb", "object " + object);
                                            String name = object.getString("name");

//                                            String gender = object.getString("gender");
                                            email = object.getString("email");

                                            uid =  object.getString("id");

                                            Log.d("main id ",object.getString("id"));
                                            Log.d("main id ",object.getString("id"));
                                            Log.d("main id ",object.getString("id"));
                                            Log.d("main id ",object.getString("id"));

                                            String picture = object.getString("picture");
                                            JSONObject jsonobj = new JSONObject(picture);
                                            String nameUser = object.getString("name");
                                            String nameArr[] = nameUser.split(" ");
                                            String first_name =nameArr[0] ;
                                            String last_name = nameArr[1];

                                            String data = jsonobj.getString("data");
                                            JSONObject jsonobj1 = new JSONObject(data);

                                            String url = jsonobj1.getString("url");


                                         /*   Log.e("fb", "name " + name);
                                            Log.e("fb", "data " + first_name);
                                            Log.e("fb", "url " + last_name);
                                            Log.e("fb", "gender " + gender);
                                            Log.e("fb", "email " + email);
                                            Log.e("fb", "picture " + picture);
                                            Log.e("fb", "data " + data);
                                            Log.e("fb", "url " + url);*/


                                            editor.putString("userFirstName", first_name);
                                            editor.putString("userLastName", last_name);
                                            editor.putString("userEmail", email);
                                            editor.putString("userImage", url);

                                            editor.commit();

                                            pDialog = new SweetAlertDialog(Login.this, SweetAlertDialog.PROGRESS_TYPE);
                                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                            pDialog.setTitleText(getResources().getString(R.string.loading));
                                            pDialog.setCancelable(false);
                                            pDialog.show();
                                            input_type = "facebook";
                                            if(fcmToken.equalsIgnoreCase("") || fcmToken.isEmpty() || fcmToken == null){
                                                Toast.makeText(getBaseContext(),getResources().getString(R.string.fcm_token_null),Toast.LENGTH_SHORT).show();
                                            }else {
                                                makeLoginReq();
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,link,birthday,last_name,location,email,picture.type(large)");
                        request.setParameters(parameters);
                        request.executeAsync();


                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.e("fb", "cancel ");
                        LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("fb", "error " + exception);
                        LoginManager.getInstance().logOut();
                    }
                });



        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);





        fb_login =(ImageView) findViewById(R.id.fb_login);
        fb_login.setOnClickListener(this);
        google_login =(ImageView) findViewById(R.id.google_login);
        google_login.setOnClickListener(this);

        forgot_pass =(MyTextView) findViewById(R.id.forgot_pass);
        forgot_pass.setOnClickListener(this);

        register =(LinearLayout) findViewById(R.id.register);
        register.setOnClickListener(this);

        login =(LinearLayout) findViewById(R.id.login);
        login.setOnClickListener(this);

        loginemail =(MyEditText) findViewById(R.id.loginemail);
        loginpassword =(MyEditText) findViewById(R.id.loginpassword);




    }








    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        Log.e("fb", "onActivityResult " + data);


        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("google", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();


         /*   pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(false);
            pDialog.show();

            makeLoginReqGmail(acct);*/
            if(acct != null){
                String[] splited = acct.getDisplayName().split("\\s+");

                String firstName="" ,lastName="";

                for(int i=0;i<splited.length;i++) {


                    if(i == 0) {
                        firstName = splited[i];
                    }
                    else
                    {
                        lastName = lastName+splited[i];
                    }

                }


                Log.d("google", acct.getDisplayName());
                Log.d("google", acct.getEmail());
                Log.d("google", acct.getId());

                //     Log.d("google", acct.getPhotoUrl().toString());

                editor.putString("userFirstName", firstName);
                editor.putString("userLastName",lastName);
            }else {
                editor.putString("userFirstName", "");
                editor.putString("userLastName","");
            }

            editor.putString("userEmail", acct.getEmail());
       //     editor.putString("userImage",acct.getPhotoUrl().toString());
            editor.putString("userImage", "");

            try {
                editor.putString("userImage", acct.getPhotoUrl().toString());
            }

            catch (Exception e)
            {
                editor.putString("userImage", "");
            }

            editor.commit();
            email = acct.getEmail();
            uid = acct.getId();

            Log.d("main id google ",uid);

         /*   Log.d("main id 2 ",""+acct.getId());
            Log.d("main id 1 ",""+acct.getIdToken());*/


            pDialog = new SweetAlertDialog(Login.this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            pDialog.setTitleText(getResources().getString(R.string.loading));
            pDialog.setCancelable(false);
            pDialog.show();
            input_type = "google";

            if(fcmToken.equalsIgnoreCase("") || fcmToken.isEmpty() || fcmToken == null){
                Toast.makeText(getBaseContext(),getResources().getString(R.string.fcm_token_null),Toast.LENGTH_SHORT).show();
            }else {
                makeLoginReq();
            }



        } else {
            // Signed out, show unauthenticated UI.
            Log.e("google", "logout");
        }
    }


    String email,password,uid;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.popup_button:
                dialog.dismiss();


                break;

            case R.id.popup_button_yes2:
                dialog1.dismiss();


                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();
                makeResendEmailVerificationReq();


                break;

            case R.id.popup_button_no2:
                dialog1.dismiss();


                break;



            case R.id.forgot_pass:
                Intent i = new Intent(Login.this, ForgotPassword.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                break;


            case R.id.register:


                loginemail.setText("");
                loginpassword.setText("");
                Intent i1 = new Intent(Login.this, Register.class);
                i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i1);
                break;

            case R.id.google_login:
                if(isOnline()) {



                    if(!wasGoogleLoggedIn) {

                        Log.d("yyo ","goooooooo " );
                        signIn();
                    }
                    else
                    {
                        Log.d("yyo ","gooooooogle " );
                        signOut();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.check_your_network),
                            Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.fb_login:

                if(isOnline()) {
                    AccessToken token = AccessToken.getCurrentAccessToken();

                    if (token == null) {
                        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email", "user_birthday"));
                    } else {

                        LoginManager.getInstance().logOut();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.check_your_network),
                            Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.login:

                //changee

                email = loginemail.getText().toString();
                email = email.trim();
                 password = loginpassword.getText().toString();

                if (email == "" || email.isEmpty() || email == null) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_the_email),
                            Toast.LENGTH_SHORT).show();


                } else {

                    if (isValidEmail(email)) {

                        if (password.equalsIgnoreCase("")) {
                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.please_enter_password), Toast.LENGTH_SHORT)
                                    .show();


                        } else {

                            if (password.length() < 6) {
                                Toast.makeText(
                                        getApplicationContext(),
                                        getResources().getString(R.string.password_lenght_msg),
                                        Toast.LENGTH_SHORT).show();
                            } else {

                                if(fcmToken.equalsIgnoreCase("") || fcmToken.isEmpty() || fcmToken == null){
                                    Toast.makeText(getBaseContext(),getResources().getString(R.string.fcm_token_null),Toast.LENGTH_SHORT).show();
                                }else {
                                    if (isOnline()) {

                                        editor.putString("userEmail",email);
                                        editor.commit();
                                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                        pDialog.setTitleText(getResources().getString(R.string.loading));
                                        pDialog.setCancelable(false);
                                        pDialog.show();
                                        input_type = "manual";
                                        makeLoginReq();


                                    } else {
                                        Toast.makeText(getApplicationContext(),
                                                getResources().getString(R.string.check_your_network),
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }
                        }
                    }

                    else {
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.please_enter_valid_email), Toast.LENGTH_SHORT)
                                .show();
                    }
                }


            /*    editor.putBoolean("isLoggedIn", true);
                editor.commit();
                Intent i7 = new Intent(Login.this, CreateProfile.class);
                i7.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i7);
                finish();*/





                break;

        }



    }












    String res;
    StringRequest strReq;

    public String makeLoginReq() {

        String url = getResources().getString(R.string.base_url)+"user/login";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkLoginResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                   /* InstanceID instanceID = InstanceID.getInstance(Login.this);
                    Log.d("token", "2");
                    String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);*/

                    Log.d("email  ",  email);



                    if(input_type.equalsIgnoreCase("manual")) {

                        Log.d("password  ",  password);

                        params.put("username", email);


                        params.put("password", password);
                        params.put("mobile_tokens", fcmToken);

                    }
                    if(input_type.equalsIgnoreCase("facebook")) {

                        params.put("username", email);
                        params.put("social", "facebook");
                        params.put("mobile_tokens", fcmToken);

                        params.put("uid", uid);
                       // params.put("mobile_tokens", pref.getString("userImage",""));

                        editor.putString("logged_in_with","facebook");
                        editor.commit();

                    }

                    if(input_type.equalsIgnoreCase("google")) {


                        params.put("username", email);
                        params.put("uid", uid);
                        params.put("social", "google");
                        params.put("mobile_tokens", fcmToken);
                        editor.putString("logged_in_with", "google");
                        editor.commit();

                    }

                    Log.e("register", params.toString());



                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "30");



        return null;
    }

    public void checkLoginResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                String status_code = reader.getString("status_code");




                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {


                 /*   initiatePopupWindow("Success",
                            false, "Email Sent", "OK");*/
               //     String sToken = reader.getString("sToken");
                    String response1 = reader.getString("response");
                    JSONObject jsonobj = new JSONObject(response1);
                    String id = jsonobj.getString("_id");
                    Log.d("userId ",id);
                    Log.d("firstname ",jsonobj.getString("firstname"));

                   /* if(jsonobj.has("firstname")){*/
                        String firstname = jsonobj.getString("firstname");
                    /*}*/


                    String lastname = jsonobj.getString("lastname");
                    String email = jsonobj.getString("email");
                    String mobile_no = jsonobj.getString("mobile_no");
                    String image = jsonobj.getString("image");
                    String security_token = jsonobj.getString("security_token");
                    String user_type = jsonobj.getString("user_type");
                    String account_type=jsonobj.getString("account_type");
                    editor.putString("user_account_type", account_type);

                    if(user_type.equalsIgnoreCase("Agent")){
                        if(jsonobj.has("business_name")){
                            String business_name = jsonobj.getString("business_name");
                            editor.putString("user_business_name", business_name);
                        }
                        editor.putString("user_account_type", account_type);
                    }




                //    Log.d("sToken ",sToken);
                    Log.d("userId ",id);
                    Log.d("firstname ",firstname);
                    Log.d("lastname ",lastname);
                    Log.d("email ",email);
                    Log.d("mobile_no ",mobile_no);
                    Log.d("image ", image);


                    editor.putBoolean("isLoggedIn", true);
                    editor.putString("userId", id);
                    editor.putString("security_token", security_token);
                    editor.commit();

                    if(!mobile_no.equalsIgnoreCase("")) {
                        editor.putString("user_firstname", firstname);
                        editor.putString("user_lastname", lastname);
                        editor.putString("user_email", email);
                        editor.putString("user_mobile_no", mobile_no);
                        editor.putString("user_image", image);
                        editor.putString("user_image_url", image);
                        editor.putString("userImage", image);
                        editor.putBoolean("isProfileCreated", true);
                        editor.commit();

                        Intent i1 = new Intent(Login.this, Home.class);
                        i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(i1);
                        finish();

                    }


                    else {

                        Intent i1 = new Intent(Login.this, CreateProfile.class);
                        i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(i1);
                        finish();
                    }
                }
                else if(status_code.equalsIgnoreCase("400") )
                {



                    initiatePopupWindow(getResources().getString(R.string.incorrect_email_id_or_password),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }


                else if(status_code.equalsIgnoreCase("204"))
                {



                    initiatePopupWindow(getResources().getString(R.string.this_email_has_not_been_registered_with_us),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }

                else if(status_code.equalsIgnoreCase("909"))
                {


                    //verify email resend verification n all

                    initiateyYesNoWindow(getResources().getString(R.string.this_email_has_not_been_verified_yet_do_you_wish_to_resend_verification_mail),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.yes),getResources().getString(R.string.no));

                }
                else if(status_code.equalsIgnoreCase("910"))
                {


                   initiatePopupWindow(getResources().getString(R.string.account_has_been_deactivated),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }
                else if(status_code.equalsIgnoreCase("401"))
                {

                    initiatePopupWindow(getResources().getString(R.string.you_have_already_registered_with_this_email_id),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }
                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {

        }
    }





    Dialog dialog1;

    private void initiateyYesNoWindow(String message, Boolean isAlert, String heading, String buttonTextYes,  String buttonTextNo) {
        try {

            dialog1 = new Dialog(Login.this);
            dialog1.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.yesnopopup);
            dialog1.setCanceledOnTouchOutside(false);
            MyTextView popup_message2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_message2);
            popup_message2.setText(message);


            ImageView popup_image2 = (ImageView) dialog1
                    .findViewById(R.id.popup_image2);
            if(isAlert)
            {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_heading2);
            popup_heading2.setText(heading);


            MyTextView popup_button_yes2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_yes2);
            popup_button_yes2.setText(buttonTextYes);
            popup_button_yes2.setOnClickListener(this);

            MyTextView popup_button_no2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_no2);
            popup_button_no2.setText(buttonTextNo);
            popup_button_no2.setOnClickListener(this);

            dialog1.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }











    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(Login.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }















    String res1;
    StringRequest strReq1;

    public String makeResendEmailVerificationReq() {

        String url = getResources().getString(R.string.base_url)+"user/resend-email-token";
        Log.d("regg", "2");
        strReq1 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res1 = response.toString();
                        checkResendEmailVerificationResponse(res1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res1 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    Log.d("email  ", email);
                    params.put("email", email);

                    Log.e("register", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq1, "31");



        return null;
    }

    public void checkResendEmailVerificationResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                String status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    Log.e("register","status_code "+status_code);

                    initiatePopupWindow(getResources().getString(R.string.an_verification_mail_has_been_sent_to_your_email_id),
                            false, getResources().getString(R.string.email_sent), getResources().getString(R.string.ok));

                }
              /*  else if(status_code.equalsIgnoreCase("400"))
                {



                    initiatePopupWindow("Incorrect username or password.",
                            true, "Error", "OK");

                }
                else if(status_code.equalsIgnoreCase("909"))
                {


                    //verify email resend verification n all

                    initiateyYesNoWindow("This email has not been verified yet. Do you wish to resend verification mail.",
                            true, "Error", "YES","NO");

                }
                else if(status_code.equalsIgnoreCase("910"))
                {


                    initiatePopupWindow("This user is inactive. Please contact us to know the problem.",
                            true, "Error", "OK");

                }*/
                else
                {
                    Log.e("register","problem ");

                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {

                Log.e("register","problem1 ");
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
            Log.e("register","problem2 "+e);
        }
    }


    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("google", "connectionResult "+connectionResult);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        signIn();
                    }
                });
    }





    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }



    boolean exitApp = false;

    @Override
    public void onBackPressed() {
        if (exitApp) {
            //super.onBackPressed();
            moveTaskToBack(true);
            //return;
        }

        Toast.makeText(this, getResources().getString(R.string.press_again_to_exit), Toast.LENGTH_SHORT).show();
        this.exitApp = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                exitApp = false;
            }
        }, 2000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showCompleteDialog(FcmToken fcmTokenObject) {
        fcmToken=fcmTokenObject.getFcmToken();
    }

}
