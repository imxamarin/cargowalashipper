package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.bumptech.glide.Glide;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class PendingShipment extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton,map_image,delete_image;
    MyTextView pending_text,route_text,schedule_text;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    CardView card_view;
    String url="";
    SweetAlertDialog pDialog;
    boolean removeOrder = false;
    SharedPreferences pref1;
    ImageView blank_incomplete_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_shipment);

        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();


        pending_text = (MyTextView) findViewById(R.id.pending_text);
        route_text = (MyTextView) findViewById(R.id.route_text);
        schedule_text = (MyTextView) findViewById(R.id.schedule_text);
        map_image = (ImageView) findViewById(R.id.map_image);
        map_image.setOnClickListener(this);
        delete_image = (ImageView) findViewById(R.id.delete_image);
        delete_image.setOnClickListener(this);
        card_view = (CardView) findViewById(R.id.card_view);


        blank_incomplete_image = (ImageView) findViewById(R.id.blank_incomplete_image);

      /*  editor.putBoolean("book_pending_order", true);
        editor.putString("book_origin_lat", "" + origin.latitude);
        editor.putString("book_origin_long", "" + origin.longitude);
        editor.putString("book_dest_lat", ""+dest.latitude);
        editor.putString("book_dest_long", ""+dest.longitude);
        editor.putString("book_date", ""+date);
        editor.putString("book_time", ""+time);
        editor.putString("book_encoded_polyline", ""+encoded_polyline);
        editor.putString("book_origin_address", ""+origin_address);
        editor.putString("book_odest_address", ""+dest_address);

        editor.putString("book_origin_city", ""+origin_city);
        editor.putString("book_dest_city", ""+dest_city);*/


        if(pref.getBoolean("book_pending_order",false))
        {
           /* url = url+"https://maps.googleapis.com/maps/api/staticmap?size=800x800&markers=shadow:true|";
            url = url+pref.getString("book_origin_lat","NA")+","+pref.getString("book_origin_long","NA");
            url = url+ "&markers=shadow:true|";
            url = url+pref.getString("book_dest_lat","NA")+","+pref.getString("book_dest_long","NA");
            url = url+ "&path=weight:3%7Ccolor:blue%7Cenc:";
            url = url+ pref.getString("book_encoded_polyline","NA");
            url = url+ "&key=AIzaSyAvYeajGEsqSHc41kbSEh8hqb3URlt4J4E";*/

            url = pref.getString("book_map_url","NA");
                   /* Picasso.with(getApplicationContext())
                    .load(url)
				.placeholder(R.drawable.loading_screen_square)
				.error(R.drawable.unable_upload_square)
				.resize(deviceWidth, deviceHeight).centerInside().into(map_image);*/
            Glide.with(PendingShipment.this)
                    .load(url)
                    .placeholder(R.drawable.loading_screen_square)
                    .error(R.drawable.unable_upload_square)
                    .into(map_image);

            route_text.setText(pref.getString("book_origin_city","NA")+" to "+pref.getString("book_dest_city","NA"));
            schedule_text.setText(pref.getString("book_date","NA")+" at "+pref.getString("book_time","NA"));
        }
        else

        {
            blank_incomplete_image.setVisibility(View.VISIBLE);
          //  pending_text.setText("No Incomplete Order");

            pending_text.setVisibility(View.GONE);
            card_view.setVisibility(View.GONE);
        }



    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {




            case R.id.backbutton:

                finish();

                break;



            case R.id.map_image:


                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
                String current_time = sdf.format(c.getTime());


                String book_time = pref.getString("book_old_date","NA")+pref.getString("book_time","NA");

                Log.d("current_time ","current_time "+current_time);
                Log.d("book_time ","book_time "+book_time);



                try {
                    Date current_time1 = sdf.parse(current_time);
                    Date book_time1 = sdf.parse(book_time);

                    long diff = book_time1.getTime() - current_time1.getTime();
                    long seconds = diff / 1000;
                    long minutes = seconds / 60;
                    long hours = minutes / 60;
                    long days = hours / 24;



                    Log.d("diff","diff "+diff);
                    Log.d("seconds","seconds "+seconds);
                    Log.d("minutes","minutes "+minutes);
                    Log.d("hours","hours "+hours);
                    Log.d("days","days "+days);




                    if(hours<5)
                    {
                      /*  Toast.makeText(getApplicationContext(),
                                "Time gap should be of atleast 8 hours", Toast.LENGTH_SHORT)
                                .show();*/
                        removeOrder = true;
                        initiatePopupWindow(getResources().getString(R.string.this_order_has_been_expired_and_will_be_removed_please_create_new_order),
                                true, getResources().getString(R.string.error) , getResources().getString(R.string.ok));
                    }
                    else
                    {





                           if (isOnline()) {
                                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                pDialog.setTitleText(getResources().getString(R.string.loading));
                                pDialog.setCancelable(false);
                                pDialog.show();
                                makeBookingDetailsReq();
                            } else {
                                Toast.makeText(PendingShipment.this,
                                        getResources().getString(R.string.check_your_network),
                                        Toast.LENGTH_SHORT).show();
                            }

                        }



                } catch (ParseException ex) {

                }



            /*    Intent i = new Intent(PendingShipment.this, BookOneTruck.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();*/

                break;



            case R.id.delete_image:

                initiateyYesNoWindow(getResources().getString(R.string.are_you_sure_you_want_to_remove_your_pending_shipment),
                        true, getResources().getString(R.string.delete_shipment), getResources().getString(R.string.yes), getResources().getString(R.string.no));
                break;

            case R.id.popup_button_yes2:
                dialog1.dismiss();

                editor.clear();
                editor.commit();

                Intent i1 = new Intent(PendingShipment.this, PendingShipment.class);
                i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i1);

                finish();

            break;

            case R.id.popup_button_no2:
                dialog1.dismiss();


                break;



            case R.id.popup_button:
                dialog.dismiss();

                if(removeOrder) {
                    editor.clear();
                    editor.commit();

                    Intent i2 = new Intent(PendingShipment.this, PendingShipment.class);
                    i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i2);

                    finish();
                }
                break;

        }

    }

    Dialog dialog1;

    private void initiateyYesNoWindow(String message, Boolean isAlert, String heading, String buttonTextYes,  String buttonTextNo) {
        try {

            dialog1 = new Dialog(PendingShipment.this);
            dialog1.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.yesnopopup);
            dialog1.setCanceledOnTouchOutside(false);
            MyTextView popup_message2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_message2);
            popup_message2.setText(message);


            ImageView popup_image2 = (ImageView) dialog1
                    .findViewById(R.id.popup_image2);
            if(isAlert)
            {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_heading2);
            popup_heading2.setText(heading);


            MyTextView popup_button_yes2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_yes2);
            popup_button_yes2.setText(buttonTextYes);
            popup_button_yes2.setOnClickListener(this);

            MyTextView popup_button_no2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_no2);
            popup_button_no2.setText(buttonTextNo);
            popup_button_no2.setOnClickListener(this);

            dialog1.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(PendingShipment.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }








    String res;
    StringRequest strReq;

    public String makeBookingDetailsReq() {

        String url = getResources().getString(R.string.base_url)+"user/get-data";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkBookingDetailsResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("_id",pref1.getString("userId","Something Wrong") );
                    params.put("security_token", pref1.getString("security_token","Something Wrong"));
                    Log.e("register ", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "37");

        return null;
    }

    String status_code = "0";

    public void checkBookingDetailsResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("get data ",response);

            if (response != null) {

                Log.e("register ","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register ","reader");

                status_code = reader.getString("status_code");

                Log.e("register ","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    String response1 = reader.getString("response");

                    if(response1.equalsIgnoreCase(pref.getString("book_get_data_response","")))
                    {
                        Intent i = new Intent(PendingShipment.this, BookOneTruck.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(i);
                        finish();
                    }

                    else
                    {
                        removeOrder = true;
                        initiatePopupWindow(getResources().getString(R.string.our_backend_has_been_updated_so_your_order_will_be_removed_please_create_new_order),
                                true, getResources().getString(R.string.error) , getResources().getString(R.string.ok));
                    }

                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }


                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code,getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.ok), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }












}
