package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class EditCompanyDetail extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton;

    MyTextView save;
    SweetAlertDialog pDialog;

    ImageView info_image,address_image;
    LinearLayout info_bar,address_bar;
    MaterialEditText buss_name,landline_no;

    boolean isChecked = true;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;
    String   company_business_no="",company_type_no="";


    LinearLayout registered_address,office_address,same_address;

    MaterialEditText register_address,register_city,register_state,register_pin,office_add,office_city,office_state,office_pin;
    ImageView same_checkbox;
    LinearLayout hide_same;

    LinearLayout ll1,ll2;
    ImageView img1,img2,img4,img5;

    LinearLayout ll11,ll22;
    ImageView img11,img22,img33,img44,img55,img66;
    MaterialEditText buss_type_other,comp_type_other;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_company_detail);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();



        info_image= (ImageView) findViewById(R.id.info_image);
        info_image.setOnClickListener(this);
        address_image= (ImageView) findViewById(R.id.address_image);
        address_image.setOnClickListener(this);

        info_bar= (LinearLayout) findViewById(R.id.info_bar);
        address_bar= (LinearLayout) findViewById(R.id.address_bar);

        save= (MyTextView) findViewById(R.id.save);
        save.setOnClickListener(this);



        buss_name= (MaterialEditText) findViewById(R.id.buss_name);
        landline_no= (MaterialEditText) findViewById(R.id.landline_no);
        buss_type_other = (MaterialEditText) findViewById(R.id.buss_type_other);
        comp_type_other = (MaterialEditText) findViewById(R.id.comp_type_other);

        buss_name.setText(pref.getString("user_business_name", ""));
        landline_no.setText(pref.getString("user_landline_number", ""));




        ll1 = (LinearLayout) findViewById(R.id.ll1);

        ll1.getLayoutParams().height =  (deviceHeight / 6);

        ll1.requestLayout();

        ll2 = (LinearLayout) findViewById(R.id.ll2);

        ll2.getLayoutParams().height =  (deviceHeight / 6);

        ll2.requestLayout();

        ll11 = (LinearLayout) findViewById(R.id.ll11);

        ll11.getLayoutParams().height =  (deviceHeight / 6);

        ll11.requestLayout();

        ll22 = (LinearLayout) findViewById(R.id.ll22);

        ll22.getLayoutParams().height =  (deviceHeight / 6);

        ll22.requestLayout();

        img1 = (ImageView) findViewById(R.id.img1);
        img1.setOnClickListener(this);
        img2 = (ImageView) findViewById(R.id.img2);
        img2.setOnClickListener(this);

        img4 = (ImageView) findViewById(R.id.img4);
        img4.setOnClickListener(this);
        img5 = (ImageView) findViewById(R.id.img5);
        img5.setOnClickListener(this);

        img11 = (ImageView) findViewById(R.id.img11);
        img11.setOnClickListener(this);
        img22 = (ImageView) findViewById(R.id.img22);
        img22.setOnClickListener(this);
        img33 = (ImageView) findViewById(R.id.img33);
        img33.setOnClickListener(this);
        img44 = (ImageView) findViewById(R.id.img44);
        img44.setOnClickListener(this);
        img55 = (ImageView) findViewById(R.id.img55);
        img55.setOnClickListener(this);
        img66 = (ImageView) findViewById(R.id.img66);
        img66.setOnClickListener(this);




        String business = pref.getString("user_business_type","NA");

        //Log.d("pref.getString(\"user_business_type\",\"NA\") ", "" + pref.getString("user_business_type", "NA"));

        if(business.equalsIgnoreCase("3"))
        {
            img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_active));
            company_business_no = "3";
        }
        else if(business.equalsIgnoreCase("1"))
        {
            img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_active));
            company_business_no = "1";
        }

        else if(business.equalsIgnoreCase("2"))
        {
            img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_active));
            company_business_no = "2";
        }
        else if(business.equalsIgnoreCase("4"))
        {
            img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));
            company_business_no = "4";

            buss_type_other.setVisibility(View.VISIBLE);
            buss_type_other.setText(pref.getString("user_buss_type_other", ""));
        }


        Log.d("dsgs", pref.getString("user_buss_type_other", ""));
        String type = pref.getString("user_company_type","NA");


        if(type.equalsIgnoreCase("5"))
        {
            img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_active));
            company_type_no = "5";
        }
        else if(type.equalsIgnoreCase("1"))
        {
            img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_active));
            company_type_no = "1";
        }
        else if(type.equalsIgnoreCase("2"))
        {
            img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_active));
            company_type_no = "2";
        }
        else if(type.equalsIgnoreCase("3"))
        {
            img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_active));
            company_type_no = "3";
        }
        else if(type.equalsIgnoreCase("4"))
        {
            img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));
            company_type_no = "4";

            comp_type_other.setVisibility(View.VISIBLE);
            comp_type_other.setText(pref.getString("user_comp_type_other", ""));
        }
        else if(type.equalsIgnoreCase("6"))
        {
            img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_active));
            company_type_no = "6";
        }




        register_address = (MaterialEditText) findViewById(R.id.register_address);
        register_city = (MaterialEditText) findViewById(R.id.register_city);
        register_state = (MaterialEditText) findViewById(R.id.register_state);
        register_pin = (MaterialEditText) findViewById(R.id.register_pin);
        office_add = (MaterialEditText) findViewById(R.id.office_add);
        office_city = (MaterialEditText) findViewById(R.id.office_city);
        office_state = (MaterialEditText) findViewById(R.id.office_state);
        office_pin = (MaterialEditText) findViewById(R.id.office_pin);


        register_address.setText(pref.getString("user_registered_address", ""));
        register_city.setText(pref.getString("user_register_city", ""));
        register_state.setText(pref.getString("user_register_state", ""));
        register_pin.setText(pref.getString("user_register_pin", ""));
        office_add.setText(pref.getString("user_office_address", ""));
        office_city.setText(pref.getString("user_office_city", ""));
        office_state.setText(pref.getString("user_office_state", ""));
        office_pin.setText(pref.getString("user_office_pin", ""));



        same_checkbox = (ImageView) findViewById(R.id.same_checkbox);
        same_checkbox.setOnClickListener(this);
        hide_same = (LinearLayout) findViewById(R.id.hide_same);

        registered_address = (LinearLayout) findViewById(R.id.registered_address);

        registered_address.getLayoutParams().height =  (deviceHeight / 20);

        registered_address.requestLayout();

        office_address = (LinearLayout) findViewById(R.id.office_address);

        office_address.getLayoutParams().height =  (deviceHeight / 20);

        office_address.requestLayout();

        same_address = (LinearLayout) findViewById(R.id.same_address);

        same_address.getLayoutParams().height =  (deviceHeight / 20);

        same_address.requestLayout();

            if( register_address.getText().toString().equalsIgnoreCase(office_add.getText().toString()) &&
                register_city.getText().toString().equalsIgnoreCase(office_city.getText().toString()) &&
                register_state.getText().toString().equalsIgnoreCase(register_state.getText().toString()) &&
                register_pin.getText().toString().equalsIgnoreCase(register_pin.getText().toString()))
        {
            hide_same.setVisibility(View.GONE);
            same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));
            isChecked = true;

        }
        else
        {
            hide_same.setVisibility(View.VISIBLE);
            same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));
            isChecked = false;

        }
    }

    String register_address_text,register_city_text,register_state_text,register_pin_text,
            office_add_text,office_city_text,office_state_text,office_pin_text,comp_buss_name,comp_landlin_no;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.backbutton:
                finish();
                break;
            case R.id.info_image:
                info_bar.setVisibility(View.VISIBLE);
                address_bar.setVisibility(View.GONE);
                info_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.info_active));
                address_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.address_inactive));
                break;
            case R.id.address_image:
                info_bar.setVisibility(View.GONE);
                address_bar.setVisibility(View.VISIBLE);
                info_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.info_inactive));
                address_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.address_active));
                break;
            case R.id.save:
                 comp_buss_name = buss_name.getText().toString().trim();
                 comp_landlin_no= landline_no.getText().toString().trim();
                if(comp_buss_name.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_bussiness_name), Toast.LENGTH_SHORT).show();
                }

                else {
                    if((comp_landlin_no.length() > 0 && comp_landlin_no.length() < 6) || comp_landlin_no.length() >12)
                    {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_landline), Toast.LENGTH_SHORT).show();
                    } else {



                        if(isChecked)
                        {

                             register_address_text = register_address.getText().toString().trim();
                             register_city_text = register_city.getText().toString().trim();
                             register_state_text = register_state.getText().toString().trim();
                             register_pin_text = register_pin.getText().toString().trim();

                            office_add_text = register_address_text;
                            office_city_text = register_city_text;
                            office_state_text = register_state_text;
                            office_pin_text = register_pin_text;


                            if(register_address_text.equalsIgnoreCase(""))
                            {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_address), Toast.LENGTH_SHORT).show();
                            }

                            else {

                                if(register_city_text.equalsIgnoreCase(""))
                                {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_city), Toast.LENGTH_SHORT).show();
                                }

                                else {

                                    if(register_state_text.equalsIgnoreCase(""))
                                    {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_state), Toast.LENGTH_SHORT).show();
                                    }

                                    else {

                                        if(register_pin_text.equalsIgnoreCase(""))
                                        {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_pin), Toast.LENGTH_SHORT).show();
                                        }

                                        else {
                                            if (register_pin_text.length() != 6) {
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_valid_pin), Toast.LENGTH_SHORT).show();
                                            } else {


                                                if(company_business_no.equalsIgnoreCase("4") &&  buss_type_other.getText().toString().trim().equalsIgnoreCase(""))
                                                {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_business_type), Toast.LENGTH_SHORT).show();

                                                }
                                                else {

                                                    if (company_type_no.equalsIgnoreCase("4") && comp_type_other.getText().toString().trim().equalsIgnoreCase("")) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_company_type), Toast.LENGTH_SHORT).show();

                                                    } else {
                                                        if (isOnline()) {
                                                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                                            pDialog.setTitleText(getResources().getString(R.string.loading));
                                                            pDialog.setCancelable(false);
                                                            pDialog.show();
                                                            makeRegAsCompReq();
                                                        } else {
                                                            Toast.makeText(EditCompanyDetail.this,
                                                                    getResources().getString(R.string.check_your_network),
                                                                    Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                }





                                            }
                                        }}}}


                        }
                        else
                        {

                            register_address_text = register_address.getText().toString().trim();
                            register_city_text= register_city.getText().toString().trim();
                            register_state_text= register_state.getText().toString().trim();
                            register_pin_text= register_pin.getText().toString().trim();

                            office_add_text = office_add.getText().toString().trim();
                            office_city_text= office_city.getText().toString().trim();
                            office_state_text= office_state.getText().toString().trim();
                            office_pin_text= office_pin.getText().toString().trim();





                            if(register_address_text.equalsIgnoreCase(""))
                            {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_address), Toast.LENGTH_SHORT).show();
                            }

                            else {

                                if(register_city_text.equalsIgnoreCase(""))
                                {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_city), Toast.LENGTH_SHORT).show();
                                }

                                else {

                                    if(register_state_text.equalsIgnoreCase(""))
                                    {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_state), Toast.LENGTH_SHORT).show();
                                    }

                                    else {

                                        if(register_pin_text.equalsIgnoreCase(""))
                                        {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_pin), Toast.LENGTH_SHORT).show();
                                        }

                                        else {
                                            if (register_pin_text.length() != 6) {
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_valid_pin), Toast.LENGTH_SHORT).show();
                                            } else {

                                                if (office_add_text.equalsIgnoreCase("")) {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_address), Toast.LENGTH_SHORT).show();
                                                } else {

                                                    if (office_city_text.equalsIgnoreCase("")) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_city), Toast.LENGTH_SHORT).show();
                                                    } else {

                                                        if (office_state_text.equalsIgnoreCase("")) {
                                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_state), Toast.LENGTH_SHORT).show();
                                                        } else {

                                                            if (office_pin_text.equalsIgnoreCase("")) {
                                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_pin_code), Toast.LENGTH_SHORT).show();
                                                            } else {
                                                                if (office_pin_text.length() != 6) {
                                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_valid_office_pin_code), Toast.LENGTH_SHORT).show();
                                                                } else {



                                                                 /*   if (isOnline()) {
                                                                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                                                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                                                        pDialog.setTitleText("Loading");
                                                                        pDialog.setCancelable(false);
                                                                        pDialog.show();
                                                                        makeRegAsCompReq();
                                                                    } else {
                                                                        Toast.makeText(EditCompanyDetail.this,
                                                                                "Please check your network connection",
                                                                                Toast.LENGTH_SHORT).show();
                                                                    }*/


                                                                    if(company_business_no.equalsIgnoreCase("")||company_business_no.equalsIgnoreCase("4"))
                                                                    {
                                                                        String tempCheck = buss_type_other.getText().toString();
                                                                        if(tempCheck!=null&&tempCheck.isEmpty())
                                                                        {
                                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_business_type), Toast.LENGTH_SHORT).show();
                                                                        }

                                                                    }
                                                                    else {

                                                                        if (company_type_no.equalsIgnoreCase("4") && comp_type_other.getText().toString().trim().equalsIgnoreCase("")) {
                                                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_company_type), Toast.LENGTH_SHORT).show();

                                                                        } else {
                                                                            if (isOnline()) {
                                                                                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                                                                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                                                                pDialog.setTitleText(getResources().getString(R.string.loading));
                                                                                pDialog.setCancelable(false);
                                                                                pDialog.show();
                                                                                makeRegAsCompReq();
                                                                            } else {
                                                                                Toast.makeText(EditCompanyDetail.this,
                                                                                        getResources().getString(R.string.check_your_network),
                                                                                        Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        }
                                                                    }





                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }}}}}





                        }




                    }
                }


                break;
            case R.id.same_checkbox:
                if(isChecked)
                {
                    hide_same.setVisibility(View.VISIBLE);
                    same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));
                    isChecked = false;

                }
                else
                {
                    hide_same.setVisibility(View.GONE);
                    same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));
                    isChecked = true;

                }

                break;

            case R.id.img1:
                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_active));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));

                company_business_no = "3";
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");

                break;

            case R.id.img2:
                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_active));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));

                company_business_no = "1";
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");

                break;


            case R.id.img4:

                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_active));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                company_business_no = "2";
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");

                break;

            case R.id.img5:

                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));
                company_business_no = "4";
                buss_type_other.setVisibility(View.VISIBLE);
                buss_type_other.setText("");

                break;



            case R.id.img11:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_active));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type_no = "5";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");

                break;

            case R.id.img22:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_active));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type_no = "1";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                break;

            case R.id.img33:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_active));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type_no = "2";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                break;

            case R.id.img44:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_active));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type_no = "3";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                break;

            case R.id.img55:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type_no = "4";
                comp_type_other.setVisibility(View.VISIBLE);
                comp_type_other.setText("");

                break;


            case R.id.img66:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_active));
                company_type_no = "6";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                editor.commit();


                break;



            case R.id.popup_button:
                if(status_code.equalsIgnoreCase("200"))
                {

                    dialog.dismiss();
                    finish();

                }

                else if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }



                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(EditCompanyDetail.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i2);


                    finish();

                }
                else
                {
                    dialog.dismiss();
                }

                break;



        }



    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }






    String res;
    StringRequest strReq;

    public String makeRegAsCompReq() {

        String url = getResources().getString(R.string.base_url)+"user/as-company";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkRegAsCompResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                    params.put("_id",pref.getString("userId","Something Wrong") );

                  /*  params.put("company_type", company_type_no);
                    params.put("business_type", company_business_no);*/




                    params.put("company_type[c_type]", company_type_no);
                    params.put("business_type[b_type]", company_business_no);

                    params.put("company_type[c_others]", comp_type_other.getText().toString().trim());
                    params.put("business_type[b_others]",buss_type_other.getText().toString().trim() );


                    params.put("business[business_name]", comp_buss_name);
                    params.put("business[landline_number]", comp_landlin_no);


                    params.put("business[registered_address]", register_address_text);

                    params.put("business[register_city]", register_city_text);
                    params.put("business[register_state]", register_state_text);
                    params.put("business[register_pin]", register_pin_text);

                    params.put("business[office_address]", office_add_text);

                    params.put("business[office_city]", office_city_text);
                    params.put("business[office_state]", office_state_text);
                    params.put("business[office_pin]", office_pin_text);
                    params.put("security_token", pref.getString("security_token","Something Wrong"));



                    Log.e("register", params.toString());
                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "20");



        return null;
    }

    String status_code = "0";

    public void checkRegAsCompResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register", response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    String response1 = reader.getString("response");
                    JSONObject jsonobj = new JSONObject(response1);

                    String firstname = jsonobj.getString("firstname");
                    String lastname = jsonobj.getString("lastname");
                    String email = jsonobj.getString("email");
                    String mobile_no1 = jsonobj.getString("mobile_no");
                    String image = jsonobj.getString("image");
                    String business_name = jsonobj.getString("business_name");
                    String pancard = jsonobj.getString("pancard");
                    String account_type = jsonobj.getString("account_type");
                    String company_type = jsonobj.getString("company_type");
                    String business_type = jsonobj.getString("business_type");
                    String registered_address = jsonobj.getString("registered_address");
                    String office_address = jsonobj.getString("office_address");
                    String landline_number = jsonobj.getString("landline_number");
                    String register_city = jsonobj.getString("register_city");
                    String register_state = jsonobj.getString("register_state");
                    String register_pin = jsonobj.getString("register_pin");
                    String office_city = jsonobj.getString("office_city");
                    String office_state = jsonobj.getString("office_state");
                    String office_pin = jsonobj.getString("office_pin");
                    String business_service_taxno = jsonobj.getString("business_service_taxno");


                    String ctype="",others="",btype="",others1 = "";
                    JSONObject jsonobj1 = new JSONObject(company_type);
                    if(jsonobj1.has("ctype")){
                        ctype = jsonobj1.getString("ctype");
                    }
                    if(jsonobj1.has("text")){
                        others = jsonobj1.getString("text");
                    }

                    JSONObject jsonobj2 = new JSONObject(business_type);

                    if(jsonobj2.has("btype")){
                        btype = jsonobj2.getString("btype");
                    }
                    if(jsonobj2.has("text")){
                        others1 = jsonobj2.getString("text");
                    }




                    editor.putString("user_firstname", firstname);
                    editor.putString("user_lastname", lastname);
                    editor.putString("user_email", email);
                    editor.putString("user_mobile_no", mobile_no1);

                    editor.putString("user_company_type", ctype);
                    editor.putString("user_business_type", btype);
                    /*editor.putString("company_business_other", others);
                    editor.putString("company_type_other", others1);*/


                    editor.putString("user_image_url", image);
                    editor.putString("user_image_base64", "");

                    editor.putString("user_account_type", account_type);


                    editor.putString("user_business_name", business_name);
                    editor.putString("user_landline_number", landline_number);
                    editor.putString("user_registered_address", registered_address);
                    editor.putString("user_register_city", register_city);
                    editor.putString("user_register_state", register_state);
                    editor.putString("user_register_pin", register_pin);
                    editor.putString("user_office_address", office_address);
                    editor.putString("user_office_city", office_city);
                    editor.putString("user_office_state", office_state);
                    editor.putString("user_office_pin", office_pin);
                    editor.putString("user_comp_type_other", comp_type_other.getText().toString().trim());
                    editor.putString("user_buss_type_other", buss_type_other.getText().toString().trim());
                    editor.putString("user_pan_card", pancard);
                    editor.putString("user_service_tax_no", business_service_taxno);
                    editor.commit();
                    initiatePopupWindow(getResources().getString(R.string.you_company_details_has_been_updated),
                            false, getResources().getString(R.string.success), getResources().getString(R.string.ok));

                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }


                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {

            Log.d("error"," "+e.getMessage()+ " "+e.getLocalizedMessage());

        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(EditCompanyDetail.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);

            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
