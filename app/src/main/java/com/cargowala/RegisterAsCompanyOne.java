package com.cargowala;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * Created by Akash on 2/10/2016.
 */
public class RegisterAsCompanyOne extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton,company_progress;
    MyTextView next;
    LinearLayout ll1,ll2;
    ImageView img1,img2,img4,img5;

    LinearLayout ll11,ll22;
    ImageView img11,img22,img33,img44,img55,img66;

    String company_business = "",company_type = "";

    MaterialEditText buss_name,landline_no;

    MaterialEditText buss_type_other,comp_type_other;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_as_comp_one);



        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        buss_name = (MaterialEditText) findViewById(R.id.buss_name);
        landline_no = (MaterialEditText) findViewById(R.id.landline_no);
        buss_type_other = (MaterialEditText) findViewById(R.id.buss_type_other);
        comp_type_other = (MaterialEditText) findViewById(R.id.comp_type_other);

        ll1 = (LinearLayout) findViewById(R.id.ll1);

        ll1.getLayoutParams().height =  (deviceHeight / 6);

        ll1.requestLayout();

        ll2 = (LinearLayout) findViewById(R.id.ll2);

        ll2.getLayoutParams().height =  (deviceHeight / 6);

        ll2.requestLayout();

        ll11 = (LinearLayout) findViewById(R.id.ll11);

        ll11.getLayoutParams().height =  (deviceHeight / 6);

        ll11.requestLayout();

        ll22 = (LinearLayout) findViewById(R.id.ll22);

        ll22.getLayoutParams().height =  (deviceHeight / 6);

        ll22.requestLayout();


        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);
        backbutton.requestLayout();
        backbutton.setOnClickListener(this);

        company_progress = (ImageView) findViewById(R.id.company_progress);
        company_progress.getLayoutParams().width =  (deviceWidth /2);
        company_progress.requestLayout();

        next = (MyTextView) findViewById(R.id.next);
        next.setOnClickListener(this);


        img1 = (ImageView) findViewById(R.id.img1);
        img1.setOnClickListener(this);
        img2 = (ImageView) findViewById(R.id.img2);
        img2.setOnClickListener(this);

        img4 = (ImageView) findViewById(R.id.img4);
        img4.setOnClickListener(this);
        img5 = (ImageView) findViewById(R.id.img5);
        img5.setOnClickListener(this);

        img11 = (ImageView) findViewById(R.id.img11);
        img11.setOnClickListener(this);
        img22 = (ImageView) findViewById(R.id.img22);
        img22.setOnClickListener(this);
        img33 = (ImageView) findViewById(R.id.img33);
        img33.setOnClickListener(this);
        img44 = (ImageView) findViewById(R.id.img44);
        img44.setOnClickListener(this);
        img55 = (ImageView) findViewById(R.id.img55);
        img55.setOnClickListener(this);
        img66 = (ImageView) findViewById(R.id.img66);
        img66.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.backbutton:

                finish();

                break;


            case R.id.next:


                String comp_buss_name = buss_name.getText().toString().trim();

                String comp_landlin_no= landline_no.getText().toString().trim();


                if(comp_buss_name.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_bussiness_name), Toast.LENGTH_SHORT).show();
                }

                else {
                    if((comp_landlin_no.length() > 0 && comp_landlin_no.length() < 6) || comp_landlin_no.length() >12)
                    {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_landline), Toast.LENGTH_SHORT).show();
                    }
                    else {


                        if (company_business.equalsIgnoreCase("")) {

                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_business_type), Toast.LENGTH_SHORT).show();

                        } else {


                            if(company_type.equalsIgnoreCase("")) {

                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.please_select_company_type),Toast.LENGTH_SHORT).show();

                            }
                            else {


                                if(company_business.equalsIgnoreCase("4"))
                                {
                                    String buss_type_other_text = buss_type_other.getText().toString().trim();


                                    if (buss_type_other_text.equalsIgnoreCase("")) {

                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_business_type), Toast.LENGTH_SHORT).show();

                                    } else {



                                        if(company_type.equalsIgnoreCase("4"))
                                        {
                                            String comp_type_other_text = comp_type_other.getText().toString().trim();


                                            if (comp_type_other_text.equalsIgnoreCase("")) {

                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_company_type), Toast.LENGTH_SHORT).show();

                                            } else {





                                                Intent i = new Intent(RegisterAsCompanyOne.this, RegisterAsCompanyTwo.class);
                                                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);




                                                i.putExtra("company_business", company_business);
                                                i.putExtra("comp_buss_name", comp_buss_name);
                                                i.putExtra("comp_landlin_no", comp_landlin_no);
                                                i.putExtra("company_type_other", comp_type_other_text);
                                                i.putExtra("company_business_other", buss_type_other_text);
                                                i.putExtra("company_type", company_type);

                                                startActivity(i);


                                                // Removed after tester changed the flow

                                                //finish();

                                              //  startActivity(i);



                                            }



                                        }
                                        else
                                        {

                                            Intent i = new Intent(RegisterAsCompanyOne.this, RegisterAsCompanyTwo.class);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);




                                            i.putExtra("company_business", company_business);
                                            i.putExtra("comp_buss_name", comp_buss_name);
                                            i.putExtra("comp_landlin_no", comp_landlin_no);
                                            i.putExtra("company_type_other", "");
                                            i.putExtra("company_business_other", buss_type_other_text);
                                            i.putExtra("company_type", company_type);

                                            startActivity(i);



                                            // Removed after tester changed the flow
                                         //   finish();

                                        }

                                    }



                                }
                                else if(company_type.equalsIgnoreCase("4"))
                                {
                                    String comp_type_other_text = comp_type_other.getText().toString().trim();


                                    if (comp_type_other_text.equalsIgnoreCase("")) {

                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_company_type), Toast.LENGTH_SHORT).show();

                                    } else {


                                        Intent i = new Intent(RegisterAsCompanyOne.this, RegisterAsCompanyTwo.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);




                                        i.putExtra("company_business", company_business);
                                        i.putExtra("comp_buss_name", comp_buss_name);
                                        i.putExtra("comp_landlin_no", comp_landlin_no);
                                        i.putExtra("company_type_other", comp_type_other_text);
                                        i.putExtra("company_business_other", "");
                                        i.putExtra("company_type", company_type);

                                        startActivity(i);

                                      // Removed after tester changed the flow
                                    //    finish();



                                    }



                                }
                                else {

                                    Intent i = new Intent(RegisterAsCompanyOne.this, RegisterAsCompanyTwo.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);




                                    i.putExtra("company_business", company_business);
                                    i.putExtra("comp_buss_name", comp_buss_name);
                                    i.putExtra("comp_landlin_no", comp_landlin_no);
                                    i.putExtra("company_type_other", "");
                                    i.putExtra("company_business_other", "");
                                    i.putExtra("company_type", company_type);

                                    startActivity(i);


                                    // Removed after tester changed the flow
                              //      finish();

                                }



                            }
                        }
                    }
                }
                break;

            case R.id.img1:

                /*img1.setImageDrawable(getResources()
                        .getDrawable(R.drawable.manufacturing_active));*/
                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_active));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");
                company_business="3";

                break;

            case R.id.img2:
                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_active));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");
                company_business="1";

                break;



            case R.id.img4:
                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));
                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_active));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");
                company_business="2";

                break;

            case R.id.img5:
                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));
                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));
                buss_type_other.setVisibility(View.VISIBLE);
                buss_type_other.setText("");
                company_business="4";

                break;



            case R.id.img11:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_active));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type="5";

                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");

                break;

            case R.id.img22:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_active));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type="1";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                break;

            case R.id.img33:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_active));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type="2";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                break;

            case R.id.img44:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_active));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type="3";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                break;

            case R.id.img55:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                company_type="4";
                comp_type_other.setVisibility(View.VISIBLE);
                comp_type_other.setText("");
                break;

            case R.id.img66:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_active));
                company_type="6";
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                break;








        }


    }

/*    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }*/

}
