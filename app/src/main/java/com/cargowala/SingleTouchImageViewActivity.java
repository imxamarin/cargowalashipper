package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 6/27/2016.
 */

public class SingleTouchImageViewActivity extends Activity implements View.OnClickListener{

    private TouchImageView image;
    String imageUrl;
    ImageView download_image;

    private long enqueue1;
    private DownloadManager dm1;
    BroadcastReceiver  receiver1;

    SweetAlertDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_touchimageview);

        image = (TouchImageView) findViewById(R.id.img);
        download_image = (ImageView) findViewById(R.id.download_image);
        download_image.setOnClickListener(this);

        Intent intent = getIntent();
        imageUrl = intent.getExtras().getString("imageUrl");


        Picasso.with(getApplicationContext())
                .load(imageUrl)
                .placeholder(R.drawable.loading_fullscreen)
                .error(R.drawable.unable_upload_fullscreen).into(image);







        receiver1 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(
                            DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(enqueue1);
                    Cursor c = dm1.query(query);

                    if (c.moveToFirst()) {
                        int columnIndex = c
                                .getColumnIndex(DownloadManager.COLUMN_STATUS);

                        Log.d("mainPage","12.0 "+ c
                                .getColumnIndex(DownloadManager.COLUMN_STATUS));
                        Log.d("mainPage","12.1 "+ c
                                .getColumnIndex(DownloadManager.COLUMN_DESCRIPTION));
                        Log.d("mainPage","12.2 "+ c
                                .getColumnIndex(DownloadManager.COLUMN_REASON));


                        Log.d("mainPage","12.3 "+ c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS)));
                        Log.d("mainPage","12.4 "+ c.getInt(c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION)));
                        Log.d("mainPage","12.5 "+c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON)));



                        Log.d("mainPage","13 ");

                        unregisterReceiver(receiver1);



                        pDialog.dismiss();


                        initiatePopupWindow(getResources().getString(R.string.your_image_has_been_saved_in_sd_card_cargowala_downloads_folder_as)+" \"" +current_time+".jpg\"",
                                false, getResources().getString(R.string.success), getResources().getString(R.string.ok));



                    }
                    else
                    {
                        Log.d("mainPage","24");
                    }
                }
                else
                {
                    Log.d("mainPage","25");
                }
            }
        };




    }




    String current_time;
    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.popup_button:

                dialog.dismiss();


                break;


            case R.id.download_image:



                File subFolder = new File(Environment
                        .getExternalStorageDirectory().getPath()
                        + "/CargoWala_Downloads" );

                if (!subFolder.exists()) {
                    subFolder.mkdir();
                }


                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();



                registerReceiver(receiver1, new IntentFilter(
                        DownloadManager.ACTION_DOWNLOAD_COMPLETE));

                dm1 = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(
                        Uri.parse(imageUrl));

                Log.d("mainPage","18.1 "+imageUrl);

                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa");
                current_time = sdf.format(c.getTime());




                request.setDestinationInExternalPublicDir("/CargoWala_Downloads"
                        , current_time +".jpg");


                request.setTitle(getResources().getString(R.string.downloading_image));
                //  request.setDescription("Android Data download using DownloadManager.");
                enqueue1 = dm1.enqueue(request);




                break;



        }
    }











    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(SingleTouchImageViewActivity.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }










}