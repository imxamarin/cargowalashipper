package com.cargowala;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

public class Home extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    //http://stackoverflow.com/questions/937313/fling-gesture-detection-on-grid-layout

    private GoogleMap mMap;
    int deviceWidth, deviceHeight;
    //RelativeLayout ll1,ll2;
    // View view1,view2;
    LinearLayout bottom_layout, left_drawer;
    LatLng origin, dest;
    ImageView select_datee, select_timee, my_loc, nav_back_button, edit_profile;
    SweetAlertDialog pDialog;
    private double currentLattitude;
    private double currentLongitude;

    CircleImageView profile_image;
    String encoded_polyline;
    String origin_address, dest_address, dest_city, dest_state, origin_city, origin_state;

    SharedPreferences pref;
    SharedPreferences.Editor editor;


    SharedPreferences pref1;
    SharedPreferences.Editor editor1;


    boolean isLogout = false;

    String marker_time, marker_distance;
    RelativeLayout actionBar;
    ImageView open_drawer;
    DrawerLayout drawerLayout;
    MyTextView datee, timee;
    MyTextViewSemi book_now;
    LinearLayout book_truck_layout, payment_layout, my_shipment_layout, pending_shipment_layout, bid_board_layout, offers_layout, notification_layout, help_support_layout, settings_layout;

    private boolean isGpsEnabled = false;
    private boolean isNetworkEnabled = false;
    private GoogleApiClient mGoogleApiClient;
    PlaceAutocompleteFragment autocompleteFragment, autocompleteFragment1;


    MyTextViewSemi shipper_name;


    /////////////////////////////////////

    //aakash major change send image of route to backend shown in shipments active pending history n all
    // also send city and state name for filter

    ///////////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        // 40


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        datee = (MyTextView) findViewById(R.id.datee);
        timee = (MyTextView) findViewById(R.id.timee);

    /*    view1 = (View) findViewById(R.id.view1);
        view1.setOnClickListener(this);
        view2 = (View) findViewById(R.id.view2);
        view2.setOnClickListener(this);*/

        book_truck_layout = (LinearLayout) findViewById(R.id.book_truck_layout);
        book_truck_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white/*light_grey*/));
        book_truck_layout.setOnClickListener(this);

        payment_layout = (LinearLayout) findViewById(R.id.payment_layout);
        payment_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        payment_layout.setOnClickListener(this);

        my_shipment_layout = (LinearLayout) findViewById(R.id.my_shipment_layout);
        my_shipment_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        my_shipment_layout.setOnClickListener(this);

        pending_shipment_layout = (LinearLayout) findViewById(R.id.pending_shipment_layout);
        pending_shipment_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        pending_shipment_layout.setOnClickListener(this);

        bid_board_layout = (LinearLayout) findViewById(R.id.bid_board_layout);
        bid_board_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        bid_board_layout.setOnClickListener(this);

        offers_layout = (LinearLayout) findViewById(R.id.offers_layout);
        offers_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        offers_layout.setOnClickListener(this);

        notification_layout = (LinearLayout) findViewById(R.id.notification_layout);
        notification_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        notification_layout.setOnClickListener(this);

        help_support_layout = (LinearLayout) findViewById(R.id.help_support_layout);
        help_support_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        help_support_layout.setOnClickListener(this);

        settings_layout = (LinearLayout) findViewById(R.id.settings_layout);
        settings_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        settings_layout.setOnClickListener(this);

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        open_drawer = (ImageView) findViewById(R.id.open_drawer);

        open_drawer.setOnClickListener(this);
        open_drawer.getLayoutParams().height = (deviceHeight / 18);
        open_drawer.getLayoutParams().width = (deviceHeight / 18);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        open_drawer.requestLayout();

       /* ll1 = (RelativeLayout) findViewById(R.id.ll1);

        ll1.getLayoutParams().height =  (deviceHeight / 15);

        ll1.requestLayout();

        ll2 = (RelativeLayout) findViewById(R.id.ll2);

        ll2.getLayoutParams().height =  (deviceHeight / 15);

        ll2.requestLayout();*/


        select_datee = (ImageView) findViewById(R.id.select_datee);
        select_datee.setOnClickListener(this);
        select_datee.getLayoutParams().height = (deviceWidth / 8);
        select_datee.getLayoutParams().width = (deviceWidth / 8);

        select_datee.requestLayout();


        nav_back_button = (ImageView) findViewById(R.id.nav_back_button);
        nav_back_button.setOnClickListener(this);


        edit_profile = (ImageView) findViewById(R.id.edit_profile);
        edit_profile.setOnClickListener(this);


        shipper_name = (MyTextViewSemi) findViewById(R.id.shipper_name);

        my_loc = (ImageView) findViewById(R.id.my_loc);
        my_loc.setOnClickListener(this);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                deviceWidth / 7, deviceWidth / 7);
        lp.setMargins(0, 0, 10, (int) (deviceWidth / 3));

        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);

        my_loc.setLayoutParams(lp);


        select_timee = (ImageView) findViewById(R.id.select_timee);
        select_timee.setOnClickListener(this);
        select_timee.getLayoutParams().height = (deviceWidth / 8);
        select_timee.getLayoutParams().width = (deviceWidth / 8);

        select_timee.requestLayout();

        bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);

        bottom_layout.getLayoutParams().height = (deviceHeight / 4);

        bottom_layout.requestLayout();

        left_drawer = (LinearLayout) findViewById(R.id.left_drawer);
        left_drawer.getLayoutParams().width = (int) (deviceWidth / 1.2);
        left_drawer.requestLayout();

        book_now = (MyTextViewSemi) findViewById(R.id.book_now);
        book_now.setOnClickListener(this);

        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);


        autocompleteFragment.setHint(getResources().getString(R.string.loading_point));


        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("place", "Place: " + place.getName());
                Log.i("place", "Place: " + place.getLatLng());

                origin = place.getLatLng();

                if(mMap != null ){
                    mMap.clear();
                }

                if (dest != null) {


                    try {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(Home.this, Locale.getDefault());


                        addresses = geocoder.getFromLocation(dest.latitude, dest.longitude, 1);
                        String country = addresses.get(0).getCountryName();


/*
                        Log.d("add 000000", " " + addresses.get(0));
                        Log.d("country name 1", " " + country);

                        Log.d("postal code ", " " + addresses.get(0).getPostalCode());

                        Log.d("postal code 1", " " + addresses.get(0).getAddressLine(0));
                        Log.d("postal code 2", " " + addresses.get(0).getLocale());
                        Log.d("postal code 3", " " + addresses.get(0).getAdminArea());
                        Log.d("postal code 4", " " + addresses.get(0).getFeatureName());
                        Log.d("postal code 5", " " + addresses.get(0).getPostalCode());
                        Log.d("postal code 6", " " + addresses.get(0).getPhone());
                        Log.d("postal code 7", " " + addresses.get(0).getPremises());
                        Log.d("postal code 8", " " + addresses.get(0).getMaxAddressLineIndex());
                        Log.d("postal code 9", " " + addresses.get(0).getSubAdminArea());*/

                        dest_address = "";
                        for (int i = 0; i < (addresses.get(0).getMaxAddressLineIndex() - 1); i++) {
                            Log.d(" =Adress= dest ", addresses.get(0).getAddressLine(i));
                            dest_address = dest_address + " " + addresses.get(0).getAddressLine(i);
                        }
                        dest_address = dest_address + " " + addresses.get(0).getLocality() /*+ addresses.get(0).getAdminArea()*/;
                        dest_address = dest_address.trim();
                        Log.d("add 000000", " " + addresses.get(0));
                        Log.d("dest_address", " " + dest_address);

                        //  dest_address = addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality();

                        dest_city = addresses.get(0).getLocality();
                        dest_state = addresses.get(0).getAdminArea();


                        addresses = geocoder.getFromLocation(origin.latitude, origin.longitude, 1);
                        String country1 = addresses.get(0).getCountryName();
                        //     Log.d("country name 2", " " + country1);
                        //  origin_address = addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality();

                        origin_address = "";
                        for (int i = 0; i < (addresses.get(0).getMaxAddressLineIndex() - 1); i++) {
                            //    Log.d(" =Adress= dest ", addresses.get(0).getAddressLine(i));
                            origin_address = origin_address + " " + addresses.get(0).getAddressLine(i);
                        }
                        origin_address = origin_address + " " + addresses.get(0).getLocality() /*+ addresses.get(0).getAdminArea()*/;
                        origin_address = origin_address.trim();
                        Log.d("origin_address ", " " + origin_address);
                        Log.d("add 111111", " " + addresses.get(0));
                      /*  for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++) {
                            Log.d(" =Adress origin = ", addresses.get(0).getAddressLine(i));
                        }*/


                        origin_city = addresses.get(0).getLocality();
                        origin_state = addresses.get(0).getAdminArea();

                        if (country.equalsIgnoreCase("india") && country1.equalsIgnoreCase("india")) {


                            pDialog = new SweetAlertDialog(Home.this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();


                            mMap.addMarker(new MarkerOptions()
                                            .position(origin)

                            );

                            mMap.addMarker(new MarkerOptions()
                                            .position(dest)
                            ).showInfoWindow();

                            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                            boundsBuilder.include(origin);
                            boundsBuilder.include(dest);

                            LatLngBounds bounds = boundsBuilder.build();
                            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 3));

                            // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(origin, 5));


                            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(Marker marker) {


                                    mMap.addMarker(new MarkerOptions()
                                                    .position(dest).title(marker_time).snippet(marker_distance)
                                    ).showInfoWindow();

                                    return true;
                                }
                            });


                            // Getting URL to the Google Directions API
                            String url = getDirectionsUrl(origin, dest);

                            DownloadTask downloadTask = new DownloadTask();

                            // Start downloading json data from Google Directions API
                            downloadTask.execute(url);
                         /*   }*/

                        } else {


                            if (!country.equalsIgnoreCase("india")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_indian_destination), Toast.LENGTH_LONG).show();

                                dest = null;

                                mMap.clear();


                                //     autocompleteFragment1.setHint("Unloading point");
                                //    ((EditText) autocompleteFragment1.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                                //    autocompleteFragment1.setText("");

                                final Handler handler = new Handler();
                                Runnable runable = new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            //   autocompleteFragment1.setHint("Unloading point");
                                            //   ((EditText) autocompleteFragment1.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                                            autocompleteFragment1.setText("");
                                        } catch (Exception e) {

                                        }

                                    }
                                };
                                handler.postDelayed(runable, 1000);

                            }
                            if (!country1.equalsIgnoreCase("india")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_indian_origin), Toast.LENGTH_LONG).show();
                                origin = null;
                                mMap.clear();
                                //  autocompleteFragment.setHint("Loading point");
                                //   autocompleteFragment.setText("");
                                ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                                final Handler handler = new Handler();
                                Runnable runable = new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            //  autocompleteFragment.setHint("loading point");
                                            //   ((EditText) autocompleteFragment1.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                                            autocompleteFragment.setText("");
                                        } catch (Exception e) {

                                        }

                                    }
                                };
                                handler.postDelayed(runable, 1000);
                            }


                        }
                    } catch (Exception e) {

                    }

                }


            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.e("place", "An error occurred: " + status);
            }


        });


        autocompleteFragment.getView().findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(context, "destination", Toast.LENGTH_SHORT).show();
                ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                view.setVisibility(View.GONE);
                mMap.clear();
                origin = null;
            }
        });


        autocompleteFragment1 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment1);

        autocompleteFragment1.setHint(getResources().getString(R.string.unloading_point));

        autocompleteFragment1.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("place", "Place: " + place.getName());
                Log.i("place", "Place: " + place.getLatLng());


                dest = place.getLatLng();


//                Log.d("yo" ,""+autocompleteFragment.getArguments().toString());
//                Log.d("yo" ,""+autocompleteFragment.getText(0).toString());
//                Log.d("yo" ,""+autocompleteFragment.getString(0));


                if(mMap != null ) {
                    mMap.clear();
                }
                if (origin != null) {


                    try {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(Home.this, Locale.getDefault());

                        addresses = geocoder.getFromLocation(dest.latitude, dest.longitude, 1);
                        String country = addresses.get(0).getCountryCode();

/*
                        for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++) {
                            Log.d(" =Adress= dest ", addresses.get(0).getAddressLine(i));
                        }


                        Log.d("country name 1", " " + country);

                        Log.d("add 000000", " " + addresses.get(0));

                        Log.d("postal code ", " " + addresses.get(0).getPostalCode());

                        Log.d("postal code 1", " " + addresses.get(0).getAddressLine(0));

                     *//*   Log.d("postal code 1a", " " + addresses.get(0).getAddressLine(0));
                        Log.d("postal code 1b", " " + addresses.get(0).getAddressLine(0));
                        Log.d("postal code 1c", " " + addresses.get(0).getAddressLine(0));
                        Log.d("postal code 1d", " " + addresses.get(0).getAddressLine(0));*//*


                        Log.d("postal code 2", " " + addresses.get(0).getLocale());
                        Log.d("postal code 3", " " + addresses.get(0).getAdminArea());
                        Log.d("postal code 4", " " + addresses.get(0).getFeatureName());
                        Log.d("postal code 5", " " + addresses.get(0).getPostalCode());
                        Log.d("postal code 6", " " + addresses.get(0).getPhone());
                        Log.d("postal code 7", " " + addresses.get(0).getPremises());
                        Log.d("postal code 8", " " + addresses.get(0).getMaxAddressLineIndex());
                        Log.d("postal code 9", " " + addresses.get(0).getSubAdminArea());*/

                        //     Log.d("country name 1", " " + country);
                        // dest_address = addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality();

                        dest_address = "";
                        for (int i = 0; i < (addresses.get(0).getMaxAddressLineIndex() - 1); i++) {
                            //     Log.d(" =Adress= dest ", addresses.get(0).getAddressLine(i));
                            dest_address = dest_address + " " + addresses.get(0).getAddressLine(i);
                        }
                        dest_address = dest_address + " " + addresses.get(0).getLocality() /*+ addresses.get(0).getAdminArea()*/;
                        dest_address = dest_address.trim();
                        Log.d("dest_address", " " + dest_address);
                        Log.d("add 000000", " " + addresses.get(0));

                        dest_city = addresses.get(0).getLocality();
                        dest_state = addresses.get(0).getAdminArea();


                        addresses = geocoder.getFromLocation(origin.latitude, origin.longitude, 1);
                        String country1 = addresses.get(0).getCountryCode();
                        //   Log.d("country name 2", " " + country1);
                        // origin_address = addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality();


                        origin_address = "";
                        for (int i = 0; i < (addresses.get(0).getMaxAddressLineIndex() - 1); i++) {
                            //  Log.d(" =Adress= dest ", addresses.get(0).getAddressLine(i));
                            origin_address = origin_address + " " + addresses.get(0).getAddressLine(i);
                        }
                        origin_address = origin_address + " " + addresses.get(0).getLocality() /*+ addresses.get(0).getAdminArea()*/;
                        origin_address = origin_address.trim();
                        Log.d("add 111111", " " + addresses.get(0));
                        Log.d("origin_address ", " " + origin_address);

                     /*   for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++) {
                            Log.d(" =Adress= origin ", addresses.get(0).getAddressLine(i));
                        }
*/
                        origin_city = addresses.get(0).getLocality();
                        origin_state = addresses.get(0).getAdminArea();


                        if (country.equalsIgnoreCase("IN") && country1.equalsIgnoreCase("IN")) {


                            pDialog = new SweetAlertDialog(Home.this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();

                            mMap.addMarker(new MarkerOptions()
                                            .position(origin)
                            );

                            mMap.addMarker(new MarkerOptions()
                                            .position(dest)
                            ).showInfoWindow();


                            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                            boundsBuilder.include(origin);
                            boundsBuilder.include(dest);

                            LatLngBounds bounds = boundsBuilder.build();
                            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 3));
                            //  mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(origin, 5));


                            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(Marker marker) {


                                    Log.d("diable ", "done");
                                    mMap.addMarker(new MarkerOptions()
                                                    .position(dest).title(marker_time).snippet(marker_distance)
                                    ).showInfoWindow();

                                    return true;
                                }
                            });


                            // Getting URL to the Google Directions API
                            String url = getDirectionsUrl(origin, dest);

                            DownloadTask downloadTask = new DownloadTask();

                            // Start downloading json data from Google Directions API
                            downloadTask.execute(url);

                      /*  }*/
                        } else {
                            //     Toast.makeText(getApplicationContext(), "Please Select Indian Places", Toast.LENGTH_LONG).show();

                            if (!country.equalsIgnoreCase("IN")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_indian_destination), Toast.LENGTH_LONG).show();
                                dest = null;

                                mMap.clear();


                                final Handler handler = new Handler();
                                Runnable runable = new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            //        autocompleteFragment1.setHint("Unloading point");
                                            //   ((EditText) autocompleteFragment1.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                                            autocompleteFragment1.setText("");
                                        } catch (Exception e) {

                                        }

                                    }
                                };
                                handler.postDelayed(runable, 1000);

                            }
                            if (!country1.equalsIgnoreCase("IN")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_indian_origin), Toast.LENGTH_LONG).show();
                                origin = null;
                                mMap.clear();
                                // autocompleteFragment.setHint("Loading point");
                                //  autocompleteFragment.setText("");

                                //    ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");


                                final Handler handler = new Handler();
                                Runnable runable = new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            //   autocompleteFragment.setHint("loading point");
                                            //   ((EditText) autocompleteFragment1.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                                            autocompleteFragment.setText("");
                                        } catch (Exception e) {

                                        }

                                    }
                                };
                                handler.postDelayed(runable, 1000);

                            }
                        }
                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("place", "An error occurred: " + status);
            }
        });


        autocompleteFragment1.getView().findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(context, "destination", Toast.LENGTH_SHORT).show();
                ((EditText) autocompleteFragment1.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                view.setVisibility(View.GONE);
                mMap.clear();
                dest = null;
            }
        });

    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";

        waypoints = "waypoints=";
        waypoints += origin.latitude + "," + origin.longitude + "|";
        waypoints += dest.latitude + "," + dest.longitude + "|";


        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor/*+"&"+waypoints*/;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters+ "&key=AIzaSyAvLAdXzhvITt3E6RCbcU2jdUegV21D94U";


        return url;
    }
  //  https://maps.googleapis.com/maps/api/directions/json?origin=28.686273800000006,77.2217831&destination=30.7333148,76.7794179&sensor=false
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

        LatLng latlan = new LatLng(23, 78);
        //  mMap.addMarker(new MarkerOptions().position(latlan).title("Marker in Sydney"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlan, 5));

        mMap.setPadding(deviceWidth / 10, (deviceHeight / 3), deviceWidth / 10, (deviceHeight / 4));

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.popup_button:

                if (status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910")) {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook")) {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    } else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google")) {
                        isGoogle = true;
                    }


                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(Home.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn", isGoogle);
                    startActivity(i2);


                    finish();

                } else {
                    dialog.dismiss();
                }


                break;


            case R.id.popup_button_yes2:
                dialog1.dismiss();


                if (isLogout) {
                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook")) {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    } else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google")) {
                        isGoogle = true;
                    }


                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(Home.this, Login.class);
                    i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                    i2.putExtra("wasGoogleLoggedIn", isGoogle);
                    startActivity(i2);


                    finish();
                } else {
                    startActivityForResult(
                            new Intent(
                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                            111);
                }

                break;

            case R.id.popup_button_no2:
                dialog1.dismiss();


                break;


            case R.id.my_loc:

            /*    currentLattitude = GetCurrentLocation.latitude;
                currentLongitude = GetCurrentLocation.longitude;*/
                initiateGps();


                break;

            case R.id.open_drawer:
                if (drawerLayout.isDrawerOpen(left_drawer)) {
                    //   drawerLayout.closeDrawer(left_drawer);

                } else {
                    drawerLayout.openDrawer(left_drawer);
                }

                break;


            case R.id.select_datee:

                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        Home.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );


                dpd.setMinDate(now);
                //dpd.setThemeDark(true);
                //dpd.setTitle("Select Date");
                dpd.setAccentColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                dpd.show(getFragmentManager(), getResources().getString(R.string.datepickerdialog));
                break;


            case R.id.select_timee:

                Calendar now1 = Calendar.getInstance();
                TimePickerDialog dpd1 = TimePickerDialog.newInstance(
                        Home.this,
                        now1.get(Calendar.HOUR_OF_DAY),
                        now1.get(Calendar.MINUTE),
                        false
                );

             /*  dpd1.setMinTime(now1.get(Calendar.HOUR_OF_DAY)+1,now1.get(Calendar.MINUTE),
                        now1.get(Calendar.SECOND));*/


                dpd1.show(getFragmentManager(), getResources().getString(R.string.timepickerdialog));

                dpd1.setAccentColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                break;


            case R.id.book_now:

                if (origin == null) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_loading_point), Toast.LENGTH_SHORT)
                            .show();

                } else if (dest == null) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_unloading_point), Toast.LENGTH_SHORT)
                            .show();
                } else if (date == null) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_date), Toast.LENGTH_SHORT)
                            .show();
                } else if (time == null) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_time), Toast.LENGTH_SHORT)
                            .show();
                } else {


                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
                    String current_time = sdf.format(c.getTime());


                    String book_time = old_date + time;

                    Log.d("current_time ", "current_time " + current_time);
                    Log.d("book_time ", "book_time " + book_time);


                    try {
                        Date current_time1 = sdf.parse(current_time);
                        Date book_time1 = sdf.parse(book_time);

                        long diff = book_time1.getTime() - current_time1.getTime();
                        long seconds = diff / 1000;
                        long minutes = seconds / 60;
                        long hours = minutes / 60;
                        long days = hours / 24;


                        Log.d("diff", "diff " + diff);
                        Log.d("seconds", "seconds " + seconds);
                        Log.d("minutes", "minutes " + minutes);
                        Log.d("hours", "hours " + hours);
                        Log.d("days", "days " + days);


                        if (minutes < 30) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.booking_hours_should_be_greater_than_30_minutes), Toast.LENGTH_SHORT)
                                    .show();
                        } else {


                            if ( /*origin_city.equalsIgnoreCase("null") || origin_state.equalsIgnoreCase("null")
                                    || origin_city.isEmpty() || origin_state.isEmpty()*/
                                    origin_city == null || origin_state == null) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_another_loading_point), Toast.LENGTH_LONG).show();
                            } else if (/*dest_city.equalsIgnoreCase("null") || dest_state.equalsIgnoreCase("null")
                                    || dest_city.isEmpty() || dest_state.isEmpty()*/
                                    dest_city == null || dest_city == null) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_another_unloading_point), Toast.LENGTH_LONG).show();
                            } else if (origin_address.equalsIgnoreCase(dest_address)) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.loading_unloading_point_cannot_be_same), Toast.LENGTH_LONG).show();
                            } else {


                                if (isOnline()) {
                                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                    pDialog.setTitleText(getResources().getString(R.string.loading));
                                    pDialog.setCancelable(false);
                                    pDialog.show();
                                    makeBookingDetailsReq();
                                } else {
                                    Toast.makeText(Home.this,
                                            getResources().getString(R.string.check_your_network),
                                            Toast.LENGTH_SHORT).show();
                                }

                            }
                        }


                    } catch (ParseException ex) {

                    }


                }

            /*    Intent i = new Intent(Home.this, BookOneTruck.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);*/

                break;

            case R.id.settings_layout:
                isLogout = true;
                initiateyYesNoWindow(getResources().getString(R.string.are_you_sure_you_want_to_log_out),
                        true, getResources().getString(R.string.log_out), getResources().getString(R.string.yes), getResources().getString(R.string.no));


                break;


            case R.id.book_truck_layout:
                drawerLayout.closeDrawer(left_drawer);


                break;


            case R.id.offers_layout:
                drawerLayout.closeDrawer(left_drawer);

                Intent i1 = new Intent(Home.this, Offer.class);
                i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i1);


                break;


            case R.id.notification_layout:
                drawerLayout.closeDrawer(left_drawer);

                Intent i6 = new Intent(Home.this, MyNotification.class);
                i6.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i6);

              /*  Intent i6 = new Intent(Home.this, Rating.class);
                i6.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i6);*/


                break;


            case R.id.profile_image:
                drawerLayout.closeDrawer(left_drawer);

                Intent i7 = new Intent(Home.this, MyProfile.class);
                i7.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i7);


                break;

            case R.id.edit_profile:
                drawerLayout.closeDrawer(left_drawer);

                Intent i8 = new Intent(Home.this, MyProfile.class);
                i8.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i8);


                break;


            case R.id.bid_board_layout:
                drawerLayout.closeDrawer(left_drawer);

                editor.putBoolean("hitMyBidBoardService", true);
                editor.commit();

                Intent i9 = new Intent(Home.this, BidBoard.class);
                i9.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i9);


                break;


            case R.id.help_support_layout:
                drawerLayout.closeDrawer(left_drawer);

                Intent i4 = new Intent(Home.this, Help.class);
                i4.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i4);

            //    CommonUtils.openLink(Home.this, Constants.FAQ_CONTACTUS);

             /*   final Intent intent = new  Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + 30.7333 + "," + 76.7794 + "&daddr=" + 28.6139 + "," + 77.2090));
                intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
                startActivity(intent);*/


                break;


            case R.id.payment_layout:
                drawerLayout.closeDrawer(left_drawer);

                Intent i5 = new Intent(Home.this, MyPayments.class);
                i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i5);


                break;


            case R.id.my_shipment_layout:


                editor.putBoolean("hitActiveShipmentService", true);
                editor.commit();

                drawerLayout.closeDrawer(left_drawer);

                Intent i3 = new Intent(Home.this, MyShipments.class);
                i3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i3);


                break;

            case R.id.nav_back_button:
                drawerLayout.closeDrawer(left_drawer);

                break;

            case R.id.pending_shipment_layout:
                drawerLayout.closeDrawer(left_drawer);


                Intent i2 = new Intent(Home.this, PendingShipment.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i2);

                break;


        }
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service

            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("result", "result " + result);

            try {

                JSONObject jObject1;
                jObject1 = new JSONObject(result);

                JSONArray jRoutes = null;
                JSONArray jLegs = null;


                jRoutes = jObject1.getJSONArray("routes");
                jLegs = ((JSONObject) jRoutes.get(0)).getJSONArray("legs");


                Log.d("jLegs ", "" + jLegs);

                String distance = "";
                // distance = (String)((JSONObject)((JSONObject)jLegs.get(0)).get("distance")).get("text");

                distance = "" + ((JSONObject) ((JSONObject) jLegs.get(0)).get("distance")).get("value");

                Log.d("distance ", distance);

                int a = Integer.parseInt(distance);
                a = a / 1000;
                distance = "" + a;

                Log.d("distance ", "" + distance);

                String time = "";
                time = (String) ((JSONObject) ((JSONObject) jLegs.get(0)).get("duration")).get("text");


                JSONObject jsonObjectPackageData1 = jRoutes
                        .getJSONObject(0);

                String overview_polyline = jsonObjectPackageData1
                        .getString("overview_polyline");

                JSONObject jsonobj = new JSONObject(overview_polyline);

                encoded_polyline = jsonobj.getString("points");

                Log.d("points ", "points " + encoded_polyline);


                mMap.addMarker(new MarkerOptions()
                                .position(dest).title(time).snippet(distance)
                ).showInfoWindow();

                marker_time = time;
                marker_distance = distance;


            } catch (Exception e) {
                Log.d("exception", e.getLocalizedMessage());
            }


            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception loading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.BLUE);
            }


            // Drawing polyline in the Google Map for the i-th route
            try {
                mMap.addPolyline(lineOptions);
            } catch (Exception e) {
                e.printStackTrace();
            }


            pDialog.dismiss();


        }
    }


    String date, time;
    String old_date;

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String month_name = null;

        old_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year + " ";

        switch (monthOfYear) {

            case 0:
                month_name = "Jan";
                break;
            case 1:
                month_name = "Feb";
                break;
            case 2:
                month_name = "Mar";
                break;
            case 3:
                month_name = "Apr";
                break;
            case 4:
                month_name = "May";
                break;
            case 5:
                month_name = "Jun";
                break;
            case 6:
                month_name = "Jul";
                break;
            case 7:
                month_name = "Aug";
                break;
            case 8:
                month_name = "Sep";
                break;
            case 9:
                month_name = "Oct";
                break;
            case 10:
                month_name = "Nov";
                break;
            case 11:
                month_name = "Dec";
                break;
        }


        date = "" + dayOfMonth + " " + month_name + " " + year;
        datee.setText(date);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        time = "" + hourOfDay + ":" + minute;



      /*  try {
            final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            final Date dateObj = sdf.parse(time);


            Log.d("dateObj ", "" + dateObj);
            Log.d("dateObj ",""+new SimpleDateFormat("KK:mm").format(dateObj));

            time = new SimpleDateFormat("KK:mm").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }*/
        try {
            String _24HourTime = hourOfDay + ":" + minute;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);


            time = _12HourSDF.format(_24HourDt);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        timee.setText(time);


    }

    boolean exitApp = false;

    @Override
    public void onBackPressed() {
        if (exitApp) {
            //super.onBackPressed();
            moveTaskToBack(true);
            //return;
        }

        Toast.makeText(this, getResources().getString(R.string.press_again_to_exit), Toast.LENGTH_SHORT).show();
        this.exitApp = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                exitApp = false;
            }
        }, 2000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    private void initiateGps() {

        Log.d("1", "1");

        //showLocationProgressDialog();
        isGpsEnabled = Util.isGpsEnabled(this);
        isNetworkEnabled = Util.isNetworkEnabled(this);

        if (isNetworkEnabled && isGpsEnabled) {

            pDialog = new SweetAlertDialog(Home.this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(getResources().getString(R.string.loading));
            pDialog.setCancelable(false);


            pDialog.show();

            Log.d("1", "2");
            GetCurrentLocation getCurrentLocation = new GetCurrentLocation();
            Log.d("1", "3");
            getCurrentLocation.setLocationListener(locationListener);
            Log.d("1", "4");
            getCurrentLocation.getLocation(this, isGpsEnabled, isNetworkEnabled);
            Log.d("1", "5");


        } else {
            //dismissProgressDialog();
            // showNetworkAlartDialog();
            isLogout = false;
            initiateyYesNoWindow(getResources().getString(R.string.all_your_location_services_are_either_disabled_or_unavailable),
                    true, getResources().getString(R.string.enable_gps_services), getResources().getString(R.string.yes), getResources().getString(R.string.no));

        }
    }

  /*  private void showNetworkAlartDialog(){

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(Constants.GPS_NOT_ACTIVATE_TITLE);
        dialog.setMessage(Constants.GPS_NOT_ACTIVATE_MESSAGE);
        dialog.setCancelable(true);
        dialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(
                                new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                Constants.CALL_LOCATION_SETTINGS);
                        dialog.dismiss();
                    }
                });

        dialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //  finish();// dismiss the alert dialog
                    }
                });

        dialog.create();
        dialog.show();
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            Log.d("1", "6");
            if (Util.isGpsEnabled(this) && Util.isNetworkEnabled(this)) {
                Log.d("1", "7");
                initiateGps();
            } else {
                Log.d("1", "8");
                finish();
            }
        }
    }

    // on location received
    private LocationListener locationListener = new LocationListener() {
        public void onLocationUpdate() {
            runOnUiThread(new Runnable() {
                public void run() {
                    //System.out.println("diwanshu location get");
                    Log.d("1", "9");
                    processAfterLocationReceived();
                }
            });
        }
    };

    private void processAfterLocationReceived() {

        Log.d("1", "10");

        currentLattitude = GetCurrentLocation.latitude;
        currentLongitude = GetCurrentLocation.longitude;


        Log.d("currentLattitude ", "currentLattitude " + currentLattitude);
        Log.d("currentLongitude ", "currentLongitude " + currentLongitude);


        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(Home.this, Locale.getDefault());

            addresses = geocoder.getFromLocation(currentLattitude, currentLongitude, 1);
           /* String country = addresses.get(0).getCountryName();
            Log.d("country name 1", " " + country);*/
            Log.d("country name 1", " " + addresses);
            Log.d("country name 2", " " + addresses.get(0));

            Log.d("country name 3", " " + addresses.get(0).getAddressLine(0));
            Log.d("country name 4", " " + addresses.get(0).getLocality());
            Log.d("country name 5", " " + addresses.get(0).getLocale());
            Log.d("country name 6", " " + addresses.get(0).getMaxAddressLineIndex());


            origin = new LatLng(currentLattitude, currentLongitude);

            autocompleteFragment.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());

            //origin_address = addresses.get(0).getAddressLine(0) + " "+addresses.get(0).getLocality();

            origin_address = "";
            for (int i = 0; i < (addresses.get(0).getMaxAddressLineIndex() - 1); i++) {
                Log.d(" =Adress= dest ", addresses.get(0).getAddressLine(i));
                origin_address = origin_address + addresses.get(0).getAddressLine(i);
            }
            origin_address = origin_address + addresses.get(0).getLocality() + addresses.get(0).getAdminArea();

            origin_city = addresses.get(0).getLocality();
            origin_state = addresses.get(0).getAdminArea();


            dest = null;

            mMap.clear();

            autocompleteFragment1.setText("");


        } catch (Exception e) {

        }

        pDialog.dismiss();

    }


    @Override
    protected void onPause() {
        super.onPause();
        drawerLayout.closeDrawer(left_drawer);
    }

    Dialog dialog1;

    private void initiateyYesNoWindow(String message, Boolean isAlert, String heading, String buttonTextYes, String buttonTextNo) {
        try {

            dialog1 = new Dialog(Home.this);
            dialog1.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.yesnopopup);
            dialog1.setCanceledOnTouchOutside(false);
            MyTextView popup_message2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_message2);
            popup_message2.setText(message);


            ImageView popup_image2 = (ImageView) dialog1
                    .findViewById(R.id.popup_image2);
            if (isAlert) {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_heading2);
            popup_heading2.setText(heading);


            MyTextView popup_button_yes2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_yes2);
            popup_button_yes2.setText(buttonTextYes);
            popup_button_yes2.setOnClickListener(this);

            MyTextView popup_button_no2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_no2);
            popup_button_no2.setText(buttonTextNo);
            popup_button_no2.setOnClickListener(this);

            dialog1.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    String res;
    StringRequest strReq;

    public String makeBookingDetailsReq() {

        String url = getResources().getString(R.string.base_url) + "user/get-data";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkBookingDetailsResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                  /*  InstanceID instanceID = InstanceID.getInstance(ChangePassword.this);
                    Log.d("token", "2");
                    String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.d("regg", "6");

                    Log.d("register ", email_id);
                    Log.d("register ",pass);
                    Log.d("token ",token);

                    params.put("username", email_id);
                    params.put("password", pass);
                    params.put("mobile_tokens", token);

                    Log.e("register", params.toString());*/


                    params.put("_id", pref.getString("userId", "Something Wrong"));
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));

                    Log.e("register ", params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "23");


        return null;
    }

    String status_code = "0";

    public void checkBookingDetailsResponse(String response) {
        pDialog.dismiss();
        try {
            Log.e("get data ", response);

            if (response != null) {

                Log.e("register ", "a"+response);

                JSONObject reader = new JSONObject(response);

                Log.e("register ", "reader");

                status_code = reader.getString("status_code");

                Log.e("register ", "status_code " + status_code);

                if (status_code.equalsIgnoreCase("200")) {


                    String response1 = reader.getString("response");

                    Log.e("register ", "response1 " + response1);


                    Log.d("origin ", "origin_lat " + origin.latitude);
                    Log.d("origin ", "origin_long " + origin.longitude);
                    Log.d("origin ", "dest_lat " + dest.latitude);
                    Log.d("origin ", "dest_lat " + dest.longitude);

                    Log.d("date ", "date " + date);
                    Log.d("time ", "time " + time);
                    Log.d("encoded_polyline ", "encoded_polyline " + encoded_polyline);


                    Log.d("origin_address ", "origin_address " + origin_address);
                    Log.d("dest_address ", "dest_address " + dest_address);

                    Log.d("marker_time ", "marker_time " + marker_time);
                    Log.d("marker_distance ", "marker_distance " + marker_distance);


                    Log.d("origin_address ", "origin_city " + origin_city);
                    Log.d("dest_address ", "dest_city " + dest_city);

                    Log.d("origin_address ", "dest_state " + dest_state);
                    Log.d("dest_address ", "origin_state " + origin_state);


                    String url = "";
                    url = url + "https://maps.googleapis.com/maps/api/staticmap?size=800x800&markers=shadow:true|";
                    url = url + origin.latitude + "," + origin.longitude;
                    url = url + "&markers=shadow:true|";
                    url = url + dest.latitude + "," + dest.longitude;
                    url = url + "&path=weight:3%7Ccolor:blue%7Cenc:";
                    url = url + encoded_polyline;
                    url = url + "&key="+R.string.google_app_id;

                    Log.d("####URLGmaps",url);
                    editor1.clear();
                    editor1.commit();


                    //todo add cat and sub cat

                    editor1.putString("book_map_url", url);
                    editor1.putBoolean("book_pending_order", true);
                    editor1.putString("book_origin_lat", "" + origin.latitude);
                    editor1.putString("book_origin_long", "" + origin.longitude);
                    editor1.putString("book_dest_lat", "" + dest.latitude);
                    editor1.putString("book_dest_long", "" + dest.longitude);
                    editor1.putString("book_date", "" + date);
                    editor1.putString("book_time", "" + time);

                    // for pending shipment only
                    editor1.putString("book_old_date", "" + old_date);

                    editor1.putString("book_encoded_polyline", "" + encoded_polyline);
                    editor1.putString("book_origin_address", "" + origin_address);
                    editor1.putString("book_odest_address", "" + dest_address);
                    editor1.putString("book_get_data_response", "" + response1);

                    editor1.putString("book_origin_city", "" + origin_city);
                    editor1.putString("book_dest_city", "" + dest_city);

                    editor1.putString("book_origin_state", "" + origin_state);
                    editor1.putString("book_dest_state", "" + dest_state);

                    editor1.putString("book_est_time", "" + marker_time);
                    editor1.putString("book_est_dist", "" + marker_distance);


                    editor1.commit();


                    origin = null;
                    dest = null;
                    date = null;
                    time = null;
                    mMap.clear();

                    autocompleteFragment.setHint(getResources().getString(R.string.loading_point));
                    autocompleteFragment1.setHint(getResources().getString(R.string.unloading_point));
                    autocompleteFragment.setText("");
                    autocompleteFragment1.setText("");

                    datee.setText(getResources().getString(R.string.date));
                    timee.setText(getResources().getString(R.string.time));

                    Intent i = new Intent(Home.this, BookOneTruck.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i);






/*

                   JSONObject jsonobj = new JSONObject(response1);

                    String Categories = jsonobj.getString("Categories");

                    JSONObject jsonobj1 = new JSONObject(Categories);

                    String parent = jsonobj1.getString("parent");
                    String sub_categories = jsonobj1.getString("sub-categories");

                    JSONObject jsonobj3 = new JSONObject(sub_categories);


                    JSONObject jsonobj2 = new JSONObject(parent);

                    String self = jsonobj2.getString("self");


                    JSONArray jsonArr1 = new JSONArray(self);


                        for (int j = 0; j < jsonArr1.length(); j++) {

                            JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(j);

                            String id = jsonObjectPackageData1.getString("id");
                            String name = jsonObjectPackageData1.getString("name");

                            if(jsonobj3.has(id))
                            {


                                String id1 = jsonobj3.getString(id);

                                JSONArray jsonArr2 = new JSONArray(id1);


                                for (int k = 0; k < jsonArr2.length(); k++) {

                                    JSONObject jsonObjectPackageData2 = jsonArr2.getJSONObject(k);

                                    String id2 = jsonObjectPackageData2.getString("id");
                                    String name2 = jsonObjectPackageData2.getString("name");





                                }



                            }


                        }


                    String Trucks = jsonobj.getString("Trucks");

                    JSONObject jsonobj4 = new JSONObject(Trucks);

                    String first = jsonobj4.getString("Below 5 ton");
                    String second = jsonobj4.getString("5 ton to 15 ton");
                    String third = jsonobj4.getString("Above 15 ton");


                    JSONArray jsonArr2 = new JSONArray(first);
                    JSONArray jsonArr3 = new JSONArray(second);
                    JSONArray jsonArr4 = new JSONArray(third);



                    for (int k = 0; k < jsonArr2.length(); k++) {

                        JSONObject jsonObjectPackageData2 = jsonArr2.getJSONObject(k);

                        String id2 = jsonObjectPackageData2.getString("id");
                        String name2 = jsonObjectPackageData2.getString("name");

                        Log.d("first ",id2 +" "+name2);

                    }

                    for (int k = 0; k < jsonArr3.length(); k++) {

                        JSONObject jsonObjectPackageData2 = jsonArr3.getJSONObject(k);

                        String id2 = jsonObjectPackageData2.getString("id");
                        String name2 = jsonObjectPackageData2.getString("name");

                        Log.d("second ",id2 +" "+name2);
                    }

                    for (int k = 0; k < jsonArr4.length(); k++) {

                        JSONObject jsonObjectPackageData2 = jsonArr4.getJSONObject(k);

                        String id2 = jsonObjectPackageData2.getString("id");
                        String name2 = jsonObjectPackageData2.getString("name");

                        Log.d("third ",id2 +" "+name2);
                    }

*/


                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else {
                    initiatePopupWindow("No truckers available. Try again!!",
                            true, getResources().getString(R.string.error)+" " + status_code,getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText) {
        try {

            dialog = new Dialog(Home.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if (isAlert) {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();


        shipper_name.setText(pref.getString("user_firstname", "") + " " + pref.getString("user_lastname", ""));

        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        profile_image.setOnClickListener(this);

        if (!pref.getString("user_image_url", "").equalsIgnoreCase("")) {
            Picasso.with(getApplicationContext())
                    .load(pref.getString("user_image_url", ""))
                    .placeholder(R.drawable.default_user)
                    .error(R.drawable.default_user).into(profile_image);
        } else if (!pref.getString("user_image_base64", "").equalsIgnoreCase("")) {

            byte[] decodedString = Base64.decode(pref.getString("user_image_base64", ""), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            profile_image.setImageBitmap(decodedByte);

        }


    }
}
