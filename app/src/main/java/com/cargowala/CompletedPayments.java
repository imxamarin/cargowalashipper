package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by Akash on 2/10/2016.
 */
public class CompletedPayments extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton;
    String orderId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.completed_payment);


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        Intent intent = getIntent();
        orderId = intent.getExtras().getString("orderId");


        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);
        backbutton.requestLayout();


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.backbutton:
                finish();
                break;

        }

    }

}
