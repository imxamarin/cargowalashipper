package com.cargowala;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cargowala.helper.CommonListners;
import com.cargowala.helper.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class MyNotification extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;
    LinearLayout main_layout;
    RelativeLayout actionBar;
    ImageView backbutton;
    SweetAlertDialog pDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int record_no = 0;
    //-----------if more records exist then make it true so that further pagination can be done
    boolean doPagination = true;
    MyTextView loadingText;
    boolean scroll = true;
    boolean headernotcreated = true;
    ImageView blank_notification_image;
    ExampleScrollView scroll_layout;
    int limit=10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_notification);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        blank_notification_image=(ImageView) findViewById(R.id.blank_notification_image);
        scroll_layout=(ExampleScrollView) findViewById(R.id.scroll_layout);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);


        backbutton.requestLayout();


        main_layout = (LinearLayout) findViewById(R.id.main_layout);


       /* for (int i = 0; i < 7; i++) {


            MyTextViewSemi date_heading = new MyTextViewSemi(
                    getApplicationContext());
            date_heading.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            date_heading.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.notification_dat_bg));

            // date_heading.setGravity(Gravity.CENTER);


            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            params4.setMargins(0, 20, 0, 0);
            params4.gravity = Gravity.CENTER;

            date_heading.setPadding(10, 5, 10, 5);
            date_heading.setLayoutParams(params4);
            date_heading.setText(getResources().getString(R.string.march));

            main_layout.addView(date_heading);


            for (int j = 0; j < 2; j++) {

                LinearLayout ll1 = new LinearLayout(
                        MyNotification.this);
                ll1.setOrientation(LinearLayout.VERTICAL);

                if (j % 2 == 0) {
                    ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                } else {
                    ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));
                }


                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

              *//*  ll1.setId(i + 10000);
                ll1.setOnClickListener(MyPayments.this);*//*
                ll1.setLayoutParams(params2);

                main_layout.addView(ll1);


                RelativeLayout rl1 = new RelativeLayout(
                        MyNotification.this);

                LinearLayout.LayoutParams params22 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params22.setMargins(0, 20, 0, 0);
                rl1.setLayoutParams(params22);

                ll1.addView(rl1);


                MyTextView time_text = new MyTextView(MyNotification.this);

                RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                params3.addRule(
                        RelativeLayout.ALIGN_PARENT_RIGHT,
                        RelativeLayout.TRUE);
                params3.setMargins(0, 0, 20, 0);
                // time_text.setPadding(10, 5, 10, 5);


                time_text.setLayoutParams(params3);
                time_text.setTextSize(10f);
                time_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                //time_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                time_text.setText(getResources().getString(R.string.time));


                rl1.addView(time_text);

                MyTextView name_text = new MyTextView(MyNotification.this);

                RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


                params5.setMargins(20, 0, 0, 0);
                // time_text.setPadding(10, 5, 10, 5);


                name_text.setLayoutParams(params5);
                name_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                //time_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                name_text.setText(getResources().getString(R.string.name));
                rl1.addView(name_text);

                MyTextView message_text = new MyTextView(
                        getApplicationContext());
                message_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));


                LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                params6.setMargins(20, 5, 20, 20);
                //  params4.gravity =Gravity.CENTER;


                message_text.setLayoutParams(params6);
                message_text.setText(getResources().getString(R.string.your_tuck_has_been_reached));

                ll1.addView(message_text);

                View line = new View(
                        getApplicationContext());

                line.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, 1);

                //   params7.setMargins(0, 0, 0, 20);
                //  params4.gravity =Gravity.CENTER;


                line.setLayoutParams(params7);

                ll1.addView(line);
            }


        }*/
//----------------
        scroll_layout.setOnScrollViewListener(new ExampleScrollView.OnScrollViewListener() {
            public void onScrollChanged(ExampleScrollView v, int l, int t,
                                        int oldl, int oldt) {

                View view = (View) v.getChildAt(v.getChildCount() - 1);
                int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));

                if (diff == 0) {

                    if (scroll) {

                        if (doPagination) {

                            scroll = false;


                            if (CommonUtils.isOnline(MyNotification.this)) {
                                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                                loadingText = new MyTextView(
                                        MyNotification.this);

                                params4.setMargins(0, 10, 0, 20);

                                loadingText.setLayoutParams(params4);
                                loadingText.setText(getResources().getString(R.string.loading_dots));
                                loadingText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                                loadingText.setTypeface(null, Typeface.BOLD);

                                loadingText.setGravity(Gravity.CENTER);
                                main_layout.addView(loadingText);

                                scroll_layout.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        scroll_layout.fullScroll(ExampleScrollView.FOCUS_DOWN);
                                    }
                                });


                                getNotifications();
                            } else {

                                scroll_layout.scrollBy(0, -20);
                                scroll = true;

                                Toast.makeText(MyNotification.this,
                                        getResources().getString(R.string.check_your_network),
                                        Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                }
            }
        });
        if (CommonUtils.isOnline(MyNotification.this)) {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            pDialog.setTitleText(getResources().getString(R.string.loading));
            pDialog.setCancelable(false);
            pDialog.show();
            headernotcreated = true;
            getNotifications();

        } else {

            Toast.makeText(MyNotification.this,
                    getResources().getString(R.string.check_your_network),
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:

                finish();

                break;


        }


    }

    String res1;
    StringRequest strReq1;
    String status_code = "0";

    public String getNotifications() {
//-- * Url : http://cargowala.com/dev/frontend/web/index.php?r=driver/user/get-notifications
        String url = getResources().getString(R.string.base_url) + "user/get-notifications";
        Log.d("regg", "2");
        strReq1 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res1 = response.toString();
                        checkGetNotificationsResponse(res1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res1 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("next_records", "" + record_no);
                    params.put("limit", "" + limit);
                    params.put("_id", pref.getString("userId", "Something Wrong"));
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));


                    Log.e("register", params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq1, "21");


        return null;
    }

    private void checkGetNotificationsResponse(String response) {
        pDialog.dismiss();
        if (response != null) {

            Log.e("register", "a");

            try {
                JSONObject reader = new JSONObject(response);
                Log.e("register", "reader");
                status_code = reader.getString("status_code");
                Log.e("register", "status_code " + status_code);
                if (status_code.equalsIgnoreCase("200")) {
                    String response1 = reader.getString("response");
                    JSONArray jsonArr1 = new JSONArray(response1);

                    if (headernotcreated) {
                        headernotcreated = false;
                        if (jsonArr1.length() == 0) {
                            blank_notification_image.setVisibility(View.VISIBLE);
                        } else {
                            blank_notification_image.setVisibility(View.GONE);
                            scroll_layout.setVisibility(View.VISIBLE);
                            main_layout.removeAllViews();
                        }


                    } else {
                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll = true;
                    }

                    record_no = record_no + limit;

                    for (int i = 0; i < jsonArr1.length(); i++) {

                        if (jsonArr1.length() % limit == 0) {
                            doPagination = true;
                        } else {

                            doPagination = false;
                        }

                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);
                        String _id = jsonObjectPackageData1.getString("_id");
                        String message = jsonObjectPackageData1.getString("message");
                        String date = jsonObjectPackageData1.getString("date");
                        String time = jsonObjectPackageData1.getString("time");

                        settingData(date, time, message, i);
                    }


                } else if (status_code.equalsIgnoreCase("204")) {
                    //no more records
                    doPagination = false;
                    if (record_no > 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_more_notification), Toast.LENGTH_SHORT).show();
                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll = true;

                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_notification_found), Toast.LENGTH_LONG).show();
                    }
                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    CommonUtils.initiatePopupWindow(MyNotification.this, getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    CommonUtils.initiatePopupWindow(MyNotification.this, getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });

                } else {
                    CommonUtils.initiatePopupWindow(MyNotification.this, getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + " " + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public void settingData(String date, String time, String message, int pos) {
        {
            MyTextViewSemi date_heading = new MyTextViewSemi(
                    getApplicationContext());
            date_heading.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            date_heading.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.notification_dat_bg));
            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params4.setMargins(0, 20, 0, 0);
            params4.gravity = Gravity.CENTER;
            date_heading.setPadding(10, 5, 10, 5);
            date_heading.setLayoutParams(params4);
            date_heading.setText(date);
            main_layout.addView(date_heading);
            LinearLayout ll1 = new LinearLayout(
                    MyNotification.this);
            ll1.setOrientation(LinearLayout.VERTICAL);

            if (pos % 2 == 0) {
                ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            } else {
                ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));
            }


            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

              /*  ll1.setId(i + 10000);
                ll1.setOnClickListener(MyPayments.this);*/
            ll1.setLayoutParams(params2);

            main_layout.addView(ll1);


            RelativeLayout rl1 = new RelativeLayout(
                    MyNotification.this);

            LinearLayout.LayoutParams params22 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params22.setMargins(0, 20, 0, 0);
            rl1.setLayoutParams(params22);

            ll1.addView(rl1);


            MyTextView time_text = new MyTextView(MyNotification.this);

            RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params3.addRule(
                    RelativeLayout.ALIGN_PARENT_RIGHT,
                    RelativeLayout.TRUE);
            params3.setMargins(0, 0, 20, 0);
            // time_text.setPadding(10, 5, 10, 5);


            time_text.setLayoutParams(params3);
            time_text.setTextSize(10f);
            time_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            //time_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            time_text.setText(time);
            rl1.addView(time_text);

            MyTextView message_text = new MyTextView(
                    getApplicationContext());
            message_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));
            LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            params6.setMargins(20, 5, 20, 20);
            //  params4.gravity =Gravity.CENTER;
            message_text.setLayoutParams(params6);
            message_text.setText(message);

            ll1.addView(message_text);
            View line = new View(
                    getApplicationContext());

            line.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

            LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, 1);

            //   params7.setMargins(0, 0, 0, 20);
            //  params4.gravity =Gravity.CENTER;


            line.setLayoutParams(params7);
            ll1.addView(line);


        }

    }
}
