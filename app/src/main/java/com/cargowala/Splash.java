package com.cargowala;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.cargowala.helper.Constants;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Splash extends Activity {

    private static int SPLASH_TIME_OUT = 1500;
    SharedPreferences pref,Pref2;
    SharedPreferences.Editor editor;
    private ImageView imageview;
    private View agentScreen;
    private CircleImageView profile_image;
    private MyTextViewSemi name,companyName;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash);
        imageview=(ImageView) findViewById(R.id.imageview);
        agentScreen=findViewById(R.id.agentScreen);
        profile_image=(CircleImageView) agentScreen.findViewById(R.id.profile_image);
        name=(MyTextViewSemi) agentScreen.findViewById(R.id.name);
        companyName=(MyTextViewSemi) agentScreen.findViewById(R.id.companyName);
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.cargowala",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        Pref2 = getApplicationContext().getSharedPreferences(Constants.LANGUAGE, MODE_PRIVATE);
        editor = pref.edit();

        //----------making agent feel special
        if (pref.getBoolean("isLoggedIn", false)) {
            if (pref.getBoolean("isProfileCreated", false)) {

                if(pref.getString("user_account_type","").equalsIgnoreCase("3")){
                    imageview.setVisibility(View.GONE);
                    agentScreen.setVisibility(View.VISIBLE);
                    name.setText(pref.getString("user_firstname", "") + " " + pref.getString("user_lastname", ""));
                    companyName.setText(pref.getString("user_business_name",""));
                    if(!pref.getString("userImage","").equalsIgnoreCase(""))
                    {

                        Picasso.with(getApplicationContext())
                                .load(pref.getString("userImage", ""))
                                .placeholder(R.drawable.default_user)
                                .error(R.drawable.default_user).into(profile_image);
                        Log.e("userImage","from url:"+pref.getString("userImage", ""));

                    }else {
                        if(!pref.getString("user_image_base64","NA").equalsIgnoreCase("NA"))
                        {
                            byte[] decodedString = Base64.decode(pref.getString("user_image_base64","NA"), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            profile_image.setImageBitmap(decodedByte);
                        }
                        Log.e("userImage","imagebase64");
                    }


                }
            } else {

            }
        }

        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (pref.getBoolean("isLoggedIn", false)) {
                    if (pref.getBoolean("isProfileCreated", false)) {
                        Intent i = new Intent(Splash.this, Home.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(i);
                        finish();
                        Log.e("vvvvvvvvv", "111111111111111111111111");
                    } else {

                        Intent i = new Intent(Splash.this, CreateProfile.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(i);
                        finish();
                        Log.e("vvvvvvvvv", "2222222222222222222222");
                    }
                } else {

                    /*if(Pref2.getBoolean(Constants.ISVIEWED,false)){
                    }else {
                    }*/
                    if(Pref2.getBoolean(Constants.ISVIEWED,false)){
                        Intent i = new Intent(Splash.this, Login.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.putExtra("wasGoogleLoggedIn", false);
                        startActivity(i);
                        finish();
                        Log.e("vvvvvvvvv", "33333333333333");
                    }else {
                        Intent i1 = new Intent(Splash.this, SelectLanguage.class);
                        i1.putExtra("from_setting", false);
                        startActivity(i1);
                        finish();
                    }


                }


            }
        }, SPLASH_TIME_OUT);

    }

}
