package com.cargowala;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Akash on 2/17/2016.
 */
public class MyTextViewHeading extends TextView {

    public MyTextViewHeading(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewHeading(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewHeading(Context context) {
        super(context);
        init();
    }

    public void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/grandhotelregular.ttf");
        setTypeface(tf/* ,1*/);

    }
}