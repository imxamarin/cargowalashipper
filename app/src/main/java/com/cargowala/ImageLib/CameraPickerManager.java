package com.cargowala.ImageLib;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.cargowala.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Mickael on 10/10/2016.
 */

public class CameraPickerManager extends PickerManager {

    public CameraPickerManager(Activity activity) {
        super(activity);
    }


	
	
	protected void sendToExternalApp() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		mProcessingPhotoUri = getImageFileCompat();
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mProcessingPhotoUri);
		activity.startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE);
	}
	
	private Uri getImageFileCompat() {
		if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			// https://inthecheesefactory.com/blog/how-to-share-access-to-file-with-fileprovider-on-android-nougat/en
			String imageName = activity.getString(R.string.app_name);
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
			String imageFileName = imageName + timeStamp + "_";
			File storageDir = new File(Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_DCIM), "Camera");
			try {
				File image = File.createTempFile(
						imageFileName,  /* prefix */
						".jpg",         /* suffix */
						storageDir      /* directory */
				);
				return FileProvider.getUriForFile(activity,
						activity.getPackageName()  + ".provider",
						image);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return null;
			
		} else {
			return getImageFile();
		}
	}
	
	 
	 
	
	
}
