package com.cargowala.helper;

/**
 * Created by Akash on 8/5/2016.
 */
public class Constants {

//-----------http://cargowala.com/dev/frontend/web/index.php?r=trucker%2Fuser%2Fbill-generator
    public static String BILL_URL =  "http://www.cargowala.com/dev/billGenerator.sandbox.php";
   // public static String BILL_URL =  "https://salty-plateau-1529.herokuapp.com/billGenerator.sandbox.php";

    public static String SIGNUP_ID = "mmes2u0c7r-signup";
    public static String SIGNUP_SECRET = "d1014f6cc623be1427ab1eb8d6bd3d80";
    public static String SIGNIN_ID = "mmes2u0c7r-signin";
    public static String SIGNIN_SECRET = "f8a3fe749e81891eab7937bfec85914e";
    public static String VANITY = "mmes2u0c7r";
   // public static Environment environment = Environment.SANDBOX;
    public static String FAQ_CONTACTUS = "https://www.google.co.in";


    public static final String GEO_BASE_URL="https://maps.googleapis.com/maps/api/geocode";
    public static final String NEARBY_URL="https://maps.googleapis.com/maps/api/place/nearbysearch";
    public static final String SUGGESTION_API="https://maps.googleapis.com/maps/api/place/autocomplete";
    public static final String STATUS = "status";

    public static final String OK = "OK";
    public static final String RESULTS = "results";
    public static final String GEOMETRY = "geometry";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String FORMATTED_ADDRESS = "formatted_address";
    public static final String ADDRESS_COMPONENTS = "address_components";
    public static final String TYPES = "types";
    public static final String ROUTE = "route";
    public static final String SHORT_NAME = "short_name";
    public static final String ADMINISTRATIVE_AREA_LEVEL_2 = "administrative_area_level_2";
    public static final String NOT_FOUND = "Not found";
    public static final String BAD_REQUEST = "400 Bad Request";
    public static final String RECORD_ALREADY_EXISTS = "409 Record already exists";
    public static final String SUCCESS_MSG = "Success...";
    public static final String RETRY_LIST = "retryList";
    public static final String CMPLETEDLIST = "cmpletedList";

    public static final String LOCATION = "location";
    public static final String COUNTRY = "country";
    public static final String POSTAL_CODE = "postal_code";
    public static final String LONG_NAME ="long_name";


    public static final String LANGUAGE ="language";
    public static final String ENGLISH ="english";
    public static final String HINDI ="hindi";
    public static final String LOCALE ="locale";
    public static final String ENG ="en";
    public static final String HND ="hi";
    public static final String ISVIEWED ="isViewed";

}
