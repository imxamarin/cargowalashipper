package com.cargowala.helper;

/**
 * Created by Bhvesh on 26-Sep-16.
 */
public interface CommonListners {

    public interface AlertCallBackWithButtonsInterface {
        void positiveClick();

        void neutralClick();

        void negativeClick();
    }


    public interface AlertCallBackWithOk {
        void positiveClick();

    }
}
