package com.cargowala.helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.cargowala.Models.GeoLocationResponce;
import com.cargowala.MyTextView;
import com.cargowala.R;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by Bhvesh on 21-Oct-16.
 */
public class CommonUtils {

    /**
     * to check whether internet is available or not
     * @param context
     * @return
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    /**
     * dialog to show error or single responce with ok button
     * @param context
     * @param message
     * @param isAlert
     * @param heading
     * @param buttonText
     * @param alertCallBackWithOk
     */
    public static void initiatePopupWindow(Context context,String message, Boolean isAlert, String heading, String buttonText ,final CommonListners.AlertCallBackWithOk alertCallBackWithOk) {
        try {
            final Dialog dialog;
            dialog = new Dialog(context);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(context.getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(context.getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    alertCallBackWithOk.positiveClick();
                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param context
     * @param url
     */
    public static void openLink(final Activity context, final String url) {

        try {
            if(url.trim().startsWith("www")){
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://" + url));
                context.startActivity(i);
            }
            else if(!url.trim().startsWith("www") && !url.startsWith("http")){
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://www." + url));
                context.startActivity(i);

            }
            else {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                context.startActivity(i);

            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, context.getResources().getString(R.string.wrong_web_address_message), Toast.LENGTH_SHORT).show();
        }
    }

    public static String makeURLPathBillGenerator(String baseurl, String transectionId,String service_for,String service_id,String request_by) {
        StringBuilder sb = null;
        sb = new StringBuilder(baseurl);
        sb.append("&service_for=" + service_for);
        sb.append("&service_id=" + service_id);
        sb.append("&txn_id=" + transectionId);
       // sb.append("&transectionId=" + transectionId);
        sb.append("&request_by=" + request_by);
        return sb.toString();
    }


    /**
     * Parser Method for the GeoCodingAPI class
     * @param jsonObject to be parsed
     * @return
     */
    public static GeoLocationResponce jsonParsorForGeo(JsonObject jsonObject) {
        Log.e("GeoLocationResponce",""+jsonObject);
        String formatted_address = "";
        String city = "";
        Double lat = 0.0, lng = 0.0;
        String country="";
        String postal_code="";
        GeoLocationResponce geoLocationResponce = new GeoLocationResponce();
        try {
            JSONObject mainOject = new JSONObject(jsonObject.toString());
            JSONObject geometry = null;
            JSONObject location = null;
            String status = mainOject.getString(Constants.STATUS);
            if (status.equalsIgnoreCase(Constants.OK)) {
                if (mainOject.has(Constants.RESULTS)) {
                    JSONArray results = mainOject.getJSONArray(Constants.RESULTS);
                    if (results.length() > 0) {
                        JSONObject resultObject = results.getJSONObject(0);
                        geometry = resultObject.getJSONObject(Constants.GEOMETRY);
                        location = geometry.getJSONObject(Constants.LOCATION);
                        lat = location.getDouble(Constants.LAT);
                        lng = location.getDouble(Constants.LNG);
                        formatted_address = resultObject.getString(Constants.FORMATTED_ADDRESS);
                        if (resultObject.has(Constants.ADDRESS_COMPONENTS)) {
                            JSONArray address_components = resultObject.getJSONArray(Constants.ADDRESS_COMPONENTS);
                            outerloop:
                            for (int k = 0; k < address_components.length(); k++) {
                                JSONObject innerObejct = address_components.getJSONObject(k);
                                if (innerObejct.has(Constants.TYPES)) {
                                    JSONArray types = innerObejct.getJSONArray(Constants.TYPES);

                                    if(types.getString(0).equalsIgnoreCase(Constants.COUNTRY)){
                                        country=innerObejct.getString(Constants.LONG_NAME);
                                    }
                                    if(types.getString(0).equalsIgnoreCase(Constants.POSTAL_CODE)){
                                        postal_code=innerObejct.getString(Constants.LONG_NAME);
                                    }
                                    if (types.getString(0).equalsIgnoreCase(Constants.ROUTE)) {
                                        city = innerObejct.getString(Constants.SHORT_NAME);
                                    }

                                }


                            }

                        }
                        //------------setValues
                        geoLocationResponce.setAddress(formatted_address);
                        geoLocationResponce.setCity(city);
                        geoLocationResponce.setCountry(country);
                        geoLocationResponce.setPostal_code(postal_code);
                        geoLocationResponce.setLatitude(lat);
                        geoLocationResponce.setLongitude(lng);

                    }
                } else {

                }
            } else {

            }
            return geoLocationResponce;

        } catch (JSONException e) {
            e.printStackTrace();

            return geoLocationResponce;
        }

    }



    public static  void settingLanguage(String localeString,Context mcontext){
        Locale locale = new Locale(localeString);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        mcontext.getResources().updateConfiguration(config,
                mcontext.getResources().getDisplayMetrics());

    }


    public static void yesNoPopupWindow(Context context,String message, Boolean isAlert, String heading, String yesText,String noText ,final CommonListners.AlertCallBackWithButtonsInterface alertCallBackWithOk) {
        try {
            final Dialog dialog;
            dialog = new Dialog(context);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.yesnopopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message2);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image2);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(context.getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(context.getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading2);
            popup_heading.setText(heading);


            MyTextView popup_button_yes = (MyTextView) dialog
                    .findViewById(R.id.popup_button_yes2);
            popup_button_yes.setText(yesText);
            popup_button_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    alertCallBackWithOk.positiveClick();
                }
            });
            MyTextView popup_button_no = (MyTextView) dialog
                    .findViewById(R.id.popup_button_no2);
            popup_button_no.setText(noText);
            popup_button_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    alertCallBackWithOk.negativeClick();
                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String roundFigTwo(float value){
        try {
            return String.format("%.2f", value);
        } catch (Exception e) {
            e.printStackTrace();
            return ""+value;
        }
    }
    public static String roundFigTwo(double value){
        try {
            return String.format("%.2f", value);
        } catch (Exception e) {
            e.printStackTrace();
            return ""+value;
        }
    }
    public static String roundFigTwo(String value){
        try {
            return String.format("%.2f", Double.parseDouble(value));
        } catch (Exception e) {
            e.printStackTrace();
            return ""+value;
        }
    }

    public static double roundFigTwoInDouble(double value){
        try {
            return Double.valueOf(String.format("%.2f", value));
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
    }
}
