package com.cargowala;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class ItemsDetail extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;
    RelativeLayout actionBar;
    ImageView backbutton,no_connection_image;
    LinearLayout main_layout;

    private List<String> itemIdArray = new ArrayList<String>();
    String orderId1;
    SweetAlertDialog pDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.items_detail);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;


        Intent intent = getIntent();

        orderId1 = intent.getExtras().getString("orderId1");


        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);

        backbutton.requestLayout();



        main_layout = (LinearLayout) findViewById(R.id.main_layout);


        no_connection_image = (ImageView) findViewById(R.id.no_connection_image);




        if (isOnline()) {




            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            pDialog.setTitleText(getResources().getString(R.string.loading));
            pDialog.setCancelable(false);
            pDialog.show();
            makeGetBidDetailsReq();




        } else {

            no_connection_image.setVisibility(View.VISIBLE);

            Toast.makeText(ItemsDetail.this,
                    getResources().getString(R.string.check_your_network),
                    Toast.LENGTH_SHORT).show();
        }



        ////////////////////////////////////////////////


         /*   LinearLayout ll = new LinearLayout(
                    BidBoardDetail.this);
            ll.setOrientation(LinearLayout.VERTICAL);



            LinearLayout.LayoutParams params0 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

            ll.setLayoutParams(params0);

            main_layout.addView(ll);


            LinearLayout ll1 = new LinearLayout(
                    BidBoardDetail.this);
            ll1.setOrientation(LinearLayout.HORIZONTAL);


            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
            params1.setMargins(20, 20, 20, 20);

            ll1.setLayoutParams(params1);

            ll.addView(ll1);




            MyTextViewBold origin_text = new MyTextViewBold(
                    getApplicationContext());
            origin_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            origin_text.setGravity(Gravity.CENTER);

            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            //  params2.gravity = Gravity.CENTER;
            params2.weight = 0.37f;
            // trucker_name.setPadding(5, 4, 5, 2);
            origin_text.setLayoutParams(params2);


            origin_text.setText("Chandigarh");

            ll1.addView(origin_text);




            View line = new View(
                    getApplicationContext());


            LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            //  params2.gravity = Gravity.CENTER;
            params5.weight = 0.05f;
            line.setLayoutParams(params5);


            ll1.addView(line);


            ImageView truck_image = new ImageView(
                    getApplicationContext());

            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
            truck_image.setAdjustViewBounds(true);

            // truck_image.setPadding(10,0,10,0);
            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            //  params2.gravity = Gravity.CENTER;
            params4.weight = 0.16f;
            //params10.gravity= Gravity.CENTER;

            //params4.setMargins(0, 10, 0, 10);
            truck_image.setLayoutParams(params4);

            ll1.addView(truck_image);



            View line1 = new View(
                    getApplicationContext());


            LinearLayout.LayoutParams params55 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            //  params2.gravity = Gravity.CENTER;
            params55.weight = 0.05f;
            line1.setLayoutParams(params55);


            ll1.addView(line1);






            MyTextViewBold destination_text = new MyTextViewBold(
                    getApplicationContext());
            destination_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            destination_text.setGravity(Gravity.CENTER);

            LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            //  params2.gravity = Gravity.CENTER;
            params3.weight = 0.37f;
            destination_text.setLayoutParams(params3);


            destination_text.setText("Delhi");

            ll1.addView(destination_text);





            LinearLayout ll2 = new LinearLayout(
                    BidBoardDetail.this);
            ll2.setOrientation(LinearLayout.HORIZONTAL);


            LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

            params6.setMargins(20, 0, 20, 20);
            ll2.setLayoutParams(params6);

            ll.addView(ll2);




            LinearLayout ll3 = new LinearLayout(
                    BidBoardDetail.this);
            ll3.setOrientation(LinearLayout.VERTICAL);


            LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);


            params7.weight = 0.498f;

            ll3.setLayoutParams(params7);

            ll2.addView(ll3);


            MyTextViewSemi load_type = new MyTextViewSemi(
                    getApplicationContext());
            load_type.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            LinearLayout.LayoutParams params8 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);


            load_type.setLayoutParams(params8);


            load_type.setText("Full Load - Priority");
            // bid_info.setPadding(5, 0, 5, 0);
            ll3.addView(load_type);


            MyTextViewSemi no_of_trucks_text = new MyTextViewSemi(
                    getApplicationContext());
            load_type.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));




            load_type.setLayoutParams(params8);
            String text2 = "<font color=#585858  >No of trucks : </font><font color=#ff5400  ><b>10</b></font>";
            no_of_trucks_text.setText(Html.fromHtml(text2));

            // no_of_trucks_text.setText("Full Load - Priority");
            // bid_info.setPadding(5, 0, 5, 0);
            ll3.addView(no_of_trucks_text);



            View line2 = new View(
                    getApplicationContext());

            line2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            LinearLayout.LayoutParams params555 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.MATCH_PARENT);

            //  params2.gravity = Gravity.CENTER;
            params555.weight = 0.004f;
            line2.setLayoutParams(params555);


            ll2.addView(line2);








            LinearLayout ll4 = new LinearLayout(
                    BidBoardDetail.this);
            ll4.setOrientation(LinearLayout.VERTICAL);


            LinearLayout.LayoutParams params9 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);


            params9.weight = 0.498f;

            ll4.setLayoutParams(params9);

            ll2.addView(ll4);


            MyTextViewSemi date_text = new MyTextViewSemi(
                    getApplicationContext());
            date_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            LinearLayout.LayoutParams params10 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

            params10.gravity = Gravity.RIGHT;
            date_text.setLayoutParams(params10);


            date_text.setText("25 March 2016");
            // bid_info.setPadding(5, 0, 5, 0);
            ll4.addView(date_text);


            MyTextViewSemi time_text = new MyTextViewSemi(
                    getApplicationContext());
            time_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            LinearLayout.LayoutParams params11 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

            params11.gravity = Gravity.RIGHT;
            time_text.setLayoutParams(params11);

            String text3 = "<font color=#585858  >at </font><font color=#ff5400  ><b>10:00 AM</b></font>";
            time_text.setText(Html.fromHtml(text3));
            //time_text.setText("25 March 2016");
            // bid_info.setPadding(5, 0, 5, 0);
            ll4.addView(time_text);




            MyTextView truck_name = new MyTextView(
                    getApplicationContext());
            truck_name.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            LinearLayout.LayoutParams params13 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

            params13.setMargins(20, 0, 20, 20);
            truck_name.setLayoutParams(params13);


            truck_name.setText("TATA PRIMA (5 TON MT)");
            // bid_info.setPadding(5, 0, 5, 0);
            ll.addView(truck_name);



        MyTextView total_weight = new MyTextView(
                getApplicationContext());

        LinearLayout.LayoutParams params15 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

        params15.setMargins(20, 0, 20, 0);
        total_weight.setLayoutParams(params15);

        String text4 = "<font color=#ff5400  >Total item weight : </font><font color=#585858  ><b>1000 kg</b></font>";
        total_weight.setText(Html.fromHtml(text4));
       // total_weight.setText("TATA PRIMA (5 TON MT)");
        // bid_info.setPadding(5, 0, 5, 0);
        ll.addView(total_weight);



        for(int i =0;i<3;i++)
        {

            LinearLayout items_layout = new LinearLayout(
                    BidBoardDetail.this);
            items_layout.setOrientation(LinearLayout.VERTICAL);
            items_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));
            items_layout.setPadding(20, 20, 20, 20);

            items_layout.setOnClickListener(this);
            items_layout.setId(i + 100);

            LinearLayout.LayoutParams params14 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
            params14.setMargins(20,20,20,0);
            items_layout.setLayoutParams(params14);

            main_layout.addView(items_layout);

            itemIdArray.add("abc " + i);

       *//*     MyTextView item_name = new MyTextView(
                    getApplicationContext());

            LinearLayout.LayoutParams params15 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

            item_name.setLayoutParams(params15);

            String text5 = "<font color=#585858  >Item 1 : </font><font color=#ff5400  ><b>Plates</b></font>";
            total_weight.setText(Html.fromHtml(text5));

            items_layout.addView(item_name);*//*


            LinearLayout horizontalLayout = new LinearLayout(
                    getApplicationContext());


            LinearLayout.LayoutParams paramshorizontalLayout = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
          *//*  paramshorizontalLayout.setMargins(10, 10, 0,
                    0);*//*


            horizontalLayout
                    .setLayoutParams(paramshorizontalLayout);

            horizontalLayout
                    .setOrientation(LinearLayout.HORIZONTAL);
            items_layout.addView(horizontalLayout);


            MyTextView item_no = new MyTextView(
                    getApplicationContext());
            item_no.setText("Item " + (i + 1) + " : ");

            item_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));
            LinearLayout.LayoutParams paramsitemMargin = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);


            item_no
                    .setLayoutParams(paramsitemMargin);
            horizontalLayout.addView(item_no);


            MyTextView item_name2 = new MyTextView(
                    getApplicationContext());
            item_name2.setText("Bed");

            item_name2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


            item_name2
                    .setLayoutParams(paramsitemMargin);
            horizontalLayout.addView(item_name2);


            LinearLayout horizontalLayout2 = new LinearLayout(
                    getApplicationContext());


            horizontalLayout2
                    .setLayoutParams(paramshorizontalLayout);

            horizontalLayout2
                    .setOrientation(LinearLayout.HORIZONTAL);
            items_layout.addView(horizontalLayout2);


            MyTextView item_cat = new MyTextView(
                    getApplicationContext());
            item_cat.setText("Category  : ");

            item_cat.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));


            item_cat
                    .setLayoutParams(paramsitemMargin);
            horizontalLayout2.addView(item_cat);


            MyTextView item_cat2 = new MyTextView(
                    getApplicationContext());
            item_cat2.setText("Furniture");

            item_cat2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


            item_cat2
                    .setLayoutParams(paramsitemMargin);
            horizontalLayout2.addView(item_cat2);










            LinearLayout horizontalLayout3 = new LinearLayout(
                    getApplicationContext());


            horizontalLayout3
                    .setLayoutParams(paramshorizontalLayout);

            horizontalLayout3
                    .setOrientation(LinearLayout.HORIZONTAL);
            items_layout.addView(horizontalLayout3);


            MyTextView item_sub_cat = new MyTextView(
                    getApplicationContext());
            item_sub_cat.setText("Sub Category  : ");

            item_sub_cat.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));


            item_sub_cat
                    .setLayoutParams(paramsitemMargin);
            horizontalLayout3.addView(item_sub_cat);


            MyTextView item_sub_cat2 = new MyTextView(
                    getApplicationContext());
            item_sub_cat2.setText("Home Goods");

            item_sub_cat2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


            item_sub_cat2
                    .setLayoutParams(paramsitemMargin);
            horizontalLayout3.addView(item_sub_cat2);



        }



        MyTextView insurance_text = new MyTextView(
                getApplicationContext());
        insurance_text.setText("Insurance  : Yes");

        LinearLayout.LayoutParams paramsitemMargin = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsitemMargin.setMargins(20, 20, 20, 20);

        insurance_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


        insurance_text.setLayoutParams(paramsitemMargin);
        main_layout.addView(insurance_text);


        RelativeLayout insurance_image_layout= new RelativeLayout(
                getApplicationContext());

        LinearLayout.LayoutParams paramsitemMargin1 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                deviceHeight/4);
        paramsitemMargin1.setMargins(20, 0, 20, 20);
        insurance_image_layout.setLayoutParams(paramsitemMargin1);
        main_layout.addView(insurance_image_layout);


        ImageView insurance_image = new ImageView(
                getApplicationContext());
        insurance_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dummy_image));



        LinearLayout.LayoutParams paramsitemMargin2 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        //paramsitemMargin1.setMargins(20,0,20,20);




        insurance_image.setScaleType(ImageView.ScaleType.FIT_XY);
        insurance_image.setLayoutParams(paramsitemMargin2);
        insurance_image_layout.addView(insurance_image);



        View alpha_screen = new View(getApplicationContext());

        alpha_screen.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
        alpha_screen.setAlpha(.5f);

        alpha_screen.setLayoutParams(paramsitemMargin2);
        insurance_image_layout.addView(alpha_screen);*/



    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {




            case R.id.backbutton:

                finish();

                break;







        }



        for (int a = 0; a < itemIdArray.size(); a++) {
            if (v.getId() == (100 + a)) {


                Intent i = new Intent(ItemsDetail.this, ItemDetail.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("orderId",itemIdArray.get(a));
                startActivity(i);


            }

        }



        if (v.getId() == 0+10000) {


            if(!invoice_image1.equalsIgnoreCase(""))
            {

                Intent i13 = new Intent(ItemsDetail.this, SingleTouchImageViewActivity.class);
                i13.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i13.putExtra("imageUrl", invoice_image1);
                startActivity(i13);

            }


        }





    }





    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }






    String invoice_image1="";




    String res1;
    StringRequest strReq1;
    String status_code = "0";

    public String makeGetBidDetailsReq() {

        String url = getResources().getString(R.string.base_url)+"user/get-bid-detail";
        Log.d("regg", "2");
        strReq1 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res1 = response.toString();
                        checkGetBidDetailsResponse(res1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res1 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                    params.put("shipment_id",orderId1);
                    params.put("_id",pref.getString("userId","Something Wrong"));
                    params.put("security_token", pref.getString("security_token","Something Wrong"));


                    Log.e("register", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq1, "8");



        return null;
    }

    public void checkGetBidDetailsResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    String response1 = reader.getString("response");

                    JSONArray jsonArr1 = new JSONArray(response1);
                    JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(0);

                    String shipment_id1 = jsonObjectPackageData1.getString("shipment_id");
                    String load_type1 = jsonObjectPackageData1.getString("load_type");
                    String from_address1 = jsonObjectPackageData1.getString("from_address");
                    String to_address1 = jsonObjectPackageData1.getString("to_address");
                    String priority_delivery1 = jsonObjectPackageData1.getString("priority_delivery");
                    String transit_date1 = jsonObjectPackageData1.getString("transit_date");
                    String transit_time1 = jsonObjectPackageData1.getString("transit_time");
                    String truck_name1 = jsonObjectPackageData1.getString("truck_name");
                    String truck_quantity1 = jsonObjectPackageData1.getString("truck_quantity");
                    String overall_weight1 = jsonObjectPackageData1.getString("overall_weight");
                    String items_arr1 = jsonObjectPackageData1.getString("items_arr");
                    String insurance_option1 = jsonObjectPackageData1.getString("insurance_option");
                    String insurance_image1 = jsonObjectPackageData1.getString("insurance_image");
                    String truck_category1 = jsonObjectPackageData1.getString("truck_category");

                    Log.d("main",shipment_id1);
                    //////////////////////////////////////
                    LinearLayout ll = new LinearLayout(
                            ItemsDetail.this);
                    ll.setOrientation(LinearLayout.VERTICAL);



                    LinearLayout.LayoutParams params0 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

                    ll.setLayoutParams(params0);

                    main_layout.addView(ll);


                    LinearLayout ll1 = new LinearLayout(
                            ItemsDetail.this);
                    ll1.setOrientation(LinearLayout.HORIZONTAL);


                    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
                    params1.setMargins(20, 20, 20, 20);

                    ll1.setLayoutParams(params1);

                    ll.addView(ll1);




                    MyTextViewBold origin_text = new MyTextViewBold(
                            getApplicationContext());
                    origin_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                    origin_text.setGravity(Gravity.CENTER);

                    LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                            0,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    params2.gravity = Gravity.CENTER;
                    params2.weight = 0.37f;
                    // trucker_name.setPadding(5, 4, 5, 2);
                    origin_text.setLayoutParams(params2);


                    origin_text.setText(from_address1);

                    ll1.addView(origin_text);




                    View line = new View(
                            getApplicationContext());


                    LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                            0,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    //  params2.gravity = Gravity.CENTER;
                    params5.weight = 0.05f;
                    line.setLayoutParams(params5);


                    ll1.addView(line);


                    ImageView truck_image = new ImageView(
                            getApplicationContext());



                    //   truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));


                    if(truck_category1.equalsIgnoreCase("1"))
                    {
                        truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.small_grey_truck));
                    }
                    else if(truck_category1.equalsIgnoreCase("2"))
                    {
                        truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
                    }
                    else if(truck_category1.equalsIgnoreCase("3"))
                    {
                        truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.large_grey_truck));
                    }
                    else
                    {
                        Log.d("error","unreachable code");
                    }



                    truck_image.setAdjustViewBounds(true);

                    // truck_image.setPadding(10,0,10,0);
                    LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                            0,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    params4.gravity = Gravity.CENTER;
                    params4.weight = 0.16f;
                    //params10.gravity= Gravity.CENTER;

                    //params4.setMargins(0, 10, 0, 10);
                    truck_image.setLayoutParams(params4);

                    ll1.addView(truck_image);



                    View line1 = new View(
                            getApplicationContext());


                    LinearLayout.LayoutParams params55 = new LinearLayout.LayoutParams(
                            0,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    //  params2.gravity = Gravity.CENTER;
                    params55.weight = 0.05f;
                    line1.setLayoutParams(params55);


                    ll1.addView(line1);






                    MyTextViewBold destination_text = new MyTextViewBold(
                            getApplicationContext());
                    destination_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                    destination_text.setGravity(Gravity.CENTER);

                    LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                            0,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    params3.gravity = Gravity.CENTER;
                    params3.weight = 0.37f;
                    destination_text.setLayoutParams(params3);


                    destination_text.setText(to_address1);

                    ll1.addView(destination_text);





                    LinearLayout ll2 = new LinearLayout(
                            ItemsDetail.this);
                    ll2.setOrientation(LinearLayout.HORIZONTAL);


                    LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

                    params6.setMargins(20, 0, 20, 20);
                    ll2.setLayoutParams(params6);

                    ll.addView(ll2);




                    LinearLayout ll3 = new LinearLayout(
                            ItemsDetail.this);
                    ll3.setOrientation(LinearLayout.VERTICAL);


                    LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                            0,
                            ViewGroup.LayoutParams.WRAP_CONTENT);


                    params7.weight = 0.498f;

                    ll3.setLayoutParams(params7);

                    ll2.addView(ll3);


                    MyTextViewSemi load_type = new MyTextViewSemi(
                            getApplicationContext());
                    load_type.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                    LinearLayout.LayoutParams params8 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);


                    load_type.setLayoutParams(params8);

                    if(priority_delivery1.equalsIgnoreCase("Yes"))
                    {
                        load_type.setText(load_type1+" - "+getResources().getString(R.string.priority));
                    }
                    else
                    {
                        load_type.setText(load_type1);
                    }

                    // load_type.setText("Full Load - Priority");
                    // bid_info.setPadding(5, 0, 5, 0);
                    ll3.addView(load_type);


                    MyTextViewSemi no_of_trucks_text = new MyTextViewSemi(
                            getApplicationContext());
                    load_type.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));




                    load_type.setLayoutParams(params8);
                    //   String text2 = "<font color=#585858  >No of trucks : </font><font color=#ff5400  ><b>10</b></font>";
                    String text2 = "<font color=#585858  >"+getResources().getString(R.string.no_of_trucks)+"</font><font color=#ff5400  ><b>"+truck_quantity1+"</b></font>";
                    no_of_trucks_text.setText(Html.fromHtml(text2));

                    // no_of_trucks_text.setText("Full Load - Priority");
                    // bid_info.setPadding(5, 0, 5, 0);
                    ll3.addView(no_of_trucks_text);



                    View line2 = new View(
                            getApplicationContext());

                    line2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                    LinearLayout.LayoutParams params555 = new LinearLayout.LayoutParams(
                            0,
                            ViewGroup.LayoutParams.MATCH_PARENT);

                    //  params2.gravity = Gravity.CENTER;
                    params555.weight = 0.004f;
                    line2.setLayoutParams(params555);


                    ll2.addView(line2);








                    LinearLayout ll4 = new LinearLayout(
                            ItemsDetail.this);
                    ll4.setOrientation(LinearLayout.VERTICAL);


                    LinearLayout.LayoutParams params9 = new LinearLayout.LayoutParams(
                            0,
                            ViewGroup.LayoutParams.WRAP_CONTENT);


                    params9.weight = 0.498f;

                    ll4.setLayoutParams(params9);

                    ll2.addView(ll4);


                    MyTextViewSemi date_text = new MyTextViewSemi(
                            getApplicationContext());
                    date_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                    LinearLayout.LayoutParams params10 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

                    params10.gravity = Gravity.RIGHT;
                    date_text.setLayoutParams(params10);


                    date_text.setText(transit_date1);
                    // bid_info.setPadding(5, 0, 5, 0);
                    ll4.addView(date_text);


                    MyTextViewSemi time_text = new MyTextViewSemi(
                            getApplicationContext());
                    time_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                    LinearLayout.LayoutParams params11 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

                    params11.gravity = Gravity.RIGHT;
                    time_text.setLayoutParams(params11);

                    String text3 = "<font color=#585858  >"+getResources().getString(R.string.at)+" </font><font color=#ff5400  ><b>"+transit_time1+"</b></font>";
                    time_text.setText(Html.fromHtml(text3));
                    //time_text.setText("25 March 2016");
                    // bid_info.setPadding(5, 0, 5, 0);
                    ll4.addView(time_text);




                    MyTextView truck_name = new MyTextView(
                            getApplicationContext());
                    truck_name.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                    LinearLayout.LayoutParams params13 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

                    params13.setMargins(20, 0, 20, 20);
                    truck_name.setLayoutParams(params13);


                    truck_name.setText(truck_name1);
                    // bid_info.setPadding(5, 0, 5, 0);
                    ll.addView(truck_name);



                    MyTextView total_weight = new MyTextView(
                            getApplicationContext());

                    LinearLayout.LayoutParams params15 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

                    params15.setMargins(20, 0, 20, 0);
                    total_weight.setLayoutParams(params15);

                    String text4 = "<font color=#ff5400  >"+getResources().getString(R.string.total_item_weight)+"</font><font color=#585858  ><b>"+overall_weight1+" "+getResources().getString(R.string.kg)+"</b></font>";
                    total_weight.setText(Html.fromHtml(text4));
                    // total_weight.setText("TATA PRIMA (5 TON MT)");
                    // bid_info.setPadding(5, 0, 5, 0);
                    ll.addView(total_weight);






                    JSONArray jsonArr2 = new JSONArray(items_arr1);


                    for (int i = 0; i < jsonArr2.length(); i++) {



                        JSONObject jsonObjectPackageData2 = jsonArr2.getJSONObject(i);


                        String name1 = jsonObjectPackageData2.getString("name");
                        String parent_category_name1 = jsonObjectPackageData2.getString("parent_category_name");
                        String sub_category_name1 = jsonObjectPackageData2.getString("sub_category_name");



                        LinearLayout items_layout = new LinearLayout(
                                ItemsDetail.this);
                        items_layout.setOrientation(LinearLayout.VERTICAL);
                        items_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));
                        items_layout.setPadding(20, 20, 20, 20);

                        items_layout.setOnClickListener(this);
                        items_layout.setId(i + 100);

                        LinearLayout.LayoutParams params14 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
                        params14.setMargins(20,20,20,0);
                        items_layout.setLayoutParams(params14);

                        main_layout.addView(items_layout);

                        itemIdArray.add(jsonObjectPackageData2.toString());

       /*     MyTextView item_name = new MyTextView(
                    getApplicationContext());

            LinearLayout.LayoutParams params15 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

            item_name.setLayoutParams(params15);

            String text5 = "<font color=#585858  >Item 1 : </font><font color=#ff5400  ><b>Plates</b></font>";
            total_weight.setText(Html.fromHtml(text5));

            items_layout.addView(item_name);*/


                        LinearLayout horizontalLayout = new LinearLayout(
                                getApplicationContext());


                        LinearLayout.LayoutParams paramshorizontalLayout = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
          /*  paramshorizontalLayout.setMargins(10, 10, 0,
                    0);*/


                        horizontalLayout
                                .setLayoutParams(paramshorizontalLayout);

                        horizontalLayout
                                .setOrientation(LinearLayout.HORIZONTAL);
                        items_layout.addView(horizontalLayout);


                        MyTextView item_no = new MyTextView(
                                getApplicationContext());
                        item_no.setText(getResources().getString(R.string.item_space) + (i + 1) + " : ");

                        item_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));
                        LinearLayout.LayoutParams paramsitemMargin = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);


                        item_no
                                .setLayoutParams(paramsitemMargin);
                        horizontalLayout.addView(item_no);


                        MyTextView item_name2 = new MyTextView(
                                getApplicationContext());
                        item_name2.setText(name1);

                        item_name2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                        item_name2
                                .setLayoutParams(paramsitemMargin);
                        horizontalLayout.addView(item_name2);


                        LinearLayout horizontalLayout2 = new LinearLayout(
                                getApplicationContext());


                        horizontalLayout2
                                .setLayoutParams(paramshorizontalLayout);

                        horizontalLayout2
                                .setOrientation(LinearLayout.HORIZONTAL);
                        items_layout.addView(horizontalLayout2);


                        MyTextView item_cat = new MyTextView(
                                getApplicationContext());
                        item_cat.setText(getResources().getString(R.string.category)+"  : ");

                        item_cat.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));


                        item_cat
                                .setLayoutParams(paramsitemMargin);
                        horizontalLayout2.addView(item_cat);


                        MyTextView item_cat2 = new MyTextView(
                                getApplicationContext());
                        item_cat2.setText(parent_category_name1);

                        item_cat2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                        item_cat2
                                .setLayoutParams(paramsitemMargin);
                        horizontalLayout2.addView(item_cat2);










                        LinearLayout horizontalLayout3 = new LinearLayout(
                                getApplicationContext());


                        horizontalLayout3
                                .setLayoutParams(paramshorizontalLayout);

                        horizontalLayout3
                                .setOrientation(LinearLayout.HORIZONTAL);
                        items_layout.addView(horizontalLayout3);


                        MyTextView item_sub_cat = new MyTextView(
                                getApplicationContext());
                        item_sub_cat.setText(getResources().getString(R.string.sub_category)+"  : ");

                        item_sub_cat.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));


                        item_sub_cat
                                .setLayoutParams(paramsitemMargin);
                        horizontalLayout3.addView(item_sub_cat);


                        MyTextView item_sub_cat2 = new MyTextView(
                                getApplicationContext());
                        item_sub_cat2.setText(sub_category_name1);

                        item_sub_cat2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                        item_sub_cat2
                                .setLayoutParams(paramsitemMargin);
                        horizontalLayout3.addView(item_sub_cat2);



                    }



                    MyTextView insurance_text = new MyTextView(
                            getApplicationContext());

                    if(insurance_option1.equalsIgnoreCase("1"))
                    {
                        insurance_text.setText(getResources().getString(R.string.insurance_yet_to_buy));
                    }
                    else if(insurance_option1.equalsIgnoreCase("2"))
                    {
                        insurance_text.setText(getResources().getString(R.string.insurance_yes));
                    }
                    else if(insurance_option1.equalsIgnoreCase("3"))
                    {
                        insurance_text.setText(getResources().getString(R.string.no));
                    }
                    else
                    {
                        Log.d("error","unreachable code");
                    }


                    LinearLayout.LayoutParams paramsitemMargin = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    paramsitemMargin.setMargins(20, 20, 20, 20);

                    insurance_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                    insurance_text.setLayoutParams(paramsitemMargin);
                    main_layout.addView(insurance_text);


                    if(!insurance_image1.equalsIgnoreCase(""))

                    {




                        MyTextViewSemi insurance_text1 = new MyTextViewSemi(
                                getApplicationContext());


                        insurance_text1.setText(getResources().getString(R.string.insurance_copy));


                        LinearLayout.LayoutParams paramsitemMargin5 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                        paramsitemMargin5.setMargins(20, 0, 20, 20);

                        insurance_text1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                        insurance_text1.setLayoutParams(paramsitemMargin5);
                        main_layout.addView(insurance_text1);


                        RelativeLayout insurance_image_layout = new RelativeLayout(
                                getApplicationContext());
                        insurance_image_layout.setId(0+10000);
                        LinearLayout.LayoutParams paramsitemMargin1 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                deviceHeight / 4);
                        paramsitemMargin1.setMargins(20, 0, 20, 20);
                        insurance_image_layout.setLayoutParams(paramsitemMargin1);
                        main_layout.addView(insurance_image_layout);


                        ImageView insurance_image = new ImageView(
                                getApplicationContext());
                        // insurance_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dummy_image));
                        invoice_image1 = insurance_image1;

                        Picasso.with(getApplicationContext())
                                .load(insurance_image1)
                                .placeholder(R.drawable.loading_image)
                                .error(R.drawable.unable_image_upload).into(insurance_image);


                        LinearLayout.LayoutParams paramsitemMargin2 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT);
                        //paramsitemMargin1.setMargins(20,0,20,20);


                        insurance_image.setScaleType(ImageView.ScaleType.FIT_XY);
                        insurance_image.setLayoutParams(paramsitemMargin2);
                        insurance_image_layout.addView(insurance_image);


                        View alpha_screen = new View(getApplicationContext());

                        alpha_screen.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                        alpha_screen.setAlpha(.5f);

                        alpha_screen.setLayoutParams(paramsitemMargin2);
                        insurance_image_layout.addView(alpha_screen);


                    }





                    /////////////////////////////////////








                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
            Log.d("error ",e.getMessage()+" "+e.getLocalizedMessage());

        }
    }


    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(ItemsDetail.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }











}
