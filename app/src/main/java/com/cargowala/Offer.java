package com.cargowala;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by Akash on 2/17/2016.
 */
public class Offer extends FragmentActivity implements View.OnClickListener{

    TestPreviewFragmentAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
    static String previewImages[];

    int deviceWidth, deviceHeight;
    RelativeLayout actionBar;
    ImageView backbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offer);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;



        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();



        previewImages = new String[4];


        for (int l = 0; l < 4; l++) {


           // previewImages[l] = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpa1/v/t1.0-1/p200x200/10306457_10200969592841159_2957337293139756078_n.jpg?oh=1ad488fa685df4a53399c95e43f8850e&oe=57963EC0&__gda__=1468476753_21f3c8262a571f6ad727f5f2f9a394f3";
            previewImages[l] =  "https://www.cargowala.com/cargo/dev/webappshipper/images/offer.png";
        }




        mAdapter = new TestPreviewFragmentAdapter(getSupportFragmentManager());

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
        mIndicator.setCurrentItem(0);






    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:
                finish();


                break;






        }


    }














}
