package com.cargowala;

public interface LocationListener {
    public void onLocationUpdate();
}
