package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cargowala.helper.CommonListners;
import com.cargowala.helper.CommonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class MyPayments extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton;


    ImageView complete_image, pending_image;
    LinearLayout complete_bar, pending_bar, complete_bar_main, pending_bar_main;


    private List<String> orderIdArrayPending = new ArrayList<String>();
    ImageView completed_blank_image, pending_blank_image;
    ImageView no_connection_image1, no_connection_image2;
    ExampleScrollView complete_scroll_bar, pending_scroll_bar;
    int record_no1 = 0;
    boolean scroll1 = true;
    boolean headernotcreated1 = true;
    MyTextView loadingText;
    boolean doPagination1 = true;

    SweetAlertDialog pDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;

    int record_no2 = 0;
    boolean scroll2 = true;
    boolean headernotcreated2 = true;
    boolean doPagination2 = true;
    boolean createPending = true, createCompleted = true;
    private List<String> orderIdArray = new ArrayList<String>();
    private List<String> shipmentIdArray = new ArrayList<String>();
    private List<String> shipmentIdHistoryArray = new ArrayList<String>();
    private List<String> orderIdHistoryArray = new ArrayList<String>();
    int pageLimit=10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_payments);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;
        no_connection_image1 = (ImageView) findViewById(R.id.no_connection_image1);
        no_connection_image2 = (ImageView) findViewById(R.id.no_connection_image2);
        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);


        backbutton.requestLayout();


        complete_image = (ImageView) findViewById(R.id.complete_image);
        complete_image.setOnClickListener(this);
        pending_image = (ImageView) findViewById(R.id.pending_image);
        pending_image.setOnClickListener(this);

        complete_bar = (LinearLayout) findViewById(R.id.complete_bar);
        pending_bar = (LinearLayout) findViewById(R.id.pending_bar);
        complete_bar_main = (LinearLayout) findViewById(R.id.complete_bar_main);
        pending_bar_main = (LinearLayout) findViewById(R.id.pending_bar_main);
        complete_scroll_bar = (ExampleScrollView) findViewById(R.id.completed_scroll_bar);
        completed_blank_image = (ImageView) findViewById(R.id.completed_blank_image);
        pending_scroll_bar = (ExampleScrollView) findViewById(R.id.pending_scroll_bar);
        pending_blank_image = (ImageView) findViewById(R.id.pending_blank_image);


       /* int a = 7;

        if (a == 0) {



            completed_blank_image.setVisibility(View.VISIBLE);
            complete_scroll_bar.setVisibility(View.GONE);

        } else {

            completed_blank_image.setVisibility(View.GONE);
            complete_scroll_bar.setVisibility(View.VISIBLE);
            complete_bar_main.removeAllViews();

            for (int i = 0; i < a; i++) {

                CardView card_view = new CardView(
                        getApplicationContext());
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT

                );
                //  card_view.setRadius(10f);
                //left top right bottom
                params1.setMargins(20, 20, 20, 20);
                card_view.setLayoutParams(params1);
                orderIdArray.add("abc " + i);


                LinearLayout ll1 = new LinearLayout(
                        MyPayments.this);
                ll1.setOrientation(LinearLayout.VERTICAL);

                ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                ll1.setId(i + 100);
                ll1.setOnClickListener(MyPayments.this);
                ll1.setLayoutParams(params2);

                card_view.addView(ll1);


                MyTextView order_id = new MyTextView(
                        getApplicationContext());
                order_id.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                order_id.setGravity(Gravity.CENTER);


                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);


                order_id.setPadding(5, 4, 5, 2);
                order_id.setLayoutParams(params4);

                String text1 = "<font color=#585858  >" + getResources().getString(R.string.order_id) + "</font> <font color=#ff5400><b>ISUDNV75TN43</b></font>";
                order_id.setText(Html.fromHtml(text1));


                //  order_id.setText("Order id : #4H7RJ5");

                ll1.addView(order_id);


                MyTextView route_text = new MyTextView(
                        getApplicationContext());
                route_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                route_text.setGravity(Gravity.CENTER);


                route_text.setLayoutParams(params4);
                route_text.setText(getResources().getString(R.string.chandigarh_to_delhi));
                route_text.setPadding(5, 2, 5, 2);
                ll1.addView(route_text);


                MyTextView schedule_text = new MyTextView(
                        getApplicationContext());
                schedule_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                schedule_text.setGravity(Gravity.CENTER);


                schedule_text.setLayoutParams(params4);
                schedule_text.setText("24 March 2016 at 10:00 AM");
                schedule_text.setPadding(5, 2, 5, 4);
                ll1.addView(schedule_text);


                MyTextView invoice_text = new MyTextView(
                        getApplicationContext());
                invoice_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                invoice_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                invoice_text.setGravity(Gravity.CENTER);


                String text = "<font color=#ffffff  >" + getResources().getString(R.string.invoice_number) + "</font> <font color=#ffffff><b><u>345FHE74574</u></b></font>";
                invoice_text.setText(Html.fromHtml(text));

                invoice_text.setLayoutParams(params4);
                // invoice_text.setText("Invoice Number : 345FHE74574");
                invoice_text.setPadding(5, 10, 5, 10);
                ll1.addView(invoice_text);


                complete_bar_main.addView(card_view);


            }

        }


        int a2 = 4;

        if (a2 == 0) {


            pending_blank_image.setVisibility(View.VISIBLE);
            pending_scroll_bar.setVisibility(View.GONE);

        } else {

            pending_blank_image.setVisibility(View.GONE);
            pending_scroll_bar.setVisibility(View.VISIBLE);
            pending_bar_main.removeAllViews();

            for (int i = 0; i < 3; i++) {

                CardView card_view = new CardView(
                        getApplicationContext());
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT

                );
                //  card_view.setRadius(10f);
                //left top right bottom
                params1.setMargins(20, 20, 20, 20);


                card_view.setLayoutParams(params1);


                orderIdArrayPending.add("abcd " + i);


                LinearLayout ll1 = new LinearLayout(
                        MyPayments.this);
                ll1.setOrientation(LinearLayout.VERTICAL);

                ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                ll1.setId(i + 10000);
                ll1.setOnClickListener(MyPayments.this);
                ll1.setLayoutParams(params2);

                card_view.addView(ll1);


                MyTextView order_id = new MyTextView(
                        getApplicationContext());
                order_id.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                order_id.setGravity(Gravity.CENTER);


                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);


                order_id.setPadding(5, 4, 5, 2);
                order_id.setLayoutParams(params4);

                String text1 = "<font color=#585858  >" + getResources().getString(R.string.order_id) + "</font> <font color=#ff5400><b>ISUDNV75TN43</b></font>";
                order_id.setText(Html.fromHtml(text1));

                // order_id.setText("Order id : #4H7RJ5");

                ll1.addView(order_id);


                MyTextView route_text = new MyTextView(
                        getApplicationContext());
                route_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));
                route_text.setGravity(Gravity.CENTER);
                route_text.setLayoutParams(params4);
                route_text.setText(getResources().getString(R.string.chandigarh_to_delhi));
                route_text.setPadding(5, 2, 5, 2);
                ll1.addView(route_text);


                MyTextView schedule_text = new MyTextView(
                        getApplicationContext());
                schedule_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                schedule_text.setGravity(Gravity.CENTER);


                schedule_text.setLayoutParams(params4);
                schedule_text.setText("24 March 2016 at 10:00 AM");
                schedule_text.setPadding(5, 2, 5, 4);
                ll1.addView(schedule_text);


                MyTextView invoice_text = new MyTextView(
                        getApplicationContext());
                invoice_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                invoice_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                invoice_text.setGravity(Gravity.CENTER);


                invoice_text.setLayoutParams(params4);
                //

                String text = "<font color=#ffffff  >" + getResources().getString(R.string.pending_payment) + "</font> <font color=#ffffff><b>₹ 5000</b></font>";
                invoice_text.setText(Html.fromHtml(text));

                //    invoice_text.setText("Pending Payment : 5000");
                invoice_text.setPadding(5, 10, 5, 10);
                ll1.addView(invoice_text);


                pending_bar_main.addView(card_view);


            }


        }*/

//---------------

        complete_scroll_bar.setOnScrollViewListener(new ExampleScrollView.OnScrollViewListener() {
            public void onScrollChanged(ExampleScrollView v, int l, int t,
                                        int oldl, int oldt) {

                View view = (View) v.getChildAt(v.getChildCount() - 1);
                int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));

                if (diff == 0) {

                    if (scroll1) {

                        if (doPagination1) {
                            scroll1 = false;
                            if (CommonUtils.isOnline(MyPayments.this)) {
                                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                                loadingText = new MyTextView(
                                        MyPayments.this);

                                params4.setMargins(0, 10, 0, 20);

                                loadingText.setLayoutParams(params4);
                                loadingText.setText(getResources().getString(R.string.loading_dots));
                                loadingText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                                loadingText.setTypeface(null, Typeface.BOLD);

                                loadingText.setGravity(Gravity.CENTER);
                                complete_bar_main.addView(loadingText);

                                complete_scroll_bar.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        complete_scroll_bar.fullScroll(ExampleScrollView.FOCUS_DOWN);
                                    }
                                });


                                makeCompletedtReq("1");
                            } else {
                                Toast.makeText(MyPayments.this,
                                        getResources().getString(R.string.check_your_network),
                                        Toast.LENGTH_SHORT).show();

                                complete_scroll_bar.scrollBy(0, -20);
                                scroll1 = true;
                            }

                        }
                    }

                }
            }
        });



        pending_scroll_bar.setOnScrollViewListener(new ExampleScrollView.OnScrollViewListener() {
            public void onScrollChanged(ExampleScrollView v, int l, int t,
                                        int oldl, int oldt) {

                View view = (View) v.getChildAt(v.getChildCount() - 1);
                int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));

                if (diff == 0) {

                    if (scroll2) {

                        if (doPagination2) {

                            scroll2 = false;


                            if (CommonUtils.isOnline(MyPayments.this)) {

                                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                                loadingText = new MyTextView(
                                        MyPayments.this);

                                params4.setMargins(0, 10, 0, 20);

                                loadingText.setLayoutParams(params4);
                                loadingText.setText(getResources().getString(R.string.loading_dots));
                                loadingText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                                loadingText.setTypeface(null, Typeface.BOLD);

                                loadingText.setGravity(Gravity.CENTER);
                                pending_bar_main.addView(loadingText);

                                pending_scroll_bar.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        pending_scroll_bar.fullScroll(ExampleScrollView.FOCUS_DOWN);
                                    }
                                });


                                makePendingReq("0");
                            } else {
                                Toast.makeText(MyPayments.this,
                                        getResources().getString(R.string.check_your_network),
                                        Toast.LENGTH_SHORT).show();

                                pending_scroll_bar.scrollBy(0, -20);
                                scroll2 = true;
                            }

                        }
                    }

                }
            }
        });
//----------------------------------------------------------
        no_connection_image1.setVisibility(View.GONE);
        completed_blank_image.setVisibility(View.GONE);
        complete_scroll_bar.setVisibility(View.GONE);
        orderIdArray.clear();
        shipmentIdArray.clear();
        record_no1 = 0;
        scroll1 = true;
        headernotcreated1 = true;
        doPagination1 = true;
        complete_bar_main.removeAllViews();

        //////////
        no_connection_image2.setVisibility(View.GONE);
        pending_blank_image.setVisibility(View.GONE);
        complete_scroll_bar.setVisibility(View.GONE);

        orderIdHistoryArray.clear();
        shipmentIdHistoryArray.clear();
        record_no2 = 0;
        scroll2 = true;
        headernotcreated2 = true;
        doPagination2 = true;
        pending_bar_main.removeAllViews();
        createPending = true;

        ///////////


        if (CommonUtils.isOnline(MyPayments.this)) {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            pDialog.setTitleText(getResources().getString(R.string.loading));
            pDialog.setCancelable(false);
            pDialog.show();
            makeCompletedtReq("1");
        } else {
            //    no_connection_image.setVisibility(View.VISIBLE);
            no_connection_image1.setVisibility(View.VISIBLE);
            Toast.makeText(MyPayments.this,
                    getResources().getString(R.string.check_your_network),
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:

                finish();

                break;


            case R.id.complete_image:

                complete_bar.setVisibility(View.VISIBLE);
                pending_bar.setVisibility(View.GONE);
                complete_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.payment_completed_active));
                pending_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.payment_pending_inactive));


                if(createCompleted){
                    no_connection_image1.setVisibility(View.GONE);
                    completed_blank_image.setVisibility(View.GONE);
                    complete_scroll_bar.setVisibility(View.GONE);
                    orderIdArray.clear();
                     shipmentIdArray.clear();

                    record_no1 = 0;
                    scroll1 = true;
                    headernotcreated1 = true;
                    doPagination1 = true;
                    complete_bar_main.removeAllViews();

                    if (CommonUtils.isOnline(MyPayments.this)) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makeCompletedtReq("1");
                    } else {
                        //    no_connection_image.setVisibility(View.VISIBLE);
                        no_connection_image1.setVisibility(View.VISIBLE);
                        Toast.makeText(MyPayments.this,
                                getResources().getString(R.string.check_your_network),
                                Toast.LENGTH_SHORT).show();
                    }
                }

                break;


            case R.id.pending_image:

                complete_bar.setVisibility(View.GONE);
                pending_bar.setVisibility(View.VISIBLE);
                complete_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.payment_completed_inactive));
                pending_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.payment_pending_active));


                if (createPending) {
                    no_connection_image2.setVisibility(View.GONE);
                    pending_blank_image.setVisibility(View.GONE);
                    pending_scroll_bar.setVisibility(View.GONE);

                    orderIdHistoryArray.clear();
                    shipmentIdHistoryArray.clear();
                    record_no2 = 0;
                    scroll2 = true;
                    headernotcreated2 = true;
                    doPagination2 = true;
                    pending_bar_main.removeAllViews();


                    if (CommonUtils.isOnline(MyPayments.this)) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makePendingReq("0");
                    } else {
                        //    no_connection_image.setVisibility(View.VISIBLE);
                        no_connection_image2.setVisibility(View.VISIBLE);

                        Toast.makeText(MyPayments.this,
                                getResources().getString(R.string.check_your_network),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {

                }




                break;


        }


        for (int a = 0; a < orderIdArray.size(); a++) {
            if (v.getId() == (orderIdArray.size() + a)) {


                Intent i = new Intent(MyPayments.this, CompletedPayments.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("orderId", orderIdArray.get(a));
                i.putExtra("shipmentId", shipmentIdArray.get(a));
                startActivity(i);


            }

        }


        for (int a = 0; a < orderIdArrayPending.size(); a++) {
            if (v.getId() == (10000 + (a+1))) {

                Intent i = new Intent(MyPayments.this, PendingPayments.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("orderId", orderIdArrayPending.get(a));
                i.putExtra("shipmentId", shipmentIdHistoryArray.get(a));
                startActivity(i);


            }

        }


    }

    String res;
    StringRequest strReq;

    public String makeCompletedtReq(final String payment_type) {
        String url = getResources().getString(R.string.base_url) + "user/get-payments";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");
                        Log.d("register", response.toString());
                        res = response.toString();
                        checkCompletedResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));
                    params.put("_id", pref.getString("userId", "Something Wrong"));
                    params.put("next_records", "" + record_no1);
                    params.put("limit", "" + (record_no1 + pageLimit));
                    params.put("payment_type", payment_type);
                  //  1 / 0 (Comment 1 for Completed, 0 for Pending
                    Log.e("MyPayment_params", params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };

        AppController.getInstance().addToRequestQueue(strReq, "35");


        return null;
    }


   /* @Override
    protected void onResume() {
        super.onResume();
        no_connection_image1.setVisibility(View.GONE);
        completed_blank_image.setVisibility(View.GONE);
        complete_scroll_bar.setVisibility(View.GONE);
        orderIdArray.clear();
        shipmentIdArray.clear();
        record_no1 = 0;
        scroll1 = true;
        headernotcreated1 = true;
        doPagination1 = true;
        complete_bar_main.removeAllViews();

        //////////
        no_connection_image2.setVisibility(View.GONE);
        pending_blank_image.setVisibility(View.GONE);
        complete_scroll_bar.setVisibility(View.GONE);

        orderIdHistoryArray.clear();
        shipmentIdHistoryArray.clear();
        record_no2 = 0;
        scroll2 = true;
        headernotcreated2 = true;
        doPagination2 = true;
        pending_bar_main.removeAllViews();
        createPending = true;

        ///////////


        if (CommonUtils.isOnline(MyPayments.this)) {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            pDialog.setTitleText(getResources().getString(R.string.loading));
            pDialog.setCancelable(false);
            pDialog.show();
            makeCompletedtReq("1");
        } else {
            //    no_connection_image.setVisibility(View.VISIBLE);
            no_connection_image1.setVisibility(View.VISIBLE);
            Toast.makeText(MyPayments.this,
                    getResources().getString(R.string.check_your_network),
                    Toast.LENGTH_SHORT).show();
        }
    }*/

    String status_code = "0";
    public void checkCompletedResponse(String response) {
        if (headernotcreated1) {
            pDialog.dismiss();
        }
        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);

                createCompleted = false;
                if (status_code.equalsIgnoreCase("200")) {
                    String response1 = reader.getString("response");
                    JSONArray jsonArr1 = new JSONArray(response1);
                    if (headernotcreated1) {
                        headernotcreated1 = false;
                        if (jsonArr1.length() == 0) {
                            completed_blank_image.setVisibility(View.VISIBLE);

                            //     ll_layout.setVisibility(View.GONE);

                        } else {

                            //   blank_no_driver_image.setVisibility(View.GONE);
                            complete_scroll_bar.setVisibility(View.VISIBLE);
                            complete_bar_main.removeAllViews();
                        }
                    } else {
                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll1 = true;
                    }

                    record_no1 = record_no1 + pageLimit;

                    complete_bar_main.removeAllViews();
                    for (int i = 0; i < jsonArr1.length(); i++) {

                        if (jsonArr1.length() % pageLimit == 0) {
                            doPagination1 = true;
                        } else {
                            doPagination1 = false;
                        }

                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);
                        String id = jsonObjectPackageData1.getString("id");
                        String shipment_id = jsonObjectPackageData1.getString("shipment_id");
                        String from_city = jsonObjectPackageData1.getString("from_city");
                        String to_city = jsonObjectPackageData1.getString("to_city");
                        String transit_date = jsonObjectPackageData1.getString("transit_date");
                        String transit_time = jsonObjectPackageData1.getString("transit_time");

                        String order_number = jsonObjectPackageData1.getString("order_number");
                        String load_type = jsonObjectPackageData1.getString("load_type");
                        String from_state = jsonObjectPackageData1.getString("from_state");
                        String to_address = jsonObjectPackageData1.getString("to_address");
                        String priority_delivery = jsonObjectPackageData1.getString("priority_delivery");
                        String truck_name = jsonObjectPackageData1.getString("truck_name");
                        String truck_quantity = jsonObjectPackageData1.getString("truck_quantity");
                        String total_payment = jsonObjectPackageData1.getString("total_payment");
                        String payment_status = jsonObjectPackageData1.getString("payment_status");




                        completed_blank_image.setVisibility(View.GONE);
                        complete_scroll_bar.setVisibility(View.VISIBLE);

                        // orderIdBigArray.add(id);

                        CardView card_view = new CardView(
                                getApplicationContext());
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT

                        );
                        //  card_view.setRadius(10f);
                        //left top right bottom
                        params1.setMargins(20, 20, 20, 20);
                        card_view.setLayoutParams(params1);
                        orderIdArray.add(order_number);
                        shipmentIdArray.add(shipment_id);


                        LinearLayout ll1 = new LinearLayout(
                                MyPayments.this);
                        ll1.setOrientation(LinearLayout.VERTICAL);

                        ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

                        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                        ll1.setId(orderIdArray.size() + 100);
                        ll1.setOnClickListener(MyPayments.this);
                        ll1.setLayoutParams(params2);

                        card_view.addView(ll1);


                        MyTextView order_id = new MyTextView(
                                getApplicationContext());
                        order_id.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                        order_id.setGravity(Gravity.CENTER);


                        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);


                        order_id.setPadding(5, 4, 5, 2);
                        order_id.setLayoutParams(params4);

                        String text1 = "<font color=#585858  >" + getResources().getString(R.string.order_id) + "</font> <font color=#ff5400><b>"+order_number+"</b></font>";
                        order_id.setText(Html.fromHtml(text1));


                        //  order_id.setText("Order id : #4H7RJ5");

                        ll1.addView(order_id);


                        MyTextView route_text = new MyTextView(
                                getApplicationContext());
                        route_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                        route_text.setGravity(Gravity.CENTER);


                        route_text.setLayoutParams(params4);
                        route_text.setText(from_city + " " + getResources().getString(R.string.to) + " " + to_city);
                        route_text.setPadding(5, 2, 5, 2);
                        ll1.addView(route_text);


                        MyTextView schedule_text = new MyTextView(
                                getApplicationContext());
                        schedule_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                        schedule_text.setGravity(Gravity.CENTER);


                        schedule_text.setLayoutParams(params4);
                        schedule_text.setText(transit_date+" "+getResources().getString(R.string.at)+" "+transit_time);
                        schedule_text.setPadding(5, 2, 5, 4);
                        ll1.addView(schedule_text);

                        MyTextView invoice_text = new MyTextView(
                                getApplicationContext());
                        invoice_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        invoice_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        invoice_text.setGravity(Gravity.CENTER);

                        String text = "<font color=#ffffff  >" + getResources().getString(R.string.invoice_number) + "</font> <font color=#ffffff><b><u>"+order_number+"</u></b></font>";
                        invoice_text.setText(Html.fromHtml(text));

                        invoice_text.setLayoutParams(params4);
                        // invoice_text.setText("Invoice Number : 345FHE74574");
                        invoice_text.setPadding(5, 10, 5, 10);
                        ll1.addView(invoice_text);


                        complete_bar_main.addView(card_view);


                    }
                } else if (status_code.equalsIgnoreCase("204")) {

                    //no more records
                    doPagination1 = false;
                    if (record_no1 > 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_more_active_shipments), Toast.LENGTH_SHORT).show();

                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll1 = true;

                    } else {
                        no_connection_image1.setVisibility(View.GONE);
                        completed_blank_image.setVisibility(View.VISIBLE);
                        complete_scroll_bar.setVisibility(View.GONE);

                    }

                } else if (status_code.equalsIgnoreCase("406")) {
                    //password changed
                    CommonUtils.initiatePopupWindow(MyPayments.this, getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });

                } else if (status_code.equalsIgnoreCase("910")) {
                    //user_inactive
                    CommonUtils.initiatePopupWindow(MyPayments.this, getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else {
                    CommonUtils.initiatePopupWindow(MyPayments.this, getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + " " + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
            Log.d("error ", e.getLocalizedMessage() + " " + e.getMessage());

        }
    }




    ////////////////////  Pending shipment

    String res2;
    StringRequest strReq2;

    public String makePendingReq(final String payment_type) {

        String url = getResources().getString(R.string.base_url) + "user/get-payments";
        Log.d("regg", "2");
        strReq2 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");
                        Log.d("register", response.toString());
                        res2 = response.toString();
                        checkPendingResponse(res2);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res2 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("security_token", pref.getString("security_token", "Something Wrong"));
                    params.put("_id", pref.getString("userId", "Something Wrong"));
                    params.put("next_records", "" + record_no2);
                    params.put("limit", "" + (record_no1 + pageLimit));
                    params.put("payment_type", payment_type);
                    //  1 / 0 (Comment 1 for Completed, 0 for Pending
                    Log.e("MyPayment_params", params.toString());


                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };

        AppController.getInstance().addToRequestQueue(strReq2, "36");
        return null;
    }





    public void checkPendingResponse(String response) {
        if (headernotcreated2) {
            pDialog.dismiss();
        }

        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);

                createPending = false;

                if (status_code.equalsIgnoreCase("200")) {

                    String response1 = reader.getString("response");

                    JSONArray jsonArr1 = new JSONArray(response1);

                    if (headernotcreated2) {

                        headernotcreated2 = false;

                        if (jsonArr1.length() == 0) {

                            pending_blank_image.setVisibility(View.VISIBLE);
                            pending_scroll_bar.setVisibility(View.GONE);

                            //     ll_layout.setVisibility(View.GONE);

                        } else {


                            //   blank_no_driver_image.setVisibility(View.GONE);

                            pending_blank_image.setVisibility(View.GONE);
                            pending_scroll_bar.setVisibility(View.VISIBLE);
                            pending_bar_main.removeAllViews();
                        }

                    } else {
                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll2 = true;
                    }


                    record_no2 = record_no2 + pageLimit;


                    for (int i = 0; i < jsonArr1.length(); i++) {

                        if (jsonArr1.length() % pageLimit == 0) {
                            doPagination2 = true;
                        } else {

                            doPagination2 = false;
                        }


                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);


                        String id = jsonObjectPackageData1.getString("id");
                        String shipment_id = jsonObjectPackageData1.getString("shipment_id");
                        String from_city = jsonObjectPackageData1.getString("from_city");
                        String to_city = jsonObjectPackageData1.getString("to_city");
                        String transit_date = jsonObjectPackageData1.getString("transit_date");
                        String transit_time = jsonObjectPackageData1.getString("transit_time");

                        String order_number = jsonObjectPackageData1.getString("order_number");
                        String load_type = jsonObjectPackageData1.getString("load_type");
                        String from_state = jsonObjectPackageData1.getString("from_state");
                        String to_address = jsonObjectPackageData1.getString("to_address");
                        String priority_delivery = jsonObjectPackageData1.getString("priority_delivery");
                        String truck_name = jsonObjectPackageData1.getString("truck_name");
                        String truck_quantity = jsonObjectPackageData1.getString("truck_quantity");
                        String total_payment = jsonObjectPackageData1.getString("total_payment");
                        String payment_status = jsonObjectPackageData1.getString("payment_status");
                        String pending_payment = jsonObjectPackageData1.getString("pending_payment");


                        pending_blank_image.setVisibility(View.GONE);
                        pending_scroll_bar.setVisibility(View.VISIBLE);

                        // orderIdBigArray.add(id);

                        CardView card_view = new CardView(
                                getApplicationContext());
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT

                        );
                        //  card_view.setRadius(10f);
                        //left top right bottom
                        params1.setMargins(20, 20, 20, 20);


                        card_view.setLayoutParams(params1);


                        orderIdArrayPending.add(order_number);
                        shipmentIdHistoryArray.add(shipment_id);


                        LinearLayout ll1 = new LinearLayout(
                                MyPayments.this);
                        ll1.setOrientation(LinearLayout.VERTICAL);

                        ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

                        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                        ll1.setId(orderIdArrayPending.size() + 10000);
                        ll1.setOnClickListener(MyPayments.this);
                        ll1.setLayoutParams(params2);

                        card_view.addView(ll1);


                        MyTextView order_id = new MyTextView(
                                getApplicationContext());
                        order_id.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                        order_id.setGravity(Gravity.CENTER);


                        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);


                        order_id.setPadding(5, 4, 5, 2);
                        order_id.setLayoutParams(params4);

                        String text1 = "<font color=#585858  >" + getResources().getString(R.string.reference_no) + "</font> <font color=#ff5400><b>"+order_number+"</b></font>";
                        order_id.setText(Html.fromHtml(text1));

                        // order_id.setText("Order id : #4H7RJ5");

                        ll1.addView(order_id);


                        MyTextView route_text = new MyTextView(
                                getApplicationContext());
                        route_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));
                        route_text.setGravity(Gravity.CENTER);
                        route_text.setLayoutParams(params4);
                        route_text.setText(from_city + " " + getResources().getString(R.string.to) + " " + to_city);
                        route_text.setPadding(5, 2, 5, 2);
                        ll1.addView(route_text);


                        MyTextView schedule_text = new MyTextView(
                                getApplicationContext());
                        schedule_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                        schedule_text.setGravity(Gravity.CENTER);


                        schedule_text.setLayoutParams(params4);
                        schedule_text.setText(transit_date+" "+getResources().getString(R.string.at)+" "+transit_time);
                        schedule_text.setPadding(5, 2, 5, 4);
                        ll1.addView(schedule_text);


                        MyTextView invoice_text = new MyTextView(
                                getApplicationContext());
                        invoice_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        invoice_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        invoice_text.setGravity(Gravity.CENTER);


                        invoice_text.setLayoutParams(params4);
                        //

                        String text = "<font color=#ffffff  >" + getResources().getString(R.string.pending_payment) + "</font> <font color=#ffffff><b>₹ "+pending_payment+"</b></font>";
                        invoice_text.setText(Html.fromHtml(text));

                        //    invoice_text.setText("Pending Payment : 5000");
                        invoice_text.setPadding(5, 10, 5, 10);
                        ll1.addView(invoice_text);


                        pending_bar_main.addView(card_view);


                    }


                 /*   initiatePopupWindow("Your password has been changed.",
                            false, "Success", "OK");*/


                } else if (status_code.equalsIgnoreCase("204")) {


                    //no more records

                    doPagination2 = false;
                    if (record_no2 > 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_more_history_shipments), Toast.LENGTH_SHORT).show();


                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll2 = true;

                    } else {


                        no_connection_image2.setVisibility(View.GONE);
                        pending_blank_image.setVisibility(View.VISIBLE);
                        pending_scroll_bar.setVisibility(View.GONE);

                    }

                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    CommonUtils.initiatePopupWindow(MyPayments.this, getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    CommonUtils.initiatePopupWindow(MyPayments.this, getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else {
                    CommonUtils.initiatePopupWindow(MyPayments.this, getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + " " + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
            Log.d("error ", e.getLocalizedMessage() + " " + e.getMessage());

        }
    }
}
