package com.cargowala.retrofit;

import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;


/**
 * Created by Bhvesh on 22-Sep-16.
 */
public interface ApiService {

    @GET("/json")
    void getAddress(
            @Query("address") String address
            , @Query("key") String key,
            Callback<JsonObject> callback);


    @GET("/json")
    void getAddressWithLatLong(
            @Query("latlng") String address
            , @Query("key") String key,
            Callback<JsonObject> callback);


}
