package com.cargowala.retrofit;

import android.content.Context;


import com.cargowala.helper.Constants;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;


public class RestClient {


    private static RestClient mInstance;
    public Context mContext;
    private static ApiService apiService = null;

   /* public static ApiService getApiService() {
        if (apiService == null) {
            OkHttpClient mOkHttpClient = new OkHttpClient();
            // For object response which is default

            //------
            RestAdapter restAdapter = null;
            try {
                restAdapter = new RestAdapter.Builder()
                        .setEndpoint(GlobalConstant.BASE_URL)
                        .setClient(new OkClient(mOkHttpClient))
                        .setLogLevel(RestAdapter.LogLevel.FULL)
                        .build();
            }catch (Exception e){
                e.printStackTrace();
            }

            apiService = restAdapter.create(ApiService.class);
        }
        return apiService;
    }*/

    public static ApiService getAddressDetail() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(15, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(15, TimeUnit.SECONDS);
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(Constants.GEO_BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL);
        RestAdapter restAdapter = builder.build();
        return restAdapter.create(ApiService.class);
    }


}