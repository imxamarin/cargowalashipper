package com.cargowala;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

class TestPreviewFragmentAdapter extends FragmentPagerAdapter {

	private int mCount = Offer.previewImages.length;

	public TestPreviewFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return TestPreviewFragment.newInstance(position
				% Offer.previewImages.length);
	}

	@Override
	public int getCount() {
		return mCount;
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}