package com.cargowala;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.squareup.picasso.Picasso;

public final class TestPreviewFragment extends Fragment {

	public static TestPreviewFragment newInstance(int content) {
		TestPreviewFragment fragment = new TestPreviewFragment();

		fragment.mContent = content;

		return fragment;
	}

	private int mContent = 0;
	int deviceWidth, deviceHeight;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();

	
			display.getSize(size);
			deviceWidth = size.x;
			deviceHeight = size.y;



	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ImageView text = new ImageView(getActivity());

		Picasso.with(getActivity()).load(Offer.previewImages[mContent])
				/*.placeholder(R.drawable.loading_screenshot)
				.error(R.drawable.no_image_screenshot)*/
				/*.resize(deviceWidth, deviceHeight).centerInside()*/.into(text);

		LayoutParams paramsVerticalLayout = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

		text.setLayoutParams(paramsVerticalLayout);
	//	text.setAdjustViewBounds(true);

		
		//if device length less than something
		text.setScaleType(ScaleType.CENTER_CROP);
		LinearLayout layout = new LinearLayout(getActivity());
		layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		layout.addView(text);

		return layout;
	}

}
