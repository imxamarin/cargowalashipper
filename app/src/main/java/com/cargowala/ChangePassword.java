package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/17/2016.
 */
public class ChangePassword extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;
    RelativeLayout actionBar;
    ImageView backbutton;
    SweetAlertDialog pDialog;
    MyTextView change_password;
    MaterialEditText edittext_old_pass,edittext_new_pass,edittext_confirm_pass;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    SharedPreferences pref1;
    SharedPreferences.Editor editor1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pass);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);


        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        change_password= (MyTextView) findViewById(R.id.change_password);
        change_password.setOnClickListener(this);

        backbutton.requestLayout();

        backbutton.setOnClickListener(this);

        edittext_old_pass= (MaterialEditText) findViewById(R.id.edittext_old_pass);



        edittext_old_pass.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(edittext_old_pass) {
            @Override
            public boolean onDrawableClick() {
                Log.d("m", "1");


                Log.d("main", " " + edittext_old_pass.getInputType());
                if(edittext_old_pass.getInputType()==1)
                {

                    Log.d("main", "here " );
                    edittext_old_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edittext_old_pass.setSelection(edittext_old_pass.getText().length());
                }
                else {
                    edittext_old_pass.setInputType(InputType.TYPE_CLASS_TEXT /*|
                        InputType.TYPE_TEXT_VARIATION_PASSWORD*/);
                    edittext_old_pass.setSelection(edittext_old_pass.getText().length());
                }
                return true;
            }
        });


        edittext_new_pass= (MaterialEditText) findViewById(R.id.edittext_new_pass);



        edittext_new_pass.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(edittext_new_pass) {
            @Override
            public boolean onDrawableClick() {
                Log.d("m", "1");


                Log.d("main", " " + edittext_new_pass.getInputType());
                if (edittext_new_pass.getInputType() == 1) {

                    Log.d("main", "here ");
                    edittext_new_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edittext_new_pass.setSelection(edittext_new_pass.getText().length());
                } else {
                    edittext_new_pass.setInputType(InputType.TYPE_CLASS_TEXT /*|
                        InputType.TYPE_TEXT_VARIATION_PASSWORD*/);
                    edittext_new_pass.setSelection(edittext_new_pass.getText().length());
                }
                return true;
            }
        });



        edittext_confirm_pass= (MaterialEditText) findViewById(R.id.edittext_confirm_pass);



        edittext_confirm_pass.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(edittext_confirm_pass) {
            @Override
            public boolean onDrawableClick() {
                Log.d("m", "1");


                Log.d("main", " " + edittext_confirm_pass.getInputType());
                if (edittext_confirm_pass.getInputType() == 1) {

                    Log.d("main", "here ");
                    edittext_confirm_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edittext_confirm_pass.setSelection(edittext_confirm_pass.getText().length());
                } else {
                    edittext_confirm_pass.setInputType(InputType.TYPE_CLASS_TEXT /*|
                        InputType.TYPE_TEXT_VARIATION_PASSWORD*/);
                    edittext_confirm_pass.setSelection(edittext_confirm_pass.getText().length());
                }
                return true;
            }
        });

        Drawable img = getResources().getDrawable( R.drawable.eye_icon );
        //img.setBounds(0, 0, 30, 20);
       Log.d("device width", "" + deviceWidth / 16);
        img.setBounds(0, 0, deviceWidth / 16, deviceWidth / 24);

        edittext_old_pass.setCompoundDrawables(null, null, img, null);
        edittext_new_pass.setCompoundDrawables(null, null, img, null);
        edittext_confirm_pass.setCompoundDrawables(null, null, img, null);







    }

    String old_pass,new_pass,confirm_pass;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:
                finish();


                break;


            case R.id.popup_button:


                if(status_code.equalsIgnoreCase("200"))
                {

                    dialog.dismiss();
                    finish();

                }

                else if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }



                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(ChangePassword.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i2);


                    finish();

                }
                else
                {
                    dialog.dismiss();
                }


                break;

            case R.id.change_password:



                old_pass = edittext_old_pass.getText().toString();
                new_pass = edittext_new_pass.getText().toString();
                confirm_pass = edittext_confirm_pass.getText().toString();


                if(old_pass.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_old_password),
                            Toast.LENGTH_SHORT).show();
                }

                else  if(new_pass.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_new_password), Toast.LENGTH_SHORT).show();
                }

                else  if(confirm_pass.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_confirm_password), Toast.LENGTH_SHORT).show();
                }

                else  if(old_pass.equalsIgnoreCase(new_pass))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.old_and_new_password_cannot_be_same), Toast.LENGTH_SHORT).show();
                }
                else if (new_pass.length() < 6) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.password_lenght_msg),
                            Toast.LENGTH_SHORT).show();
                }

                else  if(!new_pass.equalsIgnoreCase(confirm_pass))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.new_and_confirm_password_does_not_match), Toast.LENGTH_SHORT).show();
                }

              else
                {

                    if(isOnline())
                    {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makeChangePassReq();
                    }
                    else {
                        Toast.makeText(ChangePassword.this,
                                getResources().getString(R.string.check_your_network),
                                Toast.LENGTH_SHORT).show();
                    }

                }

                break;

        }

    }



    String res;
    StringRequest strReq;

    public String makeChangePassReq() {

        String url = getResources().getString(R.string.base_url)+"user/update-password";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkChangePassResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                  /*  InstanceID instanceID = InstanceID.getInstance(ChangePassword.this);
                    Log.d("token", "2");
                    String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.d("regg", "6");

                    Log.d("register ", email_id);
                    Log.d("register ",pass);
                    Log.d("token ",token);

                    params.put("username", email_id);
                    params.put("password", pass);
                    params.put("mobile_tokens", token);

                    */

                    params.put("oldpassword", old_pass);
                    params.put("newpassword", new_pass);

                    params.put("security_token", pref.getString("security_token","Something Wrong"));

                    params.put("_id",pref.getString("userId","Something Wrong") );
                    Log.e("register", params.toString());


                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "14");



        return null;
    }

    String status_code = "0";

    public void checkChangePassResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);



                if(status_code.equalsIgnoreCase("200"))
                {


                    initiatePopupWindow(getResources().getString(R.string.your_password_has_been_changed),
                            false, getResources().getString(R.string.success), getResources().getString(R.string.ok));



                }
               else if(status_code.equalsIgnoreCase("400"))
                {



                    initiatePopupWindow(getResources().getString(R.string.the_old_password_entered_is_wrong),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error),getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error),getResources().getString(R.string.ok));


                }


                else
                {
                    initiatePopupWindow("Wrong old Password",
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(ChangePassword.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }

}
