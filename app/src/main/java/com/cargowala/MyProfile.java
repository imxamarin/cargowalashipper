package com.cargowala;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.cargowala.ImageLib.PickerBuilder;
import com.cargowala.helper.CommonListners;
import com.cargowala.helper.CommonUtils;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.cargowala.CreateBase.encodeImage;

/**
 * Created by Akash on 2/10/2016.
 */

@RuntimePermissions
public class MyProfile extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton,menu_button;

   File mediaStorageDir;
    public static Uri uri;
    public static int CAMERA_REQUEST = 2, GALLERY_REQUEST = 4, CROP_FROM_CAMERA = 3;
    Dialog dialogBox;
    private String path;
    private String filepath;
    private String base64="";
    private String croppath;




    boolean isCamera = false;

    View root_menu;
    boolean boo = true;
    SweetAlertDialog pDialog;
    LinearLayout menu_edit_profile,menu_change_number,menu_change_pass,menu_forgot_pass,selectLanguage,
            reg_as_comp,edit_comp_det,edit_agent_det,reg_as_agent;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;
    MyTextView save;
    CircleImageView profile_image;

    MaterialEditText first_name,last_name,pan_card,service_tax_no;

    MyTextViewSemi emailIdText,mobile_no;
    MyTextViewSemi actionBarHeading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile);


        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();


        editor.putBoolean("isMyProfileHitService",true);
        editor.commit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);

        menu_button = (ImageView) findViewById(R.id.menu_button);
        menu_button.setOnClickListener(this);


        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);

        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        profile_image.setOnClickListener(this);

        actionBarHeading = (MyTextViewSemi) findViewById(R.id.actionBarHeading);



        backbutton.requestLayout();


        first_name= (MaterialEditText) findViewById(R.id.first_name);
        last_name= (MaterialEditText) findViewById(R.id.last_name);
        service_tax_no= (MaterialEditText) findViewById(R.id.service_tax_no);
        pan_card= (MaterialEditText) findViewById(R.id.pan_card);

        root_menu= (View) findViewById(R.id.root_menu);

        save= (MyTextView) findViewById(R.id.save);
        save.setOnClickListener(this);

        emailIdText= (MyTextViewSemi) findViewById(R.id.emailIdText);
        mobile_no= (MyTextViewSemi) findViewById(R.id.mobile_no);


        Log.d("profile details ", "userId " + pref.getString("userId", ""));
        Log.d("profile details ", "user_firstname " + pref.getString("user_firstname", ""));
        Log.d("profile details ","user_lastname "+pref.getString("user_lastname",""));
        Log.d("profile details ","user_email "+pref.getString("user_email",""));
        Log.d("profile details ","user_mobile_no "+pref.getString("user_mobile_no",""));
        Log.d("profile details ","user_company_type "+pref.getString("user_company_type",""));
        Log.d("profile details ","user_business_type "+pref.getString("user_business_type",""));
        Log.d("profile details ","user_business_name "+pref.getString("user_business_name",""));
        Log.d("profile details ","user_landline_number "+pref.getString("user_landline_number",""));
        Log.d("profile details ","user_registered_address "+pref.getString("user_registered_address",""));
        Log.d("profile details ","user_register_city "+pref.getString("user_register_city",""));
        Log.d("profile details ","user_register_state "+pref.getString("user_register_state",""));
        Log.d("profile details ","user_register_pin "+pref.getString("user_register_pin",""));
        Log.d("profile details ","user_office_address "+pref.getString("user_office_address",""));
        Log.d("profile details ","user_office_city "+pref.getString("user_office_city",""));
        Log.d("profile details ","user_office_state "+pref.getString("user_office_state",""));
        Log.d("profile details ","user_office_pin "+pref.getString("user_office_pin",""));
        Log.d("profile details ","user_office_pin "+pref.getString("user_office_pin",""));
        Log.d("profile details ","user_image_url "+pref.getString("user_image_url",""));
        Log.d("profile details ","user_image_base64 "+pref.getString("user_image_base64",""));



        emailIdText.setText(pref.getString("user_email", ""));
        mobile_no.setText("+91 "+pref.getString("user_mobile_no",""));

        first_name.setText(pref.getString("user_firstname", ""));
        last_name.setText(pref.getString("user_lastname", ""));
        pan_card.setText(pref.getString("user_pan_card", ""));
        pan_card.setMaxCharacters(10);
        service_tax_no.setText(pref.getString("user_service_tax_no", ""));

        pan_card.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        service_tax_no.setFilters(new InputFilter[]{new InputFilter.AllCaps()});


        String userAccountType = pref.getString("user_account_type", "");
        Log.d("MyProfile", "onCreate:   --> ".concat(userAccountType));



        //Added 21 as check as per backend


            if (userAccountType.equalsIgnoreCase("1")||userAccountType.equalsIgnoreCase("21")) {
                //individual
                service_tax_no.setVisibility(View.GONE);

            } else if (userAccountType.equalsIgnoreCase("3")) {
                //agent
                service_tax_no.setVisibility(View.VISIBLE);
            } else if (userAccountType.equalsIgnoreCase("5")) {
                //company
                service_tax_no.setVisibility(View.VISIBLE);
            } else {
                Log.d("errorrr", "not possible");
            }


            if (!pref.getString("user_image_url", "").equalsIgnoreCase("")) {
                Picasso.with(getApplicationContext())
                        .load(pref.getString("user_image_url", ""))
                        .placeholder(R.drawable.default_user)
                        .error(R.drawable.default_user).into(profile_image);
            } else if (!pref.getString("user_image_base64", "").equalsIgnoreCase("")) {

                byte[] decodedString = Base64.decode(pref.getString("user_image_base64", ""), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                profile_image.setImageBitmap(decodedByte);

            }



     /*   else
        {



        }*/










    }








    String res1;
    StringRequest strReq1;

    public String makeGetUserProfileReq() {

        String url = getResources().getString(R.string.base_url)+"user/get-user-profile";
        Log.d("regg", "2");
        strReq1 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res1 = response.toString();
                        checkGetUserProfileResponse(res1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res1 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("_id",pref.getString("userId","Something Wrong"));
                  params.put("security_token", pref.getString("security_token","Something Wrong"));

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq1, "32");



        return null;
    }

    public void checkGetUserProfileResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {


                 /*   initiatePopupWindow("Your password has been changed.",
                            false, "Success", "OK");*/

                    String response1 = reader.getString("response");
                    JSONObject jsonobj = new JSONObject(response1);




                  //  String id = jsonobj.getString("_id");
                    String firstname = jsonobj.getString("firstname");
                    String lastname = jsonobj.getString("lastname");
                    String email = jsonobj.getString("email");
                    String mobile_no1 = jsonobj.getString("mobile_no");
                    String image = jsonobj.getString("image");
                    String business_name = jsonobj.getString("business_name");
                    String pancard = jsonobj.getString("pancard");
                    String account_type = jsonobj.getString("account_type");
                    String company_type = jsonobj.getString("company_type");
                    String business_type = jsonobj.getString("business_type");
                    String registered_address = jsonobj.getString("registered_address");
                    String office_address = jsonobj.getString("office_address");
                    String landline_number = jsonobj.getString("landline_number");
                    String register_city = jsonobj.getString("register_city");
                    String register_state = jsonobj.getString("register_state");
                    String register_pin = jsonobj.getString("register_pin");
                    String office_city = jsonobj.getString("office_city");
                    String office_state = jsonobj.getString("office_state");
                    String office_pin = jsonobj.getString("office_pin");
                    String business_service_taxno = jsonobj.getString("business_service_taxno");

                    String ctype="",others="",btype="",others1 = "";
                    JSONObject jsonobj1 = new JSONObject(company_type);
                    if(jsonobj1.has("ctype")){
                        ctype = jsonobj1.getString("ctype");
                    }
                    if(jsonobj1.has("text")){
                        others = jsonobj1.getString("text");
                    }

                    JSONObject jsonobj2 = new JSONObject(business_type);

                    if(jsonobj2.has("btype")){
                     btype = jsonobj2.getString("btype");
                    }
                    if(jsonobj2.has("text")){
                     others1 = jsonobj2.getString("text");
                    }



                    //  String id = jsonobj.getString("_id");




                   /* editor.putBoolean("isLoggedIn", true);
                    editor.putString("userId", id);*/

                    editor.putString("user_firstname", firstname);
                    editor.putString("user_lastname", lastname);
                    editor.putString("user_email", email);
                    editor.putString("user_mobile_no", mobile_no1);

                    editor.putString("user_company_type", ctype);
                    editor.putString("user_business_type", btype);

                    /*editor.putString("company_business_other", others);
                    editor.putString("company_type_other", others1);*/

                    editor.putString("user_comp_type_other", others);
                    editor.putString("user_buss_type_other", others1);



                    editor.putString("user_image_url", image);
                    editor.putString("user_image_base64", "");

                    editor.putString("user_account_type", account_type);


                    editor.putString("user_business_name", business_name);
                    editor.putString("user_landline_number", landline_number);
                    editor.putString("user_registered_address", registered_address);
                    editor.putString("user_register_city", register_city);
                    editor.putString("user_register_state", register_state);
                    editor.putString("user_register_pin", register_pin);
                    editor.putString("user_office_address", office_address);
                    editor.putString("user_office_city", office_city);
                    editor.putString("user_office_state", office_state);
                    editor.putString("user_office_pin", office_pin);


                    editor.putString("user_pan_card", pancard);
                    editor.putString("user_service_tax_no", business_service_taxno);

                    editor.commit();



                    emailIdText.setText(email);
                    mobile_no.setText("+91 "+mobile_no1);

                    first_name.setText(firstname);
                    last_name.setText(lastname);
                    pan_card.setText(pancard);
                    service_tax_no.setText(business_service_taxno);


                    if (account_type.equalsIgnoreCase("21")) {
                        //individual
                        service_tax_no.setVisibility(View.GONE);

                    } else if (account_type.equalsIgnoreCase("3")) {
                        //agent
                        service_tax_no.setVisibility(View.VISIBLE);
                    } else if (account_type.equalsIgnoreCase("22")) {
                        //company
                        service_tax_no.setVisibility(View.VISIBLE);
                    } else {
                        Log.d("errorrr", "not possible");
                    }


                  if(!image.equalsIgnoreCase("")) {
                      Picasso.with(getApplicationContext())
                              .load(image)
                              .placeholder(R.drawable.default_user)
                              .error(R.drawable.default_user).into(profile_image);
                  }


                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_message),
                            true, getResources().getString(R.string.success), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true,getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }






















    String fName1 ="";
    String lName1 = "";
    String pan_card1 = "";
    String service_tax_no1 ="";

    @Override
    public void onClick(View v) {

        switch (v.getId()) {




            case R.id.backbutton:

                finish();

                break;

            case R.id.save:


                 fName1 = first_name.getText().toString().trim();
                 lName1 = last_name.getText().toString().trim();
                 pan_card1 = pan_card.getText().toString().trim();
                 service_tax_no1 = service_tax_no.getText().toString().trim();

                if(fName1.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_first_name), Toast.LENGTH_SHORT)
                            .show();
                }

                else if(lName1.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_last_name), Toast.LENGTH_SHORT)
                            .show();
                }

                else if(pan_card1.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_pan_card_number), Toast.LENGTH_SHORT)
                            .show();
                }
                else if(pan_card1.length()!=10)
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_valid_pan_card_number), Toast.LENGTH_SHORT)
                            .show();
                }

                else {


                    String userAccountType= pref.getString("user_account_type", "");
                    if(!userAccountType.equalsIgnoreCase("21")) {
                        if (service_tax_no1.equalsIgnoreCase("")) {
                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.please_enter_service_tax_number), Toast.LENGTH_SHORT)
                                    .show();
                        } else if (service_tax_no1.length() != 15) {
                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.enter_valid_service_tax), Toast.LENGTH_SHORT)
                                    .show();
                        }
                        else
                        {

                            if(isOnline())
                            {
                                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                pDialog.setTitleText(getResources().getString(R.string.loading));
                                pDialog.setCancelable(false);
                                pDialog.show();
                                makeUpdateProfileReq();
                            }
                            else {
                                Toast.makeText(MyProfile.this,
                                        getResources().getString(R.string.check_your_network),
                                        Toast.LENGTH_SHORT).show();
                            }



                        }
                    }
                    else
                    {
                        if(isOnline())
                        {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makeUpdateProfileReq();
                        }
                        else {
                            Toast.makeText(MyProfile.this,
                                    getResources().getString(R.string.check_your_network),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                }

               /* save.setVisibility(View.GONE);

                boo = true;
                first_name.setFocusable(false);
                first_name.setFocusableInTouchMode(false);

                last_name.setFocusable(false);
                last_name.setFocusableInTouchMode(false);

                service_tax_no.setFocusable(false);
                service_tax_no.setFocusableInTouchMode(false);

                pan_card.setFocusable(false);
                pan_card.setFocusableInTouchMode(false);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(first_name.getWindowToken(), 0);*/



                break;



            case R.id.profile_image:

              if(!boo)
              {
                  //now

                  DialogImage();

              }

                break;

            case R.id.selectLanguage:
                CommonUtils.yesNoPopupWindow(MyProfile.this, getResources().getString(R.string.choose_language_alert_msg), true, getResources().getString(R.string.choose_language), getResources().getString(R.string.yes), getResources().getString(R.string.no), new CommonListners.AlertCallBackWithButtonsInterface() {
                    @Override
                    public void positiveClick() {
                        Intent i1 = new Intent(MyProfile.this, SelectLanguage.class);
                        i1.putExtra("from_setting", true);
                        startActivity(i1);
                    }

                    @Override
                    public void neutralClick() {

                    }

                    @Override
                    public void negativeClick() {

                    }
                });

                break;

            case R.id.menu_button:

                Log.d("1","1");
                openSettingsPopup();

                break;

            case R.id.menu_edit_profile:

                editor.putBoolean("isMyProfileHitService",false);
                editor.commit();

                popupWindow.dismiss();

                //openSettingsPopup()

                if(boo) {
                    boo = false;
                    first_name.setFocusable(true);
                    first_name.setFocusableInTouchMode(true);

                    last_name.setFocusable(true);
                    last_name.setFocusableInTouchMode(true);

                    service_tax_no.setFocusable(true);
                    service_tax_no.setFocusableInTouchMode(true);

                    pan_card.setFocusable(true);
                    pan_card.setFocusableInTouchMode(true);

                    first_name.requestFocus();
                    InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm1.showSoftInput(first_name, InputMethodManager.SHOW_IMPLICIT);

                    save.setVisibility(View.VISIBLE);
                    actionBarHeading.setText(getResources().getString(R.string.edit_profile));

                }
                pan_card.setMaxCharacters(10);
               /* else
                {
                    boo = true;
                    first_name.setFocusable(false);
                    first_name.setFocusableInTouchMode(false);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(first_name.getWindowToken(), 0);

                    imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                            .getWindowToken(), 0);
                }*/

                break;

            case R.id.menu_change_number:

                editor.putBoolean("isMyProfileHitService",true);
                editor.commit();

                popupWindow.dismiss();

                Intent i = new Intent(MyProfile.this, ChangeMobileNumber.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);

                break;

            case R.id.menu_change_pass:

                editor.putBoolean("isMyProfileHitService",true);
                editor.commit();

                popupWindow.dismiss();
                Intent i1 = new Intent(MyProfile.this, ChangePassword.class);
                i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i1);


                break;

            case R.id.menu_forgot_pass:

                editor.putBoolean("isMyProfileHitService",true);
                editor.commit();

                popupWindow.dismiss();

                initiateyYesNoWindow(getResources().getString(R.string.we_will_send_you_mail_at_your_registered_email_to_reset_your_password),
                        true, getResources().getString(R.string.confirmation), getResources().getString(R.string.ok), getResources().getString(R.string.cancel));


                break;

            case R.id.reg_as_comp:

                editor.putBoolean("isMyProfileHitService",true);
                editor.commit();

                popupWindow.dismiss();

                Intent i2 = new Intent(MyProfile.this, RegisterAsCompanyOne.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i2);

                break;

            case R.id.edit_comp_det:

                editor.putBoolean("isMyProfileHitService",true);
                editor.commit();

                popupWindow.dismiss();

                Intent i3 = new Intent(MyProfile.this, EditCompanyDetail.class);
                i3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i3);


                break;

            case R.id.edit_agent_det:

                editor.putBoolean("isMyProfileHitService",true);
                editor.commit();

                popupWindow.dismiss();

                Intent i4 = new Intent(MyProfile.this, EditAgentDetail.class);
                i4.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i4);

                break;

            case R.id.reg_as_agent:

                editor.putBoolean("isMyProfileHitService",true);
                editor.commit();

                popupWindow.dismiss();

                Intent i5 = new Intent(MyProfile.this, RegisterAsAgent.class);
                i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i5);

                break;







            case R.id.popup_button_yes2:
                dialog1.dismiss();


                if(isOnline()) {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText(getResources().getString(R.string.loading));
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeForgotPassReq();
                }
            {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.check_your_network),
                        Toast.LENGTH_SHORT).show();
            }

                break;

            case R.id.popup_button_no2:
                dialog1.dismiss();


                break;

            case R.id.popup_button:


                if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }

                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i6 = new Intent(MyProfile.this, Login.class);
                  //  i6.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i6.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i6.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i6);
                    finish();

                }
                else
                {
                    dialog.dismiss();
                }



                break;



        }



    }












    String res2;
    StringRequest strReq2;

    public String makeUpdateProfileReq() {

        String url = getResources().getString(R.string.base_url)+"user/update-profile";
        Log.d("regg", "2");
        strReq2 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res2 = response.toString();
                        checkUpdateProfileResponse(res2);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res2 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("_id",pref.getString("userId","Something Wrong") );

                  //  params.put("firstname",fName1 );
                    params.put("firstname",fName1 );
                    params.put("lastname",lName1 );
                    params.put("pancard",pan_card1 );
                    params.put("business[business_service_taxno]",service_tax_no1 );
                    params.put("image_base64",base64);
                    params.put("security_token", pref.getString("security_token","Something Wrong"));

                    Log.e("register", params.toString());


                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq2, "33");

        return null;
    }


    String status_code = "0";

    public void checkUpdateProfileResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    editor.putString("user_firstname", fName1);
                    editor.putString("user_lastname", lName1);

                    if(!base64.equalsIgnoreCase("")) {
                        editor.putString("user_image_url", "");
                        editor.putString("user_image_base64", base64);
                    }
                    editor.putString("user_pan_card", pan_card1);
                    editor.putString("user_service_tax_no",service_tax_no1);

                    editor.commit();
                    save.setVisibility(View.GONE);
                    actionBarHeading.setText(getResources().getString(R.string.profile));

                    boo = true;
                    first_name.setFocusable(false);
                    first_name.setFocusableInTouchMode(false);

                    last_name.setFocusable(false);
                    last_name.setFocusableInTouchMode(false);

                    service_tax_no.setFocusable(false);
                    service_tax_no.setFocusableInTouchMode(false);

                    pan_card.setFocusable(false);
                    pan_card.setFocusableInTouchMode(false);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(first_name.getWindowToken(), 0);




                    initiatePopupWindow(getResources().getString(R.string.your_profile_has_been_updated),
                            false, getResources().getString(R.string.success), getResources().getString(R.string.ok));

                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else
                {
                    initiatePopupWindow("This was some problem. Please try again.",
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }








/*

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }
*/



    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    /**
     * Popup show -->Settings
     */
    PopupWindow popupWindow;

    public void openSettingsPopup() {

        Log.d("1","1");

        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
         final View popupView = layoutInflater.inflate(R.layout.popup_settings, null);
         popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        menu_edit_profile= (LinearLayout) popupView.findViewById(R.id.menu_edit_profile);
        menu_change_number= (LinearLayout) popupView.findViewById(R.id.menu_change_number);
        menu_change_pass= (LinearLayout) popupView.findViewById(R.id.menu_change_pass);
        menu_forgot_pass= (LinearLayout) popupView.findViewById(R.id.menu_forgot_pass);
        reg_as_comp= (LinearLayout) popupView.findViewById(R.id.reg_as_comp);
        edit_comp_det= (LinearLayout) popupView.findViewById(R.id.edit_comp_det);
        edit_agent_det= (LinearLayout) popupView.findViewById(R.id.edit_agent_det);
        reg_as_agent= (LinearLayout) popupView.findViewById(R.id.reg_as_agent);
        selectLanguage= (LinearLayout) popupView.findViewById(R.id.selectLanguage);
        selectLanguage.setOnClickListener(this);
        menu_edit_profile.setOnClickListener(this);
        menu_change_number.setOnClickListener(this);
        menu_change_pass.setOnClickListener(this);
        menu_forgot_pass.setOnClickListener(this);
        reg_as_comp.setOnClickListener(this);
        edit_comp_det.setOnClickListener(this);
        edit_agent_det.setOnClickListener(this);
        reg_as_agent.setOnClickListener(this);




        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        Log.e("account_type",pref.getString("user_account_type", ""));
        if (pref.getString("user_account_type", "").equalsIgnoreCase("21")) {
            //individual
            reg_as_comp.setVisibility(View.VISIBLE);
            reg_as_agent.setVisibility(View.VISIBLE);
            edit_comp_det.setVisibility(View.GONE);
            edit_agent_det.setVisibility(View.GONE);

        } else if (pref.getString("user_account_type", "").equalsIgnoreCase("3")) {
            //agent

            reg_as_comp.setVisibility(View.GONE);
            reg_as_agent.setVisibility(View.GONE);
            edit_comp_det.setVisibility(View.GONE);
            edit_agent_det.setVisibility(View.VISIBLE);


        } else if (pref.getString("user_account_type", "").equalsIgnoreCase("22") || pref.getString("user_account_type", "").equalsIgnoreCase("5")) {
            //company

            reg_as_comp.setVisibility(View.GONE);
            reg_as_agent.setVisibility(View.VISIBLE);
            edit_comp_det.setVisibility(View.VISIBLE);
            edit_agent_det.setVisibility(View.GONE);

        } else {
            Log.d("errorrr", "not possible");
        }







      /*  menu_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               popupWindow.dismiss();
            }
        });*/



        // Closes the popup window when touch outside of it - when looses focus
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        // Removes default black background
        //popupWindow.setBackgroundDrawable(new BitmapDrawable());

       popupWindow.showAsDropDown(root_menu, 0, 0);
     //   popupWindow.showAtLocation(root_view, Gravity.TOP| Gravity.RIGHT, 0, 0);
        Log.d("1", "1");

    }



    Dialog dialog1;

    private void initiateyYesNoWindow(String message, Boolean isAlert, String heading, String buttonTextYes,  String buttonTextNo) {
        try {

            dialog1 = new Dialog(MyProfile.this);
            dialog1.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.yesnopopup);
            dialog1.setCanceledOnTouchOutside(false);
            MyTextView popup_message2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_message2);
            popup_message2.setText(message);


            ImageView popup_image2 = (ImageView) dialog1
                    .findViewById(R.id.popup_image2);
            if(isAlert)
            {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_heading2);
            popup_heading2.setText(heading);


            MyTextView popup_button_yes2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_yes2);
            popup_button_yes2.setText(buttonTextYes);
            popup_button_yes2.setOnClickListener(this);

            MyTextView popup_button_no2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_no2);
            popup_button_no2.setText(buttonTextNo);
            popup_button_no2.setOnClickListener(this);

            dialog1.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }










    String res;
    StringRequest strReq;

    public String makeForgotPassReq() {

        String url = "http://52.33.155.25/api/v1/user/forgot-password";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkForgotPassResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    Log.d("email_text ",  pref.getString("user_email","NA"));

                    params.put("email", pref.getString("user_email","NA"));

                    Log.e("register", params.toString());



                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "34");



        return null;
    }

    public void checkForgotPassResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                String status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {


                    initiatePopupWindow(getResources().getString(R.string.an_email_has_been_to_reset_your_password),
                            false, getResources().getString(R.string.email_sent), getResources().getString(R.string.ok));

                }
                else if(status_code.equalsIgnoreCase("429"))
                {



                    initiatePopupWindow(getResources().getString(R.string.this_email_id_is_not_verified_by_the_user),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }
                else if(status_code.equalsIgnoreCase("204"))
                {



                    initiatePopupWindow(getResources().getString(R.string.this_email_id_is_not_registerd_with_us),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }
                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(MyProfile.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }








    // camera gallery photo


    TextView camera_text,open_gallery_text,cancel_text;

    private void DialogImage() {
        dialogBox = new Dialog(MyProfile.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogBox.setContentView(R.layout.camera_gallery_popup);

        // Animation animation = AnimationUtils.loadAnimation(context, R.anim.bottom_up_anim);
        dialogBox.show();

        camera_text = (TextView) dialogBox.findViewById(R.id.camera_text);

        // RelativeLayout dialog_rel = (RelativeLayout) dialogBox.findViewById(R.id.dialog_rel);

        open_gallery_text = (TextView) dialogBox.findViewById(R.id.open_gallery_text);
        //remove_photo_text= (TextView) dialogBox.findViewById(R.id.remove_photo_text);
        cancel_text = (TextView) dialogBox.findViewById(R.id.cancel_text);

        cancel_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                dialogBox.dismiss();
            }
        });

        //remove_photo_text.setVisibility(View.GONE);
        camera_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();

                isCamera = true;

             MyProfilePermissionsDispatcher.takephotoWithPermissionCheck(MyProfile.this);

            }
        });

        open_gallery_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();

                isCamera = false;

                //takephotoFromGallery();
//                initGallery();
                MyProfilePermissionsDispatcher.initGalleryWithPermissionCheck(MyProfile.this);
            }
        });

    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE})
    public void initGallery() {
        new PickerBuilder(MyProfile.this, PickerBuilder.SELECT_FROM_GALLERY)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {

                        Picasso.with(MyProfile.this).load(imageUri).into(profile_image);

                        final InputStream imageStream;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                             base64 = encodeImage(selectedImage);


                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();

    }

    public void takephotoFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);

        /*Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image*//*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);*/

    }


    @NeedsPermission(Manifest.permission.CAMERA)
    public void takephoto() {
//        mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "CargoWala");
//        // Create the storage directory(MyCameraVideo) if it
//        // does not
//        // exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                Toast.makeText(getApplicationContext(),
//                        getResources().getString(R.string.failed_to_create_directory_cargowala),
//                        Toast.LENGTH_LONG).show();
//                Log.d("MyCameraVideo",
//                        "Failed to create directory CargoWala.");
//            }
//        }
//
//        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//        File file = new File(mediaStorageDir.getAbsolutePath(),
//                "CargoWala.jpg");
//        path = file.getAbsolutePath();
//        filepath = path;
//        uri = Uri.fromFile(file);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
//        startActivityForResult(intent, CAMERA_REQUEST);
//        uri = null;

        new PickerBuilder(MyProfile.this, PickerBuilder.SELECT_FROM_CAMERA)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {

                        Picasso.with(MyProfile.this).load(imageUri).into(profile_image);

                        final InputStream imageStream;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            base64 = encodeImage(selectedImage);


                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }


                    }
                }).start();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_REQUEST
                && resultCode ==RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(
                    selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = decodeSampledBitmapFromFile(filepath, 1600, 1000);
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(filepath);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
            }

            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
            ByteArrayOutputStream ful_stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, ful_stream);
            byte[] ful_bytes = ful_stream.toByteArray();
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "Title", null);
            uri = Uri.parse(path);


            try {
                beginCrop(data.getData());
                // doCrop();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // decode
            // byte[] imageAsBytes = Base64.decode(Image.getBytes(),0);
            // user_image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes,
            // 0, imageAsBytes.length));

        } else if ((requestCode == CAMERA_REQUEST)
                && (resultCode == RESULT_OK)) {
            if (resultCode == RESULT_OK) {
                beginCrop(uri);
            }
        } else if ((requestCode == CROP_FROM_CAMERA)) {
            // Bitmap bitmap = decodeSampledBitmapFromFile(croppath, 600, 600);
            Bitmap bitmap = BitmapFactory.decodeFile(croppath);
            //  ByteArrayOutputStream thumb_stream = new ByteArrayOutputStream();
            if (bitmap != null) {
                //  bitmap.compress(Bitmap.CompressFormat.PNG, 100, thumb_stream);
                profile_image.setImageBitmap(bitmap);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);

                base64 = Base64.encodeToString(stream.toByteArray(),
                        Base64.NO_WRAP);

                Log.d("im", "base64 " + base64);

               /* editor.putString("imagebase64",base64);

                editor.putString("userImage","");
                editor.commit();*/



                Log.d("base64", base64);

            }
            // byte[] ful_bytes = thumb_stream.toByteArray();
            // base64 = Base64.encodeBytes(ful_bytes);



            //hit service here



            try {
                File f = new File(croppath);
                f.delete();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }
    private void handleCrop(int resultCode, Intent result ) {
        if (resultCode == RESULT_OK) {
//            .setImageURI(Crop.getOutput(result));

            Uri selectedImage = Crop.getOutput(result);
            Bitmap bmp = null, bmp_new = null;
            try {
                bmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(filepath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Log.d("orientation ", "" + orientation);


                Matrix matrix = new Matrix();
                if (orientation == 6  && isCamera) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }

                bmp_new = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            } catch (IOException e) {
                e.printStackTrace();
            }

            // Bitmap bitmap = BitmapFactory.decodeFile(filepath);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp_new.compress(Bitmap.CompressFormat.JPEG, 70, stream);

            profile_image.setImageBitmap(bmp_new);

            base64 = Base64.encodeToString(stream.toByteArray(),
                    Base64.NO_WRAP);
            Log.i("kjbnjhbase64 ", base64);


            Log.d("im", "base64 " + base64);

           /* editor.putString("imagebase64",base64);

            editor.putString("userImage", "");
            editor.commit();*/


            /*if (CommonUtils.isNetworkAvailable(context)) {

                updateUserInfoReq();
            } else {
                CommonUtils.showCustomErrorDialog1(context, getResources().getString(R.string.bad_connection));
            }*/

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


    public Bitmap decodeSampledBitmapFromFile(String path, int reqWidth,
                                              int reqHeight) { // BEST QUALITY MATCH
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }

        int expectedWidth = width / inSampleSize;
        if (expectedWidth > reqWidth) {
            // if(Math.round((float)width / (float)reqWidth) > inSampleSize) //
            // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(pref.getBoolean("isMyProfileHitService",true)) {
            if (isOnline()) {
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();
                makeGetUserProfileReq();
            } else {
                Toast.makeText(MyProfile.this,
                        getResources().getString(R.string.check_your_network),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}
