package com.cargowala;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class Help extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton;

    ImageView faq_image, contactus_image;
    LinearLayout contactus_bar, faq_bar;

    ImageView plus_one, plus_two, address_map;
    MyTextView ans_one, ans_two;
    boolean show_one = true, show_two = true;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SweetAlertDialog pDialog;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;
    String res;
    StringRequest strReq;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);


        backbutton.requestLayout();

        faq_image = (ImageView) findViewById(R.id.faq_image);
        faq_image.setOnClickListener(this);
        contactus_image = (ImageView) findViewById(R.id.contactus_image);
        contactus_image.setOnClickListener(this);

        contactus_bar = (LinearLayout) findViewById(R.id.contactus_bar);
        faq_bar = (LinearLayout) findViewById(R.id.faq_bar);


        plus_one = (ImageView) findViewById(R.id.plus_one);
        plus_one.setOnClickListener(this);
        plus_two = (ImageView) findViewById(R.id.plus_two);
        plus_two.setOnClickListener(this);

        ans_one = (MyTextView) findViewById(R.id.ans_one);
        ans_two = (MyTextView) findViewById(R.id.ans_two);


        address_map = (ImageView) findViewById(R.id.address_map);

        address_map.measure(0, 0);
        int width = address_map.getMeasuredWidth() / 2;


        int height = address_map.getMeasuredHeight() / 2;

        // Log.d("width ", "w "+width+ " h "+height);


        String url = "";
        url = url + "https://maps.googleapis.com/maps/api/staticmap?";
        url = url   /*+"center=Delhi,IN&"*/ + "zoom=15";
        url = url + "&size=" + width + "x" + height;
        url = url + "&maptype=roadmap";

        url = url + "&markers=color:orange%7Clabel:L%7C40.702147,-74.015794";


        url = url + "&key=AIzaSyAvLAdXzhvITt3E6RCbcU2jdUegV21D94U";
        Log.d("####URLGmaps",url);

        Picasso.with(getApplicationContext())
                .load(url)
                /*.placeholder(R.drawable.loading_screenshot)
				.error(R.drawable.no_image_screenshot)*/
				/*.resize(deviceWidth, deviceHeight).centerInside()*/.into(address_map);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:

                finish();

                break;

            case R.id.faq_image:

                faq_bar.setVisibility(View.VISIBLE);
                contactus_bar.setVisibility(View.GONE);
                faq_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.help_faq_selected));
                contactus_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.help_contactus_unselected));

                break;


            case R.id.contactus_image:

                faq_bar.setVisibility(View.GONE);
                contactus_bar.setVisibility(View.VISIBLE);
                faq_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.help_faq_unselected));
                contactus_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.help_contactus_selected));

                break;

            case R.id.plus_one:

                if (show_one) {
                    show_one = false;
                    plus_one.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.faq_minus));
                    ans_one.setVisibility(View.VISIBLE);
                } else {
                    show_one = true;
                    plus_one.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.faq_plus));
                    ans_one.setVisibility(View.GONE);
                }

                break;

            case R.id.plus_two:

                if (show_two) {
                    show_two = false;
                    plus_two.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.faq_minus));
                    ans_two.setVisibility(View.VISIBLE);
                } else {
                    show_two = true;
                    plus_two.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.faq_plus));
                    ans_two.setVisibility(View.GONE);
                }


                break;


        }


    }
    
    public void gettingTopics() {

        String url = getResources().getString(R.string.base_url) + "user/book-truck";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkeBookNowResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                    params.put("security_token", pref1.getString("security_token", "Something Wrong"));
                    params.put("_id", pref1.getString("userId", "_id Wrong"));
                    Log.e("regg", "7 " + params.toString());
                
                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "29");


    }

    private void checkeBookNowResponse(String res) {
    }

}
