package com.cargowala;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * Created by Akash on 2/10/2016.
 */
public class CompanyBusiness extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton,company_progress;
    MyTextView next;
    LinearLayout ll1,ll2;
    ImageView img1,img2,img4,img5;

    LinearLayout ll11,ll22;
    ImageView img11,img22,img33,img44,img55,img66;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    MaterialEditText buss_name,landline_no;
    String first_name,last_name,email_id,image_url,account_type;
    ScrollView exampleScrollView;


    MaterialEditText buss_type_other,comp_type_other;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_business);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        Intent intent = getIntent();


        first_name = intent.getExtras().getString("first_name");
        last_name = intent.getExtras().getString("last_name");
        email_id = intent.getExtras().getString("email_id");
        image_url = intent.getExtras().getString("image_url");

        account_type = intent.getExtras().getString("account_type");

        exampleScrollView = (ScrollView) findViewById(R.id.exampleScrollView);

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();


        buss_name = (MaterialEditText) findViewById(R.id.buss_name);
        landline_no = (MaterialEditText) findViewById(R.id.landline_no);

        buss_type_other = (MaterialEditText) findViewById(R.id.buss_type_other);
        comp_type_other = (MaterialEditText) findViewById(R.id.comp_type_other);



        ll1 = (LinearLayout) findViewById(R.id.ll1);

        ll1.getLayoutParams().height =  (deviceHeight / 6);

        ll1.requestLayout();

        ll2 = (LinearLayout) findViewById(R.id.ll2);

        ll2.getLayoutParams().height =  (deviceHeight / 6);

        ll2.requestLayout();

        ll11 = (LinearLayout) findViewById(R.id.ll11);

        ll11.getLayoutParams().height =  (deviceHeight / 6);

        ll11.requestLayout();

        ll22 = (LinearLayout) findViewById(R.id.ll22);

        ll22.getLayoutParams().height =  (deviceHeight / 6);

        ll22.requestLayout();


        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);
        backbutton.requestLayout();
        backbutton.setOnClickListener(this);

        company_progress = (ImageView) findViewById(R.id.company_progress);
        company_progress.getLayoutParams().width =  (deviceWidth /2);
        company_progress.requestLayout();

        next = (MyTextView) findViewById(R.id.next);
        next.setOnClickListener(this);


        img1 = (ImageView) findViewById(R.id.img1);
        img1.setOnClickListener(this);
        img2 = (ImageView) findViewById(R.id.img2);
        img2.setOnClickListener(this);

        img4 = (ImageView) findViewById(R.id.img4);
        img4.setOnClickListener(this);
        img5 = (ImageView) findViewById(R.id.img5);
        img5.setOnClickListener(this);

        img11 = (ImageView) findViewById(R.id.img11);
        img11.setOnClickListener(this);
        img22 = (ImageView) findViewById(R.id.img22);
        img22.setOnClickListener(this);
        img33 = (ImageView) findViewById(R.id.img33);
        img33.setOnClickListener(this);
        img44 = (ImageView) findViewById(R.id.img44);
        img44.setOnClickListener(this);
        img55 = (ImageView) findViewById(R.id.img55);
        img55.setOnClickListener(this);
        img66 = (ImageView) findViewById(R.id.img66);
        img66.setOnClickListener(this);


        String business = pref.getString("company_business","NA");


        if(business.equalsIgnoreCase("3"))
        {
            img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_active));
        }
        else if(business.equalsIgnoreCase("1"))
        {
            img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_active));
        }

        else if(business.equalsIgnoreCase("2"))
        {
            img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_active));
        }
        else if(business.equalsIgnoreCase("4"))
        {
            img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));

            buss_type_other.setVisibility(View.VISIBLE);
            buss_type_other.setText(pref.getString("company_business_other", ""));
        }


        String comp_buss_name = pref.getString("comp_buss_name","NA");
        String company_landline_no = pref.getString("company_landline_no","NA");

        if(!comp_buss_name.equalsIgnoreCase("NA"))
        {
            buss_name.setText(comp_buss_name);
        }
        if(!company_landline_no.equalsIgnoreCase("NA"))
        {
            landline_no.setText(company_landline_no);
        }




        String type = pref.getString("company_type","NA");


        if(type.equalsIgnoreCase("5"))
        {
            img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_active));
        }
        else if(type.equalsIgnoreCase("1"))
        {
            img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_active));
        }
        else if(type.equalsIgnoreCase("2"))
        {
            img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_active));
        }
        else if(type.equalsIgnoreCase("3"))
        {
            img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_active));
        }
        else if(type.equalsIgnoreCase("6"))
        {
            img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_active));
        }
        else if(type.equalsIgnoreCase("4"))
        {
            img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));

            comp_type_other.setVisibility(View.VISIBLE);
            comp_type_other.setText(pref.getString("company_type_other", ""));
        }










    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.backbutton:
                finish();


                break;

            case R.id.next:


                String comp_buss_name = buss_name.getText().toString();
                comp_buss_name = comp_buss_name.trim();
                String comp_landlin_no= landline_no.getText().toString();
                comp_landlin_no = comp_landlin_no.trim();

                if(comp_buss_name.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_bussiness_name), Toast.LENGTH_SHORT).show();
                }

                    else {
                    if((comp_landlin_no.length() > 0 && comp_landlin_no.length() < 6) || comp_landlin_no.length() >12)
                    {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_landline), Toast.LENGTH_SHORT).show();
                    }

                    else {

                        String company_business = pref.getString("company_business", "NA");

                        if (company_business.equalsIgnoreCase("NA")) {

                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_business_type), Toast.LENGTH_SHORT).show();

                        } else {

                            String company_type = pref.getString("company_type","NA");

                            if(company_type.equalsIgnoreCase("NA")) {

                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.please_select_company_type),Toast.LENGTH_SHORT).show();

                            }
                            else {


                                if(company_business.equalsIgnoreCase("4"))
                                {
                                    String buss_type_other_text = buss_type_other.getText().toString().trim();


                                    if (buss_type_other_text.equalsIgnoreCase("")) {

                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_business_type), Toast.LENGTH_SHORT).show();

                                    } else {



                                        if(company_type.equalsIgnoreCase("4"))
                                        {
                                            String comp_type_other_text = comp_type_other.getText().toString().trim();


                                            if (comp_type_other_text.equalsIgnoreCase("")) {

                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_company_type), Toast.LENGTH_SHORT).show();

                                            } else {


                                                editor.putString("comp_buss_name", comp_buss_name);
                                                editor.putString("company_landline_no", comp_landlin_no);

                                                editor.putString("company_type_other", comp_type_other_text);
                                                editor.putString("company_business_other", buss_type_other_text);

                                                editor.commit();

                                                Intent i = new Intent(CompanyBusiness.this, CompanyAddress.class);
                                                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                                i.putExtra("first_name", first_name);
                                                i.putExtra("last_name", last_name);
                                                i.putExtra("email_id", email_id);
                                                i.putExtra("image_url", image_url);
                                                i.putExtra("account_type", account_type);

                                                i.putExtra("company_type_other", comp_type_other_text);
                                                i.putExtra("company_business_other", buss_type_other_text);


                                                i.putExtra("company_business", company_business);
                                                i.putExtra("comp_buss_name", comp_buss_name);
                                                i.putExtra("comp_landlin_no", comp_landlin_no);
                                                Log.d("mmm", "comp_landlin_no " + comp_landlin_no);

                                                i.putExtra("company_type", company_type);

                                                startActivity(i);



                                            }



                                        }
                                        else
                                        {

                                            editor.putString("comp_buss_name", comp_buss_name);
                                            editor.putString("company_landline_no", comp_landlin_no);

                                            editor.putString("company_business_other", buss_type_other_text);

                                            editor.commit();

                                            Intent i = new Intent(CompanyBusiness.this, CompanyAddress.class);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                            i.putExtra("first_name", first_name);
                                            i.putExtra("last_name", last_name);
                                            i.putExtra("email_id", email_id);
                                            i.putExtra("image_url", image_url);
                                            i.putExtra("account_type", account_type);

                                            i.putExtra("company_type_other", "");
                                            i.putExtra("company_business_other", buss_type_other_text);


                                            i.putExtra("company_business", company_business);
                                            i.putExtra("comp_buss_name", comp_buss_name);
                                            i.putExtra("comp_landlin_no", comp_landlin_no);
                                            Log.d("mmm", "comp_landlin_no " + comp_landlin_no);

                                            i.putExtra("company_type", company_type);

                                            startActivity(i);

                                        }





                                    }



                                    }
                                else if(company_type.equalsIgnoreCase("4"))
                                {
                                    String comp_type_other_text = comp_type_other.getText().toString().trim();


                                    if (comp_type_other_text.equalsIgnoreCase("")) {

                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_company_type), Toast.LENGTH_SHORT).show();

                                    } else {


                                        editor.putString("comp_buss_name", comp_buss_name);
                                        editor.putString("company_landline_no", comp_landlin_no);
                                        editor.putString("company_type_other", comp_type_other_text);


                                        editor.commit();

                                        Intent i = new Intent(CompanyBusiness.this, CompanyAddress.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                        i.putExtra("first_name", first_name);
                                        i.putExtra("last_name", last_name);
                                        i.putExtra("email_id", email_id);
                                        i.putExtra("image_url", image_url);
                                        i.putExtra("account_type", account_type);

                                        i.putExtra("company_type_other", comp_type_other_text);
                                        i.putExtra("company_business_other", "");


                                        i.putExtra("company_business", company_business);
                                        i.putExtra("comp_buss_name", comp_buss_name);
                                        i.putExtra("comp_landlin_no", comp_landlin_no);
                                        Log.d("mmm", "comp_landlin_no " + comp_landlin_no);

                                        i.putExtra("company_type", company_type);

                                        startActivity(i);



                                    }



                                }
                                else {
                                    editor.putString("comp_buss_name", comp_buss_name);
                                    editor.putString("company_landline_no", comp_landlin_no);
                                    editor.commit();

                                    Intent i = new Intent(CompanyBusiness.this, CompanyAddress.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    i.putExtra("first_name", first_name);
                                    i.putExtra("last_name", last_name);
                                    i.putExtra("email_id", email_id);
                                    i.putExtra("image_url", image_url);
                                    i.putExtra("account_type", account_type);

                                    i.putExtra("company_type_other", "");
                                    i.putExtra("company_business_other", "");


                                    i.putExtra("company_business", company_business);
                                    i.putExtra("comp_buss_name", comp_buss_name);
                                    i.putExtra("comp_landlin_no", comp_landlin_no);
                                    Log.d("mmm", "comp_landlin_no " + comp_landlin_no);

                                    i.putExtra("company_type", company_type);

                                    startActivity(i);
                                }
                        }
                        }
                    }
                }
                break;

            case R.id.img1:

                /*img1.setImageDrawable(getResources()
                        .getDrawable(R.drawable.manufacturing_active));*/
                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_active));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                editor.putString("company_business", "3");
                editor.putString("company_business_other", "");
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");

                editor.commit();

                break;

            case R.id.img2:

                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_active));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                editor.putString("company_business", "1");
                editor.putString("company_business_other", "");
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");
                editor.commit();

                break;



            case R.id.img4:
                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_active));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                editor.putString("company_business", "2");
                editor.putString("company_business_other", "");
                buss_type_other.setVisibility(View.GONE);
                buss_type_other.setText("");
                editor.commit();

                break;

            case R.id.img5:

                img1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.manufacturing_inactive));
                img2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logstick_inactive));

                img4.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.warehouse_inactive));
                img5.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));
                editor.putString("company_business", "4");
                editor.putString("company_business_other", "");
                buss_type_other.setVisibility(View.VISIBLE);
                buss_type_other.setText("");
                editor.commit();

                break;



            case R.id.img11:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_active));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                editor.putString("company_type", "5");

                editor.putString("company_type_other", "");
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");

                editor.commit();


                break;

            case R.id.img22:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_active));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                editor.putString("company_type", "1");
                editor.putString("company_type_other", "");
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                editor.commit();

                break;

            case R.id.img33:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_active));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                editor.putString("company_type", "2");
                editor.putString("company_type_other", "");
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                editor.commit();


                break;

            case R.id.img44:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_active));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                editor.putString("company_type", "3");
                editor.putString("company_type_other", "");
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                editor.commit();


                break;

            case R.id.img55:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_active));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_inactive));
                editor.putString("company_type", "4");
                editor.putString("company_type_other", "");
                comp_type_other.setVisibility(View.VISIBLE);
                comp_type_other.setText("");
                editor.commit();

                exampleScrollView.post(new Runnable() {

                    @Override
                    public void run() {
                        exampleScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });


                break;


            case R.id.img66:

                img11.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.public_inactive));
                img22.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.propeitary_inactive));
                img33.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partnership_inactive));
                img44.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.private_inactive));
                img55.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.others_inactive));
                img66.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.trade_active));
                editor.putString("company_type", "6");
                editor.putString("company_type_other", "");
                comp_type_other.setVisibility(View.GONE);
                comp_type_other.setText("");
                editor.commit();


                break;





        }


    }

   /* @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }*/

}
