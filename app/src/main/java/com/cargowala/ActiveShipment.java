package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cargowala.Models.GeoLocationResponce;
import com.cargowala.helper.CommonUtils;
import com.cargowala.retrofit.RestClient;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.RetrofitError;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class ActiveShipment extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton, map_image;

    String jsonObjectPackageData;
    MyTextView show_hide_details;
    boolean showDetails = true;
    LinearLayout detail_layout, truck_track_layout;

    String id1, shipment_id1, load_type1, from_city1, from_state1, from_address1, to_city1, to_state1, to_address1,
            shipment_url1, priority_delivery1, transit_date1, transit_time1, truck_category1, truck_name1, truck_quantity1,
            shipment_status1, today_date, trucker_name, trucker_number, trucker_rating;

    int ratingBarImages2[] = {R.drawable.zero_rating, R.drawable.point_five_rating,
            R.drawable.one__rating, R.drawable.one_point_five_rating, R.drawable.two_rating,
            R.drawable.two_point_five_rating, R.drawable.three_rating, R.drawable.three_point_five_rating,
            R.drawable.four_rating, R.drawable.four_point_five_rating, R.drawable.five_rating};


    MyTextViewSemi shipper_name_text, shipper_name_number;
    ImageView t_rating_home;

    LinearLayout trucker_details;

    SweetAlertDialog pDialog;


    MyTextViewSemi from_place_text, to_place_text, load_type_priority, no_of_trucks_text, date_text, time_text, order_id_text,
            truck_name_text;

    MyTextView shipment_status_text, reschedule_button;

    ImageView truck_image;

    MyTextView cancel_order;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;

    MyTextViewSemi view_details_text;

    View line_view;

    boolean createAssigned = true;

    CardView card_view1;
    String fcmToken="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_shipment);

        Intent intent = getIntent();
        jsonObjectPackageData = intent.getExtras().getString("orderId");


        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        try {
            fcmToken = pref.getString("fcmToken","");
            /*if(fcmToken.equalsIgnoreCase("") || fcmToken.isEmpty() || fcmToken == null){
                if(FirebaseInstanceId.getInstance().getToken() != null){
                    fcmToken=FirebaseInstanceId.getInstance().getToken();
                }else {
                    Log.e("getToken", "null");
                }
            }else {

            }*/
        } catch (Exception e) {
            fcmToken="";
        }
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        Log.d("aaaa ", "" + jsonObjectPackageData);

        try {

            JSONObject jsonObjectPackageData1 = new JSONObject(jsonObjectPackageData);

            Log.d("My App", jsonObjectPackageData1.toString());


            id1 = jsonObjectPackageData1.getString("id");
            shipment_id1 = jsonObjectPackageData1.getString("shipment_id");
            load_type1 = jsonObjectPackageData1.getString("load_type");
            from_city1 = jsonObjectPackageData1.getString("from_city");
            from_state1 = jsonObjectPackageData1.getString("from_state");
            from_address1 = jsonObjectPackageData1.getString("from_address");
            to_city1 = jsonObjectPackageData1.getString("to_city");
            to_state1 = jsonObjectPackageData1.getString("to_state");
            to_address1 = jsonObjectPackageData1.getString("to_address");
            shipment_url1 = jsonObjectPackageData1.getString("shipment_url");
            priority_delivery1 = jsonObjectPackageData1.getString("priority_delivery");
            transit_date1 = jsonObjectPackageData1.getString("transit_date");
            transit_time1 = jsonObjectPackageData1.getString("transit_time");
            truck_category1 = jsonObjectPackageData1.getString("truck_category");
            truck_name1 = jsonObjectPackageData1.getString("truck_name");
            truck_quantity1 = jsonObjectPackageData1.getString("truck_quantity");
            shipment_status1 = jsonObjectPackageData1.getString("shipment_status");


            today_date = jsonObjectPackageData1.getString("today_date");

            trucker_name = jsonObjectPackageData1.getString("trucker_name");
            trucker_number = jsonObjectPackageData1.getString("trucker_number");
            //    trucker_alternate_number = jsonObjectPackageData1.getString("trucker_alternate_number");
            //     trucker_business_name = jsonObjectPackageData1.getString("trucker_business_name");
            trucker_rating = jsonObjectPackageData1.getString("trucker_rating");


        } catch (Throwable t) {
            Log.e("My App", "Could not parse malformed JSON:");
        }


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);


        backbutton.requestLayout();

        card_view1 = (CardView) findViewById(R.id.card_view1);


        line_view = (View) findViewById(R.id.line_view);

        map_image = (ImageView) findViewById(R.id.map_image);
        map_image.getLayoutParams().height = (int) (deviceWidth / 1.1);
        map_image.requestLayout();

        show_hide_details = (MyTextView) findViewById(R.id.show_hide_details);
        show_hide_details.setOnClickListener(this);

        cancel_order = (MyTextView) findViewById(R.id.cancel_order);
        cancel_order.setOnClickListener(this);

        trucker_details = (LinearLayout) findViewById(R.id.trucker_details);

        detail_layout = (LinearLayout) findViewById(R.id.detail_layout);
        truck_track_layout = (LinearLayout) findViewById(R.id.truck_track_layout);


        shipment_status_text = (MyTextView) findViewById(R.id.shipment_status_text);

        view_details_text = (MyTextViewSemi) findViewById(R.id.view_details_text);
        String text = "<font color=#585858><b><u>"+getResources().getString(R.string.view_item_details)+"</u></b></font>";
        view_details_text.setText(Html.fromHtml(text));
        view_details_text.setOnClickListener(this);


        truck_image = (ImageView) findViewById(R.id.truck_image);

        from_place_text = (MyTextViewSemi) findViewById(R.id.from_place_text);
        to_place_text = (MyTextViewSemi) findViewById(R.id.to_place_text);
        load_type_priority = (MyTextViewSemi) findViewById(R.id.load_type_priority);
        no_of_trucks_text = (MyTextViewSemi) findViewById(R.id.no_of_trucks_text);
        date_text = (MyTextViewSemi) findViewById(R.id.date_text);
        time_text = (MyTextViewSemi) findViewById(R.id.time_text);
        order_id_text = (MyTextViewSemi) findViewById(R.id.order_id_text);
        truck_name_text = (MyTextViewSemi) findViewById(R.id.truck_name_text);


        reschedule_button = (MyTextView) findViewById(R.id.reschedule_button);
        reschedule_button.setOnClickListener(this);


        if (truck_category1.equalsIgnoreCase("1")) {
            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.small_grey_truck));
        } else if (truck_category1.equalsIgnoreCase("2")) {
            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
        } else if (truck_category1.equalsIgnoreCase("3")) {
            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.large_grey_truck));
        } else {
            Log.d("error", "unreachable code");
        }


        from_place_text.setText(from_address1);
        to_place_text.setText(to_address1);

        if (priority_delivery1.equalsIgnoreCase("Yes")) {
            load_type_priority.setText(load_type1 + " - "+getResources().getString(R.string.priority));
        } else {
            load_type_priority.setText(load_type1);
        }
        date_text.setText(transit_date1);
        time_text.setText(transit_time1);
        no_of_trucks_text.setText(truck_quantity1);
        truck_name_text.setText(truck_name1);

        order_id_text.setText(getResources().getString(R.string.order_id)+"- " + shipment_id1);


        shipment_status_text.setText(shipment_status1);

        if (shipment_status1.equalsIgnoreCase("pending")) {
            line_view.setVisibility(View.VISIBLE);

            cancel_order.setVisibility(View.VISIBLE);


            reschedule_button.setVisibility(View.VISIBLE);

        } else if (shipment_status1.equalsIgnoreCase("In Transit")) {

            trucker_details.setVisibility(View.VISIBLE);


            if (isOnline()) {
                card_view1.setVisibility(View.VISIBLE);
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();
                makeTrackShipmentReq();
            } else {


                Toast.makeText(ActiveShipment.this,
                        getResources().getString(R.string.check_your_network),
                        Toast.LENGTH_SHORT).show();
            }


        }

        Glide.with(ActiveShipment.this)
                .load(shipment_url1)
                .centerCrop()
                .placeholder(R.drawable.loading_screen_square)
                .error(R.drawable.unable_upload_square)
                .into(map_image);
       /* Picasso.with(getApplicationContext())
                .load(shipment_url1)
                .placeholder(R.drawable.loading_screen_square)
                .error(R.drawable.unable_upload_square)
                *//*.resize(deviceWidth, deviceHeight).centerInside()*//*.into(map_image);*/


        shipper_name_text = (MyTextViewSemi) findViewById(R.id.shipper_name_text);
        shipper_name_number = (MyTextViewSemi) findViewById(R.id.shipper_name_number);
        t_rating_home = (ImageView) findViewById(R.id.t_rating_home);


        shipper_name_text.setText(trucker_name);
        shipper_name_number.setText("+91 " + trucker_number);


        float ratingValue = Float.parseFloat(trucker_rating);

        int ratingNumber = (int) ((ratingValue * 10) / 5);


        t_rating_home.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), ratingBarImages2[ratingNumber]));






















/*

        for(int i = 0; i<9; i++)
        {


            CardView card_view = new CardView(
                    getApplicationContext());
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                   */
/* (int)(deviceHeight/1.5)*//*

            );
            //  card_view.setRadius(10f);
            //left top right bottom

          */
/* if(i==0)
            {
                params1.setMargins(0, 0, 0, 0);
            }
            else if(i==2)
            {
                params1.setMargins(0, 0, 0, 0);
            }
            else {
                params1.setMargins(0, 20, 0, 0);
            }*//*


            if(i!=0 )
            {
                params1.setMargins(0, 20, 0, 0);
            }

            card_view.setLayoutParams(params1);





            LinearLayout ll1 = new LinearLayout(
                    ActiveShipment.this);
            ll1.setOrientation(LinearLayout.VERTICAL);

            ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);


            ll1.setLayoutParams(params2);

            card_view.addView(ll1);



            MyTextView truck_no = new MyTextView(
                    getApplicationContext());
            truck_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            truck_no.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);


            truck_no.setLayoutParams(params4);
            truck_no.setText("#"+(i+1));
            truck_no.setPadding(20, 5, 20, 5);
            ll1.addView(truck_no);



            MyTextView vehicle_line_one = new MyTextView(
                    getApplicationContext());
            vehicle_line_one.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            vehicle_line_one.setGravity(Gravity.CENTER);

            LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            vehicle_line_one.setLayoutParams(params5);
            vehicle_line_one.setText("No Vehicle");
            vehicle_line_one.setPadding(5, 4, 5, 1);
            ll1.addView(vehicle_line_one);

            MyTextView vehicle_line_two = new MyTextView(
                    getApplicationContext());
            vehicle_line_two.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            vehicle_line_two.setGravity(Gravity.CENTER);


            vehicle_line_two.setLayoutParams(params5);
            vehicle_line_two.setText("is assigned for your shipment");
            vehicle_line_two.setPadding(5, 1, 5, 0);
            ll1.addView(vehicle_line_two);



            View line = new View(
                    getApplicationContext());
            line.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


            LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                 1);

            params6.setMargins(20, 20, 20, 20);
            line.setLayoutParams(params6);


            ll1.addView(line);




            LinearLayout ll2 = new LinearLayout(
                    ActiveShipment.this);
            ll2.setOrientation(LinearLayout.HORIZONTAL);


            LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

            params7.setMargins(20, 0, 20, 20);
            ll2.setLayoutParams(params7);

            ll1.addView(ll2);




            CircleImageView driver_image = new CircleImageView(
                    getApplicationContext());

            driver_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_user));

            LinearLayout.LayoutParams params8 = new LinearLayout.LayoutParams(
                    deviceWidth/4,
                    deviceWidth/4);

            driver_image.setLayoutParams(params8);

            ll2.addView(driver_image);




            LinearLayout ll3 = new LinearLayout(
                    ActiveShipment.this);
            ll3.setOrientation(LinearLayout.VERTICAL);


            LinearLayout.LayoutParams params9 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
            params9.gravity= Gravity.CENTER;

            ll3.setLayoutParams(params9);

            ll2.addView(ll3);

            MyTextView driver_name = new MyTextView(
                    getApplicationContext());
            driver_name.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            driver_name.setGravity(Gravity.CENTER);
            LinearLayout.LayoutParams params11 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);


            driver_name.setLayoutParams(params11);
            driver_name.setText("No Driver assigned");
            driver_name.setPadding(5, 0, 5, 0);
            ll3.addView(driver_name);



            ImageView rating_image = new ImageView(
                    getApplicationContext());

            rating_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.zero_rating));
            rating_image.setAdjustViewBounds(true);


            LinearLayout.LayoutParams params10 = new LinearLayout.LayoutParams(
                    deviceWidth/3,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params10.gravity= Gravity.CENTER;

            params10.setMargins(0, 10, 0, 10);
            rating_image.setLayoutParams(params10);

            ll3.addView(rating_image);



            MyTextView driver_phone_no = new MyTextView(
                    getApplicationContext());
          //  driver_phone_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            driver_phone_no.setGravity(Gravity.CENTER);


            driver_phone_no.setLayoutParams(params11);

            String text2 = "<font color=#ff5400  ><u>+91 9888766654</u></font>";
            driver_phone_no.setText(Html.fromHtml(text2));
           // driver_phone_no.setText("No Driver assigned");
            driver_phone_no.setPadding(5, 0, 5, 0);
            ll3.addView(driver_phone_no);



            detail_layout.addView(card_view);





      */
/*      if(true && i == 8 )
            {

                for(int j = 0; j<9; j++) {
                    MyTextView truck_detail_text = new MyTextView(
                            getApplicationContext());


                    LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
                    params12.setMargins(0,20,0,0);

                    truck_detail_text.setLayoutParams(params12);

                    String text3 = "<font color=#ff5400  ><b>Truck 1 </b></font><font color=#585858  >with no </font>" +
                            "<font color=#ff5400  ><b>HR 05Z 0069 </b></font>\n<font color=#585858  >has reached </font>" +
                            "<font color=#ff5400  ><b>Chandigarh </b></font> \n<font color=#585858  >at </font>   <font color=#ff5400  ><b>10:00 AM</b></font>";
                    truck_detail_text.setText(Html.fromHtml(text3));

                    truck_detail_text.setPadding(5, 0, 5, 0);
                    truck_track_layout.addView(truck_detail_text);

                }

            }*//*




        }
*/


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:

                finish();

                break;


            case R.id.popup_button_yes2:
                dialog1.dismiss();

                if (isOnline()) {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText(getResources().getString(R.string.loading));
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeRemoveOrderReq();
                } else {


                    Toast.makeText(ActiveShipment.this,
                            getResources().getString(R.string.check_your_network),
                            Toast.LENGTH_SHORT).show();
                }


                break;


            case R.id.popup_button_no2:
                dialog1.dismiss();


                break;


            case R.id.show_hide_details:

                if (showDetails) {
                    showDetails = false;
                    detail_layout.setVisibility(View.VISIBLE);
                    show_hide_details.setText(getResources().getString(R.string.hide_assigned_vehicles));


                    if (createAssigned) {
                        createAssigned = false;


                        if (isOnline()) {


                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makeGetAssignListReq();


                        } else {


                            Toast.makeText(ActiveShipment.this,
                                    getResources().getString(R.string.check_your_network),
                                    Toast.LENGTH_SHORT).show();
                        }

                    }


                } else {
                    showDetails = true;
                    detail_layout.setVisibility(View.GONE);
                    show_hide_details.setText(getResources().getString(R.string.show_assigned_vehicles));
                }

                break;


            case R.id.cancel_order:


            /*    if(isOnline())
                {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText("Loading");
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeRemoveOrderReq();
                }
                else {



                    Toast.makeText(ActiveShipment.this,
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }
*/

                initiateyYesNoWindow(getResources().getString(R.string.are_you_sure_you_want_to_cancel),
                        true, getResources().getString(R.string.cancel_order), getResources().getString(R.string.yes), getResources().getString(R.string.no));


                break;


            case R.id.view_details_text:


                Intent i2 = new Intent(ActiveShipment.this, ItemsDetail.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                i2.putExtra("orderId1", id1);


                startActivity(i2);

                break;


            case R.id.reschedule_button:


                Intent i4 = new Intent(ActiveShipment.this, RescheduleOrder.class);
                i4.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i4.putExtra("orderId1", id1);
                i4.putExtra("date", transit_date1);


                Log.d("main ", "transit_date1 " + transit_date1 + " , today_date " + today_date);

                if (transit_date1.equalsIgnoreCase(today_date)) {
                    i4.putExtra("sameDate", true);
                } else {
                    i4.putExtra("sameDate", false);
                }

                startActivity(i4);


                break;


            case R.id.popup_button:

                if (status_code.equalsIgnoreCase("200")) {
                    dialog.dismiss();
                    finish();

                } else if (status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910")) {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook")) {


                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    } else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google")) {
                        isGoogle = true;
                    }


                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i3 = new Intent(ActiveShipment.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i3.putExtra("wasGoogleLoggedIn", isGoogle);
                    startActivity(i3);


                    finish();

                } else {
                    dialog.dismiss();


                }


                break;


        }


    }


    String res4;


    StringRequest strReq4;

    public String makeRemoveOrderReq() {

        String url = getResources().getString(R.string.base_url) + "user/cancel-shipment";
        Log.d("regg", "2");
        strReq4 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res4 = response.toString();
                        checkRemoveOrderResponse(res4);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res4 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("shipment_id", id1);


                    params.put("_id", pref.getString("userId", "Something Wrong"));
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));

                    Log.e("register", params.toString());


                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq4, "1");


        return null;
    }

    public void checkRemoveOrderResponse(String response) {
        pDialog.dismiss();
        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);

                if (status_code.equalsIgnoreCase("200")) {

                    editor.putBoolean("hitActiveShipmentService", true);
                    editor.commit();


                    initiatePopupWindow(getResources().getString(R.string.order_has_been_cancelled),
                            false, getResources().getString(R.string.success), getResources().getString(R.string.ok));


                    //     hit service true here for bid board


                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText) {
        try {

            dialog = new Dialog(ActiveShipment.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if (isAlert) {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    Dialog dialog1;

    private void initiateyYesNoWindow(String message, Boolean isAlert, String heading, String buttonTextYes, String buttonTextNo) {
        try {

            dialog1 = new Dialog(ActiveShipment.this);
            dialog1.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.yesnopopup);
            dialog1.setCanceledOnTouchOutside(false);
            MyTextView popup_message2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_message2);
            popup_message2.setText(message);


            ImageView popup_image2 = (ImageView) dialog1
                    .findViewById(R.id.popup_image2);
            if (isAlert) {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_heading2);
            popup_heading2.setText(heading);


            MyTextView popup_button_yes2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_yes2);
            popup_button_yes2.setText(buttonTextYes);
            popup_button_yes2.setOnClickListener(this);

            MyTextView popup_button_no2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_no2);
            popup_button_no2.setText(buttonTextNo);
            popup_button_no2.setOnClickListener(this);

            dialog1.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    String res1;
    StringRequest strReq1;
    String status_code = "0";

    public String makeGetAssignListReq() {

        String url = getResources().getString(R.string.base_url) + "user/view-assigned-list";
        Log.d("regg", "2");
        strReq1 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res1 = response.toString();
                        checkGetAssignListResponse(res1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res1 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                    params.put("shipment_id", id1);
                    params.put("_id", pref.getString("userId", "Something Wrong"));
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));


                    Log.e("register", params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq1, "37");


        return null;
    }

    public void checkGetAssignListResponse(String response) {
        pDialog.dismiss();
        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);

                if (status_code.equalsIgnoreCase("200")) {

                    String response1 = reader.getString("response");

                    JSONArray jsonArr1 = new JSONArray(response1);


                    for (int i = 0; i < jsonArr1.length(); i++) {


                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);


                        Log.d("jsonObjectPackageData1 ", jsonObjectPackageData1.toString());


                        String vehicle_num = jsonObjectPackageData1.getString("vehicle_num");

                        String vehicle_id = jsonObjectPackageData1.getString("vehicle_id");
                        String driver_id = jsonObjectPackageData1.getString("driver_id");
                        String driver_name1 = jsonObjectPackageData1.getString("driver_name");
                        String driver_num = jsonObjectPackageData1.getString("driver_num");
                        String driver_pic = jsonObjectPackageData1.getString("driver_pic");


                        // adding my static code here for dynamic

                        CardView card_view = new CardView(
                                getApplicationContext());
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                   /* (int)(deviceHeight/1.5)*/
                        );
                        //  card_view.setRadius(10f);
                        //left top right bottom

          /* if(i==0)
            {
                params1.setMargins(0, 0, 0, 0);
            }
            else if(i==2)
            {
                params1.setMargins(0, 0, 0, 0);
            }
            else {
                params1.setMargins(0, 20, 0, 0);
            }*/

                        if (i != 0) {
                            params1.setMargins(0, 20, 0, 0);
                        }

                        card_view.setLayoutParams(params1);


                        LinearLayout ll1 = new LinearLayout(
                                ActiveShipment.this);
                        ll1.setOrientation(LinearLayout.VERTICAL);

                        ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

                        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


                        ll1.setLayoutParams(params2);

                        card_view.addView(ll1);


                        MyTextView truck_no = new MyTextView(
                                getApplicationContext());
                        truck_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        truck_no.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);


                        truck_no.setLayoutParams(params4);
                        truck_no.setText("#" + (i + 1));
                        truck_no.setPadding(20, 5, 20, 5);
                        ll1.addView(truck_no);


                        MyTextView vehicle_line_one = new MyTextView(
                                getApplicationContext());
                        vehicle_line_one.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        vehicle_line_one.setGravity(Gravity.CENTER);

                        LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        vehicle_line_one.setLayoutParams(params5);
                        if (vehicle_num.equalsIgnoreCase("")) {
                            vehicle_line_one.setText(getResources().getString(R.string.no_vehicle));
                        } else {
                            vehicle_line_one.setText(vehicle_num);
                        }
                        vehicle_line_one.setPadding(5, 4, 5, 1);
                        ll1.addView(vehicle_line_one);

                        MyTextView vehicle_line_two = new MyTextView(
                                getApplicationContext());
                        vehicle_line_two.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        vehicle_line_two.setGravity(Gravity.CENTER);


                        vehicle_line_two.setLayoutParams(params5);
                        vehicle_line_two.setText(getResources().getString(R.string.is_assigned_for_your_shipment));
                        vehicle_line_two.setPadding(5, 1, 5, 0);
                        ll1.addView(vehicle_line_two);


                        View line = new View(
                                getApplicationContext());
                        line.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                        LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                1);

                        params6.setMargins(20, 20, 20, 20);
                        line.setLayoutParams(params6);


                        ll1.addView(line);


                        LinearLayout ll2 = new LinearLayout(
                                ActiveShipment.this);
                        ll2.setOrientation(LinearLayout.HORIZONTAL);


                        LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        params7.setMargins(20, 0, 20, 20);
                        ll2.setLayoutParams(params7);

                        ll1.addView(ll2);


                        CircleImageView driver_image = new CircleImageView(
                                getApplicationContext());

                        driver_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_user));

                        if (!driver_pic.equalsIgnoreCase("")) {
                            Picasso.with(getApplicationContext())
                                    .load(driver_pic)
                                    .placeholder(R.drawable.default_user)
                                    .error(R.drawable.default_user)
				/*.resize(deviceWidth, deviceHeight).centerInside()*/.into(driver_image);
                        }

                        LinearLayout.LayoutParams params8 = new LinearLayout.LayoutParams(
                                deviceWidth / 4,
                                deviceWidth / 4);

                        driver_image.setLayoutParams(params8);

                        ll2.addView(driver_image);


                        LinearLayout ll3 = new LinearLayout(
                                ActiveShipment.this);
                        ll3.setOrientation(LinearLayout.VERTICAL);


                        LinearLayout.LayoutParams params9 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params9.gravity = Gravity.CENTER;

                        ll3.setLayoutParams(params9);

                        ll2.addView(ll3);

                        MyTextView driver_name = new MyTextView(
                                getApplicationContext());
                        driver_name.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        driver_name.setGravity(Gravity.CENTER);
                        LinearLayout.LayoutParams params11 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


                        driver_name.setLayoutParams(params11);
                        if (driver_name1.equalsIgnoreCase("")) {
                            driver_name.setText(getResources().getString(R.string.no_driver));
                        } else {
                            driver_name.setText(driver_name1);
                        }
                        driver_name.setPadding(5, 0, 5, 0);
                        ll3.addView(driver_name);


                        MyTextView driver_name2 = new MyTextView(
                                getApplicationContext());
                        driver_name2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        driver_name2.setGravity(Gravity.CENTER);
                        LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


                        driver_name2.setLayoutParams(params12);
                        driver_name2.setText(getResources().getString(R.string.is_assigned));


                        driver_name2.setPadding(5, 0, 5, 0);
                        ll3.addView(driver_name2);


                        MyTextView driver_phone_no = new MyTextView(
                                getApplicationContext());
                        //  driver_phone_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        driver_phone_no.setGravity(Gravity.CENTER);


                        driver_phone_no.setLayoutParams(params11);

                        String text2 = "";

                        if (!driver_num.equalsIgnoreCase("")) {
                            text2 = "<font color=#ff5400  ><u>+91 " + driver_num + "</u></font>";
                        }
                        driver_phone_no.setText(Html.fromHtml(text2));

                        // driver_phone_no.setText("No Driver assigned");
                        driver_phone_no.setPadding(5, 0, 5, 0);


                        if (shipment_status1.equalsIgnoreCase(getResources().getString(R.string.in_transit))) {
                            ll3.addView(driver_phone_no);
                        }


                        detail_layout.addView(card_view);


                    }


                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    String res3;


    StringRequest strReq3;

    public String makeTrackShipmentReq() {

        String url = getResources().getString(R.string.base_url) + "user/track-shipment";
        Log.d("regg", "2");
        strReq3 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res3 = response.toString();
                        checkTrackShipmentResponse(res3);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res3 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("shipment_id", id1);


                    params.put("_id", pref.getString("userId", "Something Wrong"));
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));

                    Log.e("register", params.toString());


                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq3, "1");


        return null;
    }

    public void checkTrackShipmentResponse(String response) {
        pDialog.dismiss();
        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);

                if (status_code.equalsIgnoreCase("200")) {


                    String response1 = reader.getString("response");

                    JSONObject jsonobj = new JSONObject(response1);


                    String location = jsonobj.getString("location");
                    String truck = jsonobj.getString("truck");


                    JSONArray jsonArr = new JSONArray(location);


                   /* for (int i = 0; i < jsonArr.length(); i++) {*/


                    JSONObject jsonObjectPackageData = jsonArr.getJSONObject(0);


                    String city = jsonObjectPackageData.getString("city");
                    String start_time = jsonObjectPackageData.getString("start_time");


                    //  String address = jsonObjectPackageData.getString("address");

                    //    String state = jsonObjectPackageData.getString("state");

                    //    String latitude = jsonObjectPackageData.getString("latitude");


                    //     String longitude = jsonObjectPackageData.getString("longitude");

                    //     String end_time = jsonObjectPackageData.getString("end_time");

                    /*}
*/


                  /*  Calendar calendar = Calendar.getInstance();
                    TimeZone tz = TimeZone.getDefault();
                    calendar.setTimeInMillis((long)(Long.parseLong(start_time)*1000L) );
                    calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm a");
                    sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                    Date currenTimeZone = (Date) calendar.getTime();

                    String current_text = sdf.format(currenTimeZone);

                    Log.d("current_time ", "current_time " + current_text);*/

                    try {
                        Calendar calendar = Calendar.getInstance();
                        TimeZone tz = TimeZone.getDefault();
                       // calendar.setTimeInMillis((long) (Long.parseLong(start_time) * 1000L));
                        calendar.setTimeInMillis(Long.valueOf(start_time));
                        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                        sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                        Date currenTimeZone = (Date) calendar.getTime();

                        String current_text = sdf.format(currenTimeZone);

                        Log.d("current_time ", "current_time " + current_text);


                        Calendar calendar1 = Calendar.getInstance();
                        TimeZone tz1 = TimeZone.getDefault();
                     //   calendar1.setTimeInMillis((long) (Long.parseLong(start_time) * 1000L));
                        calendar1.setTimeInMillis(Long.valueOf(start_time));
                        calendar1.add(Calendar.MILLISECOND, tz1.getOffset(calendar.getTimeInMillis()));

                        SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
                        sdf1.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                        Date currenTimeZone1 = (Date) calendar1.getTime();

                        String current_text1 = sdf1.format(currenTimeZone1);

                        Log.d("current_time1 ", "current_time1 " + current_text1);


                        MyTextView truck_start_details = new MyTextView(
                                getApplicationContext());


                        LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        truck_start_details.setLayoutParams(params12);

                        String text3 = "<font color=#585858  >"+getResources().getString(R.string.shipment_started_on)+" </font><font color=#ff5400  ><b>" + current_text + "</b> </font>" +
                                "<font color=#585858  > "+getResources().getString(R.string.at)+" </font><font color=#ff5400  ><b>" + current_text1 + "</b></font>" +
                                "<font color=#585858  > "+getResources().getString(R.string.from)+" </font>   <font color=#ff5400  ><b>" + city + "</b></font>";
                        truck_start_details.setText(Html.fromHtml(text3));

                        truck_start_details.setPadding(5, 0, 5, 0);
                        truck_track_layout.addView(truck_start_details);


                    } catch (Exception e) {
                    }


                    JSONArray jsonArr1 = new JSONArray(truck);


                    Log.e("jsonArr1",""+jsonArr1);
                    for (int i = 0; i < jsonArr1.length(); i++) {


                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);


                        String vehicle_number = jsonObjectPackageData1.getString("vehicle_number");
                        String status = jsonObjectPackageData1.getString("status");
                        String start_on = jsonObjectPackageData1.getString("start_on");
                        String end_at = jsonObjectPackageData1.getString("end_at");

                        String current_location = jsonObjectPackageData1.getString("current_location");

                        JSONObject jsonobj1 = new JSONObject(current_location);

                        String latitude = jsonobj1.getString("latitude");
                        String longitude = jsonobj1.getString("longitude");
                        String last_updated = jsonobj1.getString("last_updated");

                        Double lat = Double.parseDouble(latitude);
                        Double log = Double.parseDouble(longitude);


                        String current_text = "", current_text1 = "", reached_city = "";

                        try {

                            Calendar calendar = Calendar.getInstance();
                            TimeZone tz = TimeZone.getDefault();
                            //calendar.setTimeInMillis((long) (Long.parseLong(last_updated) * 1000L));
                            calendar.setTimeInMillis(Long.valueOf(last_updated));
                            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                            sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                            Date currenTimeZone = (Date) calendar.getTime();

                            current_text = sdf.format(currenTimeZone);

                            Log.e("current_time ", "current_time " + current_text);


                            Calendar calendar1 = Calendar.getInstance();
                            TimeZone tz1 = TimeZone.getDefault();
                          //  calendar1.setTimeInMillis((long) (Long.parseLong(last_updated) * 1000L));
                            calendar1.setTimeInMillis(Long.valueOf(last_updated));
                            calendar1.add(Calendar.MILLISECOND, tz1.getOffset(calendar.getTimeInMillis()));

                            SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
                            sdf1.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                            Date currenTimeZone1 = (Date) calendar1.getTime();

                            current_text1 = sdf1.format(currenTimeZone1);

                            Log.e("current_time1 ", "current_time1 " + current_text1);

                            /*Geocoder geocoder;
                            List<Address> addresses;
                            geocoder = new Geocoder(ActiveShipment.this, Locale.getDefault());
                            addresses = geocoder.getFromLocation(lat, log, 1);
                            reached_city = addresses.get(0).getLocality();*/

                          //  Log.e("reached_city ", "reached_city " + reached_city);


                        } catch (Exception e) {
                        }


                        MyTextView truck_detail_text = new MyTextView(
                                getApplicationContext());


                        LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params12.setMargins(0, 20, 0, 0);

                        truck_detail_text.setLayoutParams(params12);

                        String text3 = "";
                        if (status.equalsIgnoreCase("In-Transit")) {
                           /* text3 = "<font color=#ff5400  ><b>Truck " + (i + 1) + " </b></font><font color=#585858  > with number </font>" +
                                    "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  > has reached </font>" +
                                    "<font color=#ff5400  ><b>" + reached_city + " </b></font> <font color=#585858  > on </font>   <font color=#ff5400  ><b>" + current_text + "</b></font>" +
                                    "<font color=#585858  > at </font>   <font color=#ff5400  ><b>" + current_text1 + "</b></font>";
                            truck_detail_text.setText(Html.fromHtml(text3));
                            truck_detail_text.setPadding(5, 0, 5, 0);
                            truck_track_layout.addView(truck_detail_text);*/

                          settingDriverLocation(lat+","+log,getResources().getString(R.string.api_key),truck_detail_text,truck_track_layout,i,vehicle_number,current_text1,current_text);


                        } else if (status.equalsIgnoreCase("Completed")) {
                            text3 = "<font color=#ff5400  ><b>"+getResources().getString(R.string.truck) + (i + 1) + " </b></font><font color=#585858  > "+getResources().getString(R.string.with_number)+" </font>" +
                                    "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  > "+getResources().getString(R.string.has_completed_the_trip)+" </font>";

                            truck_detail_text.setText(Html.fromHtml(text3));
                            truck_detail_text.setPadding(5, 0, 5, 0);
                            truck_track_layout.addView(truck_detail_text);
                        } else if (status.equalsIgnoreCase("not started")) {

                            text3 = "<font color=#ff5400  ><b>"+getResources().getString(R.string.truck) + (i + 1) + " </b></font><font color=#585858  > "+getResources().getString(R.string.with_number)+" </font>" +
                                    "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  >"+getResources().getString(R.string.has_not_started_the_trip)+"</font>";

                            truck_detail_text.setText(Html.fromHtml(text3));
                            truck_detail_text.setPadding(5, 0, 5, 0);
                            truck_track_layout.addView(truck_detail_text);
                        }



                    }


                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("204")) {

                    // shipment status was different than in-transit


                } else {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    @Override
    protected void onResume() {
        super.onResume();


        if (pref.getBoolean("timeChanged", false)) {

            editor.putBoolean("timeChanged", false);

            editor.putBoolean("hitActiveShipmentService", true);
            editor.commit();

            finish();
        }


    }

    public void settingDriverLocation(String latLng, String key,final MyTextView truck_detail_text , LinearLayout truck_track_layout,final int pos,final String vehicle_number,final String current_text1,final String current_text) {
        Log.e("settingDriverLocation","settingDriverLocation");
        final WeakReference<LinearLayout> truck_track_lay = new WeakReference<LinearLayout>(truck_track_layout);
        RestClient.getAddressDetail().getAddressWithLatLong(latLng, key, new Callback<JsonObject>() {
            @Override
            public void success(JsonObject jsonObject, retrofit.client.Response response) {
                Log.e("onResponse", "" + response);
                GeoLocationResponce geoLocationResponce = CommonUtils.jsonParsorForGeo(jsonObject);

                String text3 = "<font color=#ff5400  ><b>"+getResources().getString(R.string.truck) + (pos + 1) + " </b></font><font color=#585858  > "+getResources().getString(R.string.with_number)+" </font>" +
                        "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  > "+getResources().getString(R.string.has_reached)+" </font>" +
                        "<font color=#ff5400  ><b>" + geoLocationResponce.getCity() + " </b></font> <font color=#585858  > "+getResources().getString(R.string.on)+" </font>   <font color=#ff5400  ><b>" + current_text + "</b></font>" +
                        "<font color=#585858  > "+getResources().getString(R.string.at)+" </font>   <font color=#ff5400  ><b>" + current_text1 + "</b></font>";

                if (truck_track_lay != null) {
                    final LinearLayout linearLayout = truck_track_lay.get();
                    if (linearLayout != null) {
                        truck_detail_text.setText(Html.fromHtml(text3));
                        truck_detail_text.setPadding(5, 0, 5, 0);
                        linearLayout.addView(truck_detail_text);
                    }

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Throwable", "error"+error);
                String text3 = "<font color=#ff5400  ><b>"+getResources().getString(R.string.truck) + (pos + 1) + " </b></font><font color=#585858  > "+getResources().getString(R.string.with_number)+" </font>" +
                        "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  > has reached </font>" +
                        "<font color=#ff5400  ><b>" + getResources().getString(R.string.truck) + " </b></font> <font color=#585858  > "+getResources().getString(R.string.on)+" </font>   <font color=#ff5400  ><b>" + current_text + "</b></font>" +
                        "<font color=#585858  > "+getResources().getString(R.string.at)+" </font>   <font color=#ff5400  ><b>" + current_text1 + "</b></font>";

                if (truck_track_lay != null) {
                    final LinearLayout linearLayout = truck_track_lay.get();
                    if (linearLayout != null) {
                        truck_detail_text.setText(Html.fromHtml(text3));
                        truck_detail_text.setPadding(5, 0, 5, 0);
                        linearLayout.addView(truck_detail_text);
                    }

                }
            }
        });
       /* Call<JsonObject> call = RestClient.getAddressDetail().getAddressWithLatLong(latLng, key);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e("onResponse", "" + response);
                Log.e("onResponsecall", "" + call);
                GeoLocationResponce geoLocationResponce = CommonUtils.jsonParsorForGeo(response.body());
                String text3 = "<font color=#ff5400  ><b>Truck " + (pos + 1) + " </b></font><font color=#585858  > with number </font>" +
                        "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  > has reached </font>" +
                        "<font color=#ff5400  ><b>" + geoLocationResponce.getCity() + " </b></font> <font color=#585858  > on </font>   <font color=#ff5400  ><b>" + current_text + "</b></font>" +
                        "<font color=#585858  > at </font>   <font color=#ff5400  ><b>" + current_text1 + "</b></font>";

                if (truck_track_lay != null) {
                    final LinearLayout linearLayout = truck_track_lay.get();
                    if (linearLayout != null) {
                        truck_detail_text.setText(Html.fromHtml(text3));
                        truck_detail_text.setPadding(5, 0, 5, 0);
                        linearLayout.addView(truck_detail_text);
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("Throwable", "error");
                String text3 = "<font color=#ff5400  ><b>Truck " + (pos + 1) + " </b></font><font color=#585858  > with number </font>" +
                        "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  > has reached </font>" +
                        "<font color=#ff5400  ><b>" + "unknown" + " </b></font> <font color=#585858  > on </font>   <font color=#ff5400  ><b>" + current_text + "</b></font>" +
                        "<font color=#585858  > at </font>   <font color=#ff5400  ><b>" + current_text1 + "</b></font>";

                if (truck_track_lay != null) {
                    final LinearLayout linearLayout = truck_track_lay.get();
                    if (linearLayout != null) {
                        truck_detail_text.setText(Html.fromHtml(text3));
                        truck_detail_text.setPadding(5, 0, 5, 0);
                        linearLayout.addView(truck_detail_text);
                    }

                }
            }
            @Override
            public void onResponse(retrofit.Response<JsonObject> response, Retrofit retrofit) {
                Log.e("onResponse", "" + response);
                GeoLocationResponce geoLocationResponce = CommonUtils.jsonParsorForGeo(response.body());
                String text3 = "<font color=#ff5400  ><b>Truck " + (pos + 1) + " </b></font><font color=#585858  > with number </font>" +
                        "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  > has reached </font>" +
                        "<font color=#ff5400  ><b>" + geoLocationResponce.getCity() + " </b></font> <font color=#585858  > on </font>   <font color=#ff5400  ><b>" + current_text + "</b></font>" +
                        "<font color=#585858  > at </font>   <font color=#ff5400  ><b>" + current_text1 + "</b></font>";

                if (truck_track_lay != null) {
                    final LinearLayout linearLayout = truck_track_lay.get();
                    if (linearLayout != null) {
                        truck_detail_text.setText(Html.fromHtml(text3));
                        truck_detail_text.setPadding(5, 0, 5, 0);
                        linearLayout.addView(truck_detail_text);
                    }

                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Throwable", "error");
                String text3 = "<font color=#ff5400  ><b>Truck " + (pos + 1) + " </b></font><font color=#585858  > with number </font>" +
                        "<font color=#ff5400  ><b>" + vehicle_number + " </b></font><font color=#585858  > has reached </font>" +
                        "<font color=#ff5400  ><b>" + "unknown" + " </b></font> <font color=#585858  > on </font>   <font color=#ff5400  ><b>" + current_text + "</b></font>" +
                        "<font color=#585858  > at </font>   <font color=#ff5400  ><b>" + current_text1 + "</b></font>";

                if (truck_track_lay != null) {
                    final LinearLayout linearLayout = truck_track_lay.get();
                    if (linearLayout != null) {
                        truck_detail_text.setText(Html.fromHtml(text3));
                        truck_detail_text.setPadding(5, 0, 5, 0);
                        linearLayout.addView(truck_detail_text);
                    }

                }
            }
        });*/




    }
}
