package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.cargowala.Models.FcmToken;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.RegexpValidator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/17/2016.
 */
public class Register extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;
    RelativeLayout actionBar;
    ImageView backbutton;
    MaterialEditText edittext_email,edittext_new_pass,edittext_confirm_pass,login_here;
    MyTextView register1;
    SweetAlertDialog pDialog;
    boolean status200 = false;
    String fcmToken="";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        EventBus.getDefault().register(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        try {
            fcmToken = pref.getString("fcmToken","");
            if(fcmToken.equalsIgnoreCase("") || fcmToken.isEmpty() || fcmToken == null){
                if(FirebaseInstanceId.getInstance().getToken() != null){
                    fcmToken=FirebaseInstanceId.getInstance().getToken();
                }else {
                    Log.e("getToken", "null");
                }
            }else {

            }
        } catch (Exception e) {
            fcmToken="";
        }

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;



        register1 = (MyTextView) findViewById(R.id.register1);
        register1.setOnClickListener(this);

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();



        login_here= (MaterialEditText) findViewById(R.id.login_here);
        login_here.setOnClickListener(this);
        edittext_email= (MaterialEditText) findViewById(R.id.edittext_email);
        edittext_new_pass= (MaterialEditText) findViewById(R.id.edittext_new_pass);


        edittext_new_pass.setFilters(new InputFilter[] { filter });

     //   edittext_email.validateWith(new RegexpValidator("Only Integer Valid!", "\\d+"));

   /*     edittext_email.validate("abc", "invalid");

        edittext_email.isValid("abc");
*/



      /*  edittext_email.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
              //  edittext_email.validateWith(new RegexpValidator("Only Integer Valid!", "\\d+"));

                edittext_email.validate("abc", "invalid");

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });*/


        edittext_new_pass.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(edittext_new_pass) {
            @Override
            public boolean onDrawableClick() {
                Log.d("m", "1");


                Log.d("main", " " + edittext_new_pass.getInputType());
                if (edittext_new_pass.getInputType() == 1) {

                    Log.d("main", "here ");
                    edittext_new_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edittext_new_pass.setSelection(edittext_new_pass.getText().length());
                } else {
                    edittext_new_pass.setInputType(InputType.TYPE_CLASS_TEXT /*|
                        InputType.TYPE_TEXT_VARIATION_PASSWORD*/);
                    edittext_new_pass.setSelection(edittext_new_pass.getText().length());
                }
                return true;
            }
        });



        edittext_confirm_pass= (MaterialEditText) findViewById(R.id.edittext_confirm_pass);
        edittext_confirm_pass.setFilters(new InputFilter[] { filter });


        edittext_confirm_pass.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(edittext_confirm_pass) {
            @Override
            public boolean onDrawableClick() {
                Log.d("m", "1");


                Log.d("main", " " + edittext_confirm_pass.getInputType());
                if (edittext_confirm_pass.getInputType() == 1) {

                    Log.d("main", "here ");
                    edittext_confirm_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edittext_confirm_pass.setSelection(edittext_confirm_pass.getText().length());
                } else {
                    edittext_confirm_pass.setInputType(InputType.TYPE_CLASS_TEXT /*|
                        InputType.TYPE_TEXT_VARIATION_PASSWORD*/);
                    edittext_confirm_pass.setSelection(edittext_confirm_pass.getText().length());
                }
                return true;
            }
        });

        Drawable img = getResources().getDrawable( R.drawable.eye_icon );
        //img.setBounds(0, 0, 30, 20);
        Log.d("device width", "" + deviceWidth / 16);
        img.setBounds(0, 0, deviceWidth / 16, deviceWidth / 24);


        edittext_new_pass.setCompoundDrawables(null, null, img, null);
        edittext_confirm_pass.setCompoundDrawables(null, null, img, null);







    }

    String email_id,pass,confirm_pass;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:
                finish();


                break;

            case R.id.login_here:
               finish();


                break;

            case R.id.popup_button:
                dialog.dismiss();
                if(status200)
                {
                    finish();
                }


            break;

            case R.id.register1:

               /* Intent i = new Intent(Register.this, CreateProfile.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
               */

                 email_id = edittext_email.getText().toString();
                 pass = edittext_new_pass.getText().toString();
                 confirm_pass = edittext_confirm_pass.getText().toString();
                email_id = email_id.trim();




                if (email_id.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_the_email),
                            Toast.LENGTH_SHORT).show();

                }
                else if(!isValidEmail(email_id))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_valid_email), Toast.LENGTH_SHORT).show();
                }
                else if (pass.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_password),
                            Toast.LENGTH_SHORT).show();

                }

                else if (confirm_pass.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_confirm_password),
                            Toast.LENGTH_SHORT).show();

                }
                else if (pass.length() < 6) {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.password_lenght_msg),
                            Toast.LENGTH_SHORT).show();
                }
                else if (!pass.equalsIgnoreCase(confirm_pass)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.password_does_not_match_confirm_password),
                            Toast.LENGTH_SHORT).show();
                }
                else
                {

                    if(fcmToken.equalsIgnoreCase("") || fcmToken.isEmpty() || fcmToken == null){
                        Toast.makeText(getBaseContext(),getResources().getString(R.string.fcm_token_null),Toast.LENGTH_SHORT).show();
                    }else {
                        if(isOnline())
                        {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makeRegistrationReq();
                        }
                        else {
                            Toast.makeText(Register.this,
                                    getResources().getString(R.string.check_your_network),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }


                }




                break;



        }


    }










    String res;
    StringRequest strReq;

    public String makeRegistrationReq() {

        String url = getResources().getString(R.string.base_url)+"user/register";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkRegistrationResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                   /* InstanceID instanceID = InstanceID.getInstance(Register.this);
                     Log.d("token", "2");
                     String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                             GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);*/
                     Log.d("regg", "6");

                    Log.d("register ", email_id);
                    Log.d("register ",pass);

                    //    Log.d("register", token);

                    params.put("username", email_id);
                    params.put("password", pass);
                    params.put("mobile_tokens", fcmToken);

                    Log.e("register", params.toString());



                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "38");



        return null;
    }

    public void checkRegistrationResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                String status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {
                /*    JSONObject responseee = new JSONObject(responsee);
                    String id = responseee.getString("id");*/



                 /*   new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Successfully Registered")
                            .setContentText("Please verify your email.")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .show();*/

                    status200 = true;
                    initiatePopupWindow(getResources().getString(R.string.your_email_has_been_registered_please_check_your_mail_and_click_the_link_to_confirm_your_mail_and_login),
                            false, getResources().getString(R.string.email_registered), getResources().getString(R.string.ok));

                }
                else if(status_code.equalsIgnoreCase("409"))
                {

                   /* Log.e("register","b");


                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("This email is already registered.")
                            .show();*/

                    initiatePopupWindow(getResources().getString(R.string.this_email_has_already_been_registered),
                            true, getResources().getString(R.string.error),getResources().getString(R.string.ok));

                }
                else if(status_code.equalsIgnoreCase("401"))
                {

                    initiatePopupWindow(getResources().getString(R.string.you_have_already_registered_with_this_email_id),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }
                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }


    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(Register.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

















    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }








    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }




    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };




    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showCompleteDialog(FcmToken fcmTokenObject) {
        fcmToken=fcmTokenObject.getFcmToken();
    }

}
