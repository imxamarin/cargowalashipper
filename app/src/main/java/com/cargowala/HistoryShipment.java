package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class HistoryShipment extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton,map_image;
    MyTextView invoice_no_text;
    String jsonObjectPackageData;

    MyTextViewSemi view_details_text;
    String id1,shipment_id1,load_type1,from_city1,from_state1,from_address1,to_city1,to_state1,to_address1,
            shipment_url1,priority_delivery1,transit_date1,transit_time1,truck_category1,truck_name1,truck_quantity1,shipment_status1;


    MyTextViewSemi from_place_text, to_place_text,load_type_priority, no_of_trucks_text, date_text, time_text, order_id_text,
            truck_name_text;

    MyTextView shipment_status_text;

    ImageView truck_image;

    MyTextView started_text,ended_text;
    CardView card_view1;
    SweetAlertDialog pDialog;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_shipment);

        Intent intent = getIntent();
        jsonObjectPackageData = intent.getExtras().getString("orderId");

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();

        map_image = (ImageView) findViewById(R.id.map_image);
        map_image.getLayoutParams().height =  (int)(deviceWidth / 1.1);
        map_image.requestLayout();

        invoice_no_text= (MyTextView) findViewById(R.id.invoice_no_text);
        invoice_no_text.setOnClickListener(this);


        card_view1= (CardView) findViewById(R.id.card_view1);
        started_text = (MyTextView) findViewById(R.id.started_text);
        ended_text = (MyTextView) findViewById(R.id.ended_text);








        Log.d("aaaa ", "" + jsonObjectPackageData);

        try {

            JSONObject  jsonObjectPackageData1 = new JSONObject(jsonObjectPackageData);

            Log.d("My App", jsonObjectPackageData1.toString());


            id1 = jsonObjectPackageData1.getString("id");
            shipment_id1 = jsonObjectPackageData1.getString("shipment_id");
            load_type1 = jsonObjectPackageData1.getString("load_type");
            from_city1 = jsonObjectPackageData1.getString("from_city");
            from_state1 = jsonObjectPackageData1.getString("from_state");
            from_address1 = jsonObjectPackageData1.getString("from_address");
            to_city1 = jsonObjectPackageData1.getString("to_city");
            to_state1 = jsonObjectPackageData1.getString("to_state");
            to_address1 = jsonObjectPackageData1.getString("to_address");
            shipment_url1 = jsonObjectPackageData1.getString("shipment_url");
            priority_delivery1 = jsonObjectPackageData1.getString("priority_delivery");
            transit_date1 = jsonObjectPackageData1.getString("transit_date");
            transit_time1 = jsonObjectPackageData1.getString("transit_time");
            truck_category1 = jsonObjectPackageData1.getString("truck_category");
            truck_name1 = jsonObjectPackageData1.getString("truck_name");
            truck_quantity1 = jsonObjectPackageData1.getString("truck_quantity");
            shipment_status1 = jsonObjectPackageData1.getString("shipment_status");





        } catch (Throwable t) {
            Log.e("My App", "Could not parse malformed JSON:");
        }







        shipment_status_text= (MyTextView) findViewById(R.id.shipment_status_text);





        truck_image = (ImageView) findViewById(R.id.truck_image);

        from_place_text = (MyTextViewSemi) findViewById(R.id.from_place_text);
        to_place_text = (MyTextViewSemi) findViewById(R.id.to_place_text);
        load_type_priority = (MyTextViewSemi) findViewById(R.id.load_type_priority);
        no_of_trucks_text = (MyTextViewSemi) findViewById(R.id.no_of_trucks_text);
        date_text = (MyTextViewSemi) findViewById(R.id.date_text);
        time_text = (MyTextViewSemi) findViewById(R.id.time_text);
        order_id_text = (MyTextViewSemi) findViewById(R.id.order_id_text);
        truck_name_text = (MyTextViewSemi) findViewById(R.id.truck_name_text);



        if(truck_category1.equalsIgnoreCase("1"))
        {
            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.small_grey_truck));
        }
        else if(truck_category1.equalsIgnoreCase("2"))
        {
            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
        }
        else if(truck_category1.equalsIgnoreCase("3"))
        {
            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.large_grey_truck));
        }
        else
        {
            Log.d("error","unreachable code");
        }


        view_details_text = (MyTextViewSemi) findViewById(R.id.view_details_text);
        String text = "<font color=#585858><b><u>"+getResources().getString(R.string.view_item_details)+"</u></b></font>";
        view_details_text.setText(Html.fromHtml(text));
        view_details_text.setOnClickListener(this);

        from_place_text.setText(from_address1);
        to_place_text.setText(to_address1);

        if(priority_delivery1.equalsIgnoreCase("Yes")) {
            load_type_priority.setText(load_type1 + " - "+getResources().getString(R.string.priority));
        }
        else
        {
            load_type_priority.setText(load_type1);
        }
        date_text.setText(transit_date1);
        time_text.setText(transit_time1);
        no_of_trucks_text.setText(truck_quantity1);
        truck_name_text.setText(truck_name1);

        order_id_text.setText(getResources().getString(R.string.order_id)+"- " + shipment_id1);


        shipment_status_text.setText(shipment_status1);


        Glide.with(HistoryShipment.this)
                .load(shipment_url1)
                .placeholder(R.drawable.loading_screen_square)
                .error(R.drawable.unable_upload_square)
                .into(map_image);
      /*  Picasso.with(getApplicationContext())
                .load(shipment_url1)
                .placeholder(R.drawable.loading_screen_square)
                .error(R.drawable.unable_upload_square)
				*//*.resize(deviceWidth, deviceHeight).centerInside()*//*.into(map_image);*/






        if(shipment_status1.equalsIgnoreCase("Completed")) {


            if (isOnline()) {

                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();
                makeTrackShipmentReq();
            } else {


                Toast.makeText(HistoryShipment.this,
                        getResources().getString(R.string.check_your_network),
                        Toast.LENGTH_SHORT).show();
            }


        }



    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {




            case R.id.backbutton:

                finish();

                break;


            case R.id.invoice_no_text:

                Intent i = new Intent(HistoryShipment.this, CompletedPayments.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("orderId","");
                startActivity(i);

                break;




            case R.id.view_details_text:


                Intent i2 = new Intent(HistoryShipment.this, ItemsDetail.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                i2.putExtra("orderId1", id1);


                startActivity(i2);

                break;





        }


    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }















    String res3;
    String status_code = "0";

    StringRequest strReq3;

    public String makeTrackShipmentReq() {

        String url = getResources().getString(R.string.base_url)+"user/track-shipment";
        Log.d("regg", "2");
        strReq3 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res3 = response.toString();
                        checkTrackShipmentResponse(res3);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res3 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("shipment_id", id1);


                    params.put("_id",pref.getString("userId","Something Wrong"));
                    params.put("security_token", pref.getString("security_token","Something Wrong"));

                    Log.e("register", params.toString());


                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq3, "1");



        return null;
    }

    public void checkTrackShipmentResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);

                if(status_code.equalsIgnoreCase("200"))
                {


                    String response1 = reader.getString("response");

                    JSONObject jsonobj = new JSONObject(response1);


                    String location = jsonobj.getString("location");

                    JSONArray jsonArr = new JSONArray(location);

                    for (int i = 0; i < jsonArr.length(); i++) {

                        JSONObject jsonObjectPackageData = jsonArr.getJSONObject(i);


                        String city = jsonObjectPackageData.getString("city");
                        String start_time = jsonObjectPackageData.getString("start_time");
                        String end_time = jsonObjectPackageData.getString("end_time");



                        try {
                            Calendar calendar = Calendar.getInstance();
                            TimeZone tz = TimeZone.getDefault();
                            if(i==0) {
                               // calendar.setTimeInMillis((long) (Long.parseLong(start_time) * 1000L));
                                calendar.setTimeInMillis(Long.valueOf(start_time));
                            }
                            else
                            {
                              //  calendar.setTimeInMillis((long) (Long.parseLong(end_time) * 1000L));
                                calendar.setTimeInMillis(Long.valueOf(end_time));
                            }

                            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                            sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                            Date currenTimeZone = (Date) calendar.getTime();

                            String current_text = sdf.format(currenTimeZone);

                            Log.d("current_time ", "current_time " + current_text);


                            Calendar calendar1 = Calendar.getInstance();
                            TimeZone tz1 = TimeZone.getDefault();
                            if(i==0) {
                              //  calendar1.setTimeInMillis((long) (Long.parseLong(start_time) * 1000L));
                                calendar1.setTimeInMillis(Long.valueOf(start_time));
                            }
                            else
                            {
                               // calendar1.setTimeInMillis((long) (Long.parseLong(end_time) * 1000L));
                                calendar1.setTimeInMillis(Long.valueOf(end_time));
                            }

                            calendar1.add(Calendar.MILLISECOND, tz1.getOffset(calendar.getTimeInMillis()));

                            SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
                            sdf1.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                            Date currenTimeZone1 = (Date) calendar1.getTime();

                            String current_text1 = sdf1.format(currenTimeZone1);

                            Log.d("current_time1 ", "current_time1 " + current_text1);
                            card_view1.setVisibility(View.VISIBLE);



                            if(i==0) {

                                String text3 = "<font color=#585858  >"+getResources().getString(R.string.shipment_started_on)+"</font><font color=#ff5400  ><b>" + current_text + "</b> </font>" +
                                        "<font color=#585858  > "+getResources().getString(R.string.at)+" </font><font color=#ff5400  ><b>" + current_text1 + "</b></font>" +
                                        "<font color=#585858  > "+getResources().getString(R.string.from)+" </font>   <font color=#ff5400  ><b>" + city + "</b></font>";

                                started_text.setText(Html.fromHtml(text3));
                            }
                            else
                            {

                                String text3 = "<font color=#585858  >"+getResources().getString(R.string.shipment_ended_on)+" </font><font color=#ff5400  ><b>" + current_text + "</b> </font>" +
                                        "<font color=#585858  > "+getResources().getString(R.string.at)+" </font><font color=#ff5400  ><b>" + current_text1 + "</b></font>" +
                                        "<font color=#585858  > "+getResources().getString(R.string.from)+" </font>   <font color=#ff5400  ><b>" + city + "</b></font>";

                                ended_text.setText(Html.fromHtml(text3));

                            }


                        } catch (Exception e) {
                        }


                    }






                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("204"))
                {

                    // shipment status was different than in-transit


                }



                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }





    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(HistoryShipment.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }








}
