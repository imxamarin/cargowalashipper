package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.cargowala.Payment.Network.APIService;
import com.cargowala.Payment.Network.NetworkService;
import com.cargowala.Payment.PaymentResponse;
import com.cargowala.helper.CommonListners;
import com.cargowala.helper.CommonUtils;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import retrofit2.Call;
import retrofit2.Callback;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/17/2016.
 */
public class AcceptBidPayment extends Activity implements View.OnClickListener {


    int deviceWidth, deviceHeight;
    RelativeLayout actionBar;
    ImageView backbutton;
    String $amount,$txd_id;

    MyTextViewSemi bid_price_text, percent_value_text, insurance_value_text;
    MyTextViewBold total_value_text;
    MaterialEditText pan_card_text;

    MyTextView pay_online, pay_in_cash, remaing_amount_line;
    ImageView or_imagee;

    String future_payment_type = "",current_payment="1";;

    ImageView select_cash_image, select_online_image;
    LinearLayout select_cash, select_online;

    SeekBar seek;

    float discrete = 0;
    float start = 0;
    float end = 100;
    float start_pos = 0;
    int start_position = 0;
    int percent_value = 25;
    float tot_total = 0;
    float percent_value_int = 0;

    String insuranceAmount, orderIdBig1, truckerBidId, priceOfBid, userPanCard = "", shipmentId = "",bid_id="";
    LinearLayout hide_if_100_percent;


    LinearLayout check_info_layout;
    boolean isChecked = false;

    ImageView check_info_checkbox;
    float tot_value;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;
    JSONObject jsonObject;
    private JSONArray paymentArray = new JSONArray();
    SweetAlertDialog pDialog;
    String status_code = "0";
    float bidPrice=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accept_bid_payment);

        //  android:theme="@android:style/Theme.Translucent.NoTitleBar"
        // mue6084397nu

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;


        Intent intent = getIntent();

        insuranceAmount = intent.getExtras().getString("insuranceAmount");
        orderIdBig1 = intent.getExtras().getString("orderIdBig1");
        truckerBidId = intent.getExtras().getString("truckerBidId");
        if (intent.hasExtra("shipment_id")) {
            shipmentId = intent.getExtras().getString("shipment_id");
        }


        priceOfBid = intent.getExtras().getString("priceOfBid");
        try {
            float bidPrice=Float.parseFloat(priceOfBid);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);

        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);


        backbutton.requestLayout();

        hide_if_100_percent = (LinearLayout) findViewById(R.id.hide_if_100_percent);

        select_cash = (LinearLayout) findViewById(R.id.select_cash);
        select_cash.setOnClickListener(this);
        select_online = (LinearLayout) findViewById(R.id.select_online);
        select_online.setOnClickListener(this);

        select_cash_image = (ImageView) findViewById(R.id.select_cash_image);
        select_online_image = (ImageView) findViewById(R.id.select_online_image);


        percent_value_text = (MyTextViewSemi) findViewById(R.id.percent_value_text);
        bid_price_text = (MyTextViewSemi) findViewById(R.id.bid_price_text);
        insurance_value_text = (MyTextViewSemi) findViewById(R.id.insurance_value_text);

        total_value_text = (MyTextViewBold) findViewById(R.id.total_value_text);
        pan_card_text = (MaterialEditText) findViewById(R.id.pan_card_text);
        pay_online = (MyTextView) findViewById(R.id.pay_online);
        pay_online.setOnClickListener(this);
        pay_in_cash = (MyTextView) findViewById(R.id.pay_in_cash);
        pay_in_cash.setOnClickListener(this);
        or_imagee = (ImageView) findViewById(R.id.or_imagee);


        pan_card_text.setText(pref.getString("user_pan_card", ""));
        remaing_amount_line = (MyTextView) findViewById(R.id.remaing_amount_line);
        check_info_layout = (LinearLayout) findViewById(R.id.check_info_layout);
        tot_total = Float.parseFloat(priceOfBid);
        bid_price_text.setText("₹ " + priceOfBid);
        percent_value_int =  tot_total / 4;
        percent_value_text.setText("₹ " + CommonUtils.roundFigTwo(percent_value_int));
        insurance_value_text.setText("₹ " + CommonUtils.roundFigTwo(insuranceAmount));
        tot_value = percent_value_int + Float.parseFloat(insuranceAmount);

        total_value_text.setText("" + tot_value);
        remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_pay_rest_of_the_75)+"(₹ " + (tot_total - percent_value_int) + ") "+getResources().getString(R.string.payment_in_future)+"?");

        try {
            paymentArray = null;
            paymentArray = new JSONArray();
            JSONObject firstPart = new JSONObject();
            firstPart.put("instalment", "" + percent_value);

            firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_value));
            firstPart.put("mode", future_payment_type);
            firstPart.put("remarks", "empty");
            JSONObject SecondPart = new JSONObject();
            SecondPart.put("instalment", "75");
            SecondPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - tot_value));
            SecondPart.put("mode", future_payment_type);
            SecondPart.put("remarks", "empty");
            paymentArray.put(firstPart);
            paymentArray.put(SecondPart);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("PaymentArrayOnCreate", "" + paymentArray);

        if (insuranceAmount.equalsIgnoreCase("00.00")) {
            check_info_layout.setVisibility(View.GONE);

            or_imagee.setVisibility(View.VISIBLE);
            pay_in_cash.setVisibility(View.VISIBLE);
        }


        start = 25;      //you need to give starting value of SeekBar
        end = 100;         //you need to give end value of SeekBar
        start_pos = 25;    //you need to give starting position value of SeekBar


        start_position = (int) (((start_pos - start) / (end - start)) * 100);
        discrete = start_pos;
        seek = (SeekBar) findViewById(R.id.seek);

        seek.setProgress(start_position);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //   Toast.makeText(getBaseContext(), "discrete = " + String.valueOf(discrete), Toast.LENGTH_SHORT).show();


                //      Toast.makeText(getBaseContext(), "percent_value = " + percent_value/*String.valueOf(discrete)*/, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                // To convert it as discrete value


              /*  progress = ((int)Math.round(progress/25))*25;
                seekBar.setProgress(progress);*/

                float temp = progress;
                float dis = end - start;
                discrete = (float) (Math.ceil((start + ((temp / 100) * dis))));

                if (discrete < 37)

                {
                    percent_value = 25;
                    progress = 0;
                    seekBar.setProgress(progress);

                   // percent_value_int = (int) tot_total / 4;
                    percent_value_int =  tot_total / 4;
                    percent_value_text.setText("₹ " + percent_value_int);

                    remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_pay_rest_of_the_75)+"(₹ " + (tot_total - percent_value_int) + ")"+getResources().getString(R.string.payment_in_future)+"?");
                    pay_online.setText(getResources().getString(R.string.pay)+" 25% "+getResources().getString(R.string.online_now));
                    pay_in_cash.setText(getResources().getString(R.string.pay)+" 25% "+getResources().getString(R.string.in_cash_on_loading));

                    hide_if_100_percent.setVisibility(View.VISIBLE);

                    // tot_value = percent_value_int +Float.parseFloat(insuranceAmount);


                    if (!isChecked) {
                        tot_value = percent_value_int + Float.parseFloat(insuranceAmount);

                    } else {
                        tot_value = percent_value_int;

                    }


                    total_value_text.setText("" + tot_value);

                    try {

                        paymentArray = null;
                        paymentArray = new JSONArray();
                        JSONObject firstPart = new JSONObject();
                        firstPart.put("instalment", "" + percent_value);
                        firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_value));
                        firstPart.put("mode", future_payment_type);
                        firstPart.put("remarks", "empty");

                        JSONObject SecondPart = new JSONObject();
                        SecondPart.put("instalment", "75");
                        SecondPart.put("amount", "" +CommonUtils.roundFigTwo(tot_total - tot_value));
                        SecondPart.put("mode", future_payment_type);
                        SecondPart.put("remarks", "empty");

                        paymentArray.put(firstPart);
                        paymentArray.put(SecondPart);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (discrete > 36 && discrete < 63) {
                    percent_value = 50;
                    progress = 31;
                    seekBar.setProgress(progress);

                    //percent_value_int = (int) tot_total / 2;
                    percent_value_int =  tot_total / 2;
                    percent_value_text.setText("₹ " + percent_value_int);

                    // tot_value = percent_value_int +Float.parseFloat(insuranceAmount);


                    if (!isChecked) {
                        tot_value = percent_value_int + Float.parseFloat(insuranceAmount);

                    } else {
                        tot_value = percent_value_int;

                    }


                    total_value_text.setText("" + tot_value);


                    remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_payrest_of_the)+" 50% (₹ " + (tot_total - percent_value_int) + ") "+getResources().getString(R.string.payment_in_future)+"?");
                    pay_online.setText(getResources().getString(R.string.pay)+" 50% "+getResources().getString(R.string.online_now));
                    pay_in_cash.setText(getResources().getString(R.string.pay)+" 50% "+getResources().getString(R.string.in_cash_on_loading));
                    hide_if_100_percent.setVisibility(View.VISIBLE);

                    try {

                        paymentArray = null;
                        paymentArray = new JSONArray();
                        JSONObject firstPart = new JSONObject();
                        firstPart.put("instalment", "" + percent_value);
                        firstPart.put("amount", "" +CommonUtils.roundFigTwo(tot_value));
                        firstPart.put("mode", future_payment_type);
                        firstPart.put("remarks", "empty");

                        JSONObject SecondPart = new JSONObject();
                        SecondPart.put("instalment", "50");
                        SecondPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - tot_value));
                        SecondPart.put("mode", future_payment_type);
                        SecondPart.put("remarks", "empty");

                        paymentArray.put(firstPart);
                        paymentArray.put(SecondPart);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else if (discrete > 62 && discrete < 88) {
                    percent_value = 75;
                    progress = 64;
                    seekBar.setProgress(progress);
                  //  percent_value_int = (int) (tot_total / 4) * 3;
                    percent_value_int =  (tot_total / 4) * 3;
                    percent_value_text.setText("₹ " + percent_value_int);
                    // tot_value = percent_value_int +Float.parseFloat(insuranceAmount);

                    if (!isChecked) {
                        tot_value = percent_value_int + Float.parseFloat(insuranceAmount);

                    } else {
                        tot_value = percent_value_int;

                    }

                    total_value_text.setText("" + tot_value);


                    /*remaing_amount_line.setText("How would you like to payrest of the 25% (₹ " + (tot_total - percent_value_int) + ") payment in future?");
                    pay_online.setText("Pay 75% online now");
                    pay_in_cash.setText("Pay 75%in cash on loading");*/

                    remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_payrest_of_the)+" 25% (₹ " + (tot_total - percent_value_int) + ") "+getResources().getString(R.string.payment_in_future)+"?");
                    pay_online.setText(getResources().getString(R.string.pay)+" 75% "+getResources().getString(R.string.online_now));
                    pay_in_cash.setText(getResources().getString(R.string.pay)+" 75% "+getResources().getString(R.string.in_cash_on_loading));

                    hide_if_100_percent.setVisibility(View.VISIBLE);


                    try {

                        paymentArray = null;
                        paymentArray = new JSONArray();
                        JSONObject firstPart = new JSONObject();
                        firstPart.put("instalment", "" + percent_value);
                        firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_value));
                        firstPart.put("mode", future_payment_type);
                        firstPart.put("remarks", "empty");

                        JSONObject SecondPart = new JSONObject();
                        SecondPart.put("instalment", "25");
                        SecondPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - tot_value));
                        SecondPart.put("mode", future_payment_type);
                        SecondPart.put("remarks", "empty");

                        paymentArray.put(firstPart);
                        paymentArray.put(SecondPart);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    percent_value = 100;
                    progress = 100;
                    seekBar.setProgress(progress);

                   // percent_value_int = (int) (tot_total);
                    percent_value_int =  (tot_total);
                    percent_value_text.setText("₹ " + percent_value_int);

                    // tot_value = percent_value_int +Float.parseFloat(insuranceAmount);


                    if (!isChecked) {
                        tot_value = percent_value_int + Float.parseFloat(insuranceAmount);

                    } else {
                        tot_value = percent_value_int;

                    }


                    total_value_text.setText("" + tot_value);

                    /*remaing_amount_line.setText("How would you like to payrest of the 0% payment in future?");
                    pay_online.setText("Pay 100% online now");
                    pay_in_cash.setText("Pay 100%in cash on loading");*/

                    remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_payrest_of_the)+" 0% (₹ " + (tot_total - percent_value_int) + ") "+getResources().getString(R.string.payment_in_future)+"?");
                    pay_online.setText(getResources().getString(R.string.pay)+" 100% "+getResources().getString(R.string.online_now));
                    pay_in_cash.setText(getResources().getString(R.string.pay)+" 100% "+getResources().getString(R.string.in_cash_on_loading));

                    hide_if_100_percent.setVisibility(View.GONE);

                    try {

                        paymentArray = null;
                        paymentArray = new JSONArray();
                        JSONObject firstPart = new JSONObject();
                        firstPart.put("instalment", "" + percent_value);
                        firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_value));
                        firstPart.put("mode", future_payment_type);
                        firstPart.put("remarks", "empty");
                        paymentArray.put(firstPart);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        });


        check_info_layout = (LinearLayout) findViewById(R.id.check_info_layout);
        check_info_layout.setOnClickListener(this);
        check_info_checkbox = (ImageView) findViewById(R.id.check_info_checkbox);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:
                finish();


                break;


            case R.id.check_info_layout:

                if (isChecked) {

                    check_info_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));
                    isChecked = false;

                    or_imagee.setVisibility(View.GONE);
                    pay_in_cash.setVisibility(View.GONE);

                    tot_value = percent_value_int + Float.parseFloat(insuranceAmount);

                    //-----
                     bidPrice=Float.parseFloat(priceOfBid);
                    bidPrice=bidPrice+Float.parseFloat(insuranceAmount);
                    bid_price_text.setText("₹ " + bidPrice);


                    insurance_value_text.setText("₹ " + insuranceAmount);
                    total_value_text.setText("" + tot_value);

                    for(int j=0;j<paymentArray.length();j++){
                        try {
                            JSONObject innerObject=paymentArray.getJSONObject(j);
                            if(j==0){
                                innerObject.put("amount",tot_value);
                                // innerObject.put("amount",""+tot_value);
                            }else {


                            }

                            paymentArray.put(j,innerObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    check_info_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));
                    isChecked = true;

                    or_imagee.setVisibility(View.VISIBLE);
                    pay_in_cash.setVisibility(View.VISIBLE);

                    insurance_value_text.setText("₹ 0");
                    tot_value = percent_value_int;
                    total_value_text.setText("" + tot_value);

                    //----
                    bidPrice=Float.parseFloat(priceOfBid);
                    bidPrice=bidPrice+0;
                    bid_price_text.setText("₹ " + bidPrice);

                    for(int j=0;j<paymentArray.length();j++){
                        try {
                            JSONObject innerObject=paymentArray.getJSONObject(j);
                            if(j==0){
                                innerObject.put("amount",tot_value);
                                // innerObject.put("amount",""+tot_value);
                            }else {


                            }

                            paymentArray.put(j,innerObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

                break;


            case R.id.select_cash:

                select_cash_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                select_online_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                future_payment_type = "1";
                break;


            case R.id.select_online:

                select_cash_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                select_online_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                future_payment_type = "2";

                break;


            case R.id.pay_online:
                current_payment="2";
                Log.e("total_amount", "" + bidPrice);


                if(percent_value == 100){
                    for(int j=0;j<paymentArray.length();j++){
                        try {
                            JSONObject innerObject=paymentArray.getJSONObject(j);
                            if(j==0){
                                innerObject.put("mode",current_payment);
                                // innerObject.put("amount",""+tot_value);
                            }else {
                                innerObject.put("mode",future_payment_type);

                            }

                            paymentArray.put(j,innerObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    try {

                        jsonObject=new JSONObject();
                        jsonObject.put("transactions",paymentArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        jsonObject=new JSONObject();
                    }
                    Log.e("PaymentObject", "" + jsonObject);
                    Log.e("total_amount", "" + bidPrice);
                    userPanCard = pan_card_text.getText().toString();
                    editor.putString("user_pan_card", userPanCard.trim());
                    editor.commit();
                    if (CommonUtils.isOnline(getApplicationContext())) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        payMentByCash(true, "save_payment");

                    }else {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.check_your_network),Toast.LENGTH_SHORT).show();
                    }

                }else {

                    if (future_payment_type.equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_future_payment), Toast.LENGTH_SHORT).show();
                    } else {
                        for(int j=0;j<paymentArray.length();j++){
                            try {
                                JSONObject innerObject=paymentArray.getJSONObject(j);
                                if(j==0){
                                    innerObject.put("mode",current_payment);
                                    // innerObject.put("amount",""+tot_value);
                                }else {
                                    innerObject.put("mode",future_payment_type);

                                }

                                paymentArray.put(j,innerObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        try {

                            jsonObject=new JSONObject();
                            jsonObject.put("transactions",paymentArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            jsonObject=new JSONObject();
                        }
                        Log.e("PaymentObject", "" + jsonObject);
                        Log.e("total_amount", "" + bidPrice);
                        userPanCard = pan_card_text.getText().toString();
                        editor.putString("user_pan_card", userPanCard.trim());
                        editor.commit();
                        if (CommonUtils.isOnline(getApplicationContext())) {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();

                            payMentByCash(true, "save_payment");


                        }else {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.check_your_network),Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                break;
            case R.id.pay_in_cash:
                current_payment="1";

                if(percent_value == 100){
                    for(int j=0;j<paymentArray.length();j++){
                        try {
                            JSONObject innerObject=paymentArray.getJSONObject(j);
                            if(j==0){
                                innerObject.put("mode",current_payment);
                                innerObject.put("amount",""+tot_value);
                            }else {
                                innerObject.put("mode",future_payment_type);
                                innerObject.put("amount",""+tot_value);
                            }

                            paymentArray.put(j,innerObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    try {

                        jsonObject=new JSONObject();
                        jsonObject.put("transactions",paymentArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        jsonObject=new JSONObject();
                    }
                    Log.e("PaymentObject", "" + jsonObject);
                    Log.e("total_amount", "" + bidPrice);
                    if (CommonUtils.isOnline(getApplicationContext())) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();

                        payMentByCash(false,"cash_onloading");


                    }else {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.check_your_network),Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (future_payment_type.equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_future_payment), Toast.LENGTH_SHORT).show();
                    } else {
                        for(int j=0;j<paymentArray.length();j++){
                            try {
                                JSONObject innerObject=paymentArray.getJSONObject(j);
                                if(j==0){
                                    innerObject.put("mode",current_payment);
                                    innerObject.put("amount",""+tot_value);
                                }else {
                                    innerObject.put("mode",future_payment_type);
                                    innerObject.put("amount",""+tot_value);
                                }

                                paymentArray.put(j,innerObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        try {

                            jsonObject=new JSONObject();
                            jsonObject.put("transactions",paymentArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            jsonObject=new JSONObject();
                        }
                        Log.e("PaymentObject", "" + jsonObject);
                        Log.e("total_amount", "" + bidPrice);
                        if (CommonUtils.isOnline(getApplicationContext())) {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();

                            payMentByCash(false,"cash_onloading");


                        }else {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.check_your_network),Toast.LENGTH_SHORT).show();
                        }

                    }
                }


                break;


        }


    }




    String res;
    StringRequest strReq;

    public String payMentByCash(final  boolean isOnlinePayment,final String action) {
        Log.e("PaymentObject", "" + jsonObject);
        String url = getResources().getString(R.string.base_url) + "user/save-transactions";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");
                        Log.e("register", response.toString());
                        res = response.toString();
                        checkPayMentByCash(res,isOnlinePayment);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));
                    params.put("_id", pref.getString("userId", "_id Wrong"));
                    params.put("shipment_id", shipmentId);
                    params.put("action", action);
                    params.put("total_amount", ""+CommonUtils.roundFigTwo(bidPrice));
                    params.put("payments", jsonObject.toString());
                    params.put("bid_id", truckerBidId);
                    params.put("service_for", "accept-bid");
                    Log.e("params", ""+params.toString());
                    Log.e("action", action);
                    Log.e("total_amount", "" + CommonUtils.roundFigTwo(bidPrice));
                    Log.e("payments", ""+paymentArray);
                    Log.e("regg", "7 " + params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "29");


        return null;
    }

    public void checkPayMentByCash(String response,final boolean isOnlinePayment) {
        pDialog.dismiss();
        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);
                Log.e("register", "reader");
                status_code = reader.getString("status_code");
                Log.e("register", "status_code " + status_code);
                if (status_code.equalsIgnoreCase("200")) {
                    if(isOnlinePayment){
                       //-------go to citres payment
                        try {
                            JSONObject responseObject=reader.getJSONObject("response");
                            if(responseObject.has("txn_id")){
                                String txn_id=responseObject.getString("txn_id");
                                goToCitrusPayment(txn_id);
                            }else {
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        CommonUtils.initiatePopupWindow(AcceptBidPayment.this, getResources().getString(R.string.your_shipment_has_been_placed),
                                false, getResources().getString(R.string.success), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                    @Override
                                    public void positiveClick() {
                                        editor.putBoolean("hitActiveShipmentService", true);
                                        editor.commit();
                                        Intent i5 = new Intent(AcceptBidPayment.this, MyShipments.class);
                                        i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                        i5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(i5);

                                    }
                                });
                    }



                } else if (status_code.equalsIgnoreCase("406")) {

                    CommonUtils.initiatePopupWindow(AcceptBidPayment.this, getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    CommonUtils.initiatePopupWindow(AcceptBidPayment.this, getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else {
                    CommonUtils.initiatePopupWindow(AcceptBidPayment.this, getResources().getString(R.string.error_msg),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });

                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    public void goToCitrusPayment(String transectionId){
        $txd_id = transectionId;

        double amoutToPay=0;
        for(int j=0;j<paymentArray.length();j++){
            try {
                JSONObject innerObject=paymentArray.getJSONObject(j);
                if(j==0){
                    innerObject.put("amount",tot_value);
                    amoutToPay=innerObject.getDouble("amount");
                }else {
                }
                $amount = String.valueOf(amoutToPay);
                paymentArray.put(j,innerObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

//        Intent i2 = new Intent(AcceptBidPayment.this, PaymentActivity.class);
//        startActivity(i2);






       callInstamojoPay("janedoe@gmail.com","9779712016","90","Hlox","Janne");


      /*  Intent i2 = new Intent(AcceptBidPayment.this, CitrusPayment.class);
        i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i2.putExtra("fromBilBoard", true);
        i2.putExtra("transectionId", transectionId);
        i2.putExtra("amount_to_pay", "" + amoutToPay);
       // i2.putExtra("amount_to_pay", "" + tot_value);
        i2.putExtra("shipment_id", "" + shipmentId);
        i2.putExtra("bid_id", truckerBidId);
        i2.putExtra("service_for","accept-bid");
        i2.putExtra("payments", jsonObject.toString());
        startActivity(i2);*/
    }



    public void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final  Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }


    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                Log.d("ResponseInstaPay",response);
                String[] alpha = response.split(":");
                String[] alp = alpha[3].split("=");
                callAfterPayAPI(alp[1]);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    private void callAfterPayAPI(String response){
        NetworkService networkService =  NetworkService.INSTANCE;
        APIService api = networkService.create();

        Map<String,String> map = new HashMap<>();
        map.put("TxId",$txd_id);
        map.put("TxStatus","SUCCESS");
        map.put("amount",$amount);
        map.put("pgTxnNo","");
        map.put("paymentMode","Online");
        map.put("currency","INR");
        map.put("service_request","complete-payment");
        map.put("processed_paymentid",response);

        Call<PaymentResponse> ab =  api.sendDataPayment(map);
        ab.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, retrofit2.Response<PaymentResponse> response) {
                Log.d("###OnSuccess Print","Response");
                Log.d("Print",response.body().getResponse().getTxnId());
                goToActiveShipments(false);
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                Log.d("###Onfailure Print","Response");
                Log.d("Error",t.toString());
                t.printStackTrace();
                Log.d("Error",t.getMessage());
            }
        });

    }

    public void goToActiveShipments(boolean isFrompandingPayment){
//        if(isFrompandingPayment){
//            Intent BackIntent = new Intent();
//            BackIntent.putExtra("orderId", orderId);
//            BackIntent.putExtra("shipmentId", shipment_id);
//            setResult(101,BackIntent);
//            finish();
//        }else {
        editor1.clear();
        editor1.commit();
        editor.putBoolean("hitActiveShipmentService", true);
        editor.commit();
        Intent i5 = new Intent(AcceptBidPayment.this, MyShipments.class);
        i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i5);
//        }

    }
}
