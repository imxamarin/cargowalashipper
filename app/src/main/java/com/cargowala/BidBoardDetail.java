package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class BidBoardDetail extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;
    LinearLayout main_layout;
    RelativeLayout actionBar;
    ImageView backbutton,no_connection_image;

    MyTextView remove_bid;

    String orderId1,loadType1,fromCity1,toCity1,priorityDelivery1,transitDate1,truckName1
            ,transitTime1,truckQuantity1,bidsQuantity1,truckCategory1,orderIdBig1,insuranceAmount;

    MyTextViewSemi from_text,to_text,load_priority_text,no_trucks_text,date_text,time_text;
    MyTextView order_id_text, truck_name_text,bid_avail_text;
    ImageView truck_cat_image;
    SweetAlertDialog pDialog;

    private List<String> orderIdArray = new ArrayList<String>();
    private List<String> priceArray = new ArrayList<String>();
    private List<String> shipment_id = new ArrayList<String>();

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;

    int ratingBarImages2[] = { R.drawable.zero_rating, R.drawable.point_five_rating,
            R.drawable.one__rating, R.drawable.one_point_five_rating, R.drawable.two_rating,
            R.drawable.two_point_five_rating, R.drawable.three_rating, R.drawable.three_point_five_rating,
            R.drawable.four_rating, R.drawable.four_point_five_rating, R.drawable.five_rating };

    String isBidRejected = "0";

    MyTextViewSemi view_details_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bid_board_detail);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        Intent intent = getIntent();

        orderId1 = intent.getExtras().getString("orderId1");
        loadType1 = intent.getExtras().getString("loadType1");
        fromCity1 = intent.getExtras().getString("fromCity1");
        toCity1 = intent.getExtras().getString("toCity1");
        priorityDelivery1 = intent.getExtras().getString("priorityDelivery1");
        transitDate1 = intent.getExtras().getString("transitDate1");
        truckName1 = intent.getExtras().getString("truckName1");
        transitTime1 = intent.getExtras().getString("transitTime1");
        truckQuantity1 = intent.getExtras().getString("truckQuantity1");
        bidsQuantity1 = intent.getExtras().getString("bidsQuantity1");
        truckCategory1 = intent.getExtras().getString("truckCategory1");
        orderIdBig1 = intent.getExtras().getString("orderIdBig1");
        insuranceAmount = intent.getExtras().getString("insuranceAmount");



        Log.d("id to hit ", orderId1);

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();


        main_layout = (LinearLayout)findViewById(R.id.main_layout);

        remove_bid = (MyTextView)findViewById(R.id.remove_bid);
        remove_bid.setOnClickListener(this);
        no_connection_image= (ImageView)findViewById(R.id.no_connection_image);
        truck_cat_image = (ImageView)findViewById(R.id.truck_cat_image);

        order_id_text = (MyTextView)findViewById(R.id.order_id_text);
        truck_name_text = (MyTextView)findViewById(R.id.truck_name_text);
        from_text = (MyTextViewSemi)findViewById(R.id.from_text);
        to_text = (MyTextViewSemi)findViewById(R.id.to_text);
        load_priority_text = (MyTextViewSemi)findViewById(R.id.load_priority_text);
        no_trucks_text = (MyTextViewSemi)findViewById(R.id.no_trucks_text);
        date_text = (MyTextViewSemi)findViewById(R.id.date_text);
        time_text = (MyTextViewSemi)findViewById(R.id.time_text);
        bid_avail_text = (MyTextView)findViewById(R.id.bid_avail_text);


        order_id_text.setText(getResources().getString(R.string.order_id)+orderId1);
        truck_name_text.setText(truckName1);
        from_text.setText(fromCity1);
        to_text.setText(toCity1);
        no_trucks_text.setText(truckQuantity1);
        date_text.setText(transitDate1);
        time_text.setText(transitTime1);


        view_details_text = (MyTextViewSemi) findViewById(R.id.view_details_text);
        String text = "<font color=#585858><b><u>"+getResources().getString(R.string.view_item_details)+"</u></b></font>";
        view_details_text.setText(Html.fromHtml(text));
        view_details_text.setOnClickListener(this);


        if(priorityDelivery1.equalsIgnoreCase("Yes"))
        {
            load_priority_text.setText(loadType1 + " - "+getResources().getString(R.string.priority));
        }
        else
        {
            load_priority_text.setText(loadType1);
        }


        if(truckCategory1.equalsIgnoreCase("1"))
        {
            truck_cat_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.small_grey_truck));
        }
        else if(truckCategory1.equalsIgnoreCase("2"))
        {
            truck_cat_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
        }
        else if(truckCategory1.equalsIgnoreCase("3"))
        {
            truck_cat_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.large_grey_truck));
        }
        else
        {
            Log.d("error","unreachable code");
        }


        if(bidsQuantity1.equalsIgnoreCase("0"))
        {
            bid_avail_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            main_layout.setVisibility(View.GONE);
            bid_avail_text.setText(getResources().getString(R.string.no_bids_available));
        }
        else
        {
            bid_avail_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            main_layout.setVisibility(View.VISIBLE);

            if (bidsQuantity1.equalsIgnoreCase("1")) {

                bid_avail_text.setText(bidsQuantity1 + " "+getResources().getString(R.string.bid_available));
            } else
            {
                bid_avail_text.setText(bidsQuantity1 + " "+getResources().getString(R.string.bids_available));
            }




            if(isOnline())
            {
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();
                makeGetBidsReq();
            }
            else {

                no_connection_image.setVisibility(View.VISIBLE);
                main_layout.setVisibility(View.GONE);

                Toast.makeText(BidBoardDetail.this,
                        getResources().getString(R.string.check_your_network),
                        Toast.LENGTH_SHORT).show();
            }



        }















/*

        for(int i = 0; i<a; i++)
        {






            LinearLayout ll1 = new LinearLayout(
                    BidBoardDetail.this);
            ll1.setOrientation(LinearLayout.HORIZONTAL);


            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);


            ll1.setLayoutParams(params1);

            main_layout.addView(ll1);







            MyTextViewBold trucker_name = new MyTextViewBold(
                    getApplicationContext());
            trucker_name.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));



            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            params2.weight = 0.70f;
            trucker_name.setLayoutParams(params2);


            trucker_name.setText("Trucker Name");

            ll1.addView(trucker_name);




            MyTextViewBold trucker_price = new MyTextViewBold(
                    getApplicationContext());
            trucker_price.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
           trucker_price.setGravity(Gravity.RIGHT);


            LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                   0,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params3.weight = 0.30f;
          trucker_price.setLayoutParams(params3);


            trucker_price.setText("Rs 20000");

            ll1.addView(trucker_price);


            ImageView rating_image = new ImageView(
                    getApplicationContext());

            rating_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.zero_rating));
            rating_image.setAdjustViewBounds(true);


            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    deviceWidth/3,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            params4.setMargins(0, 10, 0, 10);
            rating_image.setLayoutParams(params4);

            main_layout.addView(rating_image);




            MyTextView bid_info = new MyTextView(
                    getApplicationContext());
            bid_info.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

            LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);


            bid_info.setLayoutParams(params5);


            bid_info.setText("vg nods ogdfn hriuh gbiu rgiuerhgigiu hr gerug uiergui iurg eurigie gbr uiiur girue igurbviu be");
            main_layout.addView(bid_info);







            LinearLayout ll3 = new LinearLayout(
                    BidBoardDetail.this);
            ll3.setOrientation(LinearLayout.HORIZONTAL);


            LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
            params6.gravity= Gravity.CENTER;


            params6.setMargins(0, 20, 0, 20);
            ll3.setLayoutParams(params6);

            main_layout.addView(ll3);

            MyTextView accept_text = new MyTextView(
                    getApplicationContext());

            accept_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            accept_text.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.reset_pass_button));

            accept_text.setGravity(Gravity.CENTER);
            LinearLayout.LayoutParams params11 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

             accept_text.setLayoutParams(params11);
            accept_text.setText("Accept");
            accept_text.setPadding(20, 10, 20, 10);
            ll3.addView(accept_text);



            MyTextView reject_text = new MyTextView(
                    getApplicationContext());

            reject_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
           reject_text.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dark_grey_bg_button));

            reject_text.setGravity(Gravity.CENTER);
            LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
            params12.setMargins(20,0,0,0);

            reject_text.setLayoutParams(params12);
            reject_text.setText("Reject");
            reject_text.setPadding(22, 10, 22, 10);
            ll3.addView(reject_text);



            View line = new View(
                    getApplicationContext());
            line.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


            LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    1);

            params7.setMargins(10, 0, 10, 20);
            line.setLayoutParams(params7);


            main_layout.addView(line);




        }
*/









    }

    @Override
    public void onClick(View v) {



        for (int a = 0; a < orderIdArray.size(); a++) {
            if (v.getId() == (100 + a)) {


                //accept change aakash


             //   orderIdArray.get(a);
                Log.d("accept", "accept "+orderIdArray.get(a));
                Log.d("insuranceAmount", "insuranceAmount "+insuranceAmount);
                Log.d("orderIdBig1", "orderIdBig1 "+orderIdBig1);

          //


                Intent i = new Intent(BidBoardDetail.this, AcceptBidPayment.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("insuranceAmount", insuranceAmount);
                i.putExtra("orderIdBig1",orderIdBig1);
                i.putExtra("truckerBidId",orderIdArray.get(a));
                i.putExtra("priceOfBid",priceArray.get(a));
                i.putExtra("shipment_id",shipment_id.get(a));
                startActivity(i);




             /*   if(isOnline())
                {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText("Loading");
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeAcceptBidReq(orderIdArray.get(a));
                }
                else {



                    Toast.makeText(BidBoardDetail.this,
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }

*/


            }

        }


        for (int a = 0; a < orderIdArray.size(); a++) {
            if (v.getId() == (100000 + a)) {


                //reject
                Log.d("reject", "reject " + orderIdArray.get(a));
                ;


                if(isOnline())
                {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText(getResources().getString(R.string.loading));
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeRejectBidReq(orderIdArray.get(a));
                }
                else {



                    Toast.makeText(BidBoardDetail.this,
                            getResources().getString(R.string.check_your_network),
                            Toast.LENGTH_SHORT).show();
                }



            }

        }



        switch (v.getId()) {




            case R.id.backbutton:

                finish();

                break;


            case R.id.remove_bid:


                if(isOnline())
                {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText(getResources().getString(R.string.loading));
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeRemoveOrderReq();
                }
                else {



                    Toast.makeText(BidBoardDetail.this,
                            getResources().getString(R.string.check_your_network),
                            Toast.LENGTH_SHORT).show();
                }


                break;



            case R.id.view_details_text:


                Intent i2 = new Intent(BidBoardDetail.this, ItemsDetail.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                i2.putExtra("orderId1", orderIdBig1);


                startActivity(i2);

                break;





            case R.id.popup_button:


                if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }



                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i3 = new Intent(BidBoardDetail.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i3.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i3);


                    finish();

                }
                else
                {
                    dialog.dismiss();

                    if(isBidRejected.equalsIgnoreCase("1"))
                    {
                        Intent i = new Intent(BidBoardDetail.this, BidBoardDetail.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.putExtra("orderId1", orderId1);
                        i.putExtra("orderIdBig1", orderIdBig1);

                        i.putExtra("loadType1",loadType1);
                        i.putExtra("fromCity1",fromCity1);
                        i.putExtra("toCity1",toCity1);
                        i.putExtra("priorityDelivery1",priorityDelivery1);
                        i.putExtra("transitDate1",transitDate1);
                        i.putExtra("truckName1",truckName1);
                        i.putExtra("transitTime1",transitTime1);
                        i.putExtra("truckQuantity1", truckQuantity1);

                        int a =  Integer.parseInt(bidsQuantity1) - 1;
                        String b = Integer.toString(a);
                        i.putExtra("bidsQuantity1",b );
                        i.putExtra("truckCategory1",truckCategory1);
                        startActivity(i);

                        finish();


                    }




                    if(isBidRejected.equalsIgnoreCase("2"))
                    {

                        //  accepted

                        editor.putBoolean("hitActiveShipmentService", true);
                        editor.commit();


                        Intent i = new Intent(BidBoardDetail.this, MyShipments.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                      //  i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();



                      /*  Intent i = new Intent(BidBoardDetail.this, BidBoardDetail.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.putExtra("orderId1", orderId1);
                        i.putExtra("orderIdBig1", orderIdBig1);

                        i.putExtra("loadType1",loadType1);
                        i.putExtra("fromCity1",fromCity1);
                        i.putExtra("toCity1",toCity1);
                        i.putExtra("priorityDelivery1",priorityDelivery1);
                        i.putExtra("transitDate1",transitDate1);
                        i.putExtra("truckName1",truckName1);
                        i.putExtra("transitTime1",transitTime1);
                        i.putExtra("truckQuantity1", truckQuantity1);

                        int a =  Integer.parseInt(bidsQuantity1) - 1;
                        String b = Integer.toString(a);
                        i.putExtra("bidsQuantity1",b );
                        i.putExtra("truckCategory1",truckCategory1);
                        startActivity(i);

                        finish();*/


                    }


                    if(isBidRejected.equalsIgnoreCase("3"))
                    {

                        //  remove order

                        editor.putBoolean("hitActiveShipmentService", true);
                        editor.commit();

                        finish();




                    }



                }


                break;







        }



    }



    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }

















    String res;
    StringRequest strReq;

    public String makeGetBidsReq() {

        String url = getResources().getString(R.string.base_url)+"user/view-bids";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkBidBoardResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                /*  InstanceID instanceID = InstanceID.getInstance(ChangePassword.this);
                    Log.d("token", "2");
                    String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.d("regg", "6");

                    Log.d("register ", email_id);
                    Log.d("register ",pass);
                    Log.d("token ",token);

                    params.put("username", email_id);
                    params.put("password", pass);
                    params.put("mobile_tokens", token);

                    Log.e("register", params.toString());*/



                    params.put("security_token", pref.getString("security_token","Something Wrong"));

                    params.put("_id",pref.getString("userId","Something Wrong") );

                    params.put("shipment_id",orderIdBig1 );

                    Log.e("register", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "7");



        return null;
    }

    String status_code = "0";

    public void checkBidBoardResponse(String response)
    {



            pDialog.dismiss();



        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);



                if(status_code.equalsIgnoreCase("200"))
                {

                    String response1 = reader.getString("response");

                    JSONArray jsonArr1 = new JSONArray(response1);

                    if(!bidsQuantity1.equalsIgnoreCase(Integer.toString(jsonArr1.length())))
                    {

                        editor.putBoolean("hitMyBidBoardService", true);
                        editor.commit();

                        bidsQuantity1 = Integer.toString(jsonArr1.length());


                        if (bidsQuantity1.equalsIgnoreCase("0")) {
                            bid_avail_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                            main_layout.setVisibility(View.GONE);
                            bid_avail_text.setText(getResources().getString(R.string.no_bids_available));
                        } else {
                            bid_avail_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                            if (bidsQuantity1.equalsIgnoreCase("1")) {

                                bid_avail_text.setText(bidsQuantity1 + " "+getResources().getString(R.string.bid_available));
                            } else {
                                bid_avail_text.setText(bidsQuantity1 + " "+getResources().getString(R.string.bids_available));
                            }


                        }


                    }




                    for (int i = 0; i < jsonArr1.length(); i++) {
                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);


                        String _id1 = jsonObjectPackageData1.getString("_id");
                        String trucker_name1 = jsonObjectPackageData1.getString("trucker_name");
                        String business_name1 = jsonObjectPackageData1.getString("business_name");
                        String price1 = jsonObjectPackageData1.getString("price");
                        String description1 = jsonObjectPackageData1.getString("description");
                        String rating1 = jsonObjectPackageData1.getString("rating");
                        String shipmentId=jsonObjectPackageData1.getString("shipment_id");



                        priceArray.add(price1);
                        orderIdArray.add(_id1);
                        shipment_id.add(shipmentId);




                        LinearLayout ll1 = new LinearLayout(
                                BidBoardDetail.this);
                        ll1.setOrientation(LinearLayout.HORIZONTAL);


                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);


                        ll1.setLayoutParams(params1);

                        main_layout.addView(ll1);







                        MyTextViewBold trucker_name = new MyTextViewBold(
                                getApplicationContext());
                        trucker_name.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));



                        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        params2.weight = 0.70f;
                        trucker_name.setLayoutParams(params2);


                        trucker_name.setText(business_name1);

                        ll1.addView(trucker_name);




                        MyTextViewBold trucker_price = new MyTextViewBold(
                                getApplicationContext());
                        trucker_price.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        trucker_price.setGravity(Gravity.RIGHT);


                        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                        params3.weight = 0.30f;
                        trucker_price.setLayoutParams(params3);


                        trucker_price.setText("\u20B9 " + price1);

                        ll1.addView(trucker_price);


                        ImageView rating_image = new ImageView(
                                getApplicationContext());




                        float ratingValue = Float.parseFloat(rating1);

                        int ratingNumber = (int) ((ratingValue * 10) / 5);


                        rating_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), ratingBarImages2[ratingNumber]));


                    //    rating_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.zero_rating));
                        rating_image.setAdjustViewBounds(true);


                        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                deviceWidth/3,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        params4.setMargins(0, 10, 0, 10);
                        rating_image.setLayoutParams(params4);

                        main_layout.addView(rating_image);




                        MyTextView bid_info = new MyTextView(
                                getApplicationContext());
                        bid_info.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

                        LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT);


                        bid_info.setLayoutParams(params5);


                        bid_info.setText(description1);
                        main_layout.addView(bid_info);







                        LinearLayout ll3 = new LinearLayout(
                                BidBoardDetail.this);
                        ll3.setOrientation(LinearLayout.HORIZONTAL);


                        LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
                        params6.gravity= Gravity.CENTER;


                        params6.setMargins(0, 20, 0, 20);
                        ll3.setLayoutParams(params6);

                        main_layout.addView(ll3);

                        MyTextView accept_text = new MyTextView(
                                getApplicationContext());

                        accept_text.setId(i + 100);
                        accept_text.setOnClickListener(this);

                        accept_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        accept_text.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.reset_pass_button));

                        accept_text.setGravity(Gravity.CENTER);
                        LinearLayout.LayoutParams params11 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

                        accept_text.setLayoutParams(params11);
                        accept_text.setText(getResources().getString(R.string.accept));
                        accept_text.setPadding(20, 10, 20, 10);
                        ll3.addView(accept_text);



                        MyTextView reject_text = new MyTextView(
                                getApplicationContext());

                        reject_text.setId(i + 100000);
                        reject_text.setOnClickListener(this);

                        reject_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        reject_text.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dark_grey_bg_button));

                        reject_text.setGravity(Gravity.CENTER);
                        LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
                        params12.setMargins(20,0,0,0);

                        reject_text.setLayoutParams(params12);
                        reject_text.setText(getResources().getString(R.string.reject));
                        reject_text.setPadding(22, 10, 22, 10);
                        ll3.addView(reject_text);



                        View line = new View(
                                getApplicationContext());
                        line.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                        LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                1);

                        params7.setMargins(10, 0, 10, 20);
                        line.setLayoutParams(params7);


                        main_layout.addView(line);













                    }



                }
                else if(status_code.equalsIgnoreCase("204"))
                {





                }



                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }


                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" "+ status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(BidBoardDetail.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }














    String res1;
    StringRequest strReq1;

    public String makeRejectBidReq(final String id) {

        String url = getResources().getString(R.string.base_url)+"user/reject-bid";
        Log.d("regg", "2");
        strReq1 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res1 = response.toString();
                        checkRejectBidResponse(res1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res1 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("bid_id", id);
                    params.put("_id",pref.getString("userId","Something Wrong"));
                    params.put("security_token", pref.getString("security_token","Something Wrong"));
                    Log.e("BilBoardDetail", params.toString());


                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq1, "8");



        return null;
    }

    public void checkRejectBidResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    editor.putBoolean("hitMyBidBoardService", true);
                    editor.commit();

                    isBidRejected = "1";
                    initiatePopupWindow("Bid has been removed.",
                            false, "Success", "OK");



                       //     hit service true here for bid board



                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }























    String res3;
    StringRequest strReq3;

    public String makeAcceptBidReq(final String id) {

        String url = getResources().getString(R.string.base_url)+"user/accept-bid";
        Log.d("regg", "2");
        strReq3 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res3 = response.toString();
                        checkAcceptBidResponse(res3);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res3 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("shipment_id", orderIdBig1);
                    params.put("bid_id", id  );
                    params.put("_id",pref.getString("userId","Something Wrong"));
                    params.put("security_token", pref.getString("security_token","Something Wrong"));

                    Log.e("register", params.toString());


                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq3, "9");



        return null;
    }

    public void checkAcceptBidResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    editor.putBoolean("hitMyBidBoardService", true);
                    editor.commit();

                    isBidRejected = "2";

                    initiatePopupWindow(getResources().getString(R.string.bid_has_been_accepted),
                            false, getResources().getString(R.string.success), getResources().getString(R.string.ok));



                    //     hit service true here for bid board



                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }


























    String res4;
    StringRequest strReq4;

    public String makeRemoveOrderReq() {

        String url = getResources().getString(R.string.base_url)+"user/remove-shipment";
        Log.d("regg", "2");
        strReq4 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res4 = response.toString();
                        checkRemoveOrderResponse(res4);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res4 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("shipment_id", orderIdBig1);


                    params.put("_id",pref.getString("userId","Something Wrong"));
                    params.put("security_token", pref.getString("security_token","Something Wrong"));

                    Log.e("register", params.toString());


                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq4, "10");



        return null;
    }

    public void checkRemoveOrderResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    editor.putBoolean("hitMyBidBoardService", true);
                    editor.commit();

                    isBidRejected = "3";

                    initiatePopupWindow(getResources().getString(R.string.order_has_been_removed),
                            false, getResources().getString(R.string.success), getResources().getString(R.string.ok));



                    //     hit service true here for bid board



                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }

}
