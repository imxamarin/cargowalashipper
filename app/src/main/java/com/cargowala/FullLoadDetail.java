package com.cargowala;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.cargowala.ImageLib.PickerBuilder;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */

@RuntimePermissions
public class FullLoadDetail extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;
    int total_items = 0;

    MyTextViewBold sub_no_trucks, add_no_trucks;

    ScrollView exampleScrollView;
    RelativeLayout actionBar;
    ImageView backbutton, order_progress;
    MyTextView next, save;
    MaterialEditText item_name, item_desc, kg, length_in, width_in, height_in, units;
    LinearLayout added_items_layout, add_item_layout;
    ImageView add_another_item;
    int tot_weight = 0;
    MyTextViewSemi tot_wt;

    String cat_id, sub_cat_id;
    MyTextView data_saved_text;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    SweetAlertDialog pDialog;

    Spinner truck_type_spinner, truck_type_spinner1;
    ArrayAdapter<CharSequence> adapter;


    String cat_array[];
    String sub_cat_array[];


    SharedPreferences pref1;
    SharedPreferences.Editor editor1;


    ArrayList<String> cat_name_list = new ArrayList<String>();
    ArrayList<String> cat_id_list = new ArrayList<String>();
    ArrayList<String> sub_cat_name_list = new ArrayList<String>();
    ArrayList<String> sub_cat_id_list = new ArrayList<String>();


    ImageView upload_image, edit_upload_image, edit_upload_delete;
    String invoice_base64 = "";
    boolean isUpload = true;
    File mediaStorageDir;
    public static Uri uri;
    public static int CAMERA_REQUEST = 2, GALLERY_REQUEST = 4, CROP_FROM_CAMERA = 3;
    Dialog dialogBox;
    private String path;
    private String filepath;

    boolean isCamera = false;
    private String croppath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_load_detail);

        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();


        pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

       /* editor.putString("item_data", "empty");
        editor.commit();*/


        truck_type_spinner = (Spinner) findViewById(R.id.truck_type_spinner);
      /*  truckType0 = new String[1];
        truckType0[0] ="Select Truck Type";

        String cat_array[];
        String sub_cat_array[];
*/


        try {
            String res = pref.getString("book_get_data_response", "");


            JSONObject jsonobj = new JSONObject(res);



            String Categories = jsonobj.getString("Categories");

            JSONObject jsonobj1 = new JSONObject(Categories);

            String parent = jsonobj1.getString("parent");
            String sub_categories = jsonobj1.getString("sub-categories");

            JSONObject jsonobj3 = new JSONObject(sub_categories);


            JSONObject jsonobj2 = new JSONObject(parent);

            String self = jsonobj2.getString("self");


            JSONArray jsonArr1 = new JSONArray(self);


            for (int j = 0; j < jsonArr1.length(); j++) {

                JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(j);

                String id = jsonObjectPackageData1.getString("id");
                String name = jsonObjectPackageData1.getString("name");

                if (jsonobj3.has(id)) {

                    cat_name_list.add(name);
                    cat_id_list.add(id);


                    if (cat_id_list.size() == 1) {
                        String id1 = jsonobj3.getString(id);

                        JSONArray jsonArr2 = new JSONArray(id1);

                        for (int k = 0; k < jsonArr2.length(); k++) {
                            JSONObject jsonObjectPackageData2 = jsonArr2.getJSONObject(k);
                            String id2 = jsonObjectPackageData2.getString("id");
                            String name2 = jsonObjectPackageData2.getString("name");
                            sub_cat_name_list.add(name2);
                            sub_cat_id_list.add(id2);


                        }
                    }


                }


            }


        } catch (Exception e) {


            Log.d("parse error", "spinner data incorrect");

        }


        cat_array = new String[cat_name_list.size()];
        sub_cat_array = new String[sub_cat_name_list.size()];


        cat_array = cat_name_list.toArray(cat_array);
        sub_cat_array = sub_cat_name_list.toArray(sub_cat_array);


        data_saved_text = (MyTextView) findViewById(R.id.data_saved_text);
        String text2 = "<font color=#585858  >"+getResources().getString(R.string.in_case_you_miss_out_on_completing)+"</font> <font color=#585858><b>"+getResources().getString(R.string.incomplete_order_space)+"</b></font> " +
                "<font color=#585858  >"+getResources().getString(R.string.in_the_side_menu)+"</font>";

        data_saved_text.setText(Html.fromHtml(text2));


        add_another_item = (ImageView) findViewById(R.id.add_another_item);
        add_another_item.setOnClickListener(this);
        added_items_layout = (LinearLayout) findViewById(R.id.added_items_layout);
        add_item_layout = (LinearLayout) findViewById(R.id.add_item_layout);


        item_name = (MaterialEditText) findViewById(R.id.item_name);
        item_desc = (MaterialEditText) findViewById(R.id.item_desc);


        kg = (MaterialEditText) findViewById(R.id.kg);
        length_in = (MaterialEditText) findViewById(R.id.length_in);
        width_in = (MaterialEditText) findViewById(R.id.width_in);
        height_in = (MaterialEditText) findViewById(R.id.height_in);
        units = (MaterialEditText) findViewById(R.id.units);


        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int px = Math.round(40 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));


        truck_type_spinner.setDropDownWidth((int) (deviceWidth - px));

        adapter = new ArrayAdapter(this,
                R.layout.spinner_layout, cat_array);

        // adapter=ArrayAdapter.createFromResource(this,R.array.country_names,android.R.layout.simple_list_item_1);


        adapter.setDropDownViewResource(R.layout.spinner_layout);
        truck_type_spinner.setAdapter(adapter);
        truck_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent1, View view, int position, long id3) {
                //    Toast.makeText(getBaseContext(), position + " is selected "+cat_name_list.get(position), Toast.LENGTH_SHORT).show();


                Log.d("1111 ", cat_name_list.get(position));
                Log.d("2222 ", cat_id_list.get(position));


                cat_id = cat_id_list.get(position);


                try {
                    String res = pref.getString("book_get_data_response", "");
                    JSONObject jsonobj = new JSONObject(res);
                    String Categories = jsonobj.getString("Categories");

                    JSONObject jsonobj1 = new JSONObject(Categories);

                    String parent = jsonobj1.getString("parent");
                    String sub_categories = jsonobj1.getString("sub-categories");

                    JSONObject jsonobj3 = new JSONObject(sub_categories);


                    JSONObject jsonobj2 = new JSONObject(parent);

                    String self = jsonobj2.getString("self");


                    JSONArray jsonArr1 = new JSONArray(self);


                    for (int j = 0; j < jsonArr1.length(); j++) {

                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(j);

                        String id = jsonObjectPackageData1.getString("id");
                        String name = jsonObjectPackageData1.getString("name");

                    /*    if(jsonobj3.has(cat_id_list.get(position)))*/

                        Log.d("sfvs ", parent1.getSelectedItem().toString());
                        if (name.equalsIgnoreCase(parent1.getSelectedItem().toString())) {

                            String id1 = jsonobj3.getString(id);
                            JSONArray jsonArr2 = new JSONArray(id1);


                            sub_cat_name_list.clear();
                            sub_cat_id_list.clear();

                            for (int k = 0; k < jsonArr2.length(); k++) {

                                JSONObject jsonObjectPackageData2 = jsonArr2.getJSONObject(k);

                                String id2 = jsonObjectPackageData2.getString("id");
                                String name2 = jsonObjectPackageData2.getString("name");


                                sub_cat_name_list.add(name2);
                                sub_cat_id_list.add(id2);

                                Log.d("1 ", name2);
                                Log.d("2 ", id2);

                            }


                            sub_cat_array = new String[sub_cat_name_list.size()];
                            sub_cat_array = sub_cat_name_list.toArray(sub_cat_array);


                            adapter = new ArrayAdapter(FullLoadDetail.this,
                                    R.layout.spinner_layout, sub_cat_array);


                            adapter.setDropDownViewResource(R.layout.spinner_layout);
                            truck_type_spinner1.setAdapter(adapter);


                        }


                    }


                } catch (Exception e) {


                    Log.d("parse error", "spinner data incorrect " + e.getLocalizedMessage());

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


        truck_type_spinner1 = (Spinner) findViewById(R.id.truck_type_spinner1);


        truck_type_spinner1.setDropDownWidth((int) (deviceWidth - px));

        adapter = new ArrayAdapter(this,
                R.layout.spinner_layout, sub_cat_array);

        //  adapter=ArrayAdapter.createFromResource(this,R.array.country_names,android.R.layout.simple_list_item_1);
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        truck_type_spinner1.setAdapter(adapter);
        truck_type_spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(getBaseContext(), position + "is selected", Toast.LENGTH_SHORT).show();
                // String text = truck_type_spinner1.getSelectedItem().toString();

                //     Toast.makeText(getBaseContext(), position + " is selected "+sub_cat_name_list.get(position), Toast.LENGTH_SHORT).show();

                Log.d("3333 ", sub_cat_name_list.get(position));
                Log.d("4444 ", sub_cat_id_list.get(position));

                sub_cat_id = sub_cat_id_list.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();


        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);
        backbutton.requestLayout();
        backbutton.setOnClickListener(this);

        order_progress = (ImageView) findViewById(R.id.order_progress);
        order_progress.getLayoutParams().width = (deviceWidth / 2);
        order_progress.requestLayout();

        next = (MyTextView) findViewById(R.id.next);
        next.setOnClickListener(this);

        save = (MyTextView) findViewById(R.id.save);
        save.setOnClickListener(this);

        tot_wt = (MyTextViewSemi) findViewById(R.id.tot_wt);
        tot_wt.setOnClickListener(this);

        sub_no_trucks = (MyTextViewBold) findViewById(R.id.sub_no_trucks);
        sub_no_trucks.setOnClickListener(this);

        add_no_trucks = (MyTextViewBold) findViewById(R.id.add_no_trucks);
        add_no_trucks.setOnClickListener(this);


        exampleScrollView = (ScrollView) findViewById(R.id.exampleScrollView);


        upload_image = (ImageView) findViewById(R.id.upload_image);
        upload_image.setOnClickListener(this);

        edit_upload_image = (ImageView) findViewById(R.id.edit_upload_image);
        edit_upload_image.setOnClickListener(this);
        edit_upload_delete = (ImageView) findViewById(R.id.edit_upload_delete);
        edit_upload_delete.setOnClickListener(this);


        if (!pref.getString("item_data", "empty").equalsIgnoreCase("empty")) {

            add_another_item.setVisibility(View.VISIBLE);
            data_saved_text.setVisibility(View.VISIBLE);
                    /*added_items_layout*/
            add_item_layout.setVisibility(View.GONE);

            makeView(pref.getString("item_data", "empty"));

            Log.d("main item data ", pref.getString("item_data", "empty"));
        }





  /*      kg

                units*/


        kg.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                int item_wt = 0;
                int units_wt = 0;
                if (s.length() == 0) {
                    item_wt = 0;
                } else {
                    item_wt = Integer.parseInt(kg.getText().toString());
                }

                if (units.getText().toString().equalsIgnoreCase("")) {
                    units_wt = 0;
                } else {
                    units_wt = Integer.parseInt(units.getText().toString());
                }


                tot_weight = item_wt * units_wt;


                if (tot_weight == 0) {
                    tot_wt.setText(getResources().getString(R.string.total_weight_na));
                } else {
                    tot_wt.setText(getResources().getString(R.string.total_weight) + tot_weight + " "+getResources().getString(R.string.kg)+")");
                }

            }
        });


        units.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                int item_wt = 0;
                int units_wt = 0;
                if (s.length() == 0) {
                    units_wt = 0;
                } else {
                    units_wt = Integer.parseInt(units.getText().toString());
                }

                if (kg.getText().toString().equalsIgnoreCase("")) {
                    item_wt = 0;
                } else {
                    item_wt = Integer.parseInt(kg.getText().toString());
                }


                tot_weight = item_wt * units_wt;

                if (tot_weight == 0) {
                    tot_wt.setText(getResources().getString(R.string.total_weight_na));
                } else {
                   // tot_wt.setText("(total weight = " + tot_weight + " kg)");
                    tot_wt.setText(getResources().getString(R.string.total_weight) + tot_weight + " "+getResources().getString(R.string.kg)+")");
                }
            }
        });


    }

    @Override
    public void onClick(View v) {


        for (int a = 0; a < total_items; a++) {
            if (v.getId() == (100 + a)) {

                Log.d("number ", "" + a);


                try {
                    String res = pref.getString("item_data", "empty");

                    JSONObject jsonObj = new JSONObject(res);


                    JSONArray jsonArray = jsonObj.getJSONArray("Items");


                    Log.d("before object", "" + jsonObj);
                    jsonArray.remove(a);
                    Log.d("after object", "" + jsonObj);


                    if (jsonArray.length() == 0) {
                        editor.putString("item_data", "empty");
                    } else {
                        editor.putString("item_data", jsonObj.toString());
                    }

                    editor.commit();


                    makeView(jsonObj.toString());

                } catch (JSONException e) {


                }
            }

        }


        switch (v.getId()) {


            case R.id.backbutton:

                finish();

                break;

            case R.id.next:


                if (pref.getString("item_data", "empty").equalsIgnoreCase("empty")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_atleast_1_item_details), Toast.LENGTH_LONG)
                            .show();
                } else {

                    //aakash hit service here and remove data from editor1 i.e. of data details for pending screen

                  /*  Intent i = new Intent(FullLoadDetail.this, LoadSummary.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i);*/


                    Log.d("res ", "res " + pref.getString("item_data", "empty"));


                    Intent i = new Intent(FullLoadDetail.this, LoadInsurance.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i);


                     /*   if(isOnline())
                        {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText("Loading");
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makeBookNowReq();
                        }
                        else {
                            Toast.makeText(FullLoadDetail.this,
                                    "Please check your network connection",
                                    Toast.LENGTH_SHORT).show();
                        }
*/


                }

                break;


            case R.id.add_another_item:
                add_another_item.setVisibility(View.GONE);
                data_saved_text.setVisibility(View.GONE);
                    /*added_items_layout*/
                add_item_layout.setVisibility(View.VISIBLE);

                break;


            case R.id.save:

                String item_namee = item_name.getText().toString().trim();
                String category =
                        truck_type_spinner.getSelectedItem()==null ?"Default":
                                truck_type_spinner.getSelectedItem().toString();
                String sub_category =truck_type_spinner1.getSelectedItem()==null ?"Default":
                        truck_type_spinner1.getSelectedItem().toString();
                String description = item_desc.getText().toString().trim();


                if (item_namee.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_item_name), Toast.LENGTH_SHORT)
                            .show();
                }
               /* else if(category.equalsIgnoreCase("Select"))
                {
                    Toast.makeText(getApplicationContext(),
                            "Please Select Category", Toast.LENGTH_SHORT)
                            .show();
                }
                else if(sub_category.equalsIgnoreCase("Select"))
                {
                    Toast.makeText(getApplicationContext(),
                            "Please Select Sub Category", Toast.LENGTH_SHORT)
                            .show();
                }*/
                else if (description.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_description), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    //   addItem( item_namee, category, sub_category, description);


                    if (invoice_base64.equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.please_upload_image_of_invoice_copy), Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        addItem(item_namee, category, sub_category, description);
                    }


                }


                break;


            case R.id.sub_no_trucks:


                if (!units.getText().toString().equalsIgnoreCase("")) {
                    int no_of_units = Integer.parseInt(units.getText().toString());

                    if (no_of_units == 0) {

                    } else {
                        units.setText("" + (no_of_units - 1));
                    }
                }


                break;


            case R.id.add_no_trucks:

                if (!units.getText().toString().equalsIgnoreCase("")) {
                    int no_of_units = Integer.parseInt(units.getText().toString());


                    units.setText("" + (no_of_units + 1));

                } else {
                    units.setText("1");
                }


                break;


            case R.id.upload_image:


                if (isUpload) {
                    DialogImage();
                }
                break;


            case R.id.edit_upload_image:

                DialogImage();

                break;

            case R.id.edit_upload_delete:


                isUpload = true;
                invoice_base64 = "";
                edit_upload_image.setVisibility(View.GONE);
                edit_upload_delete.setVisibility(View.GONE);
                upload_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.upload_invoice));
                upload_image.setAlpha(1f);


                break;


            case R.id.popup_button:


                if (status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910")) {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook")) {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    } else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google")) {
                        isGoogle = true;
                    }


                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(FullLoadDetail.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn", isGoogle);
                    startActivity(i2);


                    finish();

                } else {
                    dialog.dismiss();
                }


                break;


        }


    }


    public void addItem(String item_namee, String category, String sub_category, String description) {
        add_another_item.setVisibility(View.VISIBLE);
        data_saved_text.setVisibility(View.VISIBLE);
        add_item_layout.setVisibility(View.GONE);


        String result = pref.getString("item_data", "empty");
        JSONObject item = new JSONObject();
        try {

            SecureRandom random = new SecureRandom();


            item.put("id", new BigInteger(130, random).toString(32));
            item.put("name", item_namee);

            item.put("parent_category_name", category);
            item.put("sub_category_name", sub_category);


            item.put("parent_category", cat_id);
            item.put("sub_category", sub_cat_id);
            item.put("desc", description);
            item.put("weight", kg.getText().toString().trim());
            item.put("length", length_in.getText().toString().trim());
            item.put("width", width_in.getText().toString().trim());
            item.put("height", height_in.getText().toString().trim());
            item.put("total_item", units.getText().toString().trim());
            item.put("invoice_image", invoice_base64);
            item.put("total_weight", tot_weight);


           /* if(tot_weight==0)
            {
                item.put("total_item_wt","N/A" );
            }
            else
            {
                item.put("total_item_wt",tot_weight );
            }*/


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }


        try {


            String res;
            if (pref.getString("item_data", "empty").equalsIgnoreCase("empty")) {
                JSONArray jsonArray = new JSONArray();

                jsonArray.put(item);
                JSONObject itemObj = new JSONObject();
                itemObj.put("Items", jsonArray);

                res = itemObj.toString();

                editor.putString("item_data", itemObj.toString());
                editor.commit();
            } else {
                JSONObject jsonObj = new JSONObject(result);


                JSONArray jsonArray = jsonObj.getJSONArray("Items");

                jsonArray.put(item);


                jsonObj.put("Items", jsonArray);


                res = jsonObj.toString();
                editor.putString("item_data", res);
                editor.commit();

            }
            makeView(res);


        } catch (JSONException e) {


        }


        item_name.setText("");
        item_desc.setText("");

        kg.setText("");
        length_in.setText("");
        width_in.setText("");
        height_in.setText("");
        units.setText("");
        tot_wt.setText(getResources().getString(R.string.total_weight_na));
        upload_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.upload_invoice));

        isUpload = true;
        tot_weight = 0;
        invoice_base64 = "";

        upload_image.setAlpha(1f);

        edit_upload_image.setVisibility(View.GONE);
        edit_upload_delete.setVisibility(View.GONE);
        upload_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.upload_invoice));


        truck_type_spinner.setSelection(0);
        truck_type_spinner1.setSelection(0);
    }


    public void makeView(String res) {

        Log.d("data", "res " + res);


        try {
            added_items_layout.removeAllViews();
            JSONObject reader = new JSONObject(res);
            String Items = reader.getString("Items");

            JSONArray jsonArr1 = new JSONArray(Items);
            total_items = jsonArr1.length();

            if (total_items == 0) {
                add_another_item.setVisibility(View.GONE);
                data_saved_text.setVisibility(View.GONE);
                add_item_layout.setVisibility(View.VISIBLE);
            }

            for (int i = 0; i < jsonArr1.length(); i++) {

                JSONObject j1 = jsonArr1.getJSONObject(i);

                String item_name1 = j1.getString("name");
                String category1 = j1.getString("parent_category_name");
                String sub_category1 = j1.getString("sub_category_name");
                //    String item_desc1 = j1.getString("desc");


                RelativeLayout bg = new RelativeLayout(
                        getApplicationContext());
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params1.setMargins(0, 20, 0,
                        0);

                LinearLayout verticalLayout = new LinearLayout(
                        getApplicationContext());

                verticalLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

                LinearLayout.LayoutParams paramsVerticalLayout = new LinearLayout.LayoutParams(
                        deviceWidth,
                        ViewGroup.LayoutParams.MATCH_PARENT);


                verticalLayout
                        .setLayoutParams(paramsVerticalLayout);

                verticalLayout
                        .setOrientation(LinearLayout.VERTICAL);

                bg.setLayoutParams(params1);
                bg.addView(verticalLayout);
                added_items_layout.addView(bg);


                LinearLayout horizontalLayout = new LinearLayout(
                        getApplicationContext());


                LinearLayout.LayoutParams paramshorizontalLayout = new LinearLayout.LayoutParams(
                        deviceWidth,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                paramshorizontalLayout.setMargins(10, 10, 0,
                        0);


                horizontalLayout
                        .setLayoutParams(paramshorizontalLayout);

                horizontalLayout
                        .setOrientation(LinearLayout.HORIZONTAL);
                verticalLayout.addView(horizontalLayout);


                MyTextView item_no = new MyTextView(
                        getApplicationContext());
                item_no.setText(getResources().getString(R.string.item_space)+ (i + 1) + " : ");

                item_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));
                LinearLayout.LayoutParams paramsitemMargin = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);


                item_no
                        .setLayoutParams(paramsitemMargin);
                horizontalLayout.addView(item_no);


                MyTextView item_name2 = new MyTextView(
                        getApplicationContext());
                item_name2.setText("" + item_name1);

                item_name2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                item_name2
                        .setLayoutParams(paramsitemMargin);
                horizontalLayout.addView(item_name2);


                LinearLayout horizontalLayout2 = new LinearLayout(
                        getApplicationContext());


                horizontalLayout2
                        .setLayoutParams(paramshorizontalLayout);

                horizontalLayout2
                        .setOrientation(LinearLayout.HORIZONTAL);
                verticalLayout.addView(horizontalLayout2);


                MyTextView item_cat = new MyTextView(
                        getApplicationContext());
                item_cat.setText(getResources().getString(R.string.category)+"  : ");

                item_cat.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));


                item_cat
                        .setLayoutParams(paramsitemMargin);
                horizontalLayout2.addView(item_cat);


                MyTextView item_cat2 = new MyTextView(
                        getApplicationContext());
                item_cat2.setText("" + category1);

                item_cat2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                item_cat2
                        .setLayoutParams(paramsitemMargin);
                horizontalLayout2.addView(item_cat2);


                LinearLayout horizontalLayout3 = new LinearLayout(
                        getApplicationContext());


                LinearLayout.LayoutParams paramshorizontalLayout1 = new LinearLayout.LayoutParams(
                        deviceWidth,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                paramshorizontalLayout1.setMargins(10, 10, 0,
                        10);


                horizontalLayout3
                        .setLayoutParams(paramshorizontalLayout1);

                horizontalLayout3
                        .setOrientation(LinearLayout.HORIZONTAL);
                verticalLayout.addView(horizontalLayout3);


                MyTextView item_sub_cat = new MyTextView(
                        getApplicationContext());
                item_sub_cat.setText(getResources().getString(R.string.sub_category)+"  : ");

                item_sub_cat.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));


                item_sub_cat
                        .setLayoutParams(paramsitemMargin);
                horizontalLayout3.addView(item_sub_cat);


                MyTextView item_sub_cat2 = new MyTextView(
                        getApplicationContext());
                item_sub_cat2.setText("" + sub_category1);

                item_sub_cat2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                item_sub_cat2
                        .setLayoutParams(paramsitemMargin);
                horizontalLayout3.addView(item_sub_cat2);


                ImageView delete = new ImageView(
                        getApplicationContext());
                RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(

                        deviceWidth / 10,
                        deviceWidth / 10);
                //   params5.setMargins(0, 0, 20, 60);

                params5.addRule(
                        RelativeLayout.ALIGN_PARENT_RIGHT,
                        RelativeLayout.TRUE);

                params5.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
                        RelativeLayout.TRUE);

                delete.setLayoutParams(params5);

                delete
                        .setOnClickListener(FullLoadDetail.this);
                delete.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.delete_icon));


                delete.setId(i + 100);

                bg.addView(delete);


            }


        } catch (Exception e) {

        }
    }

/*

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }
*/


    // camera gallery photo


    TextView camera_text, open_gallery_text, cancel_text;

    private void DialogImage() {
        dialogBox = new Dialog(FullLoadDetail.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogBox.setContentView(R.layout.camera_gallery_popup);

        // Animation animation = AnimationUtils.loadAnimation(context, R.anim.bottom_up_anim);
        dialogBox.show();

        camera_text = (TextView) dialogBox.findViewById(R.id.camera_text);

        // RelativeLayout dialog_rel = (RelativeLayout) dialogBox.findViewById(R.id.dialog_rel);

        open_gallery_text = (TextView) dialogBox.findViewById(R.id.open_gallery_text);
        //remove_photo_text= (TextView) dialogBox.findViewById(R.id.remove_photo_text);
        cancel_text = (TextView) dialogBox.findViewById(R.id.cancel_text);

        cancel_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                dialogBox.dismiss();
            }
        });

        //remove_photo_text.setVisibility(View.GONE);
        camera_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();
                FullLoadDetailPermissionsDispatcher.takephotoWithPermissionCheck(FullLoadDetail.this);
                isCamera = true;



            }
        });

        open_gallery_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();
                FullLoadDetailPermissionsDispatcher.initGalleryWithPermissionCheck(FullLoadDetail.this);
                isCamera = false;

            }
        });

    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE})
    public void initGallery() {
        //takephotoFromGallery();
//        mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "CargoWala");
//        File file = new File(mediaStorageDir.getAbsolutePath(),
//                "CargoWala.jpg");
//
//        filepath = file.getAbsolutePath().toString().trim();
//        Crop.pickImage(FullLoadDetail.this);

        new PickerBuilder(FullLoadDetail.this, PickerBuilder.SELECT_FROM_GALLERY)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {

                        Picasso.with(FullLoadDetail.this).load(imageUri).into(upload_image);

                        final InputStream imageStream;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            invoice_base64  = encodeImage(selectedImage);



                            editor.putString("imagebase64", invoice_base64);

                            editor.putString("userImage", "");
                            editor.commit();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();


    }

    public void takephotoFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);

        /*Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image*//*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);*/

    }


    @NeedsPermission(Manifest.permission.CAMERA)
    public void takephoto() {
//        mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "CargoWala");
//        // Create the storage directory(MyCameraVideo) if it
//        // does not
//        // exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                Toast.makeText(getApplicationContext(),
//                        getResources().getString(R.string.failed_to_create_directory_cargowala),
//                        Toast.LENGTH_LONG).show();
//                Log.d("MyCameraVideo",
//                        "Failed to create directory CargoWala.");
//            }
//        }
//
//        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//        File file = new File(mediaStorageDir.getAbsolutePath(),
//                "CargoWala.jpg");
//        path = file.getAbsolutePath();
//        filepath = path;
//        uri = Uri.fromFile(file);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
//        startActivityForResult(intent, CAMERA_REQUEST);
//        uri = null;


        new PickerBuilder(FullLoadDetail.this, PickerBuilder.SELECT_FROM_CAMERA)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {

                        Picasso.with(FullLoadDetail.this).load(imageUri).into(upload_image);

                        final InputStream imageStream;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            invoice_base64= encodeImage(selectedImage);

                            upload_image.setAlpha(.5f);

                            isUpload = false;

                            edit_upload_image.setVisibility(View.VISIBLE);
                            edit_upload_delete.setVisibility(View.VISIBLE);


                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();



    }


    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,75,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_REQUEST
                && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(
                    selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = decodeSampledBitmapFromFile(filepath, 1600, 1000);
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(filepath);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
            }

            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
            ByteArrayOutputStream ful_stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, ful_stream);
            byte[] ful_bytes = ful_stream.toByteArray();
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "Title", null);
            uri = Uri.parse(path);


            try {
                beginCrop(data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if ((requestCode == CAMERA_REQUEST)
                && (resultCode == RESULT_OK)) {
            if (resultCode == RESULT_OK) {
                beginCrop(uri);
            }
        } else if ((requestCode == CROP_FROM_CAMERA)) {

            Bitmap bitmap = BitmapFactory.decodeFile(croppath);

            if (bitmap != null) {

                upload_image.setImageBitmap(bitmap);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);


                upload_image.setAlpha(.5f);

                invoice_base64 = Base64.encodeToString(stream.toByteArray(),
                        Base64.NO_WRAP);

                Log.d("im", "invoice_base64 " + invoice_base64);

                isUpload = false;

                Log.d("invoice_base64", invoice_base64);

                edit_upload_image.setVisibility(View.VISIBLE);
                edit_upload_delete.setVisibility(View.VISIBLE);


            }
            // byte[] ful_bytes = thumb_stream.toByteArray();
            // base64 = Base64.encodeBytes(ful_bytes);


            //hit service here


            try {
                File f = new File(croppath);
                f.delete();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        } else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).withMaxSize(deviceWidth, deviceHeight).start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {

            Uri selectedImage = Crop.getOutput(result);
            Bitmap bmp = null, bmp_new = null;
            try {
                bmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(filepath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Log.d("orientation ", "" + orientation);


                Matrix matrix = new Matrix();
                if (orientation == 6 && isCamera) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }

                bmp_new = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp_new.compress(Bitmap.CompressFormat.JPEG, 70, stream);

            upload_image.setImageBitmap(bmp_new);

         /*   base64 = Base64.encodeToString(stream.toByteArray(),
                    Base64.NO_WRAP);
            Log.i("kjbnjhbase64 ", base64);


            Log.d("im", "base64 " + base64);*/

            upload_image.setAlpha(.5f);
            invoice_base64 = Base64.encodeToString(stream.toByteArray(),
                    Base64.NO_WRAP);

            Log.d("im", "invoice_base64 " + invoice_base64);

            isUpload = false;
            Log.d("invoice_base64", invoice_base64);

            edit_upload_image.setVisibility(View.VISIBLE);
            edit_upload_delete.setVisibility(View.VISIBLE);


        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


    public Bitmap decodeSampledBitmapFromFile(String path, int reqWidth,
                                              int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }

        int expectedWidth = width / inSampleSize;
        if (expectedWidth > reqWidth) {

            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    String res;
    StringRequest strReq;

    public String makeBookNowReq() {

        String url = getResources().getString(R.string.base_url) + "user/book-truck";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkeBookNowResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                /*    params.put("oldpassword", old_pass);
                    params.put("newpassword", new_pass);*/


                    params.put("security_token", pref1.getString("security_token", "Something Wrong"));
                    params.put("_id", pref1.getString("userId", "_id Wrong"));
                    params.put("lp_name", pref.getString("book_origin_address", "lp_name Wrong"));
                    params.put("lp_address", pref.getString("book_origin_address", "lp_address Wrong"));
                    params.put("lp_state", pref.getString("book_origin_state", "lp_state Wrong"));
                    params.put("lp_city", pref.getString("book_origin_city", "lp_city Wrong"));
                    params.put("lp_latitude", pref.getString("book_origin_lat", "lp_latitude Wrong"));
                    params.put("lp_longitude", pref.getString("book_origin_long", "lp_longitude Wrong"));
                    params.put("up_name", pref.getString("book_odest_address", "up_name Wrong"));
                    params.put("up_address", pref.getString("book_odest_address", "up_address Wrong"));
                    params.put("up_state", pref.getString("book_dest_state", "up_state Wrong"));
                    params.put("up_city", pref.getString("book_dest_city", "up_city Wrong"));
                    params.put("up_latitude", pref.getString("book_dest_lat", "up_latitude Wrong"));
                    params.put("up_longitude", pref.getString("book_dest_long", "up_longitude Wrong"));
                    params.put("t_date", pref.getString("book_date", "t_date Wrong"));
                    params.put("t_time", pref.getString("book_time", "t_time Wrong"));
                    String distance = pref.getString("book_est_dist","0");
                    if(distance == null || distance.equals("null")){
                        params.put("est_distance", "0");
                    }else{
                        params.put("est_distance", distance);

                    }
                    params.put("est_time", pref.getString("book_est_time", "est_time Wrong"));
                    params.put("shipment_url", pref.getString("book_map_url", "shipment_url Wrong"));
                    params.put("items", ""/*pref.getString("item_data","items Wrong")*/);
                    params.put("category", pref.getString("truck_type", "category Wrong"));
                    params.put("type", pref.getString("truck_id", "type Wrong"));
                    params.put("quantity", pref.getString("no_of_trucks", "quantity Wrong"));
                    params.put("load_type", pref.getString("load_type", "load_type Wrong"));
                    params.put("priority_delivery", pref.getString("priority_delivery", "priority_delivery Wrong"));
                    params.put("invoice_amount", "" + pref.getInt("total_invoice", 0));
                    params.put("overall_weight", "" + pref.getInt("total_item_wt", 0));
                    params.put("action", "get_data");


                    Log.e("register", params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "22");


        return null;
    }

    String status_code = "0";

    public void checkeBookNowResponse(String response) {
        pDialog.dismiss();
        try {
            Log.e("full_load_responce", response);

            if (response != null) {

                Log.e("full_load_responce", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("full_load_responce", "reader");

                status_code = reader.getString("status_code");

                Log.e("full_load_responce", "status_code " + status_code);


                if (status_code.equalsIgnoreCase("200")) {

                    //uncomment this

                 /*   editor.clear();
                    editor.commit();
*/
                    Log.e("book_response", ""+reader.getString("response"));

                    editor.putString("book_now_service_response", reader.getString("response"));
                    editor.commit();

                    Intent i = new Intent(FullLoadDetail.this, LoadSummary.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i);


                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText) {
        try {

            dialog = new Dialog(FullLoadDetail.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if (isAlert) {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("NeedOnRequestPermissionsResult")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method


        FullLoadDetailPermissionsDispatcher.onRequestPermissionsResult(FullLoadDetail.this,requestCode,grantResults);
    }



}
