package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class EditAgentDetail extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;
    MaterialEditText buss_name,landline_no;
    RelativeLayout actionBar;
    ImageView backbutton;
    MyTextView save;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    LinearLayout registered_address,office_address,same_address;
    MaterialEditText register_address,register_city,register_state,register_pin,office_add,office_city,office_state
           ,office_pin, pan_card_no,service_tax_no;
    ImageView same_checkbox;
    LinearLayout hide_same;
    SweetAlertDialog pDialog;
    boolean isChecked = true;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_agent_detail);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;




        register_address = (MaterialEditText) findViewById(R.id.register_address);
        register_city = (MaterialEditText) findViewById(R.id.register_city);
        register_state = (MaterialEditText) findViewById(R.id.register_state);
        register_pin = (MaterialEditText) findViewById(R.id.register_pin);
        office_add = (MaterialEditText) findViewById(R.id.office_add);
        office_city = (MaterialEditText) findViewById(R.id.office_city);
        office_state = (MaterialEditText) findViewById(R.id.office_state);
        office_pin = (MaterialEditText) findViewById(R.id.office_pin);
        pan_card_no = (MaterialEditText) findViewById(R.id.pan_card_no);
        service_tax_no = (MaterialEditText) findViewById(R.id.service_tax_no);

        buss_name = (MaterialEditText) findViewById(R.id.buss_name);
        landline_no = (MaterialEditText) findViewById(R.id.landline_no);

        same_checkbox = (ImageView) findViewById(R.id.same_checkbox);
        same_checkbox.setOnClickListener(this);
        hide_same = (LinearLayout) findViewById(R.id.hide_same);


        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        registered_address = (LinearLayout) findViewById(R.id.registered_address);

        registered_address.getLayoutParams().height =  (deviceHeight / 20);

        registered_address.requestLayout();

        office_address = (LinearLayout) findViewById(R.id.office_address);

        office_address.getLayoutParams().height =  (deviceHeight / 20);

        office_address.requestLayout();

        same_address = (LinearLayout) findViewById(R.id.same_address);

        same_address.getLayoutParams().height =  (deviceHeight / 20);

        same_address.requestLayout();




        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);
        backbutton.requestLayout();
        backbutton.setOnClickListener(this);

        save = (MyTextView) findViewById(R.id.save);
        save.setOnClickListener(this);



        buss_name.setText(pref.getString("user_business_name", ""));
        landline_no.setText(pref.getString("user_landline_number", ""));



        register_address.setText(pref.getString("user_registered_address", ""));
        register_city.setText(pref.getString("user_register_city", ""));
        register_state.setText(pref.getString("user_register_state", ""));
        register_pin.setText(pref.getString("user_register_pin", ""));
        office_add.setText(pref.getString("user_office_address", ""));
        office_city.setText(pref.getString("user_office_city", ""));
        office_state.setText(pref.getString("user_office_state", ""));
        office_pin.setText(pref.getString("user_office_pin", ""));

        pan_card_no.setText(pref.getString("user_pan_card", ""));
        service_tax_no.setText(pref.getString("user_service_tax_no", ""));


        if( register_address.getText().toString().equalsIgnoreCase(office_add.getText().toString()) &&
                register_city.getText().toString().equalsIgnoreCase(office_city.getText().toString()) &&
                register_state.getText().toString().equalsIgnoreCase(register_state.getText().toString()) &&
                register_pin.getText().toString().equalsIgnoreCase(register_pin.getText().toString()))
        {
            hide_same.setVisibility(View.GONE);
            same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));
            isChecked = true;

        }
        else
        {
            hide_same.setVisibility(View.VISIBLE);
            same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));
            isChecked = false;

        }



    }


    String register_address_text,register_city_text,register_state_text,register_pin_text,
            office_add_text,office_city_text,office_state_text,office_pin_text,comp_buss_name,comp_landlin_no,
            comp_service_tax_no="",comp_pan_card_no;


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:

                finish();

                break;


            case R.id.save:



/*
                if(isChecked) {

                    comp_buss_name = buss_name.getText().toString().trim();
                    comp_landlin_no= landline_no.getText().toString().trim();

                    register_address_text = register_address.getText().toString().trim();
                    register_city_text = register_city.getText().toString().trim();
                    register_state_text = register_state.getText().toString().trim();
                    register_pin_text = register_pin.getText().toString().trim();

                    office_add_text = register_address_text;
                    office_city_text = register_city_text;
                    office_state_text = register_state_text;
                    office_pin_text = register_pin_text;

                     comp_service_tax_no= service_tax_no.getText().toString().trim();

                     comp_pan_card_no = pan_card_no.getText().toString().trim();



                    if (isOnline()) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText("Loading");
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makeRegAsAgentReq();
                    } else {
                        Toast.makeText(EditAgentDetail.this,
                                "Please check your network connection",
                                Toast.LENGTH_SHORT).show();
                    }



                }

                else
                {

                    comp_buss_name = buss_name.getText().toString().trim();
                    comp_landlin_no= landline_no.getText().toString().trim();

                    register_address_text = register_address.getText().toString().trim();
                    register_city_text= register_city.getText().toString().trim();
                    register_state_text= register_state.getText().toString().trim();
                    register_pin_text= register_pin.getText().toString().trim();

                    office_add_text = office_add.getText().toString().trim();
                    office_city_text= office_city.getText().toString().trim();
                    office_state_text= office_state.getText().toString().trim();
                    office_pin_text= office_pin.getText().toString().trim();



                    if (isOnline()) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText("Loading");
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makeRegAsAgentReq();
                    } else {
                        Toast.makeText(EditAgentDetail.this,
                                "Please check your network connection",
                                Toast.LENGTH_SHORT).show();
                    }


                }



                */



                ///////////////////////////





                 comp_buss_name = buss_name.getText().toString().trim();

                 comp_landlin_no= landline_no.getText().toString().trim();

                 comp_service_tax_no= service_tax_no.getText().toString().trim();

                 comp_pan_card_no = pan_card_no.getText().toString().trim();





                 register_address_text = register_address.getText().toString().trim();

                 register_city_text= register_city.getText().toString().trim();

                 register_state_text= register_state.getText().toString().trim();

                 register_pin_text= register_pin.getText().toString().trim();




                if(comp_buss_name.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_name_of_business), Toast.LENGTH_SHORT).show();
                }

                else {


                    if((comp_landlin_no.length() > 0 && comp_landlin_no.length() < 6) || comp_landlin_no.length() >12)
                    {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_landline), Toast.LENGTH_SHORT).show();
                    }

                    else {




                        if(comp_pan_card_no.equalsIgnoreCase(""))
                        {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_pan), Toast.LENGTH_SHORT).show();
                        }

                        else {

                            if(comp_pan_card_no.length()!=10)
                            {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_pan), Toast.LENGTH_SHORT).show();
                            }

                            else {

                                if((comp_service_tax_no.length() > 0 && comp_service_tax_no.length() < 15) || comp_service_tax_no.length() >15)
                                {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_service_tax), Toast.LENGTH_SHORT).show();
                                }


                                else {



                                    if (register_address_text.equalsIgnoreCase("")) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_address), Toast.LENGTH_SHORT).show();
                                    } else {

                                        if (register_city_text.equalsIgnoreCase("")) {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_city), Toast.LENGTH_SHORT).show();
                                        } else {

                                            if (register_state_text.equalsIgnoreCase("")) {
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_state), Toast.LENGTH_SHORT).show();
                                            } else {

                                                if (register_pin_text.equalsIgnoreCase("")) {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_pin), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    if (register_pin_text.length()!=6) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_valid_pin), Toast.LENGTH_SHORT).show();
                                                    } else {

                                                        if (isChecked) {



                                                            office_add_text = register_address_text;
                                                            office_city_text = register_city_text;
                                                            office_state_text = register_state_text;
                                                            office_pin_text = register_pin_text;


                                                            if (isOnline()) {
                                                                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                                                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                                                pDialog.setTitleText(getResources().getString(R.string.loading));
                                                                pDialog.setCancelable(false);
                                                                pDialog.show();
                                                                makeRegAsAgentReq();
                                                            } else {
                                                                Toast.makeText(EditAgentDetail.this,
                                                                        getResources().getString(R.string.check_your_network),
                                                                        Toast.LENGTH_SHORT).show();
                                                            }


                                                        } else {


                                                             office_add_text = office_add.getText().toString().trim();

                                                             office_city_text = office_city.getText().toString().trim();

                                                             office_state_text = office_state.getText().toString().trim();

                                                             office_pin_text = office_pin.getText().toString().trim();


                                                            if (office_add_text.equalsIgnoreCase("")) {
                                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_address), Toast.LENGTH_SHORT).show();
                                                            } else {

                                                                if (office_city_text.equalsIgnoreCase("")) {
                                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_city), Toast.LENGTH_SHORT).show();
                                                                } else {

                                                                    if (office_state_text.equalsIgnoreCase("")) {
                                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_state), Toast.LENGTH_SHORT).show();
                                                                    } else {

                                                                        if (office_pin_text.equalsIgnoreCase("")) {
                                                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_pin_code), Toast.LENGTH_SHORT).show();
                                                                        } else {
                                                                            if (office_pin_text.length() != 6) {
                                                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_valid_office_pin_code), Toast.LENGTH_SHORT).show();
                                                                            } else {

                                                                                if (isOnline()) {
                                                                                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                                                                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                                                                    pDialog.setTitleText(getResources().getString(R.string.loading));
                                                                                    pDialog.setCancelable(false);
                                                                                    pDialog.show();
                                                                                    makeRegAsAgentReq();
                                                                                } else {
                                                                                    Toast.makeText(EditAgentDetail.this,
                                                                                            getResources().getString(R.string.check_your_network),
                                                                                            Toast.LENGTH_SHORT).show();
                                                                                }


                                                                            }


                                                                        }

                                                                    }

                                                                }


                                                            }}

                                                    }
                                                }
                                            }
                                        }
                                    }}
                            }


                        }

                    }

                }










                break;

            case R.id.same_checkbox:




                if(isChecked)
                {
                    hide_same.setVisibility(View.VISIBLE);
                    same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));
                    isChecked = false;

                }
                else
                {
                    hide_same.setVisibility(View.GONE);
                    same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));
                    isChecked = true;

                }




/*
                if(pref.getBoolean("same_address1",true))
                {
                    hide_same.setVisibility(View.VISIBLE);
                    same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));
                    editor.putBoolean("same_address1",false);
                    editor.commit();
                }
                else
                {
                    hide_same.setVisibility(View.GONE);
                    same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));
                    editor.putBoolean("same_address1",true);
                    editor.commit();
                }*/

                break;




            case R.id.popup_button:

                if(status_code.equalsIgnoreCase("200"))
                {

                    dialog.dismiss();
                    finish();

                }

                else if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }



                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(EditAgentDetail.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i2);


                    finish();

                }
                else
                {
                    dialog.dismiss();
                }
                break;



        }


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }








    String res;
    StringRequest strReq;

    public String makeRegAsAgentReq() {

        String url = getResources().getString(R.string.base_url)+"user/as-agent";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkRegAsAgentResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                    params.put("_id", pref.getString("userId", "Something Wrong"));


                    params.put("business[business_service_taxno]", comp_service_tax_no);
                    params.put("pancard", comp_pan_card_no);

                    params.put("business[business_name]", comp_buss_name);
                    params.put("business[landline_number]", comp_landlin_no);
                    params.put("business[registered_address]", register_address_text);
                    params.put("business[register_city]", register_city_text);
                    params.put("business[register_state]", register_state_text);
                    params.put("business[register_pin]", register_pin_text);
                    params.put("business[office_address]", office_add_text);
                    params.put("business[office_city]", office_city_text);
                    params.put("business[office_state]", office_state_text);
                    params.put("business[office_pin]", office_pin_text);
                    params.put("security_token", pref.getString("security_token","Something Wrong"));



                    Log.e("register", params.toString());
                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "19");



        return null;
    }

    String status_code = "0";

    public void checkRegAsAgentResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register", response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {




                    String response1 = reader.getString("response");
                    JSONObject jsonobj = new JSONObject(response1);

                    String firstname = jsonobj.getString("firstname");
                    String lastname = jsonobj.getString("lastname");
                    String email = jsonobj.getString("email");
                    String mobile_no1 = jsonobj.getString("mobile_no");
                    String image = jsonobj.getString("image");
                    String business_name = jsonobj.getString("business_name");
                    String pancard = jsonobj.getString("pancard");
                    String account_type = jsonobj.getString("account_type");
                    String company_type = jsonobj.getString("company_type");
                    String business_type = jsonobj.getString("business_type");
                    String registered_address = jsonobj.getString("registered_address");
                    String office_address = jsonobj.getString("office_address");
                    String landline_number = jsonobj.getString("landline_number");
                    String register_city = jsonobj.getString("register_city");
                    String register_state = jsonobj.getString("register_state");
                    String register_pin = jsonobj.getString("register_pin");
                    String office_city = jsonobj.getString("office_city");
                    String office_state = jsonobj.getString("office_state");
                    String office_pin = jsonobj.getString("office_pin");
                    String business_service_taxno = jsonobj.getString("business_service_taxno");



                    editor.putString("user_firstname", firstname);
                    editor.putString("user_lastname", lastname);
                    editor.putString("user_email", email);
                    editor.putString("user_mobile_no", mobile_no1);

                    editor.putString("user_company_type", company_type);
                    editor.putString("user_business_type", business_type);


                    editor.putString("user_image_url", image);
                    editor.putString("user_image_base64", "");

                    editor.putString("user_account_type", account_type);


                    editor.putString("user_business_name", business_name);
                    editor.putString("user_landline_number", landline_number);
                    editor.putString("user_registered_address", registered_address);
                    editor.putString("user_register_city", register_city);
                    editor.putString("user_register_state", register_state);
                    editor.putString("user_register_pin", register_pin);
                    editor.putString("user_office_address", office_address);
                    editor.putString("user_office_city", office_city);
                    editor.putString("user_office_state", office_state);
                    editor.putString("user_office_pin", office_pin);


                    editor.putString("user_pan_card", pancard);
                    editor.putString("user_service_tax_no", business_service_taxno);

                    editor.commit();



                    initiatePopupWindow(getResources().getString(R.string.you_agent_details_has_been_updated),
                            false, getResources().getString(R.string.success), getResources().getString(R.string.ok));






                }


                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(EditAgentDetail.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }












}
