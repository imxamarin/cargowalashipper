package com.cargowala.Models;

/**
 * Created by Bhvesh on 13-Oct-16.
 */
public class GeoLocationResponce {
    public String city="";
    public String address="";
    public Double latitude=0.0;
    public Double longitude=0.0;
    public boolean curretLocation=false;

    public String country="";
    public String postal_code="";

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public boolean isCurretLocation() {
        return curretLocation;
    }

    public void setCurretLocation(boolean curretLocation) {
        this.curretLocation = curretLocation;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
