package com.cargowala.Models;

/**
 * Created by Bhvesh on 03-Nov-16.
 */
public class FcmToken {
    private String fcmToken="";
    public String getFcmToken() {
        return fcmToken;
    }
    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
