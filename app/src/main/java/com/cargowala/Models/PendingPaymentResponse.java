package com.cargowala.Models;

import java.util.ArrayList;

/**
 * Created by Bhvesh on 29-Nov-16.
 */
public class PendingPaymentResponse {

    public String id;
    public String shipment_id;
    public String order_number;
    public String load_type;
    public String from_city;
    public String from_state;
    public String from_address;
    public String to_city;
    public String to_state;
    public String to_address;
    public String priority_delivery;
    public String transit_date;
    public String transit_time;
    public String truck_quantity;
    public String total_payment;
    public String payment_status;
    public String pending_payment;
    public String truck_category;
    public String truck_name;

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getShipper_id() {
        return shipper_id;
    }

    public void setShipper_id(String shipper_id) {
        this.shipper_id = shipper_id;
    }

    public String payment_id;
    public String shipper_id;

    public String getTruck_name() {
        return truck_name;
    }

    public void setTruck_name(String truck_name) {
        this.truck_name = truck_name;
    }

    public String getTruck_category() {
        return truck_category;
    }

    public void setTruck_category(String truck_category) {
        this.truck_category = truck_category;
    }

    public ArrayList<TransectionsArray> transactions=new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShipment_id() {
        return shipment_id;
    }

    public void setShipment_id(String shipment_id) {
        this.shipment_id = shipment_id;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getLoad_type() {
        return load_type;
    }

    public void setLoad_type(String load_type) {
        this.load_type = load_type;
    }

    public String getFrom_city() {
        return from_city;
    }

    public void setFrom_city(String from_city) {
        this.from_city = from_city;
    }

    public String getFrom_state() {
        return from_state;
    }

    public void setFrom_state(String from_state) {
        this.from_state = from_state;
    }

    public String getFrom_address() {
        return from_address;
    }

    public void setFrom_address(String from_address) {
        this.from_address = from_address;
    }

    public String getTo_city() {
        return to_city;
    }

    public void setTo_city(String to_city) {
        this.to_city = to_city;
    }

    public String getTo_state() {
        return to_state;
    }

    public void setTo_state(String to_state) {
        this.to_state = to_state;
    }

    public String getTo_address() {
        return to_address;
    }

    public void setTo_address(String to_address) {
        this.to_address = to_address;
    }

    public String getPriority_delivery() {
        return priority_delivery;
    }

    public void setPriority_delivery(String priority_delivery) {
        this.priority_delivery = priority_delivery;
    }

    public String getTransit_date() {
        return transit_date;
    }

    public void setTransit_date(String transit_date) {
        this.transit_date = transit_date;
    }

    public String getTransit_time() {
        return transit_time;
    }

    public void setTransit_time(String transit_time) {
        this.transit_time = transit_time;
    }

    public String getTruck_quantity() {
        return truck_quantity;
    }

    public void setTruck_quantity(String truck_quantity) {
        this.truck_quantity = truck_quantity;
    }

    public String getTotal_payment() {
        return total_payment;
    }

    public void setTotal_payment(String total_payment) {
        this.total_payment = total_payment;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getPending_payment() {
        return pending_payment;
    }

    public void setPending_payment(String pending_payment) {
        this.pending_payment = pending_payment;
    }

    public ArrayList<TransectionsArray> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<TransectionsArray> transactions) {
        this.transactions = transactions;
    }
}
