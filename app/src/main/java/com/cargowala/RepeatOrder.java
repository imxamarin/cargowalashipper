package com.cargowala;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.soundcloud.android.crop.Crop;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/17/2016.
 */

@RuntimePermissions
public class RepeatOrder extends Activity implements View.OnClickListener ,DatePickerDialog.OnDateSetListener , TimePickerDialog.OnTimeSetListener{

    int deviceWidth, deviceHeight;
    RelativeLayout actionBar;
    ImageView backbutton;

    MaterialEditText loading_date_text,loading_time_text,tot_invoice_text;
    MyTextView proceed;

    String jsonObjectPackageData,orderId;

    SweetAlertDialog pDialog;

    ImageView upload_image,edit_upload_image,edit_upload_delete;
    String invoice_base64="";
    boolean  isUpload = true;
    File mediaStorageDir;
    public static Uri uri;
    public static int CAMERA_REQUEST = 2, GALLERY_REQUEST = 4, CROP_FROM_CAMERA = 3;
    Dialog dialogBox;
    private String path;
    private String filepath;

    boolean isCamera = false;
    private String croppath;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repeat_order);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();


        pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor1 = pref1.edit();


        Intent intent = getIntent();


        jsonObjectPackageData = intent.getExtras().getString("orderId");

        Log.d("aaaa ", "" + jsonObjectPackageData);

        try {

            JSONObject  jsonObjectPackageData1 = new JSONObject(jsonObjectPackageData);

            Log.d("My App", jsonObjectPackageData1.toString());


            orderId = jsonObjectPackageData1.getString("id");
          //  orderId = jsonObjectPackageData1.getString("shipment_id");






        } catch (Throwable t) {
            Log.e("My App", "Could not parse malformed JSON:");
        }





        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();



        loading_date_text = (MaterialEditText) findViewById(R.id.loading_date_text);
        loading_time_text = (MaterialEditText) findViewById(R.id.loading_time_text);
        tot_invoice_text = (MaterialEditText) findViewById(R.id.tot_invoice_text);
        proceed= (MyTextView) findViewById(R.id.proceed);
        proceed.setOnClickListener(this);

        loading_date_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    Log.d("11", "11");

                    Calendar now = Calendar.getInstance();
                    //  now.add(Calendar.YEAR, -18);

                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            RepeatOrder.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );


                    dpd.setMinDate(now);
                    //dpd.setThemeDark(true);
                    //dpd.setTitle("Select Date");
                    dpd.setAccentColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }

                return false;
            }
        });





        loading_time_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction())
                {
                    Log.d("11", "11");

                    Calendar now1 = Calendar.getInstance();
                    TimePickerDialog dpd1 = TimePickerDialog.newInstance(
                            RepeatOrder.this,
                            now1.get(Calendar.HOUR_OF_DAY),
                            now1.get(Calendar.MINUTE),
                            false
                    );

               /* dpd1.setMinTime(now1.get(Calendar.HOUR_OF_DAY)+1,now1.get(Calendar.MINUTE),
                        now1.get(Calendar.SECOND));*/


                    dpd1.show(getFragmentManager(), "TimePickerDialog");

                    dpd1.setAccentColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                }

                return false;
            }
        });








        upload_image = (ImageView) findViewById(R.id.upload_image);
        upload_image.setOnClickListener(this);

        edit_upload_image = (ImageView) findViewById(R.id.edit_upload_image);
        edit_upload_image.setOnClickListener(this);
        edit_upload_delete = (ImageView) findViewById(R.id.edit_upload_delete);
        edit_upload_delete.setOnClickListener(this);







    }
    String tot_invoice_text1;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:
                finish();


                break;


            case R.id.proceed:
                 tot_invoice_text1 = tot_invoice_text.getText().toString().trim();
                if(date1.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_loading_date), Toast.LENGTH_SHORT)
                            .show();
                }

                else if(time.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_loading_time), Toast.LENGTH_SHORT)
                            .show();
                }



                else if(tot_invoice_text1.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_total_invoice_amount), Toast.LENGTH_LONG)
                            .show();
                }


                else   if(invoice_base64.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_upload_image_of_invoice_copy), Toast.LENGTH_SHORT)
                            .show();
                }






                else {



                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
                    String current_time = sdf.format(c.getTime());


                    String book_time = old_date+time;

                    Log.d("current_time ","current_time "+current_time);
                    Log.d("book_time ", "book_time " + book_time);



                    try {
                        Date current_time1 = sdf.parse(current_time);
                        Date book_time1 = sdf.parse(book_time);

                        long diff = book_time1.getTime() - current_time1.getTime();
                        long seconds = diff / 1000;
                        long minutes = seconds / 60;
                        long hours = minutes / 60;
                        long days = hours / 24;



                        Log.d("diff","diff "+diff);
                        Log.d("seconds","seconds "+seconds);
                        Log.d("minutes","minutes "+minutes);
                        Log.d("hours","hours "+hours);
                        Log.d("days","days "+days);




                        if(hours<1)
                        {
                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.time_gap_should_be_of_atleast_1_hours), Toast.LENGTH_SHORT)
                                    .show();
                        }
                        else
                        {

                               if (isOnline()) {
                                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                                    pDialog.setTitleText(getResources().getString(R.string.loading));
                                    pDialog.setCancelable(false);
                                    pDialog.show();
                                    makeRepeatOrderReq();
                                } else {
                                    Toast.makeText(RepeatOrder.this,
                                            getResources().getString(R.string.check_your_network),
                                            Toast.LENGTH_SHORT).show();
                                }


                        }


                    } catch (ParseException ex) {

                    }



                }




                break;





            case R.id.upload_image:


                if(isUpload) {
                    DialogImage();
                }
                break;


            case R.id.edit_upload_image:

                DialogImage();

                break;

            case R.id.edit_upload_delete:



                isUpload = true;
                invoice_base64 = "";
                edit_upload_image.setVisibility(View.GONE);
                edit_upload_delete.setVisibility(View.GONE);
                upload_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.upload_invoice));
                upload_image.setAlpha(1f);


                break;







            case R.id.popup_button:


                if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }



                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(RepeatOrder.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i2);


                    finish();

                }
                else
                {
                    dialog.dismiss();
                }


                break;


        }


    }


    String old_date;

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        old_date =dayOfMonth+"-"+(monthOfYear+1)+"-"+year+ " ";

        String date="";
        if(monthOfYear+1 <10)
        {
            date = "0"+(monthOfYear+1)+"/";

            if(dayOfMonth <10)
            {
                date = date+"0"+dayOfMonth+"/"+year;

            }
            else
            {
                date = date+dayOfMonth+"/"+year;
            }

        }
        else
        {
            date = ""+(monthOfYear+1)+"/";
            if(dayOfMonth <10)
            {
                date = date+"0"+dayOfMonth+"/"+year;

            }
            else
            {
                date = date+dayOfMonth+"/"+year;
            }
        }



        loading_date_text.setText(date);


        String month_name=null;



        switch (monthOfYear) {

            case 0:
                month_name = "Jan";
                break;
            case 1:
                month_name = "Feb";
                break;
            case 2:
                month_name = "Mar";
                break;
            case 3:
                month_name = "Apr";
                break;
            case 4:
                month_name = "May";
                break;
            case 5:
                month_name = "Jun";
                break;
            case 6:
                month_name = "Jul";
                break;
            case 7:
                month_name = "Aug";
                break;
            case 8:
                month_name = "Sep";
                break;
            case 9:
                month_name = "Oct";
                break;
            case 10:
                month_name = "Nov";
                break;
            case 11:
                month_name = "Dec";
                break;
        }



        date1 = ""+dayOfMonth+" "+month_name+" "+year;


    }

    String time="",date1="";

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {


         time = ""+hourOfDay+":"+minute;




        try {
            String _24HourTime = hourOfDay +":"+ minute;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);



            time =_12HourSDF.format(_24HourDt);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        loading_time_text.setText(time);



    }












    // camera gallery photo


    TextView camera_text,open_gallery_text,cancel_text;

    private void DialogImage() {
        dialogBox = new Dialog(RepeatOrder.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogBox.setContentView(R.layout.camera_gallery_popup);

        // Animation animation = AnimationUtils.loadAnimation(context, R.anim.bottom_up_anim);
        dialogBox.show();

        camera_text = (TextView) dialogBox.findViewById(R.id.camera_text);

        // RelativeLayout dialog_rel = (RelativeLayout) dialogBox.findViewById(R.id.dialog_rel);

        open_gallery_text = (TextView) dialogBox.findViewById(R.id.open_gallery_text);
        //remove_photo_text= (TextView) dialogBox.findViewById(R.id.remove_photo_text);
        cancel_text = (TextView) dialogBox.findViewById(R.id.cancel_text);

        cancel_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                dialogBox.dismiss();
            }
        });

        //remove_photo_text.setVisibility(View.GONE);
        camera_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();

                isCamera = true;

                RepeatOrderPermissionsDispatcher.takephotoWithPermissionCheck(RepeatOrder.this);

            }
        });

        open_gallery_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();

                isCamera = false;
                RepeatOrderPermissionsDispatcher.initGallaryWithPermissionCheck(RepeatOrder.this);



            }
        });

    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE})
    public void initGallary() {
//        //takephotoFromGallery();
//        mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "CargoWala");
//        File file = new File(mediaStorageDir.getAbsolutePath(),
//                "CargoWala.jpg");
//
//        filepath = file.getAbsolutePath().toString().trim();
//        Crop.pickImage(RepeatOrder.this);






    }

    public void takephotoFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);

        /*Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image*//*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);*/

    }

    @NeedsPermission({Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE})
    public void takephoto() {
        mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "CargoWala");
        // Create the storage directory(MyCameraVideo) if it
        // does not
        // exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.failed_to_create_directory_cargowala),
                        Toast.LENGTH_LONG).show();
                Log.d("MyCameraVideo",
                        "Failed to create directory CargoWala.");
            }
        }

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(mediaStorageDir.getAbsolutePath(),
                "CargoWala.jpg");
        path = file.getAbsolutePath();
        filepath = path;
        uri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(intent, CAMERA_REQUEST);
//        uri = null;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_REQUEST
                && resultCode ==RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(
                    selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = decodeSampledBitmapFromFile(filepath, 1600, 1000);
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(filepath);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
            }

            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
            ByteArrayOutputStream ful_stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, ful_stream);
            byte[] ful_bytes = ful_stream.toByteArray();
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "Title", null);
            uri = Uri.parse(path);


            try {
                beginCrop(data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if ((requestCode == CAMERA_REQUEST)
                && (resultCode == RESULT_OK)) {
            if (resultCode == RESULT_OK) {
                beginCrop(uri);
            }
        } else if ((requestCode == CROP_FROM_CAMERA)) {

            Bitmap bitmap = BitmapFactory.decodeFile(croppath);

            if (bitmap != null) {

                upload_image.setImageBitmap(bitmap);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);


                upload_image.setAlpha(.5f);

                invoice_base64 = Base64.encodeToString(stream.toByteArray(),
                        Base64.NO_WRAP);

                Log.d("im", "invoice_base64 " + invoice_base64);

                isUpload = false;

                Log.d("invoice_base64", invoice_base64);

                edit_upload_image.setVisibility(View.VISIBLE);
                edit_upload_delete.setVisibility(View.VISIBLE);



            }
            // byte[] ful_bytes = thumb_stream.toByteArray();
            // base64 = Base64.encodeBytes(ful_bytes);



            //hit service here



            try {
                File f = new File(croppath);
                f.delete();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).withMaxSize(deviceWidth, deviceHeight).start(this);
    }
    private void handleCrop(int resultCode, Intent result ) {
        if (resultCode == RESULT_OK) {

            Uri selectedImage = Crop.getOutput(result);
            Bitmap bmp = null, bmp_new = null;
            try {
                bmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(filepath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Log.d("orientation ", "" + orientation);


                Matrix matrix = new Matrix();
                if (orientation == 6  && isCamera) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }

                bmp_new = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp_new.compress(Bitmap.CompressFormat.JPEG, 70, stream);

            upload_image.setImageBitmap(bmp_new);

         /*   base64 = Base64.encodeToString(stream.toByteArray(),
                    Base64.NO_WRAP);
            Log.i("kjbnjhbase64 ", base64);


            Log.d("im", "base64 " + base64);*/

            upload_image.setAlpha(.5f);
            invoice_base64 = Base64.encodeToString(stream.toByteArray(),
                    Base64.NO_WRAP);

            Log.d("im", "invoice_base64 " + invoice_base64);

            isUpload = false;
            Log.d("invoice_base64", invoice_base64);

            edit_upload_image.setVisibility(View.VISIBLE);
            edit_upload_delete.setVisibility(View.VISIBLE);



        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


    public Bitmap decodeSampledBitmapFromFile(String path, int reqWidth,
                                              int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }

        int expectedWidth = width / inSampleSize;
        if (expectedWidth > reqWidth) {

            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }






    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }












    String res;
    StringRequest strReq;

    public String makeRepeatOrderReq() {

        String url = getResources().getString(R.string.base_url)+"user/repeat-order";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkeRepeatOrderResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                    Log.d("security_token", pref1.getString("security_token", "Something Wrong"));
                    Log.d("_id", pref1.getString("userId", "_id Wrong"));
                    Log.d("action", "'get_data'");
                    Log.d("repeat", "true");

                    Log.d("t_time", time);
                    Log.d("t_date", date1);
                    Log.d("shipment_id", orderId);
                    Log.d("invoice_image", invoice_base64);
                    Log.d("invoice_amount", tot_invoice_text1);


                    params.put("security_token", pref1.getString("security_token", "Something Wrong"));
                    params.put("_id", pref1.getString("userId", "_id Wrong"));
                    params.put("action", "get_data");
                    params.put("repeat", "true");
                    params.put("t_date", date1);
                    params.put("t_time", time);
                    params.put("shipment_id", orderId);
                    params.put("invoice_image", invoice_base64);
                    params.put("invoice_amount", tot_invoice_text1);
                            Log.e("register", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "28");



        return null;
    }

    String status_code = "0";

    public void checkeRepeatOrderResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {
                Log.e("repeat_response","a");
                JSONObject reader = new JSONObject(response);
                Log.e("repeat_response","reader");
                status_code = reader.getString("status_code");
                Log.e("repeat_response","status_code "+status_code);
                if(status_code.equalsIgnoreCase("200"))
                {

                    //uncomment this

                 /*   editor.clear();
                    editor.commit();
*/
                    Log.e("repeat_response",""+reader.getString("response"));
                    editor.putString("repeat_order_service_response", reader.getString("response"));
                    editor.commit();

                    Intent i = new Intent(RepeatOrder.this, RepeatLoadSummary.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i.putExtra("date1", date1);
                    i.putExtra("time", time);
                    i.putExtra("orderId", orderId);
                    i.putExtra("invoice_base64", invoice_base64);
                    i.putExtra("tot_invoice_text1", tot_invoice_text1);

                    startActivity(i);





                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }


                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(RepeatOrder.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("NeedOnRequestPermissionsResult")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        RepeatOrderPermissionsDispatcher.onRequestPermissionsResult(RepeatOrder.this,requestCode,grantResults);
    }
}
