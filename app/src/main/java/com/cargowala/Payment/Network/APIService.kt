package com.cargowala.Payment.Network

import com.cargowala.Payment.Data.ServerResponce
import com.cargowala.Payment.PaymentResponse
import retrofit2.Call
import retrofit2.http.*

interface APIService {


    @FormUrlEncoded
    @POST("web/index.php?r=shipper/user/process-payment")
    fun fetchToken(@Field("data") data : String) : Call<ServerResponce>

    @FormUrlEncoded
    @POST("web/index.php?r=shipper/user/citrus-response")
    fun makeFinalPayment(@Field("data") data : String) : Call<String>

    //http://cargowala.com/cargo/dev/frontend/web/index.php?r=shipper/user/citrus-response&TxId=154020064348448&TxStatus=SUCCESS
    // &amount=50&pgTxnNo=''&paymentMode=Online&currency=INR&service_request=complete-payment
    //&TxId={txID}&TxStatus=SUCCESS&amount={amount}&pgTxnNo=''&paymentMode={paymentMode}&currency=INR&service_request={service_request}&processed_paymentid={processed_paymentid}")
    @GET("web/index.php?r=shipper/user/citrus-response")
    fun sendDataPayment(@QueryMap action :Map<String,String> ) : Call<PaymentResponse>

}