package com.cargowala.Payment.Network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Retrofit Object
 */
object NetworkService {

    fun create() : APIService{
        // Server EndPoint
        val baseURL = "https://www.cargowala.com/cargo/dev/frontend/"

        //Timeout Settings
        val httpClient = OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS)

        //log printing
         val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(loggingInterceptor)

        //Create retrofit object
        val retrofit = Retrofit.Builder()
                .baseUrl(baseURL)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        return retrofit.create(APIService::class.java)
    }

}