//package com.cargowala.Payment
//
//import android.app.Activity
//import android.content.Intent
//import android.os.Bundle
//import android.util.Log
//import android.widget.EditText
//import android.widget.Toast
//import com.cargowala.AppController
//import com.cargowala.Payment.Data.ServerResponce
//import com.cargowala.Payment.Network.NetworkService
//import com.cargowala.R
//import com.payumoney.core.PayUmoneyConfig
//import com.payumoney.core.PayUmoneyConstants
//import com.payumoney.core.PayUmoneySdkInitializer
//import com.payumoney.core.response.TransactionResponse
//import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager
//import com.payumoney.sdkui.ui.utils.ResultModel
//import org.json.JSONObject
//import retrofit2.Call
//import retrofit2.Callback
//import retrofit2.Response
//import volley.toolbox.StringRequest
//
//
//
//
//class PaymentActivity : Activity(){
//
//    private var paymentParam: PayUmoneySdkInitializer.PaymentParam? = null
//    private var price: Double = 100.toDouble()
//    private var priceEditText: EditText? = null
//    internal var res: String? = null
//    internal var strReq: StringRequest? = null
//    val TAG = "PaymentActivity"
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//          launchPayUMoney()
//    }
//
//
//    /**
//     * This function prepares the data for payment and launches payumoney plug n play sdk
//     */
//    private fun launchPayUMoney() {
//
//        val payUmoneyConfig = PayUmoneyConfig.getInstance()
//
//        //Use this to set your custom text on result screen button
//        payUmoneyConfig.doneButtonText = "Done"
//
//        //Use this to set your custom title for the activity
//        payUmoneyConfig.payUmoneyActivityTitle = "Test Account"
//
//        val builder = PayUmoneySdkInitializer.PaymentParam.Builder()
//        val txnId = System.currentTimeMillis().toString() + ""
//        val phone = "9779712016"
//        val productName = "Lemonade"
//        val firstName = "RoHIT"
//        val email = "janedoe@gmail.com"
//        val udf1 = ""
//        val udf2 = ""
//        val udf3 = ""
//        val udf4 = ""
//        val udf5 = ""
//
//
//        val appEnvironment = (application as AppController).appEnvironment
//        builder.setAmount(price)
//                .setTxnId(txnId)
//                .setPhone(phone)
//                .setProductName(productName)
//                .setFirstName(firstName)
//                .setEmail(email)
//                .setsUrl(appEnvironment.surl())
//                .setfUrl(appEnvironment.furl())
//                .setUdf1(udf1)
//                .setUdf2(udf2)
//                .setUdf3(udf3)
//                .setUdf4(udf4)
//                .setUdf5(udf5)
//                .setIsDebug(appEnvironment.debug())
//                .setKey(appEnvironment.merchant_Key())
//                .setMerchantId(appEnvironment.merchant_ID())
//
//        try {
//            paymentParam = builder.build()
//            generateHashFromServer(paymentParam!!)
//
//        } catch (e: Exception) {
//            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
//        }
//    }
//
//    fun generateHashFromServer(paymentParam: PayUmoneySdkInitializer.PaymentParam) {
//
//
//        val params = paymentParam.params
//        var jsonObject = JSONObject()
//        try {
//            jsonObject.put(PayUmoneyConstants.KEY, params[PayUmoneyConstants.KEY])
//            jsonObject.put(PayUmoneyConstants.AMOUNT, params[PayUmoneyConstants.AMOUNT])
//            jsonObject.put(PayUmoneyConstants.TXNID, params[PayUmoneyConstants.TXNID])
//            jsonObject.put(PayUmoneyConstants.EMAIL, params[PayUmoneyConstants.EMAIL])
//            jsonObject.put("productinfo", params[PayUmoneyConstants.KEY])
//            jsonObject.put("firstname", params[PayUmoneyConstants.KEY])
//            jsonObject.put(PayUmoneyConstants.UDF1, params[PayUmoneyConstants.UDF1])
//            jsonObject.put(PayUmoneyConstants.UDF2, params[PayUmoneyConstants.UDF2])
//            jsonObject.put(PayUmoneyConstants.UDF3, params[PayUmoneyConstants.UDF3])
//            jsonObject.put(PayUmoneyConstants.UDF4, params[PayUmoneyConstants.UDF4])
//            jsonObject.put(PayUmoneyConstants.UDF5, params[PayUmoneyConstants.UDF5])
//            jsonObject.put("offerKey", "")
//            jsonObject.put("cardBin","")
//            jsonObject.put("user_credentials", "")
//
//            callAPI(jsonObject)
//
//        }catch (e : Exception){
//            Log.d("Error",e.printStackTrace().toString())
//        }
//
//    }
//
//    fun callAPI(data : JSONObject) {
//
//        var network = NetworkService.create()
//        var serverRes =  network.fetchToken(data.toString())
//        serverRes.enqueue(object : Callback<ServerResponce> {
//          override fun onFailure(call: Call<ServerResponce>, t: Throwable) {
//           Log.e("onError",t.message!!)
//          }
//
//          override fun onResponse(call: Call<ServerResponce>, response: Response<ServerResponce>) {
//
//              val serverResponce : ServerResponce = response.body()!!
//
//              Log.d("onSuccess",serverResponce.paymentHash)
//
//
//
//              paymentParam?.setMerchantHash(serverResponce.paymentHash);
//
//              try {
//                  PayUmoneyFlowManager.startPayUMoneyFlow(paymentParam, this@PaymentActivity, R.style.AppTheme_default, true);
//              }catch (e : Exception){
//                  Log.e("error",e.printStackTrace().toString())
//              }
//
//              }
//
//      })
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        Log.d("Result",resultCode.toString())
//
//        // Result Code is -1 send from Payumoney activity
//        Log.d("MainActivity", "request code $requestCode resultcode $resultCode")
//        if (requestCode === PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode === Activity.RESULT_OK && data != null) {
//
//
//            val transactionResponse : TransactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
//                    .INTENT_EXTRA_TRANSACTION_RESPONSE)
//
//            val resultModel : ResultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT)
//
//            // Check which object is non-null
//            if (transactionResponse != null ) {
//
////                && transactionResponse!!.getPayuResponse() != null
//                if (transactionResponse!!.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
//                    //Success Transaction
//                } else {
//                    //Failure Transaction
//                }
//
//                // Response from Payumoney
//             //   val payuResponse = transactionResponse!!.getPayuResponse()
//
//                // Response from SURl and FURL
//                val merchantResponse = transactionResponse!!.getTransactionDetails()
//
//
//
//            } else if (resultModel != null && resultModel!!.getError() != null) {
//                Log.d(TAG, "Error response : " + resultModel!!.getError().getTransactionResponse())
//            } else {
//                Log.d(TAG, "Both objects are null!")
//            }
//        }
//
//    }
//}