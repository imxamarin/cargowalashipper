package com.cargowala.Payment

enum class AppEnvironment {

    PRODUCTION{
        override fun merchant_Key(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun merchant_ID(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun furl(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun surl(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun salt(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun debug(): Boolean {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    },SANDBOX{
        override fun merchant_Key(): String {
            return "rjQUPktU"
        }

        override fun merchant_ID(): String {
            return "4934580"
        }

        override fun furl(): String {
            return "https://www.payumoney.com/mobileapp/payumoney/failure.php"
        }

        override fun surl(): String {
            return "https://www.payumoney.com/mobileapp/payumoney/success.php"
        }

        override fun salt(): String {
            return "e5iIg1jwi8"
        }

        override fun debug(): Boolean {
            return true
        }

    };
    abstract fun merchant_Key() : String
    abstract fun merchant_ID(): String
    abstract fun furl(): String
    abstract fun surl(): String
    abstract fun salt(): String
    abstract fun debug(): Boolean
}