package com.cargowala.Payment.Data
import com.google.gson.annotations.SerializedName


data class ServerResponce(
    @SerializedName("payment_hash") val paymentHash: String = "",
    @SerializedName("get_merchant_ibibo_codes_hash") val getMerchantIbiboCodesHash: String = "",
    @SerializedName("vas_for_mobile_sdk_hash") val vasForMobileSdkHash: String = "",
    @SerializedName("payment_related_details_for_mobile_sdk_hash") val paymentRelatedDetailsForMobileSdkHash: String = "",
    @SerializedName("verify_payment_hash") val verifyPaymentHash: String = "",
    @SerializedName("send_sms_hash") val sendSmsHash: String = ""
)