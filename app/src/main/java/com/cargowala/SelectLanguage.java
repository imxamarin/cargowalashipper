package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cargowala.helper.CommonUtils;
import com.cargowala.helper.Constants;


/**
 * Created by Akash on 6/3/2016.
 */
public class SelectLanguage extends Activity implements View.OnClickListener {
    private RelativeLayout selectHindi, selectEnglish;
    private ImageView imgHindi, imgEng;
    SharedPreferences pref,Pref2;
    SharedPreferences.Editor editor,edito2;
    private MyTextView select;
    private boolean from_setting=false;
    private String localeValue= Constants.ENG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_language);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        Pref2 = getApplicationContext().getSharedPreferences(Constants.LANGUAGE, MODE_PRIVATE);
        editor = pref.edit();
        edito2 = Pref2.edit();

        edito2.putBoolean(Constants.ISVIEWED,true);
        edito2.commit();

        if(getIntent().hasExtra("from_setting")){
            from_setting=getIntent().getBooleanExtra("from_setting",false);
        }


        select = (MyTextView) findViewById(R.id.select);
        selectHindi = (RelativeLayout) findViewById(R.id.selectHindi);
        selectEnglish = (RelativeLayout) findViewById(R.id.selectEnglish);
        imgHindi = (ImageView) findViewById(R.id.imgHindi);
        imgEng = (ImageView) findViewById(R.id.imgEng);


        select.setOnClickListener(this);
        selectHindi.setOnClickListener(this);
        selectEnglish.setOnClickListener(this);

        localeValue=pref.getString(Constants.LOCALE, Constants.ENG);
        if (pref.getString(Constants.LANGUAGE, Constants.ENGLISH).equalsIgnoreCase(Constants.ENGLISH)) {
            imgEng.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
            imgHindi.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));

        } else {
            imgHindi.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
            imgEng.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));

        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.selectHindi:
                imgHindi.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                imgEng.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));

                editor.putString(Constants.LANGUAGE, Constants.HINDI);
                editor.putString(Constants.LOCALE, Constants.HND);
                localeValue=Constants.HND;

                break;
            case R.id.selectEnglish:
                imgEng.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                imgHindi.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));

                editor.putString(Constants.LANGUAGE, Constants.ENGLISH);
                editor.putString(Constants.LOCALE, Constants.ENG);
                localeValue=Constants.ENG;

                break;

            case R.id.select:
                editor.commit();
                CommonUtils.settingLanguage(localeValue, this);

                if(from_setting){
                    Intent i1 = new Intent(SelectLanguage.this, Home.class);
                    i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i1);
                    finish();
                }else {
                    Intent i = new Intent(SelectLanguage.this, Login.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i.putExtra("wasGoogleLoggedIn", false);
                    startActivity(i);
                    finish();
                }

                break;
        }
    }
}
