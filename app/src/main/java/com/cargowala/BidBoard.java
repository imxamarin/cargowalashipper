package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class BidBoard extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;
    LinearLayout main_layout;
    RelativeLayout actionBar;
    ImageView backbutton;


    private List<String> orderIdArray = new ArrayList<String>();
    private List<String> loadTypeArray = new ArrayList<String>();
    private List<String> fromCityArray = new ArrayList<String>();
    private List<String> toCityArray = new ArrayList<String>();
    private List<String> priorityDeliveryArray = new ArrayList<String>();
    private List<String> transitDateArray = new ArrayList<String>();
    private List<String> truckNameArray = new ArrayList<String>();
    private List<String> transitTimeArray = new ArrayList<String>();
    private List<String> truckQuantityArray = new ArrayList<String>();
    private List<String> bidsQuantityArray = new ArrayList<String>();
    private List<String> truckCategoryArray = new ArrayList<String>();
    private List<String> orderIdBigArray = new ArrayList<String>();

    private List<String> insuranceAmountArray = new ArrayList<String>();


    ExampleScrollView complete_scroll_bar;
    ImageView blank_bid_image, no_connection_image;

    SweetAlertDialog pDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;


    int record_no = 0;
    boolean scroll = true;
    boolean headernotcreated = true;
    MyTextView loadingText;
    boolean doPagination = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bid_board);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);


        backbutton.requestLayout();

        complete_scroll_bar = (ExampleScrollView) findViewById(R.id.complete_scroll_bar);
        blank_bid_image = (ImageView) findViewById(R.id.blank_bid_image);
        no_connection_image = (ImageView) findViewById(R.id.no_connection_image);
        main_layout = (LinearLayout) findViewById(R.id.main_layout);


        complete_scroll_bar.setOnScrollViewListener(new ExampleScrollView.OnScrollViewListener() {
            public void onScrollChanged(ExampleScrollView v, int l, int t,
                                        int oldl, int oldt) {

                View view = (View) v.getChildAt(v.getChildCount() - 1);
                int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));

                if (diff == 0) {

                    if (scroll) {

                        if (doPagination) {

                            scroll = false;


                            if (isOnline()) {
                          /*  pDialog = new SweetAlertDialog(BidBoard.this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText("Loading");
                            pDialog.setCancelable(false);
                            pDialog.show();*/

                                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                                loadingText = new MyTextView(
                                        BidBoard.this);

                                params4.setMargins(0, 10, 0, 20);

                                loadingText.setLayoutParams(params4);
                                loadingText.setText(getResources().getString(R.string.loading_dots)
                                );
                                loadingText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                                loadingText.setTypeface(null, Typeface.BOLD);

                                loadingText.setGravity(Gravity.CENTER);
                                main_layout.addView(loadingText);

                                complete_scroll_bar.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        complete_scroll_bar.fullScroll(ExampleScrollView.FOCUS_DOWN);
                                    }
                                });


                                makeBidBoardReq();
                            } else {
                                Toast.makeText(BidBoard.this,
                                        getResources().getString(R.string.check_your_network),
                                        Toast.LENGTH_SHORT).show();

                                complete_scroll_bar.scrollBy(0, -20);
                                scroll = true;
                            }

                        }
                    }

                }
            }
        });




       /* int a1=7;

        if(a1==0)
        {


            blank_bid_image.setVisibility(View.VISIBLE);
            complete_scroll_bar.setVisibility(View.GONE);

        }
        else {


            blank_bid_image.setVisibility(View.GONE);
            complete_scroll_bar.setVisibility(View.VISIBLE);
            main_layout.removeAllViews();



            for (int i = 0; i < 5; i++) {


                LinearLayout ll = new LinearLayout(
                        BidBoard.this);
                ll.setOrientation(LinearLayout.VERTICAL);

                ll.setId(*//*orderIdArray.size()*//*i + 100);
                ll.setOnClickListener(this);
                orderIdArray.add("abc " + i);

                LinearLayout.LayoutParams params0 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                ll.setLayoutParams(params0);

                main_layout.addView(ll);


                LinearLayout ll1 = new LinearLayout(
                        BidBoard.this);
                ll1.setOrientation(LinearLayout.HORIZONTAL);


                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params1.setMargins(20, 20, 20, 20);

                ll1.setLayoutParams(params1);

                ll.addView(ll1);


                MyTextViewBold origin_text = new MyTextViewBold(
                        getApplicationContext());
                origin_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                origin_text.setGravity(Gravity.CENTER);

                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                //  params2.gravity = Gravity.CENTER;
                params2.weight = 0.37f;
                // trucker_name.setPadding(5, 4, 5, 2);
                origin_text.setLayoutParams(params2);


                origin_text.setText("Chandigarh");

                ll1.addView(origin_text);


                View line = new View(
                        getApplicationContext());


                LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                //  params2.gravity = Gravity.CENTER;
                params5.weight = 0.05f;
                line.setLayoutParams(params5);


                ll1.addView(line);


                ImageView truck_image = new ImageView(
                        getApplicationContext());

                truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
                truck_image.setAdjustViewBounds(true);

                // truck_image.setPadding(10,0,10,0);
                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                //  params2.gravity = Gravity.CENTER;
                params4.weight = 0.16f;
                //params10.gravity= Gravity.CENTER;

                //params4.setMargins(0, 10, 0, 10);
                truck_image.setLayoutParams(params4);

                ll1.addView(truck_image);


                View line1 = new View(
                        getApplicationContext());


                LinearLayout.LayoutParams params55 = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                //  params2.gravity = Gravity.CENTER;
                params55.weight = 0.05f;
                line1.setLayoutParams(params55);


                ll1.addView(line1);


                MyTextViewBold destination_text = new MyTextViewBold(
                        getApplicationContext());
                destination_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                destination_text.setGravity(Gravity.CENTER);

                LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                //  params2.gravity = Gravity.CENTER;
                params3.weight = 0.37f;
                destination_text.setLayoutParams(params3);


                destination_text.setText("Delhi");

                ll1.addView(destination_text);


                LinearLayout ll2 = new LinearLayout(
                        BidBoard.this);
                ll2.setOrientation(LinearLayout.HORIZONTAL);


                LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                params6.setMargins(20, 0, 20, 20);
                ll2.setLayoutParams(params6);

                ll.addView(ll2);


                LinearLayout ll3 = new LinearLayout(
                        BidBoard.this);
                ll3.setOrientation(LinearLayout.VERTICAL);


                LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT);


                params7.weight = 0.498f;

                ll3.setLayoutParams(params7);

                ll2.addView(ll3);


                MyTextViewSemi load_type = new MyTextViewSemi(
                        getApplicationContext());
                load_type.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                LinearLayout.LayoutParams params8 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


                load_type.setLayoutParams(params8);


                load_type.setText("Full Load - Priority");
                // bid_info.setPadding(5, 0, 5, 0);
                ll3.addView(load_type);


                MyTextViewSemi no_of_trucks_text = new MyTextViewSemi(
                        getApplicationContext());
                load_type.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                load_type.setLayoutParams(params8);
                String text2 = "<font color=#585858  >No of trucks : </font><font color=#ff5400  ><b>10</b></font>";
                no_of_trucks_text.setText(Html.fromHtml(text2));

                // no_of_trucks_text.setText("Full Load - Priority");
                // bid_info.setPadding(5, 0, 5, 0);
                ll3.addView(no_of_trucks_text);


                View line2 = new View(
                        getApplicationContext());

                line2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                LinearLayout.LayoutParams params555 = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                //  params2.gravity = Gravity.CENTER;
                params555.weight = 0.004f;
                line2.setLayoutParams(params555);

                ll2.addView(line2);


                LinearLayout ll4 = new LinearLayout(
                        BidBoard.this);
                ll4.setOrientation(LinearLayout.VERTICAL);


                LinearLayout.LayoutParams params9 = new LinearLayout.LayoutParams(
                        0, ViewGroup.LayoutParams.WRAP_CONTENT);


                params9.weight = 0.498f;

                ll4.setLayoutParams(params9);

                ll2.addView(ll4);


                MyTextViewSemi date_text = new MyTextViewSemi(
                        getApplicationContext());
                date_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                LinearLayout.LayoutParams params10 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                params10.gravity = Gravity.RIGHT;
                date_text.setLayoutParams(params10);


                date_text.setText("25 March 2016");
                // bid_info.setPadding(5, 0, 5, 0);
                ll4.addView(date_text);


                MyTextViewSemi time_text = new MyTextViewSemi(
                        getApplicationContext());
                time_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                LinearLayout.LayoutParams params11 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                params11.gravity = Gravity.RIGHT;
                time_text.setLayoutParams(params11);

                String text3 = "<font color=#585858  >at </font><font color=#ff5400  ><b>10:00 AM</b></font>";
                time_text.setText(Html.fromHtml(text3));
                //time_text.setText("25 March 2016");
                // bid_info.setPadding(5, 0, 5, 0);
                ll4.addView(time_text);


                MyTextView ref_no_text = new MyTextView(
                        getApplicationContext());
                ref_no_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                params12.setMargins(20, 0, 20, 10);
                ref_no_text.setLayoutParams(params12);


                ref_no_text.setText("Order Id : F54YH8F75G87");
                // bid_info.setPadding(5, 0, 5, 0);
                ll.addView(ref_no_text);


                MyTextView truck_name = new MyTextView(
                        getApplicationContext());
                truck_name.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                LinearLayout.LayoutParams params13 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                params13.setMargins(20, 0, 20, 20);
                truck_name.setLayoutParams(params13);


                truck_name.setText("TATA PRIMA (5 TON MT)");
                // bid_info.setPadding(5, 0, 5, 0);
                ll.addView(truck_name);


                if (i == 0 || i == 3) {
                    MyTextView no_bid_text = new MyTextView(
                            getApplicationContext());
                    no_bid_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                    no_bid_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                    LinearLayout.LayoutParams params14 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    no_bid_text.setGravity(Gravity.CENTER);
                    //params14.gravity  =Gravity.CENTER;
                    params14.setMargins(0, 0, 0, 20);
                    no_bid_text.setLayoutParams(params14);


                    no_bid_text.setText("No bids available");
                    no_bid_text.setPadding(10, 10, 10, 10);
                    ll.addView(no_bid_text);
                } else {


                    RelativeLayout bid_now_layout = new RelativeLayout(
                            getApplicationContext());
                    bid_now_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                    LinearLayout.LayoutParams params15 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    if (i == 4) {
                        params15.setMargins(0, 0, 0, 20);
                    }
                    bid_now_layout.setLayoutParams(params15);

                    ll.addView(bid_now_layout);


                    MyTextView heading = new MyTextView(
                            getApplicationContext());
                    heading.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));


                    RelativeLayout.LayoutParams params16 = new RelativeLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    //params16.setMargins(10, 10, 10, 10);
                    heading.setPadding(10, 10, 10, 10);
                    heading.setLayoutParams(params16);
                    heading.setText("4 bid available");

                    bid_now_layout.addView(heading);


                    MyTextViewBold bid_now = new MyTextViewBold(
                            getApplicationContext());
                    bid_now.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                    bid_now.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                    RelativeLayout.LayoutParams params17 = new RelativeLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);

                    params17.addRule(RelativeLayout.CENTER_VERTICAL,
                            RelativeLayout.TRUE);


                    params17.addRule(
                            RelativeLayout.ALIGN_PARENT_RIGHT,
                            RelativeLayout.TRUE);


                    bid_now.setPadding(10, 10, 10, 10);
                    bid_now.setLayoutParams(params17);
                    bid_now.setText("View Bids");


                    bid_now_layout.addView(bid_now);


                }


            }
        }

*/


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.backbutton:
                Intent i2 = new Intent(BidBoard.this, Home.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i2);
                finish();

                break;

            case R.id.popup_button:


                if (status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910")) {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook")) {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    } else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google")) {
                        isGoogle = true;
                    }


                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i3 = new Intent(BidBoard.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i3.putExtra("wasGoogleLoggedIn", isGoogle);
                    startActivity(i3);


                    finish();

                } else {
                    dialog.dismiss();
                }


                break;


        }


        for (int a = 0; a < orderIdArray.size(); a++) {
            if (v.getId() == (100 + a)) {


                Intent i = new Intent(BidBoard.this, BidBoardDetail.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("orderId1", orderIdArray.get(a));
                i.putExtra("orderIdBig1", orderIdBigArray.get(a));

                i.putExtra("insuranceAmount", insuranceAmountArray.get(a));


                i.putExtra("loadType1", loadTypeArray.get(a));
                i.putExtra("fromCity1", fromCityArray.get(a));
                i.putExtra("toCity1", toCityArray.get(a));
                i.putExtra("priorityDelivery1", priorityDeliveryArray.get(a));
                i.putExtra("transitDate1", transitDateArray.get(a));
                i.putExtra("truckName1", truckNameArray.get(a));
                i.putExtra("transitTime1", transitTimeArray.get(a));
                i.putExtra("truckQuantity1", truckQuantityArray.get(a));
                i.putExtra("bidsQuantity1", bidsQuantityArray.get(a));
                i.putExtra("truckCategory1", truckCategoryArray.get(a));
                startActivity(i);


             /*   Intent i = new Intent(BidBoard.this, SingleTouchImageViewActivity.class);
                i.putExtra("imageUrl","http://neurogadget.net/wp-content/uploads/2016/01/Google-Maps.jpg");
                startActivity(i);*/

            }

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i2 = new Intent(BidBoard.this, Home.class);
        i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i2);
        finish();

    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    String res;
    StringRequest strReq;

    public String makeBidBoardReq() {

        String url = getResources().getString(R.string.base_url) + "user/get-bids";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkBidBoardResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                /*  InstanceID instanceID = InstanceID.getInstance(ChangePassword.this);
                    Log.d("token", "2");
                    String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.d("regg", "6");

                    Log.d("register ", email_id);
                    Log.d("register ",pass);
                    Log.d("token ",token);

                    params.put("username", email_id);
                    params.put("password", pass);
                    params.put("mobile_tokens", token);

                    Log.e("register", params.toString());*/


                    params.put("security_token", pref.getString("security_token", "Something Wrong"));

                    params.put("_id", pref.getString("userId", "Something Wrong"));

                    params.put("next_records", "" + record_no);

                    Log.e("register", params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "6");


        return null;
    }

    String status_code = "0";

    public void checkBidBoardResponse(String response) {


        if (headernotcreated) {
            pDialog.dismiss();
        }


        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);


                if (status_code.equalsIgnoreCase("200")) {

                    String response1 = reader.getString("response");

                    JSONArray jsonArr1 = new JSONArray(response1);

                    if (headernotcreated) {

                        headernotcreated = false;

                        if (jsonArr1.length() == 0) {

                            blank_bid_image.setVisibility(View.VISIBLE);

                            //     ll_layout.setVisibility(View.GONE);

                        } else {

                            //   blank_no_driver_image.setVisibility(View.GONE);
                            complete_scroll_bar.setVisibility(View.VISIBLE);
                            main_layout.removeAllViews();
                        }

                    } else {
                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll = true;
                    }

                    record_no = record_no + 4;
                    for (int i = 0; i < jsonArr1.length(); i++) {

                        if (jsonArr1.length() % 4 == 0) {
                            doPagination = true;
                        } else {

                            doPagination = false;
                        }

                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);
                        String id = jsonObjectPackageData1.getString("id");
                        String shipment_id = jsonObjectPackageData1.getString("shipment_id");
                        String load_type_text = jsonObjectPackageData1.getString("load_type");
                        String from_city = jsonObjectPackageData1.getString("from_city");
                        String to_city = jsonObjectPackageData1.getString("to_city");
                        String priority_delivery = jsonObjectPackageData1.getString("priority_delivery");
                        String transit_date = jsonObjectPackageData1.getString("transit_date");
                        String transit_time = jsonObjectPackageData1.getString("transit_time");
                        String truck_name_text = jsonObjectPackageData1.getString("truck_name");
                        String truck_quantity = jsonObjectPackageData1.getString("truck_quantity");
                        String bids_quantity = jsonObjectPackageData1.getString("bids_quantity");
                        String truck_category = jsonObjectPackageData1.getString("truck_category");
                        String from_address = jsonObjectPackageData1.getString("from_address");
                        String to_address = jsonObjectPackageData1.getString("to_address");

                        String insurance_amount = jsonObjectPackageData1.getString("insurance_amount");

                        blank_bid_image.setVisibility(View.GONE);
                        complete_scroll_bar.setVisibility(View.VISIBLE);

                        orderIdBigArray.add(id);

                        insuranceAmountArray.add(insurance_amount);

                        orderIdArray.add(shipment_id);
                        loadTypeArray.add(load_type_text);
                        fromCityArray.add(from_address);
                        toCityArray.add(to_address);
                        priorityDeliveryArray.add(priority_delivery);
                        transitDateArray.add(transit_date);
                        truckNameArray.add(truck_name_text);
                        transitTimeArray.add(transit_time);
                        truckQuantityArray.add(truck_quantity);
                        bidsQuantityArray.add(bids_quantity);
                        truckCategoryArray.add(truck_category);

                        blank_bid_image.setVisibility(View.GONE);
                        complete_scroll_bar.setVisibility(View.VISIBLE);


                        LinearLayout ll = new LinearLayout(
                                BidBoard.this);
                        ll.setOrientation(LinearLayout.VERTICAL);

                        ll.setId(orderIdArray.size() + 99);
                        ll.setOnClickListener(this);


                        LinearLayout.LayoutParams params0 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        ll.setLayoutParams(params0);

                        main_layout.addView(ll);


                        LinearLayout ll1 = new LinearLayout(
                                BidBoard.this);
                        ll1.setOrientation(LinearLayout.HORIZONTAL);


                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params1.setMargins(20, 20, 20, 20);

                        ll1.setLayoutParams(params1);

                        ll.addView(ll1);


                        MyTextViewBold origin_text = new MyTextViewBold(
                                getApplicationContext());
                        origin_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        origin_text.setGravity(Gravity.CENTER);

                        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        //  params2.gravity = Gravity.CENTER;
                        params2.weight = 0.37f;
                        // trucker_name.setPadding(5, 4, 5, 2);
                        origin_text.setLayoutParams(params2);


                        origin_text.setText(from_city);

                        ll1.addView(origin_text);


                        View line = new View(
                                getApplicationContext());


                        LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        //  params2.gravity = Gravity.CENTER;
                        params5.weight = 0.05f;
                        line.setLayoutParams(params5);


                        ll1.addView(line);


                        ImageView truck_image = new ImageView(
                                getApplicationContext());


                        if (truck_category.equalsIgnoreCase("1")) {
                            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.small_grey_truck));
                        } else if (truck_category.equalsIgnoreCase("2")) {
                            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
                        } else if (truck_category.equalsIgnoreCase("3")) {
                            truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.large_grey_truck));
                        } else {
                            Log.d("error", "unreachable code");
                        }

                        //   truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
                        truck_image.setAdjustViewBounds(true);

                        // truck_image.setPadding(10,0,10,0);
                        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        //  params2.gravity = Gravity.CENTER;
                        params4.weight = 0.16f;
                        //params10.gravity= Gravity.CENTER;

                        //params4.setMargins(0, 10, 0, 10);
                        truck_image.setLayoutParams(params4);

                        ll1.addView(truck_image);


                        View line1 = new View(
                                getApplicationContext());


                        LinearLayout.LayoutParams params55 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        //  params2.gravity = Gravity.CENTER;
                        params55.weight = 0.05f;
                        line1.setLayoutParams(params55);


                        ll1.addView(line1);


                        MyTextViewBold destination_text = new MyTextViewBold(
                                getApplicationContext());
                        destination_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        destination_text.setGravity(Gravity.CENTER);

                        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        //  params2.gravity = Gravity.CENTER;
                        params3.weight = 0.37f;
                        destination_text.setLayoutParams(params3);


                        destination_text.setText(to_city);

                        ll1.addView(destination_text);


                        LinearLayout ll2 = new LinearLayout(
                                BidBoard.this);
                        ll2.setOrientation(LinearLayout.HORIZONTAL);


                        LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        params6.setMargins(20, 0, 20, 20);
                        ll2.setLayoutParams(params6);

                        ll.addView(ll2);


                        LinearLayout ll3 = new LinearLayout(
                                BidBoard.this);
                        ll3.setOrientation(LinearLayout.VERTICAL);


                        LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.WRAP_CONTENT);


                        params7.weight = 0.498f;

                        ll3.setLayoutParams(params7);

                        ll2.addView(ll3);


                        MyTextViewSemi load_type = new MyTextViewSemi(
                                getApplicationContext());
                        load_type.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        LinearLayout.LayoutParams params8 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


                        load_type.setLayoutParams(params8);


                        if (priority_delivery.equalsIgnoreCase("Yes")) {
                            load_type.setText(load_type_text + " - Priority");
                        } else {
                            load_type.setText(load_type_text);
                        }

                        // bid_info.setPadding(5, 0, 5, 0);
                        ll3.addView(load_type);


                        MyTextViewSemi no_of_trucks_text = new MyTextViewSemi(
                                getApplicationContext());
                        load_type.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                        load_type.setLayoutParams(params8);
                        String text2 = "<font color=#585858  >"+getResources().getString(R.string.no_of_trucks)+"</font><font color=#ff5400  ><b>" + truck_quantity + "</b></font>";
                        no_of_trucks_text.setText(Html.fromHtml(text2));

                        //  no_of_trucks_text.setText(Html.fromHtml("<![CDATA[<font color='#585858'>No of trucks : </font></font><font color=#ff5400  ><b>truck_quantity</b></font>]]>"));


                        // no_of_trucks_text.setText("Full Load - Priority");
                        // bid_info.setPadding(5, 0, 5, 0);
                        ll3.addView(no_of_trucks_text);


                        View line2 = new View(
                                getApplicationContext());

                        line2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        LinearLayout.LayoutParams params555 = new LinearLayout.LayoutParams(
                                0,
                                ViewGroup.LayoutParams.MATCH_PARENT);

                        //  params2.gravity = Gravity.CENTER;
                        params555.weight = 0.004f;
                        line2.setLayoutParams(params555);

                        ll2.addView(line2);


                        LinearLayout ll4 = new LinearLayout(
                                BidBoard.this);
                        ll4.setOrientation(LinearLayout.VERTICAL);


                        LinearLayout.LayoutParams params9 = new LinearLayout.LayoutParams(
                                0, ViewGroup.LayoutParams.WRAP_CONTENT);


                        params9.weight = 0.498f;

                        ll4.setLayoutParams(params9);

                        ll2.addView(ll4);


                        MyTextViewSemi date_text = new MyTextViewSemi(
                                getApplicationContext());
                        date_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        LinearLayout.LayoutParams params10 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        params10.gravity = Gravity.RIGHT;
                        date_text.setLayoutParams(params10);


                        date_text.setText(transit_date);
                        // bid_info.setPadding(5, 0, 5, 0);
                        ll4.addView(date_text);


                        MyTextViewSemi time_text = new MyTextViewSemi(
                                getApplicationContext());
                        time_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        LinearLayout.LayoutParams params11 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        params11.gravity = Gravity.RIGHT;
                        time_text.setLayoutParams(params11);


                        String text3 = "<font color=#585858  >"+getResources().getString(R.string.at)+" </font><font color=#ff5400  ><b>" + transit_time + "</b></font>";
                        time_text.setText(Html.fromHtml(text3));
                        //time_text.setText("25 March 2016");
                        // bid_info.setPadding(5, 0, 5, 0);
                        ll4.addView(time_text);


                        MyTextView ref_no_text = new MyTextView(
                                getApplicationContext());
                        ref_no_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                        params12.setMargins(20, 0, 20, 10);
                        ref_no_text.setLayoutParams(params12);


                        ref_no_text.setText(getResources().getString(R.string.order_id)+ shipment_id);
                        // bid_info.setPadding(5, 0, 5, 0);
                        ll.addView(ref_no_text);


                        MyTextView truck_name = new MyTextView(
                                getApplicationContext());
                        truck_name.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        LinearLayout.LayoutParams params13 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                        params13.setMargins(20, 0, 20, 20);
                        truck_name.setLayoutParams(params13);


                        truck_name.setText(truck_name_text);
                        // bid_info.setPadding(5, 0, 5, 0);
                        ll.addView(truck_name);


                        if (Integer.parseInt(bids_quantity) == 0) {
                            MyTextView no_bid_text = new MyTextView(
                                    getApplicationContext());
                            no_bid_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                            no_bid_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                            LinearLayout.LayoutParams params14 = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            no_bid_text.setGravity(Gravity.CENTER);
                            //params14.gravity  =Gravity.CENTER;
                            params14.setMargins(0, 0, 0, 20);
                            no_bid_text.setLayoutParams(params14);


                            no_bid_text.setText(getResources().getString(R.string.no_bids_available));
                            no_bid_text.setPadding(10, 10, 10, 10);
                            ll.addView(no_bid_text);
                        } else {

                            RelativeLayout bid_now_layout = new RelativeLayout(
                                    getApplicationContext());
                            bid_now_layout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                            LinearLayout.LayoutParams params15 = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT);
                            if (i == 4) {
                                params15.setMargins(0, 0, 0, 20);
                            }
                            bid_now_layout.setLayoutParams(params15);

                            ll.addView(bid_now_layout);


                            MyTextView heading = new MyTextView(
                                    getApplicationContext());
                            heading.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));


                            RelativeLayout.LayoutParams params16 = new RelativeLayout.LayoutParams(
                                    ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT);
                            //params16.setMargins(10, 10, 10, 10);
                            heading.setPadding(10, 10, 10, 10);
                            heading.setLayoutParams(params16);

                            if (bids_quantity.equalsIgnoreCase("1")) {
                                heading.setText(bids_quantity + " "+getResources().getString(R.string.bid_available));
                            } else {
                                heading.setText(bids_quantity + " "+getResources().getString(R.string.bids_available));
                            }


                            bid_now_layout.addView(heading);


                            MyTextViewBold bid_now = new MyTextViewBold(
                                    getApplicationContext());
                            bid_now.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                            bid_now.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                            RelativeLayout.LayoutParams params17 = new RelativeLayout.LayoutParams(
                                    ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT);

                            params17.addRule(RelativeLayout.CENTER_VERTICAL,
                                    RelativeLayout.TRUE);


                            params17.addRule(
                                    RelativeLayout.ALIGN_PARENT_RIGHT,
                                    RelativeLayout.TRUE);


                            bid_now.setPadding(10, 10, 10, 10);
                            bid_now.setLayoutParams(params17);
                            bid_now.setText(getResources().getString(R.string.view_bids));


                            bid_now_layout.addView(bid_now);


                        }


                    }


                 /*   initiatePopupWindow("Your password has been changed.",
                            false, "Success", "OK");*/


                } else if (status_code.equalsIgnoreCase("204")) {


                    //no more records

                    doPagination = false;
                    if (record_no > 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_more_bids), Toast.LENGTH_SHORT).show();


                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll = true;

                    } else {

                        no_connection_image.setVisibility(View.GONE);
                        blank_bid_image.setVisibility(View.VISIBLE);
                        complete_scroll_bar.setVisibility(View.GONE);

                    }

                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText) {
        try {

            dialog = new Dialog(BidBoard.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if (isAlert) {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (pref.getBoolean("hitMyBidBoardService", false)) {

            editor.putBoolean("hitMyBidBoardService", false);
            editor.commit();



          /*  no_connection_image.setVisibility(View.GONE);
            blank_no_driver_image.setVisibility(View.GONE);
            ll_layout.setVisibility(View.GONE);*/

            no_connection_image.setVisibility(View.GONE);
            blank_bid_image.setVisibility(View.GONE);
            complete_scroll_bar.setVisibility(View.GONE);

            orderIdArray.clear();
            truckQuantityArray.clear();
            insuranceAmountArray.clear();
            loadTypeArray.clear();
            fromCityArray.clear();
            toCityArray.clear();
            priorityDeliveryArray.clear();
            transitDateArray.clear();
            truckNameArray.clear();
            transitTimeArray.clear();
            bidsQuantityArray.clear();
            truckCategoryArray.clear();
            orderIdBigArray.clear();


            record_no = 0;
            scroll = true;
            headernotcreated = true;
            doPagination = true;

            main_layout.removeAllViews();


            if (isOnline()) {
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();
                makeBidBoardReq();
            } else {
                //    no_connection_image.setVisibility(View.VISIBLE);
                no_connection_image.setVisibility(View.VISIBLE);

                Toast.makeText(BidBoard.this,
                        getResources().getString(R.string.check_your_network),
                        Toast.LENGTH_SHORT).show();
            }
        } else {
           /* editor1.putBoolean("hitMyDriverService",true);
            editor1.commit();*/
        }

    }


}
