package com.cargowala;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.cargowala.ImageLib.PickerBuilder;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.cargowala.CreateBase.encodeImage;

/**
 * Created by Akash on 2/10/2016.
 */

@RuntimePermissions
public class LoadInsurance extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton,order_progress;
    MyTextView next;
    RelativeLayout  upload_image_layout;

    SharedPreferences pref;
    SharedPreferences.Editor editor;


    SharedPreferences pref1;
    SharedPreferences.Editor editor1;

    SweetAlertDialog pDialog;

    MaterialEditText tot_invoice_text;

    ImageView  select_per_ton_image,select_per_km_image;

    String invoicing_type="";
    String insurance_type="1";

    //insurance view
    LinearLayout select_one,select_one_again,select_two,select_three,edit_image_layout;
    LinearLayout select_per_ton,select_per_km;

    ImageView select_one_image,select_two_image,select_three_image;

    ImageView upload_image,edit_upload_image,edit_upload_delete;
    String invoice_base64="";
    boolean  isUpload = true;
    File mediaStorageDir;
    public static Uri uri;
    public static int CAMERA_REQUEST = 2, GALLERY_REQUEST = 4, CROP_FROM_CAMERA = 3;
    Dialog dialogBox;
    private String path;
    private String filepath;

    boolean isCamera = false;
    private String croppath;


    LinearLayout check_info_layout;
    boolean isChecked = false;

    ImageView check_info_checkbox;


    MyTextViewSemi upload_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_insurance);

        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();


        pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        upload_image_layout = (RelativeLayout) findViewById(R.id.upload_image_layout);

        upload_text= (MyTextViewSemi) findViewById(R.id.upload_text);

        edit_image_layout= (LinearLayout) findViewById(R.id.edit_image_layout);

        select_per_ton= (LinearLayout) findViewById(R.id.select_per_ton);
        select_per_ton.setOnClickListener(this);
        select_per_km= (LinearLayout) findViewById(R.id.select_per_km);
        select_per_km.setOnClickListener(this);

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        tot_invoice_text= (MaterialEditText) findViewById(R.id.tot_invoice_text);

        select_per_ton_image = (ImageView) findViewById(R.id.select_per_ton_image);
        select_per_km_image = (ImageView) findViewById(R.id.select_per_km_image);

        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);
        backbutton.requestLayout();
        backbutton.setOnClickListener(this);

        order_progress = (ImageView) findViewById(R.id.order_progress);
        order_progress.getLayoutParams().width =  (deviceWidth /2);
        order_progress.requestLayout();

        next = (MyTextView) findViewById(R.id.next);
        next.setOnClickListener(this);


        check_info_layout= (LinearLayout) findViewById(R.id.check_info_layout);
        check_info_layout.setOnClickListener(this);
        check_info_checkbox= (ImageView) findViewById(R.id.check_info_checkbox);




        select_one = (LinearLayout) findViewById(R.id.select_one);
        select_one.setOnClickListener(this);
        select_one_again = (LinearLayout) findViewById(R.id.select_one_again);
        select_one_again.setOnClickListener(this);
        select_two = (LinearLayout) findViewById(R.id.select_two);
        select_two.setOnClickListener(this);
        select_three = (LinearLayout) findViewById(R.id.select_three);
        select_three.setOnClickListener(this);
        select_one_image = (ImageView) findViewById(R.id.select_one_image);
        select_one_image.setOnClickListener(this);
        select_two_image = (ImageView) findViewById(R.id.select_two_image);
        select_two_image.setOnClickListener(this);
        select_three_image = (ImageView) findViewById(R.id.select_three_image);
        select_three_image.setOnClickListener(this);


        select_one_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
        select_two_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
        select_three_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));


        upload_image_layout.setVisibility(View.GONE);
        edit_image_layout.setVisibility(View.GONE);

        upload_text.setVisibility(View.GONE);

        upload_image = (ImageView) findViewById(R.id.upload_image);
        upload_image.setOnClickListener(this);

        edit_upload_image = (ImageView) findViewById(R.id.edit_upload_image);
        edit_upload_image.setOnClickListener(this);
        edit_upload_delete = (ImageView) findViewById(R.id.edit_upload_delete);
        edit_upload_delete.setOnClickListener(this);



        if(!pref.getString("total_invoice","").equalsIgnoreCase(""))
        {

           tot_invoice_text.setText(pref.getString("total_invoice",""));
        }
        if(pref.getString("invoicing_type","").equalsIgnoreCase("1"))
        {

            select_per_ton_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
            select_per_km_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
            invoicing_type = "1";
        }
        if(pref.getString("invoicing_type","").equalsIgnoreCase("2"))
        {

            select_per_ton_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
            select_per_km_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
            invoicing_type = "2";

        }



    }



    @Override
    public void onClick(View v) {







        switch (v.getId()) {



            case R.id.backbutton:

                finish();

                break;

            case R.id.next:


                callAPI();

              /*  Intent i = new Intent(LoadInsurance.this, LoadSummary.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);*/


                break;




            case R.id.select_one:

                select_one_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                select_two_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                select_three_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                upload_image_layout.setVisibility(View.GONE);
                upload_text.setVisibility(View.GONE);

                insurance_type = "1";

                break;




            case R.id.select_one_again:

                select_one_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                select_two_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                select_three_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));


                upload_image_layout.setVisibility(View.GONE);
                upload_text.setVisibility(View.GONE);


                insurance_type = "1";


                break;

            case R.id.select_two:

                select_one_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                select_two_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                select_three_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));


                upload_image_layout.setVisibility(View.VISIBLE);
                upload_text.setVisibility(View.VISIBLE);

                insurance_type = "2";

                break;

            case R.id.select_three:

                select_one_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                select_two_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                select_three_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));


                upload_image_layout.setVisibility(View.GONE);
                upload_text.setVisibility(View.GONE);

                insurance_type = "3";


                break;


            case R.id.upload_image:


                if(isUpload) {
                    DialogImage();
                }
                break;


            case R.id.edit_upload_image:

                DialogImage();

                break;

            case R.id.edit_upload_delete:



                isUpload = true;
                invoice_base64 = "";
                edit_image_layout.setVisibility(View.GONE);
                upload_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.upload_insurance));
                upload_image.setAlpha(1f);


                break;



            case R.id.select_per_ton:

                select_per_ton_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                select_per_km_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                invoicing_type = "1";
                break;


            case R.id.select_per_km:

                select_per_ton_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                select_per_km_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                invoicing_type = "2";

                break;



            case R.id.check_info_layout:

                if(isChecked)
                {

                    check_info_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));
                    isChecked = false;
                }
                else
                {
                    check_info_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));
                    isChecked = true;
                }

                break;



            case R.id.popup_button:


                if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }



                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(LoadInsurance.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i2);


                    finish();

                }
                else
                {
                    dialog.dismiss();
                }


                break;



        }


    }

    public void callAPI() {
        String tot_invoice_text1 = tot_invoice_text.getText().toString().trim();


        if(tot_invoice_text1.equalsIgnoreCase(""))
        {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.please_enter_total_invoice_amount), Toast.LENGTH_LONG)
                    .show();
        }

        else if(invoicing_type.equalsIgnoreCase(""))
        {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.please_select_invoice_type), Toast.LENGTH_LONG)
                    .show();
        }
        else if(!isChecked)
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_the_checkbox), Toast.LENGTH_SHORT).show();

        }
        else
        {
            editor.putString("total_invoice",tot_invoice_text1);
            editor.putString("insurance_type",insurance_type);
            editor.putString("insurance_image",invoice_base64);
            editor.putString("invoicing_type",invoicing_type);
            editor.commit();


            if(isOnline())
            {
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();
                makeBookNowReq();
            }
            else {
                Toast.makeText(LoadInsurance.this,
                        getResources().getString(R.string.check_your_network),
                        Toast.LENGTH_SHORT).show();
            }

        }
    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }









    String res;
    StringRequest strReq;

    public String makeBookNowReq() {

        String url = getResources().getString(R.string.base_url)+"user/book-truck";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkeBookNowResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                /*    params.put("oldpassword", old_pass);
                    params.put("newpassword", new_pass);*/


                    params.put("security_token", pref1.getString("security_token","Something Wrong"));
                    params.put("_id",pref1.getString("userId","_id Wrong") );

                    params.put("lp_name",pref.getString("book_origin_address","lp_name Wrong") );
                    params.put("lp_address",pref.getString("book_origin_address","lp_address Wrong") );
                    params.put("lp_state",pref.getString("book_origin_state","lp_state Wrong") );
                    params.put("lp_city",pref.getString("book_origin_city","lp_city Wrong") );
                    params.put("lp_latitude",pref.getString("book_origin_lat","lp_latitude Wrong") );
                    params.put("lp_longitude",pref.getString("book_origin_long","lp_longitude Wrong") );
                    params.put("up_name",pref.getString("book_odest_address","up_name Wrong") );
                    params.put("up_address",pref.getString("book_odest_address","up_address Wrong") );
                    params.put("up_state",pref.getString("book_dest_state","up_state Wrong") );
                    params.put("up_city",pref.getString("book_dest_city","up_city Wrong") );
                    params.put("up_latitude",pref.getString("book_dest_lat","up_latitude Wrong") );
                    params.put("up_longitude",pref.getString("book_dest_long","up_longitude Wrong") );
                    params.put("t_date",pref.getString("book_date","t_date Wrong") );
                    params.put("t_time",pref.getString("book_time","t_time Wrong") );
                    String distance = pref.getString("book_est_dist","0");
                    if(distance == null || distance.equals("null")){
                    params.put("est_distance", "0");
                    }else{
                    params.put("est_distance", distance);

                    }
                    params.put("est_time",pref.getString("book_est_time","est_time Wrong") );
                    params.put("shipment_url",pref.getString("book_map_url","shipment_url Wrong") );

                    params.put("items",""+pref.getString("item_data","items Wrong") );

                    params.put("category",pref.getString("truck_type", "category Wrong") );
                    params.put("ttype",pref.getString("truck_id","type Wrong") );
                    params.put("quantity",pref.getString("no_of_trucks","quantity Wrong") );
                    params.put("load_type", pref.getString("load_type", "load_type Wrong"));
                    params.put("priority_delivery", pref.getString("priority_delivery", "priority_delivery Wrong"));
                    params.put("invoice_amount", "" + pref.getString("total_invoice", "0"));
                    params.put("overall_weight", "" + pref.getString("total_item_wt", "0"));
                    params.put("invoicing_type", "" + pref.getString("invoicing_type", "something wrong"));
                    params.put("insurance_type", "" + pref.getString("insurance_type", "something wrong"));
                    params.put("insurance_image", "" + pref.getString("insurance_image", ""));
                    params.put("action", "get_data");

                    Log.d("security_token", pref1.getString("security_token", "Something Wrong"));
                    Log.d("_id", pref1.getString("userId", "_id Wrong"));
                    Log.d("lp_name", pref.getString("book_origin_address", "lp_name Wrong"));
                    Log.d("lp_address", pref.getString("book_origin_address", "lp_address Wrong"));
                    Log.d("lp_state", pref.getString("book_origin_state", "lp_state Wrong"));
                    Log.d("lp_city", pref.getString("book_origin_city", "lp_city Wrong"));
                    Log.d("lp_latitude", pref.getString("book_origin_lat", "lp_latitude Wrong"));
                    Log.d("lp_longitude", pref.getString("book_origin_long", "lp_longitude Wrong"));
                    Log.d("up_name", pref.getString("book_odest_address", "up_name Wrong"));
                    Log.d("up_address", pref.getString("book_odest_address", "up_address Wrong"));
                    Log.d("up_state", pref.getString("book_dest_state", "up_state Wrong"));
                    Log.d("up_city", pref.getString("book_dest_city", "up_city Wrong"));
                    Log.d("up_latitude", pref.getString("book_dest_lat", "up_latitude Wrong"));
                    Log.d("up_longitude", pref.getString("book_dest_long", "up_longitude Wrong"));
                    Log.d("t_date", pref.getString("book_date", "t_date Wrong"));
                    Log.d("t_time", pref.getString("book_time", "t_time Wrong"));
                    Log.d("est_distance", pref.getString("book_est_dist", "est_distance Wrong"));
                    Log.d("est_time", pref.getString("book_est_time", "est_time Wrong"));
                    Log.d("shipment_url", pref.getString("book_map_url", "shipment_url Wrong"));
                    Log.d("items", "" + pref.getString("item_data", "items Wrong"));
                    Log.d("category", pref.getString("truck_type", "category Wrong"));
                    Log.d("ttype", pref.getString("truck_id", "type Wrong"));
                    Log.d("quantity", pref.getString("no_of_trucks", "quantity Wrong"));
                    Log.d("load_type", pref.getString("load_type", "load_type Wrong"));
                    Log.d("priority_delivery", pref.getString("priority_delivery", "priority_delivery Wrong"));
                    Log.d("invoice_amount", "" + pref.getString("total_invoice", "0"));
                    Log.d("overall_weight", "" + pref.getString("total_item_wt", "0"));
                    Log.d("invoicing_type", "" + pref.getString("invoicing_type", "something wrong"));
                    Log.d("insurance_type", "" + pref.getString("insurance_type", "something wrong"));
                    Log.d("insurance_image", "" + pref.getString("insurance_image", ""));
                  //  Log.d("action", "get_data");

                    Log.d("action", "get_data");




                    Log.e("register", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "28");



        return null;
    }

    String status_code = "0";

    public void checkeBookNowResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);



                if(status_code.equalsIgnoreCase("200"))
                {

                    //uncomment this

                 /*   editor.clear();
                    editor.commit();
*/


                    editor.putString("book_now_service_response",reader.getString("response"));
                    editor.commit();

                    Intent i = new Intent(LoadInsurance.this, LoadSummary.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i);




                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }


                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(LoadInsurance.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }






// camera gallery photo


    TextView camera_text,open_gallery_text,cancel_text;

    private void DialogImage() {
        dialogBox = new Dialog(LoadInsurance.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogBox.setContentView(R.layout.camera_gallery_popup);

        // Animation animation = AnimationUtils.loadAnimation(context, R.anim.bottom_up_anim);
        dialogBox.show();

        camera_text = (TextView) dialogBox.findViewById(R.id.camera_text);

        // RelativeLayout dialog_rel = (RelativeLayout) dialogBox.findViewById(R.id.dialog_rel);

        open_gallery_text = (TextView) dialogBox.findViewById(R.id.open_gallery_text);
        //remove_photo_text= (TextView) dialogBox.findViewById(R.id.remove_photo_text);
        cancel_text = (TextView) dialogBox.findViewById(R.id.cancel_text);

        cancel_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                dialogBox.dismiss();
            }
        });

        //remove_photo_text.setVisibility(View.GONE);
        camera_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();

                isCamera = true;

            LoadInsurancePermissionsDispatcher.takephotoWithPermissionCheck(LoadInsurance.this);

            }
        });

        open_gallery_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();

                isCamera = false;

                //takephotoFromGallery();

        LoadInsurancePermissionsDispatcher.initGalleryWithPermissionCheck(LoadInsurance.this);

            }
        });

    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}  )
    public void initGallery() {
//        mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "CargoWala");
//        File file = new File(mediaStorageDir.getAbsolutePath(),
//                "CargoWala.jpg");
//
//        filepath = file.getAbsolutePath().toString().trim();
//        Crop.pickImage(LoadInsurance.this);


        new PickerBuilder(LoadInsurance.this, PickerBuilder.SELECT_FROM_GALLERY)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {

                        Picasso.with(LoadInsurance.this).load(imageUri).into(upload_image);

                        final InputStream imageStream;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            String encodedImage = encodeImage(selectedImage);


                            upload_image.setAlpha(.5f);

                            isUpload = false;

                            Log.d("invoice_base64", invoice_base64);


                            edit_image_layout.setVisibility(View.VISIBLE);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();

    }

    public void takephotoFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);

        /*Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image*//*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);*/

    }

    @NeedsPermission(Manifest.permission.CAMERA)
    public void takephoto() {
//        mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "CargoWala");
//        // Create the storage directory(MyCameraVideo) if it
//        // does not
//        // exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                Toast.makeText(getApplicationContext(),
//                        getResources().getString(R.string.failed_to_create_directory_cargowala),
//                        Toast.LENGTH_LONG).show();
//                Log.d("MyCameraVideo",
//                        "Failed to create directory CargoWala.");
//            }
//        }
//
//        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//        File file = new File(mediaStorageDir.getAbsolutePath(),
//                "CargoWala.jpg");
//        path = file.getAbsolutePath();
//        filepath = path;
//        uri = Uri.fromFile(file);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
//        startActivityForResult(intent, CAMERA_REQUEST);
////        uri = null;


        new PickerBuilder(LoadInsurance.this, PickerBuilder.SELECT_FROM_GALLERY)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {

                        Picasso.with(LoadInsurance.this).load(imageUri).into(upload_image);

                        final InputStream imageStream;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            String encodedImage = encodeImage(selectedImage);


                            upload_image.setAlpha(.5f);

                            isUpload = false;

                            Log.d("invoice_base64", invoice_base64);


                            edit_image_layout.setVisibility(View.VISIBLE);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_REQUEST
                && resultCode ==RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(
                    selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = decodeSampledBitmapFromFile(filepath, 1600, 1000);
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(filepath);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
            }

            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
            ByteArrayOutputStream ful_stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, ful_stream);
            byte[] ful_bytes = ful_stream.toByteArray();
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "Title", null);
            uri = Uri.parse(path);


            try {
                beginCrop(data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if ((requestCode == CAMERA_REQUEST)
                && (resultCode == RESULT_OK)) {
            if (resultCode == RESULT_OK) {
                beginCrop(uri);
            }
        } else if ((requestCode == CROP_FROM_CAMERA)) {

            Bitmap bitmap = BitmapFactory.decodeFile(croppath);

            if (bitmap != null) {

                upload_image.setImageBitmap(bitmap);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);


                upload_image.setAlpha(.5f);

                invoice_base64 = Base64.encodeToString(stream.toByteArray(),
                        Base64.NO_WRAP);

                Log.d("im", "invoice_base64 " + invoice_base64);

                isUpload = false;

                Log.d("invoice_base64", invoice_base64);


                edit_image_layout.setVisibility(View.VISIBLE);

            }
            // byte[] ful_bytes = thumb_stream.toByteArray();
            // base64 = Base64.encodeBytes(ful_bytes);



            //hit service here



            try {
                File f = new File(croppath);
                f.delete();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).withMaxSize(deviceWidth, deviceHeight).start(this);
    }
    private void handleCrop(int resultCode, Intent result ) {
        if (resultCode == RESULT_OK) {

            Uri selectedImage = Crop.getOutput(result);
            Bitmap bmp = null, bmp_new = null;
            try {
                bmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(filepath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Log.d("orientation ", "" + orientation);


                Matrix matrix = new Matrix();
                if (orientation == 6  && isCamera) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }

                bmp_new = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp_new.compress(Bitmap.CompressFormat.JPEG, 70, stream);

            upload_image.setImageBitmap(bmp_new);

         /*   base64 = Base64.encodeToString(stream.toByteArray(),
                    Base64.NO_WRAP);
            Log.i("kjbnjhbase64 ", base64);


            Log.d("im", "base64 " + base64);*/

            upload_image.setAlpha(.5f);
            invoice_base64 = Base64.encodeToString(stream.toByteArray(),
                    Base64.NO_WRAP);

            Log.d("im", "invoice_base64 " + invoice_base64);

            isUpload = false;
            Log.d("invoice_base64", invoice_base64);

            edit_image_layout.setVisibility(View.VISIBLE);



        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


    public Bitmap decodeSampledBitmapFromFile(String path, int reqWidth,
                                              int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }

        int expectedWidth = width / inSampleSize;
        if (expectedWidth > reqWidth) {

            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }





}
