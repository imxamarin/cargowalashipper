package com.cargowala;

import java.util.Timer;
import java.util.TimerTask;


import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class GetCurrentLocation{

    private boolean isGpsEnabled = false;
    private boolean isNetworkEnabled = false;
    private LocationManager locationManager;
    private Timer timer1;
    private Location currentLocation;
    public static double latitude = 0.0;
    public static double longitude = 0.0;
    private com.cargowala.LocationListener _listener;


    public void setLocationListener (com.cargowala.LocationListener _listener) {
        this._listener = _listener;
    }

    public boolean getLocation(Context context,boolean isGpsEnable,boolean isNetworkEnable) {
        isGpsEnabled = isGpsEnable;
        isNetworkEnabled = isNetworkEnable;
        if (locationManager == null)
            locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        if (isGpsEnabled) {
            try {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 0, 3, locationListenerGps);
            }catch (Exception e){
            }

        }
        else if (isNetworkEnabled) {
            try {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 0, 3,
                        locationListenerNetwork);
            }catch (Exception e){

            }

        }
        timer1 = new Timer();
        timer1.schedule(new GetLastLocation(), 5000);
        return true;
    }

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer1.cancel();
            setLastLocation();
            try {
                locationManager.removeUpdates(this);
                locationManager.removeUpdates(locationListenerNetwork);
            }catch (Exception e){

            }

        }

        public void onProviderDisabled(String provider)
        {
        }

        public void onProviderEnabled(String provider)
        {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer1.cancel();
            gotLocation(location);
            setLastLocation();
            try {
                locationManager.removeUpdates(this);
                locationManager.removeUpdates(locationListenerGps);
            }catch (Exception e){

            }

        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    private void setLastLocation() {
        Location net_loc = null, gps_loc = null;
        if (isGpsEnabled) {
            try {
                gps_loc = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }catch (Exception e){

            }

        }
        if (isNetworkEnabled) {
            try {
                net_loc = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }catch (Exception e){

            }

        }

        // if there are both values use the latest one
        if (gps_loc != null && net_loc != null)
        {
            if (gps_loc.getTime() > net_loc.getTime()){
                gotLocation(gps_loc);
            }
            else {
                gotLocation(net_loc);
            }
            return;
        }

        if (gps_loc != null)
        {
            gotLocation(gps_loc);
            return;
        }
        else if (net_loc != null) {
            gotLocation(net_loc);
            return;
        }
    }

    class GetLastLocation extends TimerTask {
        @Override
        public void run() {
            try {
                timer1.cancel();
                setLastLocation();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void gotLocation(final Location location) {
        currentLocation = location;
        try {
            if (currentLocation != null) {
                /**
                 * to access the shared preferences to have a accurate flow of
                 * the app
                 */
                locationManager.removeUpdates(locationListenerGps);
                locationManager.removeUpdates(locationListenerNetwork);
                latitude = (double) Math.round(currentLocation.getLatitude() * 1000000) / 1000000;
                longitude = (double) Math.round(currentLocation.getLongitude() * 1000000) / 1000000;
                _listener.onLocationUpdate();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}