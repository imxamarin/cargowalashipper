package com.cargowala;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * Created by Akash on 2/10/2016.
 */
public class CompanyAddress extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton,company_progress;
    MyTextView next;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    LinearLayout registered_address,office_address,same_address;
    String company_type,company_business,comp_buss_name,comp_landlin_no,first_name,last_name,email_id,image_url,account_type;
    MaterialEditText register_address,register_city,register_state,register_pin,office_add,office_city,office_state,office_pin;
    ImageView same_checkbox;
    LinearLayout hide_same;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_address);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;


        Intent intent = getIntent();
        company_type = intent.getExtras().getString("company_type");
        company_business = intent.getExtras().getString("company_business");
        comp_buss_name = intent.getExtras().getString("comp_buss_name");
        comp_landlin_no = intent.getExtras().getString("comp_landlin_no");

        Log.d("mmm","comp_landlin_no "+comp_landlin_no);

        first_name = intent.getExtras().getString("first_name");
        last_name = intent.getExtras().getString("last_name");
        email_id = intent.getExtras().getString("email_id");
        image_url = intent.getExtras().getString("image_url");

        account_type = intent.getExtras().getString("account_type");


        register_address = (MaterialEditText) findViewById(R.id.register_address);
        register_city = (MaterialEditText) findViewById(R.id.register_city);
        register_state = (MaterialEditText) findViewById(R.id.register_state);
        register_pin = (MaterialEditText) findViewById(R.id.register_pin);
        office_add = (MaterialEditText) findViewById(R.id.office_add);
        office_city = (MaterialEditText) findViewById(R.id.office_city);
        office_state = (MaterialEditText) findViewById(R.id.office_state);
        office_pin = (MaterialEditText) findViewById(R.id.office_pin);

        same_checkbox = (ImageView) findViewById(R.id.same_checkbox);
        same_checkbox.setOnClickListener(this);
        hide_same = (LinearLayout) findViewById(R.id.hide_same);


        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        registered_address = (LinearLayout) findViewById(R.id.registered_address);

        registered_address.getLayoutParams().height =  (deviceHeight / 20);

        registered_address.requestLayout();

        office_address = (LinearLayout) findViewById(R.id.office_address);

        office_address.getLayoutParams().height =  (deviceHeight / 20);

        office_address.requestLayout();

        same_address = (LinearLayout) findViewById(R.id.same_address);

        same_address.getLayoutParams().height =  (deviceHeight / 20);

        same_address.requestLayout();




        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);
        backbutton.requestLayout();
        backbutton.setOnClickListener(this);

        company_progress = (ImageView) findViewById(R.id.company_progress);
        company_progress.getLayoutParams().width =  (deviceWidth /2);
        company_progress.requestLayout();

        next = (MyTextView) findViewById(R.id.next);
        next.setOnClickListener(this);

        if(pref.getBoolean("same_address",true))
        {
            hide_same.setVisibility(View.GONE);
            same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));



            if(!pref.getString("register_address","NA").equalsIgnoreCase("NA"))
            {
                register_address.setText(pref.getString("register_address","NA"));
                register_city.setText(pref.getString("register_city","NA"));
                register_state.setText(pref.getString("register_state","NA"));
                register_pin.setText(pref.getString("register_pin","NA"));
            }


        }
        else
        {
            hide_same.setVisibility(View.VISIBLE);
            same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));



            if(!pref.getString("register_address","NA").equalsIgnoreCase("NA"))
            {
                register_address.setText(pref.getString("register_address","NA"));
                register_city.setText(pref.getString("register_city","NA"));
                register_state.setText(pref.getString("register_state","NA"));
                register_pin.setText(pref.getString("register_pin","NA"));

                office_add.setText(pref.getString("office_address","NA"));
                office_city.setText(pref.getString("office_city","NA"));
                office_state.setText(pref.getString("office_state","NA"));
                office_pin.setText(pref.getString("office_pin","NA"));
            }

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:
                finish();


                break;

            case R.id.next:


                String register_address_text = register_address.getText().toString();
                register_address_text = register_address_text.trim();
                String register_city_text= register_city.getText().toString();
                register_city_text = register_city_text.trim();
                String register_state_text= register_state.getText().toString();
                register_state_text = register_state_text.trim();
                String register_pin_text= register_pin.getText().toString();
                register_pin_text = register_pin_text.trim();

                if(register_address_text.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_address), Toast.LENGTH_SHORT).show();
                }

                else {

                    if(register_city_text.equalsIgnoreCase(""))
                    {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_city), Toast.LENGTH_SHORT).show();
                    }

                    else {

                        if(register_state_text.equalsIgnoreCase(""))
                        {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_state), Toast.LENGTH_SHORT).show();
                        }

                        else {

                            if(register_pin_text.equalsIgnoreCase(""))
                            {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_pin), Toast.LENGTH_SHORT).show();
                            }

                            else {
                                if (register_pin_text.length()!=6) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_registered_valid_pin), Toast.LENGTH_SHORT).show();
                                }



                                else {

                                if(pref.getBoolean("same_address",true))
                                {

                                    editor.putString("register_address",register_address_text);
                                    editor.putString("register_city",register_city_text);
                                    editor.putString("register_state",register_state_text);
                                    editor.putString("register_pin",register_pin_text);
                                    editor.commit();

                                    Intent i = new Intent(CompanyAddress.this, CompanyMobileNumber.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    i.putExtra("first_name", first_name);
                                    i.putExtra("last_name", last_name);
                                    i.putExtra("email_id", email_id);
                                    i.putExtra("image_url", image_url);

                                    i.putExtra("account_type", account_type);

                                    i.putExtra("company_business", company_business);
                                    i.putExtra("comp_buss_name", comp_buss_name);
                                    i.putExtra("comp_landlin_no", comp_landlin_no);
                                    i.putExtra("company_type", company_type);

                                    i.putExtra("register_address", register_address_text);
                                    i.putExtra("register_city", register_city_text);
                                    i.putExtra("register_state", register_state_text);
                                    i.putExtra("register_pin", register_pin_text);

                                    i.putExtra("office_address", register_address_text);
                                    i.putExtra("office_city", register_city_text);
                                    i.putExtra("office_state", register_state_text);
                                    i.putExtra("office_pin", register_pin_text);

                                    startActivity(i);

                                }
                                else
                                {


                                    String office_add_text = office_add.getText().toString();
                                    office_add_text = office_add_text.trim();
                                    String office_city_text= office_city.getText().toString();
                                    office_city_text = office_city_text.trim();
                                    String office_state_text= office_state.getText().toString();
                                    office_state_text = office_state_text.trim();
                                    String office_pin_text= office_pin.getText().toString();
                                    office_pin_text = office_pin_text.trim();

                                    if(office_add_text.equalsIgnoreCase(""))
                                    {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_address), Toast.LENGTH_SHORT).show();
                                    }

                                    else {

                                        if(office_city_text.equalsIgnoreCase(""))
                                        {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_city), Toast.LENGTH_SHORT).show();
                                        }

                                        else {

                                            if(office_state_text.equalsIgnoreCase(""))
                                            {
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_state), Toast.LENGTH_SHORT).show();
                                            }

                                            else {

                                                if(office_pin_text.equalsIgnoreCase(""))
                                                {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_pin_code), Toast.LENGTH_SHORT).show();
                                                }

                                                else {
                                                    if (office_pin_text.length() != 6) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_office_valid_office_pin_code), Toast.LENGTH_SHORT).show();
                                                    } else {


                                                        editor.putString("register_address", register_address_text);
                                                        editor.putString("register_city", register_city_text);
                                                        editor.putString("register_state", register_state_text);
                                                        editor.putString("register_pin", register_pin_text);

                                                        editor.putString("office_address", office_add_text);
                                                        editor.putString("office_city", office_city_text);
                                                        editor.putString("office_state", office_state_text);
                                                        editor.putString("office_pin", office_pin_text);

                                                        editor.commit();

                                                        Intent i = new Intent(CompanyAddress.this, CompanyMobileNumber.class);
                                                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                                        i.putExtra("first_name", first_name);
                                                        i.putExtra("last_name", last_name);
                                                        i.putExtra("email_id", email_id);
                                                        i.putExtra("image_url", image_url);
                                                        i.putExtra("comp_buss_name", comp_buss_name);
                                                        i.putExtra("comp_landlin_no", comp_landlin_no);
                                                        i.putExtra("account_type", account_type);
                                                        i.putExtra("company_business", company_business);
                                                        i.putExtra("comp_buss_name", comp_buss_name);
                                                        i.putExtra("comp_landlin_no", comp_landlin_no);
                                                        i.putExtra("company_type", company_type);
                                                        i.putExtra("register_address", register_address_text);
                                                        i.putExtra("register_city", register_city_text);
                                                        i.putExtra("register_state", register_state_text);
                                                        i.putExtra("register_pin", register_pin_text);
                                                        i.putExtra("office_address", office_add_text);
                                                        i.putExtra("office_city", office_city_text);
                                                        i.putExtra("office_state", office_state_text);
                                                        i.putExtra("office_pin", office_pin_text);





                                                        startActivity(i);


                                                    }

                                                }}

                                            }

                                        }

                                    }



                                }


                            }



                        }

                    }

                }




                break;

            case R.id.same_checkbox:


                if(pref.getBoolean("same_address", true))
                {
                    hide_same.setVisibility(View.VISIBLE);
                    same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox));
                    editor.putBoolean("same_address",false);
                    editor.commit();
                }
                else
                {
                    hide_same.setVisibility(View.GONE);
                    same_checkbox.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.chkbox_tick));
                    editor.putBoolean("same_address",true);
                    editor.commit();
                }

                break;

        }


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }

}
