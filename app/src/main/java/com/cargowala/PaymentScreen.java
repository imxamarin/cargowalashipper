package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.cargowala.Payment.Network.APIService;
import com.cargowala.Payment.Network.NetworkService;
import com.cargowala.Payment.PaymentResponse;
import com.cargowala.helper.CommonListners;
import com.cargowala.helper.CommonUtils;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import retrofit2.Call;
import retrofit2.Callback;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class PaymentScreen extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton, order_progress;
    SeekBar seek;
    float discrete = 0;
    float start = 0;
    float end = 100;
    float start_pos = 0;
    int start_position = 0;
    int percent_value = 25;
    float tot_total = 0;
    double percent_value_int = 0;
    String amount_to_pay = "0";
    SweetAlertDialog pDialog;
    ImageView or_imagee;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String $txd_id,$amount;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;

    MyTextViewBold sub_total_value, total_value, percent_value_text;
    MyTextView pay_in_cash, pay_online, remaing_amount_line;
    MaterialEditText pan_card_text, service_tax_text;
    LinearLayout tax_layout;

    ImageView select_cash_image, select_online_image;
    LinearLayout select_cash, select_online;
    String future_payment_type = "",current_payment="1";
    String sub_total = "";
    LinearLayout hide_if_100_percent;
    private JSONArray paymentArray = new JSONArray();
    JSONArray transactions;
    JSONObject jsonObject;
    private boolean isFromRepeatOrder=false;
    String date1,time,orderId,invoice_base64,tot_invoice_text1,shipment_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_screen);
        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor1 = pref1.edit();


        if(getIntent().hasExtra("isFromRepeatOrder")){
            isFromRepeatOrder=getIntent().getBooleanExtra("isFromRepeatOrder",false);
        }

        if(getIntent().hasExtra("date1")){
            date1 = getIntent().getExtras().getString("date1");
        }
        if(getIntent().hasExtra("time")){
            time = getIntent().getExtras().getString("time");
        }
        if(getIntent().hasExtra("orderId")){
            orderId = getIntent().getExtras().getString("orderId");
        }
        if(getIntent().hasExtra("invoice_base64")){
            invoice_base64 = getIntent().getExtras().getString("invoice_base64");
        }
        if(getIntent().hasExtra("tot_invoice_text1")){
            tot_invoice_text1 = getIntent().getExtras().getString("tot_invoice_text1");
        }



        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);

        order_progress = (ImageView) findViewById(R.id.order_progress);
        order_progress.getLayoutParams().width = (deviceWidth / 2);
        order_progress.requestLayout();

        or_imagee = (ImageView) findViewById(R.id.or_imagee);
        or_imagee.getLayoutParams().width = (deviceWidth / 2);
        or_imagee.requestLayout();


        backbutton.requestLayout();
        select_cash = (LinearLayout) findViewById(R.id.select_cash);
        select_cash.setOnClickListener(this);
        select_online = (LinearLayout) findViewById(R.id.select_online);
        select_online.setOnClickListener(this);
        pay_online = (MyTextView) findViewById(R.id.pay_online);
        pay_online.setOnClickListener(this);
        pay_in_cash = (MyTextView) findViewById(R.id.pay_in_cash);
        pay_in_cash.setOnClickListener(this);

        remaing_amount_line = (MyTextView) findViewById(R.id.remaing_amount_line);

        tax_layout = (LinearLayout) findViewById(R.id.tax_layout);

        sub_total_value = (MyTextViewBold) findViewById(R.id.sub_total_value);
        total_value = (MyTextViewBold) findViewById(R.id.total_value);
        percent_value_text = (MyTextViewBold) findViewById(R.id.percent_value_text);

        pan_card_text = (MaterialEditText) findViewById(R.id.pan_card_text);
        service_tax_text = (MaterialEditText) findViewById(R.id.service_tax_text);
        hide_if_100_percent = (LinearLayout) findViewById(R.id.hide_if_100_percent);
        select_cash_image = (ImageView) findViewById(R.id.select_cash_image);
        select_online_image = (ImageView) findViewById(R.id.select_online_image);

        if (pref1.getString("user_account_type", "").equalsIgnoreCase("1")) {
            //individual
            service_tax_text.setVisibility(View.GONE);

        } /*else if (pref1.getString("user_account_type", "").equalsIgnoreCase("3")) {
            //agent
            service_tax_text.setVisibility(View.VISIBLE);
        } else if (pref1.getString("user_account_type", "").equalsIgnoreCase("5")) {
            //company
            service_tax_text.setVisibility(View.VISIBLE);
        } else {
            Log.d("errorrr", "not possible");
        }*/


        pan_card_text.setText(pref1.getString("user_pan_card", ""));
        service_tax_text.setText(pref1.getString("user_service_tax_no", ""));


        start = 25;      //you need to give starting value of SeekBar
        end = 100;         //you need to give end value of SeekBar
        start_pos = 25;    //you need to give starting position value of SeekBar


        start_position = (int) (((start_pos - start) / (end - start)) * 100);
        discrete = start_pos;
        seek = (SeekBar) findViewById(R.id.seek);

        seek.setProgress(start_position);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //   Toast.makeText(getBaseContext(), "discrete = " + String.valueOf(discrete), Toast.LENGTH_SHORT).show();


                //      Toast.makeText(getBaseContext(), "percent_value = " + percent_value/*String.valueOf(discrete)*/, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                // To convert it as discrete value

                Log.e("onProgressChanged","called");

              /*  progress = ((int)Math.round(progress/25))*25;
                seekBar.setProgress(progress);*/

                float temp = progress;
                float dis = end - start;
                discrete = (float) (Math.ceil((start + ((temp / 100) * dis))));

                if (discrete < 37)

                {
                    percent_value = 25;
                    progress = 0;
                    seekBar.setProgress(progress);

                   // percent_value_int = (int) tot_total / 4;
                    percent_value_int =  tot_total / 4;

                    amount_to_pay = "" + percent_value_int;
                    percent_value_text.setText("₹ " + percent_value_int);

                    remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_pay_rest_of_the_75)+"(₹ " + (tot_total - percent_value_int) + ") "+getResources().getString(R.string.payment_in_future)+"?");
                    pay_online.setText(getResources().getString(R.string.pay)+" 25% "+getResources().getString(R.string.online_now));
                    pay_in_cash.setText(getResources().getString(R.string.pay)+" 25% "+getResources().getString(R.string.in_cash_on_loading));
                    hide_if_100_percent.setVisibility(View.VISIBLE);

                    try {

                        paymentArray = null;
                        paymentArray = new JSONArray();
                        JSONObject firstPart = new JSONObject();
                        firstPart.put("instalment", "" + percent_value);
                        firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - (tot_total - percent_value_int)));
                        firstPart.put("mode", future_payment_type);
                        firstPart.put("remarks", "empty");

                        JSONObject SecondPart = new JSONObject();
                        SecondPart.put("instalment", "75");
                        SecondPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - percent_value_int));
                        SecondPart.put("mode", future_payment_type);
                        SecondPart.put("remarks", "empty");

                        paymentArray.put(firstPart);
                        paymentArray.put(SecondPart);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (discrete > 36 && discrete < 63) {
                    percent_value = 50;
                    progress = 31;
                    seekBar.setProgress(progress);

                  //  percent_value_int = (int) tot_total / 2;
                    percent_value_int =  tot_total / 2;
                    percent_value_text.setText("₹ " + percent_value_int);
                    amount_to_pay = "" + percent_value_int;
                   /* remaing_amount_line.setText("How would you like to payrest of the 50% (₹ " + (tot_total - percent_value_int) + ") payment in future?");
                    pay_online.setText("Pay 50% online now");
                    pay_in_cash.setText("Pay 50%in cash on loading");*/

                    remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_payrest_of_the)+" 50% (₹ " + (tot_total - percent_value_int) + ") "+getResources().getString(R.string.payment_in_future)+"?");
                    pay_online.setText(getResources().getString(R.string.pay)+" 50% "+getResources().getString(R.string.online_now));
                    pay_in_cash.setText(getResources().getString(R.string.pay)+" 50% "+getResources().getString(R.string.in_cash_on_loading));

                    hide_if_100_percent.setVisibility(View.VISIBLE);

                    try {

                        paymentArray = null;
                        paymentArray = new JSONArray();
                        JSONObject firstPart = new JSONObject();
                        firstPart.put("instalment", "" + percent_value);
                        firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - (tot_total - percent_value_int)));
                        firstPart.put("mode", future_payment_type);
                        firstPart.put("remarks", "empty");

                        JSONObject SecondPart = new JSONObject();
                        SecondPart.put("instalment", "50");
                        SecondPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - percent_value_int));
                        SecondPart.put("mode", future_payment_type);
                        SecondPart.put("remarks", "empty");

                        paymentArray.put(firstPart);
                        paymentArray.put(SecondPart);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (discrete > 62 && discrete < 88) {
                    percent_value = 75;
                    progress = 64;
                    seekBar.setProgress(progress);

                   // percent_value_int = (int) (tot_total / 4) * 3;
                    percent_value_int =  (tot_total / 4) * 3;
                    percent_value_text.setText("₹ " + percent_value_int);
                    amount_to_pay = "" + percent_value_int;

                   /* remaing_amount_line.setText("How would you like to payrest of the 25% (₹ " + (tot_total - percent_value_int) + ") payment in future?");
                    pay_online.setText("Pay 75% online now");
                    pay_in_cash.setText("Pay 75%in cash on loading");*/

                    remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_payrest_of_the)+" 25% (₹ " + (tot_total - percent_value_int) + ") "+getResources().getString(R.string.payment_in_future)+"?");
                    pay_online.setText(getResources().getString(R.string.pay)+" 75% "+getResources().getString(R.string.online_now));
                    pay_in_cash.setText(getResources().getString(R.string.pay)+" 75% "+getResources().getString(R.string.in_cash_on_loading));

                    hide_if_100_percent.setVisibility(View.VISIBLE);


                    try {

                        paymentArray = null;
                        paymentArray = new JSONArray();
                        JSONObject firstPart = new JSONObject();
                        firstPart.put("instalment", "" + percent_value);
                        firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - (tot_total - percent_value_int)));
                        firstPart.put("mode", future_payment_type);
                        firstPart.put("remarks", "empty");

                        JSONObject SecondPart = new JSONObject();
                        SecondPart.put("instalment", "25");
                        SecondPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - percent_value_int));
                        SecondPart.put("mode", future_payment_type);
                        SecondPart.put("remarks", "empty");

                        paymentArray.put(firstPart);
                        paymentArray.put(SecondPart);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    percent_value = 100;
                    progress = 100;
                    seekBar.setProgress(progress);

                   // percent_value_int = (int) (tot_total);
                    percent_value_int = (tot_total);
                    percent_value_text.setText("₹ " + percent_value_int);
                    amount_to_pay = "" + percent_value_int;

                    /*remaing_amount_line.setText("How would you like to payrest of the 0% payment in future?");
                    pay_online.setText("Pay 100% online now");
                    pay_in_cash.setText("Pay 100%in cash on loading");*/
                    hide_if_100_percent.setVisibility(View.GONE);

                    remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_payrest_of_the)+" 0% (₹ " + (tot_total - percent_value_int) + ") "+getResources().getString(R.string.payment_in_future)+"?");
                    pay_online.setText(getResources().getString(R.string.pay)+" 100% "+getResources().getString(R.string.online_now));
                    pay_in_cash.setText(getResources().getString(R.string.pay) + " 100% " + getResources().getString(R.string.in_cash_on_loading));


                    try {

                        paymentArray = null;
                        paymentArray = new JSONArray();
                        JSONObject firstPart = new JSONObject();
                        firstPart.put("instalment", "" + percent_value);
                        firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total));
                        firstPart.put("mode", future_payment_type);
                        firstPart.put("remarks", "empty");
                        paymentArray.put(firstPart);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        });

//--repeat_order_service_response
        try {
            String res = pref.getString("book_now_service_response", "");
            if(isFromRepeatOrder){
                res = pref.getString("repeat_order_service_response", "");
            }

            Log.e("PAYEMNT_JSON",res);
            JSONObject jsonobj = new JSONObject(res);


            sub_total = jsonobj.getString("sub_total");
            String taxes = jsonobj.getString("taxes");

            tot_total = Float.parseFloat(sub_total);


            sub_total_value.setText("₹ " + sub_total);
            amount_to_pay = sub_total;
            JSONArray jsonArr1 = new JSONArray(taxes);


            Log.e("jsonArr1",""+jsonArr1.toString());
            for (int j = 0; j < jsonArr1.length(); j++) {

                JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(j);

               /* String id = jsonObjectPackageData1.getString("id");
                String name = jsonObjectPackageData1.getString("name");*/

                String tax_name="";String tax_rate="";
                if(jsonObjectPackageData1.has("name")){
                     tax_name = jsonObjectPackageData1.getString("name");
                }
                if(jsonObjectPackageData1.has("rate")){
                     tax_rate = jsonObjectPackageData1.getString("rate");
                }


                //  String tax_type = jsonObjectPackageData1.getString("Tax Type");

                try {
                    tot_total = tot_total + Float.parseFloat(tax_rate);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    tot_total=0;
                }
                //      tax_layout

                RelativeLayout one_layout = new RelativeLayout(
                        getApplicationContext());

                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);


                params1.setMargins(0, 5, 0, 0);

                one_layout.setLayoutParams(params1);

                tax_layout.addView(one_layout);


                MyTextView heading = new MyTextView(
                        getApplicationContext());
                heading.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));


                RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params2.setMargins(0, 0, 60, 0);
                //  heading.setPadding(10, 10, 10, 10);
                heading.setLayoutParams(params2);
                heading.setText(tax_name);

                one_layout.addView(heading);


                MyTextView bid_now = new MyTextView(
                        getApplicationContext());
                bid_now.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                //  bid_now.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                RelativeLayout.LayoutParams params17 = new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);


                params17.addRule(
                        RelativeLayout.ALIGN_PARENT_RIGHT,
                        RelativeLayout.TRUE);


                bid_now.setLayoutParams(params17);
                bid_now.setText("₹ " + tax_rate);


                one_layout.addView(bid_now);


            }
            total_value.setText("₹ " + String.format("%.2f", tot_total));
            // amount_to_pay=""+String.format("%.2f", tot_total);
            // String.format("%.2f", floatValue)
           // percent_value_int = (int) tot_total / 4;
            percent_value_int =  tot_total / 4;

            percent_value_text.setText("₹ " + percent_value_int);

           /* remaing_amount_line.setText("How would you like to pay rest of the 75% (₹ " + (tot_total - percent_value_int) + ") payment in future?");
            pay_online.setText("Pay 25% online now");
            pay_in_cash.setText("Pay 25%in cash on loading");*/

            remaing_amount_line.setText(getResources().getString(R.string.how_would_you_like_to_pay_rest_of_the_75)+"(₹ " + (tot_total - percent_value_int) + ")"+getResources().getString(R.string.payment_in_future)+"?");
            pay_online.setText(getResources().getString(R.string.pay)+" 25% "+getResources().getString(R.string.online_now));
            pay_in_cash.setText(getResources().getString(R.string.pay)+" 25% "+getResources().getString(R.string.in_cash_on_loading));

                paymentArray = null;
                paymentArray = new JSONArray();
                JSONObject firstPart = new JSONObject();
                firstPart.put("instalment", "" + percent_value);

                firstPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - (tot_total - percent_value_int)));
                firstPart.put("mode", future_payment_type);
                firstPart.put("remarks", "empty");
                JSONObject SecondPart = new JSONObject();
                SecondPart.put("instalment", "75");
                SecondPart.put("amount", "" + CommonUtils.roundFigTwo(tot_total - percent_value_int));
                SecondPart.put("mode", future_payment_type);
                SecondPart.put("remarks", "empty");
                paymentArray.put(firstPart);
                paymentArray.put(SecondPart);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("PaymentArrayOnCreate", "" + paymentArray);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.backbutton:
                finish();
                break;

            case R.id.pay_online:
                current_payment="2";
                Log.e("tot_total",""+tot_total);

                if(percent_value == 100){
                    /**
                     * Json first object shows whether payment is online or cash
                     * 2nd for future payment
                     */

                    for(int j=0;j<paymentArray.length();j++){
                        try {
                            JSONObject innerObject=paymentArray.getJSONObject(j);
                            if(j==0){
                                innerObject.put("mode",current_payment);
                            }else {
                                innerObject.put("mode",future_payment_type);
                            }

                            paymentArray.put(j,innerObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    Log.e("PaymentArray", "" + paymentArray);

                        try {
                            jsonObject=new JSONObject();
                            jsonObject.put("transactions",paymentArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            transactions=new JSONArray();
                            jsonObject=new JSONObject();
                        }
                        Log.e("PaymentObject", "" + jsonObject);
                        gotoCitrusPayment("");

                    if(isFromRepeatOrder){
                        rescheduleOrderApi(true, "save_payment");
                    }else {
                        payMentByCash(true,"save_payment");
                    }


                }else {
                    if (future_payment_type.equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_future_payment), Toast.LENGTH_SHORT).show();
                    } else {
                        /**
                         * Json first object shows whther payment is online or cash
                         * 2nd for future payment
                         */

                        for(int j=0;j<paymentArray.length();j++){
                            try {
                                JSONObject innerObject=paymentArray.getJSONObject(j);
                                if(j==0){
                                    innerObject.put("mode",current_payment);
                                }else {
                                    innerObject.put("mode",future_payment_type);
                                }

                                paymentArray.put(j,innerObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.e("PaymentArray", "" + paymentArray);

                            try {
                                jsonObject=new JSONObject();
                                jsonObject.put("transactions",paymentArray);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                transactions=new JSONArray();
                                jsonObject=new JSONObject();
                            }
                            Log.e("PaymentObject", "" + jsonObject);
                          //  gotoCitrusPayment("");
                        if (CommonUtils.isOnline(getApplicationContext())) {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();

                            if(isFromRepeatOrder){
                                rescheduleOrderApi(true,"save_payment");
                            }else {
                                payMentByCash(true,"save_payment");
                            }

                        }

                    }
                }
                break;

            case R.id.pay_in_cash:
                current_payment="1";
                Log.e("tot_total",""+tot_total);
                if(percent_value == 100){
                    /**
                     * Json first object shows whether payment is online or cash
                     * 2nd for future payment
                     */

                    for(int j=0;j<paymentArray.length();j++){
                        try {
                            JSONObject innerObject=paymentArray.getJSONObject(j);
                            if(j==0){
                                innerObject.put("mode",current_payment);
                            }else {
                                innerObject.put("mode",future_payment_type);
                            }

                            paymentArray.put(j,innerObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }



                    Log.e("PaymentArray", "" + paymentArray);

                    if (CommonUtils.isOnline(getApplicationContext())) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        try {

                            jsonObject=new JSONObject();
                            jsonObject.put("transactions",paymentArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            transactions=new JSONArray();
                            jsonObject=new JSONObject();
                        }
                        Log.e("PaymentObject", "" + jsonObject);


                        if(isFromRepeatOrder){
                            rescheduleOrderApi(false, "cash_onloading");
                        }else {
                            payMentByCash(false,"cash_onloading");
                        }

                    }
                }else {
                    if (future_payment_type.equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_future_payment), Toast.LENGTH_SHORT).show();
                    } else {
                        /**
                         * Json first object shows whther payment is online or cash
                         * 2nd for future payment
                         */

                        for(int j=0;j<paymentArray.length();j++){
                            try {
                                JSONObject innerObject=paymentArray.getJSONObject(j);
                                if(j==0){
                                    innerObject.put("mode",current_payment);
                                }else {
                                    innerObject.put("mode",future_payment_type);
                                }

                                paymentArray.put(j,innerObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }



                        Log.e("PaymentArray", "" + paymentArray);

                        if (CommonUtils.isOnline(getApplicationContext())) {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            try {
                                jsonObject=new JSONObject();
                                jsonObject.put("transactions",paymentArray);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                transactions=new JSONArray();
                                jsonObject=new JSONObject();
                            }
                            Log.e("PaymentObject", "" + jsonObject);
                            if(isFromRepeatOrder){
                                rescheduleOrderApi(false,"cash_onloading");
                            }else {
                                payMentByCash(false,"cash_onloading");
                            }

                        }
                    }
                }



                break;
            case R.id.select_cash:

                select_cash_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                select_online_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                future_payment_type = "1";
                break;


            case R.id.select_online:

                select_cash_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_unselected));
                select_online_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.radio_button_selected));
                future_payment_type = "2";

                break;

        }


    }


    String res;
    StringRequest strReq;

    public String payMentByCash(final boolean isOnline,final String action) {
        Log.e("PaymentObject", "" + jsonObject);
        String url = getResources().getString(R.string.base_url) + "user/book-truck";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("user/book-truck", "onResponse");
                        Log.e("PAymentSCreen", response.toString());
                        res = response.toString();
                        checkPayMentByCash(res,isOnline);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("regg", "4 " + error.getMessage());
                try {
                    if(pDialog != null){
                        pDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                /*    params.put("oldpassword", old_pass);
                    params.put("newpassword", new_pass);*/


                    params.put("security_token", pref1.getString("security_token", "Something Wrong"));
                    params.put("_id", pref1.getString("userId", "_id Wrong"));
                    params.put("lp_name", pref.getString("book_origin_address", "lp_name Wrong"));
                    params.put("lp_address", pref.getString("book_origin_address", "lp_address Wrong"));
                    params.put("lp_state", pref.getString("book_origin_state", "lp_state Wrong"));
                    params.put("lp_city", pref.getString("book_origin_city", "lp_city Wrong"));
                    params.put("lp_latitude", pref.getString("book_origin_lat", "lp_latitude Wrong"));
                    params.put("lp_longitude", pref.getString("book_origin_long", "lp_longitude Wrong"));
                    params.put("up_name", pref.getString("book_odest_address", "up_name Wrong"));
                    params.put("up_address", pref.getString("book_odest_address", "up_address Wrong"));
                    params.put("up_state", pref.getString("book_dest_state", "up_state Wrong"));
                    params.put("up_city", pref.getString("book_dest_city", "up_city Wrong"));
                    params.put("up_latitude", pref.getString("book_dest_lat", "up_latitude Wrong"));
                    params.put("up_longitude", pref.getString("book_dest_long", "up_longitude Wrong"));
                    params.put("t_date", pref.getString("book_date", "t_date Wrong"));
                    params.put("t_time", pref.getString("book_time", "t_time Wrong"));
                    String distance = pref.getString("book_est_dist","0");
                    if(distance == null || distance.equals("null")){
                        params.put("est_distance", "0");
                    }else{
                        params.put("est_distance", distance);

                    }

                    params.put("est_time", pref.getString("book_est_time", "est_time Wrong"));
                    params.put("shipment_url", pref.getString("book_map_url", "shipment_url Wrong"));
                    params.put("items", "" + pref.getString("item_data", "items Wrong"));

                    params.put("category", pref.getString("truck_type", "category Wrong"));
                    params.put("ttype", pref.getString("truck_id", "type Wrong"));
                    params.put("quantity", pref.getString("no_of_trucks", "quantity Wrong"));
                    params.put("load_type", pref.getString("load_type", "load_type Wrong"));
                    params.put("priority_delivery", pref.getString("priority_delivery", "priority_delivery Wrong"));
                    params.put("invoice_amount", "" + pref.getString("total_invoice", "0"));
                    params.put("overall_weight", "" + pref.getString("total_item_wt", "0"));
                    params.put("invoicing_type", "" + pref.getString("invoicing_type", "something wrong"));
                    params.put("insurance_type", "" + pref.getString("insurance_type", "something wrong"));
                    params.put("insurance_image", "" + pref.getString("insurance_image", ""));
                    params.put("action", action);
                    params.put("total_amount", ""+CommonUtils.roundFigTwo(tot_total));

                    params.put("payments", jsonObject.toString());

                    Log.e("action", action);
                    Log.e("total_amount", "" + CommonUtils.roundFigTwo(tot_total));
                    Log.e("payments", ""+paymentArray);
                    Log.e("regg", "7 " + params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "29");


        return null;
    }


    public void checkPayMentByCash(String response,final boolean isOnline) {
        try {
            if(pDialog != null){
                pDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Log.e("register", response);

            if (response != null) {
                Log.e("register", "a");
                final JSONObject reader = new JSONObject(response);
                Log.e("register", "reader");
                status_code = reader.getString("status_code");
                Log.e("register", "status_code " + status_code);
                if (status_code.equalsIgnoreCase("200")) {
                    if(isOnline){

                        try {
                            JSONObject responseObject=reader.getJSONObject("response");
                            if(responseObject.has("txn_id")){
                                String txn_id=responseObject.getString("txn_id");
                                gotoCitrusPayment(txn_id);
                            }else {
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                        }

                    }else {
                        CommonUtils.initiatePopupWindow(PaymentScreen.this, getResources().getString(R.string.your_shipment_has_been_placed),
                                false, getResources().getString(R.string.success), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                    @Override
                                    public void positiveClick() {
                                        editor1.putBoolean("hitActiveShipmentService", true);
                                        editor1.commit();
                                        Intent i5 = new Intent(PaymentScreen.this, MyShipments.class);
                                        i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                        i5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(i5);

                                    }
                                });
                    }

                } else if (status_code.equalsIgnoreCase("406")) {

                    CommonUtils.initiatePopupWindow(PaymentScreen.this, getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    CommonUtils.initiatePopupWindow(PaymentScreen.this, getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else {
                    CommonUtils.initiatePopupWindow(PaymentScreen.this, getResources().getString(R.string.error_msg),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });

                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void gotoCitrusPayment(String transectionId){
        Log.e("percent_value_int", "" + percent_value_int);
//
//        Intent i2 = new Intent(PaymentScreen.this, PaymentActivity.class);
//        startActivity(i2);
        $txd_id = transectionId;
        $amount = amount_to_pay;

   callInstamojoPay("janedoe@gmail.com","9779712016","90","Hlox","Janne");



        /*        Intent i2 = new Intent(PaymentScreen.this, CitrusPayment.class);
        i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        i2.putExtra("transectionId", transectionId);
        i2.putExtra("amount_to_pay", "" + percent_value_int);
        i2.putExtra("payments",jsonObject.toString());
        startActivity(i2);*/

    }


    public String rescheduleOrderApi(final boolean isOnline,final  String action){
        String url = getResources().getString(R.string.base_url)+"user/repeat-order";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");
                        Log.d("register", response.toString());
                        res = response.toString();
                        checkRescheduleResponse(res,isOnline);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    params.put("security_token", pref1.getString("security_token", "Something Wrong"));
                    params.put("_id", pref1.getString("userId", "_id Wrong"));
                    params.put("action", action);
                    params.put("repeat", "true");
                    params.put("t_date", date1);
                    params.put("t_time", time);
                    params.put("shipment_id", orderId);
                    params.put("invoice_image", invoice_base64);
                    params.put("invoice_amount", tot_invoice_text1);
                    params.put("payments", jsonObject.toString());
                    params.put("total_amount", ""+CommonUtils.roundFigTwo(tot_total));
                    Log.e("rescheduleOrderApi", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "29");
        return null;
    }



    String status_code = "0";

    public void checkRescheduleResponse(String response,final boolean isOnline) {
        pDialog.dismiss();
        try {
            Log.e("register", response);
            if (response != null) {
                Log.e("register", "a");
                JSONObject reader = new JSONObject(response);
                Log.e("register", "reader");
                status_code = reader.getString("status_code");
                Log.e("register", "status_code " + status_code);
                if (status_code.equalsIgnoreCase("200")) {
                    editor.clear();
                    editor.commit();
                    editor1.putBoolean("hitMyBidBoardService", true);
                    editor1.commit();
                    if(isOnline){

                        try {
                            JSONObject responseObject=reader.getJSONObject("response");
                            if(responseObject.has("txn_id")){
                                String txn_id=responseObject.getString("txn_id");
                                gotoCitrusPayment(txn_id);
                            }else {
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                        }

                    }else {
                        CommonUtils.initiatePopupWindow(PaymentScreen.this, getResources().getString(R.string.your_shipment_has_been_placed),
                                false, getResources().getString(R.string.success), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                    @Override
                                    public void positiveClick() {
                                        editor1.putBoolean("hitActiveShipmentService", true);
                                        editor1.commit();
                                        Intent i5 = new Intent(PaymentScreen.this, MyShipments.class);
                                        i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                        i5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(i5);

                                    }
                                });
                    }


                } else if (status_code.equalsIgnoreCase("406")) {

                    //password changed
                    CommonUtils.initiatePopupWindow(PaymentScreen.this, getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });
                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    CommonUtils.initiatePopupWindow(PaymentScreen.this, getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else {
                    CommonUtils.initiatePopupWindow(PaymentScreen.this, getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + " " + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }



    public void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final  Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }


    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
     //           Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                String[] alpha = response.split(":");
                String[] alp = alpha[3].split("=");
                callAfterPayAPI(alp[1]);


            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }


    public void goToActiveShipments(boolean isFrompandingPayment){
        if(isFrompandingPayment){
            Intent BackIntent = new Intent();
            BackIntent.putExtra("orderId", orderId);
            BackIntent.putExtra("shipmentId", shipment_id);
            setResult(101,BackIntent);
            finish();
        }else {
            editor1.clear();
            editor1.commit();
            editor.putBoolean("hitActiveShipmentService", true);
            editor.commit();
            Intent i5 = new Intent(PaymentScreen.this, MyShipments.class);
            i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            i5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i5);
        }

    }

    private void callAfterPayAPI(String response){
        NetworkService networkService =  NetworkService.INSTANCE;
        APIService api = networkService.create();

        Map<String,String> map = new HashMap<>();
        map.put("TxId",$txd_id);
        map.put("TxStatus","SUCCESS");
        map.put("amount",$amount);
        map.put("pgTxnNo","");
        map.put("paymentMode","Online");
        map.put("currency","INR");
        map.put("service_request","complete-payment");
        map.put("processed_paymentid",response);

        Call<PaymentResponse> ab =  api.sendDataPayment(map);
        ab.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, retrofit2.Response<PaymentResponse> response) {
                Log.d("###OnSuccess Print","Response");
                Log.d("Print",response.body().getResponse().getTxnId());
                goToActiveShipments(false);
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                Log.d("###Onfailure Print","Response");
                Log.d("Error",t.toString());
                t.printStackTrace();
                Log.d("Error",t.getMessage());
            }
        });

    }
}
