package com.cargowala.fcm;

import android.content.SharedPreferences;
import android.util.Log;


import com.cargowala.Models.FcmToken;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Bhvesh on 03-Nov-16.
 */
public class TokenService  extends FirebaseInstanceIdService {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("notificationToken", refreshedToken);
        sendRegistrationToServer(refreshedToken);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        editor.putString("fcmToken", refreshedToken);
        editor.commit();

        FcmToken fcmToken=new FcmToken();
        fcmToken.setFcmToken(refreshedToken);
        EventBus.getDefault().post(fcmToken);
    }

    private void sendRegistrationToServer(String token) {
    }
}
