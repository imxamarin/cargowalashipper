package com.cargowala;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by Akash on 3/11/2016.
 */
public class Util {

    public static boolean isGpsEnabled (Context context) {
        return ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isNetworkEnabled (Context context) {
        return ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

}
