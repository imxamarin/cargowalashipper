package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

/**
 * Created by Akash on 2/10/2016.
 */
public class ItemDetail extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton;
    String jsonObjectPackageData;

    String name1, parent_category_name1 , sub_category_name1 ,desc1,weight1,length1,width1,height1,total_item1,total_weight1,invoice_image1;


    MyTextViewSemi item_name_text,category_text,sub_category_text,item_wt_text,no_of_units_text;
    MyTextView desc_text,tot_wt_text;
    MaterialEditText length_in,width_in,height_in;

    ImageView upload_license_image;

    // android:text="(total weight = 1300 kg)"

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_detail);


        Intent intent = getIntent();
        jsonObjectPackageData = intent.getExtras().getString("orderId");





        Log.d("aaaa ",""+jsonObjectPackageData);

        try {

            JSONObject  jsonObjectPackageData2 = new JSONObject(jsonObjectPackageData);

            Log.d("My App", jsonObjectPackageData2.toString());

             name1 = jsonObjectPackageData2.getString("name");
             parent_category_name1 = jsonObjectPackageData2.getString("parent_category_name");
             sub_category_name1 = jsonObjectPackageData2.getString("sub_category_name");
            desc1 = jsonObjectPackageData2.getString("desc");
            weight1 = jsonObjectPackageData2.getString("weight");
            length1 = jsonObjectPackageData2.getString("length");
            width1 = jsonObjectPackageData2.getString("width");
            height1 = jsonObjectPackageData2.getString("height");
            total_item1 = jsonObjectPackageData2.getString("total_item");
            total_weight1 = jsonObjectPackageData2.getString("total_weight");
            invoice_image1 = jsonObjectPackageData2.getString("invoice_image");


            Log.d("My App", invoice_image1);

        } catch (Throwable t) {
            Log.e("My App", "Could not parse malformed JSON:");
        }


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();





        upload_license_image = (ImageView) findViewById(R.id.upload_license_image);
        upload_license_image.setOnClickListener(this);

        length_in = (MaterialEditText) findViewById(R.id.length_in);
        width_in = (MaterialEditText) findViewById(R.id.width_in);
        height_in = (MaterialEditText) findViewById(R.id.height_in);

        desc_text = (MyTextView) findViewById(R.id.desc_text);
        tot_wt_text = (MyTextView) findViewById(R.id.tot_wt_text);

        item_name_text = (MyTextViewSemi) findViewById(R.id.item_name_text);
        category_text = (MyTextViewSemi) findViewById(R.id.category_text);
        sub_category_text = (MyTextViewSemi) findViewById(R.id.sub_category_text);
        item_wt_text = (MyTextViewSemi) findViewById(R.id.item_wt_text);
        no_of_units_text = (MyTextViewSemi) findViewById(R.id.no_of_units_text);


        if(!name1.equalsIgnoreCase(""))
        {
            item_name_text.setText(name1);
        }
        if(!parent_category_name1.equalsIgnoreCase(""))
        {
            category_text.setText(parent_category_name1);
        }
        if(!sub_category_name1.equalsIgnoreCase(""))
        {
            sub_category_text.setText(sub_category_name1);
        }
        if(!desc1.equalsIgnoreCase(""))
        {
            desc_text.setText(desc1);
        }
        if(!weight1.equalsIgnoreCase(""))
        {
            item_wt_text.setText(weight1);
        }
        if(!length1.equalsIgnoreCase(""))
        {
            length_in.setText(length1);
        }
        if(!width1.equalsIgnoreCase(""))
        {
            width_in.setText(width1);
        }

        if(!height1.equalsIgnoreCase(""))
        {
            height_in.setText(height1);
        }
        if(!total_item1.equalsIgnoreCase(""))
        {
            no_of_units_text.setText(total_item1);
        }
        if(!total_weight1.equalsIgnoreCase("") && !total_weight1.equalsIgnoreCase("0"))
        {
            tot_wt_text.setText(getResources().getString(R.string.total_weight)+total_weight1+" "+getResources().getString(R.string.kg)+")");
        }
        if(!invoice_image1.equalsIgnoreCase(""))
        {
            Picasso.with(getApplicationContext())
                    .load(invoice_image1)
                    .placeholder(R.drawable.loading_image)
                    .error(R.drawable.unable_image_upload).into(upload_license_image);
        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {




            case R.id.backbutton:

                finish();

                break;


            case R.id.upload_license_image:

                if(!invoice_image1.equalsIgnoreCase(""))
                {

                    Intent i13 = new Intent(ItemDetail.this, SingleTouchImageViewActivity.class);
                    i13.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i13.putExtra("imageUrl", invoice_image1);
                    startActivity(i13);

                }

                break;




        }



    }








}
