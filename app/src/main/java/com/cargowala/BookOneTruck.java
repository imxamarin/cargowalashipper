package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akash on 2/10/2016.
 */
public class BookOneTruck extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton,order_progress;
    MyTextView next;
    ImageView truck_type_1,truck_type_2,truck_type_3;
    ImageView partial_load_image,full_load_image,priority_image;

    LinearLayout sub_no_trucks,add_no_trucks;
    LinearLayout no_of_trucks_layout,priority_del_layout,priority_del_info_layout;
    MyTextViewSemi no_tucks_text;
    ScrollView exampleScrollView;

    MaterialEditText tot_wt_text;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
   Spinner truck_type_spinner;
    ArrayAdapter<CharSequence> adapter;
   /* String[] Type1 = {"Select","Tata Ace ( 0.75 Ton )","Pick Up ( 0.5 Ton )","Tata 407 ( 2.5 Ton )"};
    String[] Type2 = {"Select","19 Feet Open ( 7 Ton )","20 Feet Close ( 7 Ton )","6 Tyres Open ( 10 Ton )","10 Tyre Taurus ( 15 Ton )"};
    String[] Type3 = {"Select","15 Tyre Taurus ( 20 Ton )","Refrigerated Truck (10 Ton)"};*/



    String truckType0[],truckType1[],truckType2[],truckType3[];

    String truckTypeId1[],truckTypeId2[],truckTypeId3[];

    String truck_type="";
    String load_type="";

    Boolean isPriorityDelivery = false;
    String truck_name = "";
    String truck_id = "";


  /*  ComboSeekBar csb;*/



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_one_truck);

        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        truckType0 = new String[1];
        truckType0[0] ="Select Truck Type";

        try {
            String res = pref.getString("book_get_data_response", "");

            Log.e("BookOneTruck",""+res);

            JSONObject jsonobj = new JSONObject(res);
            String Trucks = jsonobj.getString("Trucks");

            JSONObject jsonobj4 = new JSONObject(Trucks);

            String first = jsonobj4.getString("Below 5 ton");
            String second = jsonobj4.getString("5 ton to 15 ton");
            String third = jsonobj4.getString("Above 15 ton");


            JSONArray jsonArr2 = new JSONArray(first);
            JSONArray jsonArr3 = new JSONArray(second);
            JSONArray jsonArr4 = new JSONArray(third);


            truckType1 = new String[jsonArr2.length()];
            truckType2 = new String[jsonArr3.length()];
            truckType3 = new String[jsonArr4.length()];

            truckTypeId1 = new String[jsonArr2.length()];
            truckTypeId2 = new String[jsonArr3.length()];
            truckTypeId3 = new String[jsonArr4.length()];



            for (int k = 0; k < jsonArr2.length(); k++) {

                JSONObject jsonObjectPackageData2 = jsonArr2.getJSONObject(k);

                String id2 = jsonObjectPackageData2.getString("id");
                String name2 = jsonObjectPackageData2.getString("name");

                Log.d("first ", id2 + " " + name2);

                truckType1[k] =name2;
                truckTypeId1[k] =id2;




            }

            for (int k = 0; k < jsonArr3.length(); k++) {

                JSONObject jsonObjectPackageData2 = jsonArr3.getJSONObject(k);

                String id2 = jsonObjectPackageData2.getString("id");
                String name2 = jsonObjectPackageData2.getString("name");

                Log.d("second ", id2 + " " + name2);

                truckType2[k] =name2;
                truckTypeId2[k] =id2;
            }

            for (int k = 0; k < jsonArr4.length(); k++) {

                JSONObject jsonObjectPackageData2 = jsonArr4.getJSONObject(k);

                String id2 = jsonObjectPackageData2.getString("id");
                String name2 = jsonObjectPackageData2.getString("name");

                Log.d("third ", id2 + " " + name2);

                truckType3[k] =name2;
                truckTypeId3[k] =id2;

            }

        }
        catch (Exception e)
        {


            Log.d("parse error", "spinner data incorrect");

        }


        tot_wt_text=(MaterialEditText)findViewById(R.id.tot_wt_text);


        truck_type_spinner=(Spinner)findViewById(R.id.truck_type_spinner);


            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int px = Math.round(40 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));


        truck_type_spinner.setDropDownWidth((int) (deviceWidth - px));

      //  adapter=ArrayAdapter.createFromResource(this,R.array.country_names,android.R.layout.simple_list_item_1);


        adapter =  new ArrayAdapter(this,
                R.layout.spinner_layout,  truckType0);


       // adapter.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        truck_type_spinner.setAdapter(adapter);
        truck_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(getBaseContext(), position + " is selected "+parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                Log.d("Truck Type Main ", " truck_type = " + truck_type);
                Log.d("Truck Type", position + " is selected, value = " + parent.getSelectedItem());

                truck_name = parent.getSelectedItem().toString();



                if(truck_type.equalsIgnoreCase("1")) {

                    Log.d("truck_id"," "+truckTypeId1[position]);
                    truck_id = truckTypeId1[position];

                }

               else if(truck_type.equalsIgnoreCase("2")) {

                    Log.d("truck_id"," "+truckTypeId2[position]);
                    truck_id = truckTypeId2[position];
                }

                else if(truck_type.equalsIgnoreCase("3")) {

                    Log.d("truck_id"," "+truckTypeId3[position]);
                    truck_id = truckTypeId3[position];
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });



        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        exampleScrollView = (ScrollView) findViewById(R.id.exampleScrollView);

        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);
        backbutton.requestLayout();
        backbutton.setOnClickListener(this);



        order_progress = (ImageView) findViewById(R.id.order_progress);
        order_progress.getLayoutParams().width =  (deviceWidth /2);
        order_progress.requestLayout();

        next = (MyTextView) findViewById(R.id.next);
        next.setOnClickListener(this);



        truck_type_1 = (ImageView) findViewById(R.id.truck_type_1);
        truck_type_1.setOnClickListener(this);
        truck_type_2 = (ImageView) findViewById(R.id.truck_type_2);
        truck_type_2.setOnClickListener(this);
        truck_type_3 = (ImageView) findViewById(R.id.truck_type_3);
        truck_type_3.setOnClickListener(this);
        partial_load_image = (ImageView) findViewById(R.id.partial_load_image);
        partial_load_image.setOnClickListener(this);
        full_load_image = (ImageView) findViewById(R.id.full_load_image);
        full_load_image.setOnClickListener(this);
        priority_image = (ImageView) findViewById(R.id.priority_image);
        priority_image.setOnClickListener(this);



        sub_no_trucks = (LinearLayout) findViewById(R.id.sub_no_trucks);
        sub_no_trucks.setOnClickListener(this);

        add_no_trucks = (LinearLayout) findViewById(R.id.add_no_trucks);
        add_no_trucks.setOnClickListener(this);

        no_tucks_text = (MyTextViewSemi) findViewById(R.id.no_tucks_text);

        no_of_trucks_layout = (LinearLayout) findViewById(R.id.no_of_trucks_layout);
        priority_del_layout = (LinearLayout) findViewById(R.id.priority_del_layout);
        priority_del_info_layout = (LinearLayout) findViewById(R.id.priority_del_info_layout);


        no_of_trucks_layout.setVisibility(View.GONE);
        priority_del_layout.setVisibility(View.GONE);
        priority_del_info_layout.setVisibility(View.GONE);





        if(!pref.getString("truck_type","NA").equalsIgnoreCase("NA"))
        {
            if(pref.getString("truck_type","NA").equalsIgnoreCase("1")) {
                adapter = new ArrayAdapter(this,
                        R.layout.spinner_layout, truckType1);


                adapter.setDropDownViewResource(R.layout.spinner_layout);
                truck_type_spinner.setAdapter(adapter);


                truck_type_1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_1_selected));
                truck_type_2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_2_unselected));
                truck_type_3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_3_unselected));
                truck_type = "1";

            }

            if(pref.getString("truck_type","NA").equalsIgnoreCase("2")) {

                adapter =  new ArrayAdapter(this,
                        R.layout.spinner_layout,  truckType2);


                adapter.setDropDownViewResource(R.layout.spinner_layout);
                truck_type_spinner.setAdapter(adapter);


                truck_type_1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_1_unselected));
                truck_type_2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_2_selected));
                truck_type_3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_3_unselected));
                truck_type = "2";

            }

            if(pref.getString("truck_type","NA").equalsIgnoreCase("3")) {
                adapter =  new ArrayAdapter(this,
                        R.layout.spinner_layout,  truckType3);


                adapter.setDropDownViewResource(R.layout.spinner_layout);
                truck_type_spinner.setAdapter(adapter);

                truck_type_1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_1_unselected));
                truck_type_2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_2_unselected));
                truck_type_3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_3_selected));

                truck_type = "3";

            }
        }


        if(!pref.getString("load_type","NA").equalsIgnoreCase("NA"))
        {
            if(pref.getString("load_type","NA").equalsIgnoreCase("0")) {
                no_of_trucks_layout.setVisibility(View.GONE);
                priority_del_layout.setVisibility(View.GONE);
                priority_del_info_layout.setVisibility(View.GONE);

                partial_load_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partial_load_selected));
                full_load_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.full_load_unselected));
                load_type = "0";
            }

            if(pref.getString("load_type","NA").equalsIgnoreCase("1")) {


                no_of_trucks_layout.setVisibility(View.VISIBLE);
                priority_del_layout.setVisibility(View.VISIBLE);



                if (pref.getString("priority_delivery", "NA").equalsIgnoreCase("1")) {
                    priority_del_info_layout.setVisibility(View.VISIBLE);
                    isPriorityDelivery = true;
                    priority_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.on_button));
                } else {
                    priority_del_info_layout.setVisibility(View.GONE);
                    isPriorityDelivery = false;
                    priority_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.off_button));
                }




                partial_load_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partial_load_unselected));
                full_load_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.full_load_selected));

                load_type = "1";

                no_tucks_text.setText(pref.getString("no_of_trucks", "1"));

            }
        }



        if(!pref.getString("truck_name","NA").equalsIgnoreCase("NA"))
        {
            truck_type_spinner.setSelection(adapter.getPosition(pref.getString("truck_name",getResources().getString(R.string.select_truck_type))));
        }
        if(!pref.getString("total_item_wt","NA").equalsIgnoreCase("NA"))
        {

            tot_wt_text.setText(pref.getString("total_item_wt",""));
        }



    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {



            case R.id.backbutton:

                finish();

                break;


            case R.id.next:

                String tot_wt_text1 = tot_wt_text.getText().toString().trim();

                if( truck_type.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_truck_type), Toast.LENGTH_SHORT).show();
                }

                else if(truck_name.equalsIgnoreCase("Select Truck Type"))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_truck_name), Toast.LENGTH_SHORT).show();
                }

                else if(tot_wt_text1.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_total_weight), Toast.LENGTH_SHORT).show();
                }


                else if(Integer.parseInt(tot_wt_text1)<1)
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_valid_total_weight), Toast.LENGTH_SHORT).show();

                }

                else if(load_type.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_load_type), Toast.LENGTH_SHORT).show();
                }


                else {


                    editor.putString("truck_type", truck_type);
                    editor.putString("load_type", load_type);

                 //   editor.putString("total_item_wt", tot_wt_text1);
                    editor.putString("total_item_wt", tot_wt_text1.replaceFirst("^0+(?!$)", ""));



                    if(load_type.equalsIgnoreCase("1"))
                    {
                        if (isPriorityDelivery)
                        {
                            editor.putString("priority_delivery", "1");
                        } else {

                            editor.putString("priority_delivery", "0");
                        }
                        editor.putString("no_of_trucks",no_tucks_text.getText().toString());
                    }
                    else
                    {
                        editor.putString("no_of_trucks","1");
                        editor.putString("priority_delivery", "0");
                    }


                    editor.putString("truck_name", truck_name);
                    editor.putString("truck_id", truck_id);

                    editor.commit();

                        Intent i = new Intent(BookOneTruck.this, FullLoadDetail.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(i);

                }

                break;

            case R.id.truck_type_1:

                adapter =  new ArrayAdapter(this,
                        R.layout.spinner_layout,  truckType1);


                adapter.setDropDownViewResource(R.layout.spinner_layout);
                truck_type_spinner.setAdapter(adapter);



                truck_type_1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_1_selected));
                truck_type_2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_2_unselected));
                truck_type_3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_3_unselected));
                truck_type = "1";

                break;

            case R.id.truck_type_2:

                adapter =  new ArrayAdapter(this,
                        R.layout.spinner_layout,  truckType2);


                adapter.setDropDownViewResource(R.layout.spinner_layout);
                truck_type_spinner.setAdapter(adapter);


                truck_type_1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_1_unselected));
                truck_type_2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_2_selected));
                truck_type_3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_3_unselected));
                truck_type = "2";

                break;

            case R.id.truck_type_3:

                adapter =  new ArrayAdapter(this,
                        R.layout.spinner_layout,  truckType3);


                adapter.setDropDownViewResource(R.layout.spinner_layout);
                truck_type_spinner.setAdapter(adapter);

                truck_type_1.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_1_unselected));
                truck_type_2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_2_unselected));
                truck_type_3.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.truck_type_3_selected));

                truck_type = "3";

                break;

            case R.id.partial_load_image:

                no_of_trucks_layout.setVisibility(View.GONE);
                priority_del_layout.setVisibility(View.GONE);
                priority_del_info_layout.setVisibility(View.GONE);

                partial_load_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partial_load_selected));
                full_load_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.full_load_unselected));
                load_type = "0";
                break;

            case R.id.full_load_image:


                no_of_trucks_layout.setVisibility(View.VISIBLE);
                priority_del_layout.setVisibility(View.VISIBLE);
                if(isPriorityDelivery) {
                    priority_del_info_layout.setVisibility(View.VISIBLE);
                }
                else
                {
                    priority_del_info_layout.setVisibility(View.GONE);
                }


                exampleScrollView.post(new Runnable() {

                    @Override
                    public void run() {
                        exampleScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });

                partial_load_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.partial_load_unselected));
                full_load_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.full_load_selected));

                load_type = "1";

                break;

            case R.id.sub_no_trucks:


                int no_of_trucks = Integer.parseInt(no_tucks_text.getText().toString());

                if(no_of_trucks==1)
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_of_trucks_cannot_be_more_than), Toast.LENGTH_SHORT).show();
                }

                else
                {
                    no_tucks_text.setText(""+(no_of_trucks-1));
                }


                break;

            case R.id.add_no_trucks:

                int no_of_trucks1 = Integer.parseInt(no_tucks_text.getText().toString());

                if(no_of_trucks1==15)
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_of_trucks_cannot_be_more_than), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    no_tucks_text.setText(""+(no_of_trucks1+1));
                }

                break;

            case R.id.priority_image:

                if(isPriorityDelivery)
                {
                    isPriorityDelivery = false;
                    priority_del_info_layout.setVisibility(View.GONE);
                    priority_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.off_button));
                }
                else
                {
                    isPriorityDelivery = true;
                    priority_del_info_layout.setVisibility(View.VISIBLE);
                    priority_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.on_button));
                }

                exampleScrollView.post(new Runnable() {

                    @Override
                    public void run() {
                        exampleScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });


                break;








        }


    }



}