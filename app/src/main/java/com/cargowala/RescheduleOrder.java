package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/17/2016.
 */
public class RescheduleOrder extends Activity implements View.OnClickListener , TimePickerDialog.OnTimeSetListener{



    int deviceWidth, deviceHeight;
    RelativeLayout actionBar;
    ImageView backbutton;
    SweetAlertDialog pDialog;

    MaterialEditText loading_time_text;
    MyTextView proceed;

    SharedPreferences pref;
    SharedPreferences.Editor editor;


    SharedPreferences pref1;
    SharedPreferences.Editor editor1;



    String orderId,date;

    boolean sameDate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reschedule_order);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;


        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();


        pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor1 = pref1.edit();



        Intent intent = getIntent();


        orderId = intent.getExtras().getString("orderId1");
        date = intent.getExtras().getString("date");
        sameDate= intent.getExtras().getBoolean("sameDate");




        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();


        loading_time_text = (MaterialEditText) findViewById(R.id.loading_time_text);
        proceed= (MyTextView) findViewById(R.id.proceed);
        proceed.setOnClickListener(this);





        loading_time_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    Log.d("11", "11");

                    Calendar now1 = Calendar.getInstance();
                    TimePickerDialog dpd1 = TimePickerDialog.newInstance(
                            RescheduleOrder.this,
                            now1.get(Calendar.HOUR_OF_DAY),
                            now1.get(Calendar.MINUTE),
                            false
                    );

                    Log.d("main ","sameDate "+sameDate);



                    if(sameDate) {
                        dpd1.setMinTime(now1.get(Calendar.HOUR_OF_DAY)+1 , now1.get(Calendar.MINUTE) ,
                                now1.get(Calendar.SECOND));
                    }

                    dpd1.show(getFragmentManager(), "TimePickerDialog");

                    dpd1.setAccentColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));


                }

                return false;
            }
        });



    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:
                finish();


                break;




            case R.id.proceed:

                if(time.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_rescheduled_time), Toast.LENGTH_SHORT)
                            .show();
                }
                else
                {

                    if (isOnline()) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makeRescheduleOrderReq();
                    } else {
                        Toast.makeText(RescheduleOrder.this,
                                getResources().getString(R.string.check_your_network),
                                Toast.LENGTH_SHORT).show();
                    }


                }


                break;









            case R.id.popup_button:


                if(status_code.equalsIgnoreCase("200"))
            {
                dialog.dismiss();
                finish();
            }

                else if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref1.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref1.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }



                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(RescheduleOrder.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i2);


                    finish();

                }
                else
                {
                    dialog.dismiss();
                }

                break;


        }


    }





    String time="";

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {


        time = ""+hourOfDay+":"+minute;




        try {
            String _24HourTime = hourOfDay +":"+ minute;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);



            time =_12HourSDF.format(_24HourDt);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        loading_time_text.setText(time);



    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    String res;
    StringRequest strReq;

    public String makeRescheduleOrderReq() {

        String url = getResources().getString(R.string.base_url)+"user/reschedule-order";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkeRescheduleOrderResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {




                    Log.d("security_token", pref1.getString("security_token", "Something Wrong"));
                    Log.d("_id", pref1.getString("userId", "_id Wrong"));


                    Log.d("t_time", time);
                    Log.d("t_date", date);
                    Log.d("shipment_id", orderId);



                    params.put("security_token", pref1.getString("security_token", "Something Wrong"));
                    params.put("_id", pref1.getString("userId", "_id Wrong"));

                    params.put("t_date", date);
                    params.put("t_time", time);
                    params.put("shipment_id", orderId);






                    Log.e("register", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "28");



        return null;
    }

    String status_code = "0";

    public void checkeRescheduleOrderResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);



                if(status_code.equalsIgnoreCase("200"))
                {


                    editor1.putBoolean("timeChanged", true);
                    editor1.commit();



                    initiatePopupWindow(getResources().getString(R.string.your_order_has_been_rescheduled),
                            false,getResources().getString(R.string.success), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }


                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(RescheduleOrder.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
