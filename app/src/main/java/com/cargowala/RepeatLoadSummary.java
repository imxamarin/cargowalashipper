package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class RepeatLoadSummary extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    SharedPreferences pref1;
    SharedPreferences.Editor editor1;

    MyTextViewBold total_value;
    MyTextViewSemi load_type_priority, date_text, time_text, no_of_trucks_text, truck_name_text, from_place_text, to_place_text;
    MyTextView load_fare_text, today_rate_text, load_fare_value, priority_rate_value, insurance_value;
    ImageView truck_image;
    SweetAlertDialog pDialog;


    MyTextView proceed_to_pay, post_on_bid_board;
    ImageView or_imagee;

    String date1, time, orderId, invoice_base64, tot_invoice_text1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repeat_load_summary);


        Intent intent = getIntent();

        date1 = intent.getExtras().getString("date1");
        time = intent.getExtras().getString("time");
        orderId = intent.getExtras().getString("orderId");
        invoice_base64 = intent.getExtras().getString("invoice_base64");
        tot_invoice_text1 = intent.getExtras().getString("tot_invoice_text1");


        pref = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor = pref.edit();

        pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        or_imagee = (ImageView) findViewById(R.id.or_imagee);
        or_imagee.getLayoutParams().width = (deviceWidth / 2);
        or_imagee.requestLayout();

        proceed_to_pay = (MyTextView) findViewById(R.id.proceed_to_pay);
        proceed_to_pay.setOnClickListener(this);
        post_on_bid_board = (MyTextView) findViewById(R.id.post_on_bid_board);
        post_on_bid_board.setOnClickListener(this);

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);


        from_place_text = (MyTextViewSemi) findViewById(R.id.from_place_text);
        to_place_text = (MyTextViewSemi) findViewById(R.id.to_place_text);
        total_value = (MyTextViewBold) findViewById(R.id.total_value);
        load_type_priority = (MyTextViewSemi) findViewById(R.id.load_type_priority);
        date_text = (MyTextViewSemi) findViewById(R.id.date_text);
        time_text = (MyTextViewSemi) findViewById(R.id.time_text);
        no_of_trucks_text = (MyTextViewSemi) findViewById(R.id.no_of_trucks_text);
        truck_name_text = (MyTextViewSemi) findViewById(R.id.truck_name_text);
        load_fare_text = (MyTextView) findViewById(R.id.load_fare_text);
        today_rate_text = (MyTextView) findViewById(R.id.today_rate_text);
        load_fare_value = (MyTextView) findViewById(R.id.load_fare_value);

        priority_rate_value = (MyTextView) findViewById(R.id.priority_rate_value);
        insurance_value = (MyTextView) findViewById(R.id.insurance_value);
        truck_image = (ImageView) findViewById(R.id.truck_image);


        backbutton.requestLayout();


        try {
            String res = pref.getString("repeat_order_service_response", "");

            Log.d("resss", res);

            JSONObject jsonobj = new JSONObject(res);

            String load_type = jsonobj.getString("load_type");
            String from_name = jsonobj.getString("from_city");
            String to_name = jsonobj.getString("to_city");
            String priority_delivery = jsonobj.getString("priority_delivery");
            String transit_date = jsonobj.getString("transit_date");
            String transit_time = jsonobj.getString("transit_time");
            String truck_name = jsonobj.getString("truck_name");
            String truck_quantity = jsonobj.getString("truck_quantity");
            String today_price = jsonobj.getString("today_price");
            String load_fare = jsonobj.getString("load_fare");
            String distance_fare = jsonobj.getString("distance_fare");
            String insurance = jsonobj.getString("insurance");
            String priority_delivery_charge = jsonobj.getString("priority_delivery_charges");
            String sub_total = jsonobj.getString("sub_total");
            String truck_category = jsonobj.getString("truck_category");

            String service_taxno = jsonobj.getString("service_taxno");
            String pancard = jsonobj.getString("pancard");

            editor1.putString("user_pan_card", pancard);
            editor1.putString("user_service_tax_no", service_taxno);
            editor1.commit();


            if (truck_category.equalsIgnoreCase("1")) {
                truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.small_grey_truck));
            } else if (truck_category.equalsIgnoreCase("2")) {
                truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
            } else if (truck_category.equalsIgnoreCase("3")) {
                truck_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.large_grey_truck));
            } else {
                Log.d("error", "unreachable code");
            }

            from_place_text.setText(from_name);
            to_place_text.setText(to_name);
            total_value.setText("₹ " + sub_total);
            if (priority_delivery.equalsIgnoreCase("Yes")) {
                load_type_priority.setText(load_type + " - " + getResources().getString(R.string.priority));
            } else {
                load_type_priority.setText(load_type);
            }
            date_text.setText(transit_date);
            time_text.setText(transit_time);
            no_of_trucks_text.setText(truck_quantity);
            truck_name_text.setText(truck_name);
            load_fare_text.setText(getResources().getString(R.string.load_fare_for) + truck_quantity + getResources().getString(R.string.trucks_bracket));
            today_rate_text.setText(getResources().getString(R.string.load_rate_for_today) + "\u20B9 " + today_price + "/" + getResources().getString(R.string.km) + ")");
            load_fare_value.setText("₹ " + load_fare);

            priority_rate_value.setText("₹ " + priority_delivery_charge);
            insurance_value.setText("₹ " + insurance);


        } catch (Exception e) {

            Log.d("error", e.getLocalizedMessage() + "\n2/" + e.getMessage());
        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:

                finish();

                break;

            case R.id.proceed_to_pay:

                Intent i = new Intent(RepeatLoadSummary.this, PaymentScreen.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("isFromRepeatOrder", true);
                i.putExtra("date1", date1);
                i.putExtra("time", time);
                i.putExtra("orderId", orderId);
                i.putExtra("invoice_base64", invoice_base64);
                i.putExtra("tot_invoice_text1", tot_invoice_text1);
                startActivity(i);

                break;

            case R.id.post_on_bid_board:


                if (isOnline()) {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText(getResources().getString(R.string.loading));
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeBookNowReq();
                } else {
                    Toast.makeText(RepeatLoadSummary.this,
                            getResources().getString(R.string.check_your_network),
                            Toast.LENGTH_SHORT).show();
                }


                break;


            case R.id.popup_button:


                if (status_code.equalsIgnoreCase("200")) {
                    dialog.dismiss();
                    Intent i5 = new Intent(RepeatLoadSummary.this, BidBoard.class);
                    i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i5);
                } else if (status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910")) {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook")) {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    } else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google")) {
                        isGoogle = true;
                    }


                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(RepeatLoadSummary.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn", isGoogle);
                    startActivity(i2);


                    finish();

                } else {
                    dialog.dismiss();
                }


                break;


        }


    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    String res;
    StringRequest strReq;

    public String makeBookNowReq() {

        String url = getResources().getString(R.string.base_url) + "user/repeat-order";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkeBookNowResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                /*    params.put("oldpassword", old_pass);
                    params.put("newpassword", new_pass);*/


                    params.put("security_token", pref1.getString("security_token", "Something Wrong"));
                    params.put("_id", pref1.getString("userId", "_id Wrong"));
                    params.put("action", "save_bid_board");
                    params.put("repeat", "true");
                    params.put("t_date", date1);
                    params.put("t_time", time);
                    params.put("shipment_id", orderId);
                    params.put("invoice_image", invoice_base64);
                    params.put("invoice_amount", tot_invoice_text1);


                    Log.e("register", params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "29");


        return null;
    }

    String status_code = "0";

    public void checkeBookNowResponse(String response) {
        pDialog.dismiss();
        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);


                if (status_code.equalsIgnoreCase("200")) {


                    editor.clear();
                    editor.commit();

                    editor1.putBoolean("hitMyBidBoardService", true);
                    editor1.commit();


                    initiatePopupWindow(getResources().getString(R.string.your_shipment_has_been_placed_and_added_to_bid_load_board),
                            false, getResources().getString(R.string.success), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("406")) {

                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + " " + status_code, getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText) {
        try {

            dialog = new Dialog(RepeatLoadSummary.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if (isAlert) {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
