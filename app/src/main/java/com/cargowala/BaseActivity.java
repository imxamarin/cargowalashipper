package com.cargowala;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.cargowala.helper.CommonUtils;
import com.cargowala.helper.Constants;


/**
 * Created by Bhvesh on 17-Nov-16.
 */
public class BaseActivity extends Activity {
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        CommonUtils.settingLanguage(pref.getString(Constants.LOCALE, Constants.ENG), this);

    }
}
