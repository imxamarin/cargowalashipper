package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Bhvesh on 15-Nov-16.
 */
public class AgentSplash extends Activity {
    private static int SPLASH_TIME_OUT = 1500;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private CircleImageView profile_image;
    private MyTextViewSemi name,companyName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.agent_special_screen);
        profile_image=(CircleImageView) findViewById(R.id.profile_image);
        name=(MyTextViewSemi) findViewById(R.id.name);
        companyName=(MyTextViewSemi) findViewById(R.id.companyName);
        // Add code to print out the key hash

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        //----------making agent feel special

                    name.setText(pref.getString("user_firstname", "") + " " + pref.getString("user_lastname", ""));
                    companyName.setText(pref.getString("user_business_name",""));
                    if(!pref.getString("user_image_url","").equalsIgnoreCase(""))
                    {

                        Picasso.with(getApplicationContext())
                                .load(pref.getString("user_image_url", ""))
                                .placeholder(R.drawable.default_user)
                                .error(R.drawable.default_user).into(profile_image);

                    }else {
                        if(!pref.getString("user_image_base64","NA").equalsIgnoreCase("NA"))
                        {
                            byte[] decodedString = Base64.decode(pref.getString("user_image_base64","NA"), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            profile_image.setImageBitmap(decodedByte);
                        }
                    }

        new Handler().postDelayed(new Runnable() {

            public void run() {
                Intent i1 = new Intent(AgentSplash.this, Home.class);
                i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i1);
                finish();


            }
        }, SPLASH_TIME_OUT);

    }
}
