package com.cargowala;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cargowala.Models.PendingPaymentResponse;
import com.cargowala.Models.TransectionsArray;
import com.cargowala.Payment.Network.APIService;
import com.cargowala.Payment.Network.NetworkService;
import com.cargowala.Payment.PaymentResponse;
import com.cargowala.Payment.PrePayment.PrePaymentResponse;
import com.cargowala.helper.CommonListners;
import com.cargowala.helper.CommonUtils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import retrofit2.Call;
import retrofit2.Callback;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class PendingPayments extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton, no_connection_image2;

    ImageView or_imagee;
    MyTextViewBold tvFromAddress, tvToAddress, tvFinalPayment, tvSecondPayment, tvFirstPayment, tvFirstPaymentTitle;
    MyTextViewSemi tvLoadType, tvTruckCount, tvTime;
    MyTextView tvDate, tvOrderId, tvTruckName, tvSecondPercentage, tvSecondLineOne, tvSecondLineSecond, tvSecondCashOnLoadingButton, tvSecondOnlineButton, payCompletePaymentNow, tvFirstPercentage, tvFirstLineOne, tvFirstLineSecond, tvFirstOnlineButton;
    ImageView imgTruck;
    LinearLayout secondPaymentLinear, firstPaymentLinear, mainLinear;
    SweetAlertDialog pDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;
    String shipmentId = "", orderId = "";
    RelativeLayout rvCompletePayment;
    PendingPaymentResponse pendingPaymentResponse;
    JSONObject jsonObject=null;
    String $shipment_id,$shipper_id,$payment_id,$id,$order_number,$txd_id,$amount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_payment);

        if (getIntent().hasExtra("shipmentId")) {
            shipmentId = getIntent().getStringExtra("shipmentId");
        }
        if (getIntent().hasExtra("orderId")) {
            orderId = getIntent().getStringExtra("orderId");
        }
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        Intent intent = getIntent();
        orderId = intent.getExtras().getString("orderId");
        actionBar = (RelativeLayout) findViewById(R.id.actionBar);
        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        imgTruck = (ImageView) findViewById(R.id.imgTruck);
        no_connection_image2 = (ImageView) findViewById(R.id.no_connection_image2);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);

        rvCompletePayment = (RelativeLayout) findViewById(R.id.rvCompletePayment);
        tvFromAddress = (MyTextViewBold) findViewById(R.id.tvFromAddress);
        tvToAddress = (MyTextViewBold) findViewById(R.id.tvToAddress);
        tvFinalPayment = (MyTextViewBold) findViewById(R.id.tvFinalPayment);
        tvSecondPayment = (MyTextViewBold) findViewById(R.id.tvSecondPayment);
        tvFirstPayment = (MyTextViewBold) findViewById(R.id.tvFirstPayment);
        tvFirstPaymentTitle = (MyTextViewBold) findViewById(R.id.tvFirstPaymentTitle);

        tvLoadType = (MyTextViewSemi) findViewById(R.id.tvLoadType);
        tvTruckCount = (MyTextViewSemi) findViewById(R.id.tvTruckCount);

      /*  MyTextView tvDate,tvTime,tvOrderId,tvTruckName,tvSecondPercentage,tvSecondLineOne,tvSecondLineSecond,tvSecondCashOnLoadingButton,tvSecondOnlineButton
                ,pay_complete_payment_now,tvFirstPercentage,tvFirstLineOne,tvFirstLineSecond,tvFirstOnlineButton;*/

        tvDate = (MyTextView) findViewById(R.id.tvDate);
        tvTime = (MyTextViewSemi) findViewById(R.id.tvTime);
        tvOrderId = (MyTextView) findViewById(R.id.tvOrderId);
        tvTruckName = (MyTextView) findViewById(R.id.tvTruckName);
        tvSecondPercentage = (MyTextView) findViewById(R.id.tvSecondPercentage);
        tvSecondLineOne = (MyTextView) findViewById(R.id.tvSecondLineOne);
        tvSecondLineSecond = (MyTextView) findViewById(R.id.tvSecondLineSecond);
        tvSecondCashOnLoadingButton = (MyTextView) findViewById(R.id.tvSecondCashOnLoadingButton);
        tvSecondOnlineButton = (MyTextView) findViewById(R.id.tvSecondOnlineButton);

        payCompletePaymentNow = (MyTextView) findViewById(R.id.payCompletePaymentNow);
        tvFirstPercentage = (MyTextView) findViewById(R.id.tvFirstPercentage);
        tvFirstLineOne = (MyTextView) findViewById(R.id.tvFirstLineOne);
        tvFirstLineOne = (MyTextView) findViewById(R.id.tvFirstLineOne);
        tvFirstLineSecond = (MyTextView) findViewById(R.id.tvFirstLineSecond);
        tvFirstOnlineButton = (MyTextView) findViewById(R.id.tvFirstOnlineButton);

        secondPaymentLinear = (LinearLayout) findViewById(R.id.secondPaymentLinear);
        firstPaymentLinear = (LinearLayout) findViewById(R.id.firstPaymentLinear);
        mainLinear = (LinearLayout) findViewById(R.id.mainLinear);
        mainLinear.setVisibility(View.GONE);
        backbutton.requestLayout();


        or_imagee = (ImageView) findViewById(R.id.or_imagee);
        or_imagee.getLayoutParams().width = (deviceWidth / 2);
        or_imagee.requestLayout();


        if (CommonUtils.isOnline(PendingPayments.this)) {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            pDialog.setTitleText(getResources().getString(R.string.loading));
            pDialog.setCancelable(false);
            pDialog.show();
            makePendingDetailReq();
        } else {
            //    no_connection_image.setVisibility(View.VISIBLE);
            no_connection_image2.setVisibility(View.VISIBLE);
            Toast.makeText(PendingPayments.this,
                    getResources().getString(R.string.check_your_network),
                    Toast.LENGTH_SHORT).show();
        }


        tvFirstOnlineButton.setOnClickListener(this);
        tvSecondCashOnLoadingButton.setOnClickListener(this);
        tvSecondOnlineButton.setOnClickListener(this);
        rvCompletePayment.setOnClickListener(this);

    }


    String res;
    StringRequest strReq;



    public String makePendingDetailReq() {
        String url = getResources().getString(R.string.base_url) + "user/get-payment-detail";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");
                        Log.d("register", response.toString());
                        res = response.toString();


                        checkDetailResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));
                    params.put("_id", pref.getString("userId", "Something Wrong"));
                    params.put("shipment_id", shipmentId);


                    //  1 / 0 (Comment 1 for Completed, 0 for Pending
                    Log.e("paymentDetails", params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };

        AppController.getInstance().addToRequestQueue(strReq, "35");


        return null;
    }

    private void checkDetailResponse(String res) {
        Log.e("Pending", "" + res);

        try {
            JSONObject mainObject = new JSONObject(res);
            String status_code = mainObject.getString("status_code");
            if (status_code.equalsIgnoreCase("200")) {
                mainLinear.setVisibility(View.VISIBLE);
                JSONArray response = mainObject.getJSONArray("response");
                JSONObject responseObject = response.getJSONObject(0);

                pendingPaymentResponse = new Gson().fromJson(responseObject.toString(), PendingPaymentResponse.class);
                Log.e("pendingPaymentResponse", new Gson().toJson(pendingPaymentResponse));
                tvFromAddress.setText(pendingPaymentResponse.getFrom_city());
                tvToAddress.setText(pendingPaymentResponse.getTo_city());

                $shipment_id = pendingPaymentResponse.getShipment_id();
                $id = pendingPaymentResponse.getId();
                $order_number = pendingPaymentResponse.getOrder_number();
                $payment_id = pendingPaymentResponse.getPayment_id();
                $shipper_id = pendingPaymentResponse.getShipper_id();


                if (pendingPaymentResponse.getTruck_category().equalsIgnoreCase("1")) {
                    imgTruck.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.small_grey_truck));
                } else if (pendingPaymentResponse.getTruck_category().equalsIgnoreCase("2")) {
                    imgTruck.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.medium_grey_truck));
                } else if (pendingPaymentResponse.getTruck_category().equalsIgnoreCase("3")) {
                    imgTruck.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.large_grey_truck));
                } else {

                }


                if (pendingPaymentResponse.getTransactions().size() == 0) {
                    secondPaymentLinear.setVisibility(View.GONE);
                    firstPaymentLinear.setVisibility(View.GONE);
                    rvCompletePayment.setVisibility(View.GONE);
                } else if (pendingPaymentResponse.getTransactions().size() == 1) {
                    secondPaymentLinear.setVisibility(View.GONE);
                    firstPaymentLinear.setVisibility(View.VISIBLE);
                    rvCompletePayment.setVisibility(View.GONE);
                    if (pendingPaymentResponse.getTransactions().get(0).getStatus().equalsIgnoreCase("Pending")) {
                        tvFirstPaymentTitle.setText(getResources().getString(R.string.full_payment));
                        secondPaymentLinear.setVisibility(View.GONE);
                        firstPaymentLinear.setVisibility(View.VISIBLE);
                        tvFirstPercentage.setText("(" + pendingPaymentResponse.getTransactions().get(0).getPart() + "% " + getResources().getString(R.string.of_final_payment) + ")");
                        tvFirstLineSecond.setVisibility(View.VISIBLE);
                        tvFirstLineOne.setVisibility(View.VISIBLE);
                        String text_1 = "<font color=#585858  >" + getResources().getString(R.string.you_selected_pay_on) + "</font><font color=#000000  ><b> " + getResources().getString(R.string.loading) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.option_earlier) + "</font>";
                        tvFirstLineOne.setText(Html.fromHtml(text_1));
                        String text_2 = "<font color=#585858  >" + getResources().getString(R.string.you_can_also_pay_this_amount) + "</font><font color=#000000  ><b> " + getResources().getString(R.string.online) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.now) + "</font>";
                        tvFirstLineSecond.setText(Html.fromHtml(text_2));
                        tvFirstPayment.setText("₹ " + CommonUtils.roundFigTwo(pendingPaymentResponse.getTransactions().get(0).getAmount()));
                        tvFirstOnlineButton.setVisibility(View.VISIBLE);

                    } else {


                        tvFirstPercentage.setText("(" + pendingPaymentResponse.getTransactions().get(0).getPart() + "% " + getResources().getString(R.string.of_final_payment) + ")");
                        tvFirstLineSecond.setVisibility(View.GONE);
                        tvFirstLineOne.setVisibility(View.VISIBLE);
                        tvFirstPaymentTitle.setText(getResources().getString(R.string.full_payment));
                        String text2 = "<font color=#585858  >" + getResources().getString(R.string.you_have_done) + "</font><font color=#000000><b> " + getResources().getString(R.string.complete_payment) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.online) + "</font>";
                        tvFirstLineOne.setText(Html.fromHtml(text2));
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tvFirstLineOne.getLayoutParams();
                        params.setMargins(20, 20, 20, 20);
                        tvFirstLineOne.setLayoutParams(params);
                        tvFirstPayment.setText("₹ " + CommonUtils.roundFigTwo(pendingPaymentResponse.getTransactions().get(0).getAmount()));
                        tvFirstOnlineButton.setVisibility(View.GONE);
                        tvFirstLineSecond.setVisibility(View.GONE);

                    }

                } else if (pendingPaymentResponse.getTransactions().size() == 2) {
                    secondPaymentLinear.setVisibility(View.VISIBLE);
                    firstPaymentLinear.setVisibility(View.VISIBLE);
//------------------for 0 index
                    if (pendingPaymentResponse.getTransactions().get(0).getStatus().equalsIgnoreCase("Pending")) {
                        tvFirstPercentage.setText("(" + pendingPaymentResponse.getTransactions().get(0).getPart() + "% " + getResources().getString(R.string.of_final_payment) + ")");
                        tvFirstLineSecond.setVisibility(View.VISIBLE);
                        tvFirstLineOne.setVisibility(View.VISIBLE);
                        String text_1 = "<font color=#585858  >" + getResources().getString(R.string.you_selected_pay_on) + "</font><font color=#000000  ><b> " + getResources().getString(R.string.loading) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.option_earlier) + "</font>";
                        tvFirstLineOne.setText(Html.fromHtml(text_1));
                        String text_2 = "<font color=#585858  >" + getResources().getString(R.string.you_can_also_pay_this_amount) + "</font><font color=#000000  ><b> " + getResources().getString(R.string.online) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.now) + "</font>";
                        tvFirstLineSecond.setText(Html.fromHtml(text_2));
                        tvFirstPayment.setText("₹ " + CommonUtils.roundFigTwo(pendingPaymentResponse.getTransactions().get(0).getAmount()));
                        tvFirstOnlineButton.setVisibility(View.VISIBLE);

                    } else {
                        rvCompletePayment.setVisibility(View.GONE);
                        tvFirstPercentage.setText("(" + pendingPaymentResponse.getTransactions().get(0).getPart() + "% " + getResources().getString(R.string.of_final_payment) + ")");
                        String text2 = "<font color=#585858  >" + getResources().getString(R.string.your) + "</font><font color=#000000><b> " + getResources().getString(R.string.first) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.payment_is_done) + "</font>";
                        tvFirstLineOne.setText(Html.fromHtml(text2));
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tvFirstLineOne.getLayoutParams();
                        params.setMargins(20, 20, 20, 20);
                        tvFirstLineOne.setLayoutParams(params);
                        tvFirstPayment.setText("₹ " + CommonUtils.roundFigTwo(pendingPaymentResponse.getTransactions().get(0).getAmount()));
                        tvFirstOnlineButton.setVisibility(View.GONE);
                        tvFirstLineSecond.setVisibility(View.GONE);

                    }

//------------------for 1 index

                    //------------when second payment is pending
                    if (pendingPaymentResponse.getTransactions().get(1).getStatus().equalsIgnoreCase("Pending")) {
                        if (pendingPaymentResponse.getTransactions().get(1).getMethod().equalsIgnoreCase("Cash")) {
                            tvSecondPercentage.setText("(" + pendingPaymentResponse.getTransactions().get(1).getPart() + "% " + getResources().getString(R.string.of_final_payment) + ")");
                            String text_1 = "<font color=#585858  >" + getResources().getString(R.string.you_selected_pay_on) + "</font><font color=#000000  ><b> " + getResources().getString(R.string.loading) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.option_earlier) + "</font>";
                            tvSecondLineOne.setText(Html.fromHtml(text_1));
                            String text_2 = "<font color=#585858  >" + getResources().getString(R.string.you_can_also_pay_this_amount) + "</font><font color=#000000  ><b> " + getResources().getString(R.string.online) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.now) + "</font>";
                            tvSecondLineSecond.setText(Html.fromHtml(text_2));
                            tvSecondPayment.setText("₹ " + CommonUtils.roundFigTwo(pendingPaymentResponse.getTransactions().get(1).getAmount()));
                            tvSecondOnlineButton.setVisibility(View.VISIBLE);
                            or_imagee.setVisibility(View.GONE);
                            tvSecondCashOnLoadingButton.setVisibility(View.GONE);

                            tvSecondLineOne.setVisibility(View.VISIBLE);
                            tvSecondLineSecond.setVisibility(View.VISIBLE);

                        } else {
                            tvSecondPercentage.setText("(" + pendingPaymentResponse.getTransactions().get(1).getPart() + "% " + getResources().getString(R.string.of_final_payment) + ")");
                            String text_1 = "<font color=#585858  >" + getResources().getString(R.string.you_selected_pay_on) + "</font><font color=#000000  ><b> " + getResources().getString(R.string.loading) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.option_earlier) + "</font>";
                            tvSecondLineOne.setText(Html.fromHtml(text_1));
                            String text_2 = "<font color=#585858  >" + getResources().getString(R.string.you_can_also_pay_this_amount) + "</font><font color=#000000  ><b> " + getResources().getString(R.string.online) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.now) + "</font>";
                            tvSecondLineSecond.setText(Html.fromHtml(text_2));
                            tvSecondPayment.setText("₹ " + CommonUtils.roundFigTwo(pendingPaymentResponse.getTransactions().get(1).getAmount()));

                            tvSecondLineOne.setVisibility(View.GONE);
                            tvSecondLineSecond.setVisibility(View.GONE);
                            tvSecondOnlineButton.setVisibility(View.VISIBLE);
                            or_imagee.setVisibility(View.VISIBLE);
                            tvSecondCashOnLoadingButton.setVisibility(View.VISIBLE);

                        }
                    }else {

                        //----------when second payment is done
                        tvSecondPercentage.setText("(" + pendingPaymentResponse.getTransactions().get(1).getPart() + "% " + getResources().getString(R.string.of_final_payment) + ")");
                        tvSecondPayment.setText("₹ " + CommonUtils.roundFigTwo(pendingPaymentResponse.getTransactions().get(1).getAmount()));

                        tvSecondOnlineButton.setVisibility(View.GONE);
                        or_imagee.setVisibility(View.GONE);
                        tvSecondCashOnLoadingButton.setVisibility(View.GONE);

                        tvSecondLineOne.setVisibility(View.VISIBLE);
                        tvSecondLineSecond.setVisibility(View.GONE);
                        String text2 = "<font color=#585858  >" + getResources().getString(R.string.your) + "</font><font color=#000000><b> " + getResources().getString(R.string.second) + " </b></font>" + "<font color=#585858  >" + getResources().getString(R.string.payment_is_done) + "</font>";
                        tvSecondLineOne.setText(Html.fromHtml(text2));
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tvSecondLineOne.getLayoutParams();
                        params.setMargins(20, 20, 20, 20);
                        tvSecondLineOne.setLayoutParams(params);
                    }



                }

                if (pendingPaymentResponse.getPriority_delivery().equalsIgnoreCase("No")) {
                    tvLoadType.setText(pendingPaymentResponse.getLoad_type());
                } else {
                    tvLoadType.setText(pendingPaymentResponse.getLoad_type() + " - " + getResources().getString(R.string.priority));
                }

                tvTruckCount.setText(pendingPaymentResponse.getTruck_quantity());
                tvOrderId.setText(getResources().getString(R.string.order_id) + " " + pendingPaymentResponse.getOrder_number());
                tvTruckName.setText(pendingPaymentResponse.getTruck_name());
                tvFinalPayment.setText("₹ " + CommonUtils.roundFigTwo(pendingPaymentResponse.getTotal_payment()));
                tvDate.setText(pendingPaymentResponse.getTransit_date());
                tvTime.setText(pendingPaymentResponse.getTransit_time());

            } else if (status_code.equalsIgnoreCase("406")) {
                //password changed
                CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.pass_changed_error),
                        true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                            @Override
                            public void positiveClick() {

                            }
                        });

            } else if (status_code.equalsIgnoreCase("910")) {
                //user_inactive
                CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.inactive_user_error),
                        true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                            @Override
                            public void positiveClick() {

                            }
                        });


            } else {
                CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.some_problem_try_again_text),
                        true, getResources().getString(R.string.error) + " " + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                            @Override
                            public void positiveClick() {

                            }
                        });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pDialog.dismiss();
    }


    @Override
    public void onClick(View v) {

        //-- "method": "Cash", "method": "Online",
        switch (v.getId()) {
            case R.id.backbutton:

                finish();

                break;
            case R.id.tvFirstOnlineButton:
                ArrayList<TransectionsArray> transactions = new ArrayList<>();

                if (pendingPaymentResponse != null) {
                    transactions = pendingPaymentResponse.getTransactions();
                    Log.e("transactions", "" + new Gson().toJson(transactions));
                    TransectionsArray transectionsArray = transactions.get(0);
                    //transectionsArray.setMethod("Online");
                   // transactions.set(0, transectionsArray);
                    Log.e("transactionsAfter", "" + new Gson().toJson(transactions));

                    if (CommonUtils.isOnline(PendingPayments.this)) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makeTransectionApi(true,pendingPaymentResponse.getId(),""+transectionsArray.getAmount(),transectionsArray.getTxn_id(),"2");
                    } else {
                        //    no_connection_image.setVisibility(View.VISIBLE);
                        no_connection_image2.setVisibility(View.VISIBLE);
                        Toast.makeText(PendingPayments.this,
                                getResources().getString(R.string.check_your_network),
                                Toast.LENGTH_SHORT).show();
                    }
                }


                break;
            case R.id.tvSecondCashOnLoadingButton:
                if (pendingPaymentResponse != null) {
                    transactions = pendingPaymentResponse.getTransactions();
                    if(transactions.size()>0){
                        Log.e("transactions", "" + new Gson().toJson(transactions));
                        TransectionsArray transectionsArray = transactions.get(1);
                      /*  transectionsArray.setMethod("Online");
                        transactions.set(1, transectionsArray);*/
                        Log.e("transactionsAfter", "" + new Gson().toJson(transactions));


                        if (CommonUtils.isOnline(PendingPayments.this)) {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makeTransectionApi(false,pendingPaymentResponse.getId(),""+transectionsArray.getAmount(),transectionsArray.getTxn_id(),"1");
                        } else {
                            //    no_connection_image.setVisibility(View.VISIBLE);
                            no_connection_image2.setVisibility(View.VISIBLE);
                            Toast.makeText(PendingPayments.this,
                                    getResources().getString(R.string.check_your_network),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                }


                break;

            case R.id.tvSecondOnlineButton:
                if (pendingPaymentResponse != null) {
                    transactions = pendingPaymentResponse.getTransactions();
                    if(transactions.size()>0){
                        Log.e("transactions", "" + new Gson().toJson(transactions));
                        TransectionsArray transectionsArray = transactions.get(1);
                      /*  transectionsArray.setMethod("Online");
                        transactions.set(1, transectionsArray);*/
                        Log.e("transactionsAfter", "" + new Gson().toJson(transactions));


                        if (CommonUtils.isOnline(PendingPayments.this)) {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makeTransectionApi(true,pendingPaymentResponse.getId(),""+transectionsArray.getAmount(),transectionsArray.getTxn_id(),"2");
                        } else {
                            //    no_connection_image.setVisibility(View.VISIBLE);
                            no_connection_image2.setVisibility(View.VISIBLE);
                            Toast.makeText(PendingPayments.this,
                                    getResources().getString(R.string.check_your_network),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                break;
            case R.id.rvCompletePayment:
                try {

                    JSONArray paymentArray = null;
                    paymentArray = new JSONArray();
                    JSONObject firstPart = new JSONObject();
                    firstPart.put("instalment", "100");
                    firstPart.put("amount", "" + CommonUtils.roundFigTwo(pendingPaymentResponse.getTotal_payment()));
                    firstPart.put("mode", "2");
                    firstPart.put("remarks", "payment done");
                    paymentArray.put(firstPart);

                    jsonObject=new JSONObject();
                    jsonObject.put("transactions", paymentArray);

                   /* Log.e("security_token", pref.getString("security_token", "Something Wrong"));
                    Log.e("_id", pref.getString("userId", "_id Wrong"));
                    Log.e("payments", "" + jsonObject.toString());
                    Log.e("shipment_id",""+shipmentId);
                    Log.e("action","save_payment");
                    Log.e("total_amount",""+pendingPaymentResponse.getTotal_payment());
                    Log.e("jsonObject",""+jsonObject.toString());*/

                    if (CommonUtils.isOnline(PendingPayments.this)) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                       completeFunctioning(true,"save_payment");
                    } else {
                        //    no_connection_image.setVisibility(View.VISIBLE);
                        no_connection_image2.setVisibility(View.VISIBLE);
                        Toast.makeText(PendingPayments.this,
                                getResources().getString(R.string.check_your_network),
                                Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;


        }


    }

    public void gotoCitrusPayment(String transectionId,String amountTopay,String usersEmail,String  phonenumber){

//        Intent i2 = new Intent(PendingPayments.this, PaymentActivity.class);
//        startActivity(i2);
    $txd_id = transectionId;
    $amount = amountTopay;
    callInstamojoPay(usersEmail,phonenumber,$amount,"Shipmment","Cargowala");


     /*   Intent i2 = new Intent(PendingPayments.this, CitrusPayment.class);
        i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i2.putExtra("transectionId", transectionId);
        i2.putExtra("amount_to_pay", amountTopay);
        i2.putExtra("fromPendingPayment",true);
        i2.putExtra("orderId", orderId);
        i2.putExtra("shipmentId", shipmentId);
        startActivityForResult(i2, 101);*/

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 101){
            try {
                if(data != null){
                    if (data.hasExtra("shipmentId")) {
                        shipmentId = data.getStringExtra("shipmentId");
                    }
                    if (data.hasExtra("orderId")) {
                        orderId = data.getStringExtra("orderId");
                    }

                    if (CommonUtils.isOnline(PendingPayments.this)) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makePendingDetailReq();
                    } else {
                        //    no_connection_image.setVisibility(View.VISIBLE);
                        no_connection_image2.setVisibility(View.VISIBLE);
                        Toast.makeText(PendingPayments.this,
                                getResources().getString(R.string.check_your_network),
                                Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    public String makeTransectionApi(final boolean isOnline,final String payment_id,final String amount,final String transaction_id,final String mode){
        Log.e("makeTransectionApi", "called");
        String url = getResources().getString(R.string.base_url)+"user/make-transaction";
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("makeTransectionApi", response.toString());
                        res = response.toString();
                        checkTransectionResponse(res, isOnline,amount);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("onErrorResponse", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
//--- 'Shipper / Trucker'
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));
                    params.put("_id", pref.getString("userId", "_id Wrong"));
                    params.put("payment_id", payment_id);
                    params.put("amount", amount);
                    params.put("transaction_id", transaction_id);
                    params.put("user_type", "Shipper");
                    params.put("mode", mode);

                    Log.e("Pendingpayment", params.toString());

                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "29");
        return null;
    }


    String status_code = "0";
    public void checkTransectionResponse(String response,final boolean isOnline,final String amountToPay) {
        pDialog.dismiss();
        try {

            Gson gson = new Gson();
            PrePaymentResponse pre = gson.fromJson(response,PrePaymentResponse.class);

            Log.e("register", response);
            if (response != null) {
                Log.e("register", "a");
                JSONObject reader = new JSONObject(response);
                Log.e("register", "reader");
                status_code = reader.getString("status_code");
                Log.e("register", "status_code " + status_code);
                if (status_code.equalsIgnoreCase("200")) {
                    if(isOnline){

                        try {
                            JSONObject responseObject=reader.getJSONObject("response");
                            if(responseObject.has("txn_id")){
                                String txn_id=responseObject.getString("txn_id");
                                gotoCitrusPayment(txn_id,amountToPay,pre.getResponse().getUserDetails().getEmail(),pre.getResponse().getUserDetails().getPhone());
                            }else {
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                        }

                    }else {
                        if (CommonUtils.isOnline(PendingPayments.this)) {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makePendingDetailReq();
                        } else {
                            //    no_connection_image.setVisibility(View.VISIBLE);
                            no_connection_image2.setVisibility(View.VISIBLE);
                            Toast.makeText(PendingPayments.this,
                                    getResources().getString(R.string.check_your_network),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }


                } else if (status_code.equalsIgnoreCase("406")) {

                    //password changed
                    CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });
                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else {
                    CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error) + " " + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }





    //-----------complete functioning
    public String completeFunctioning(final  boolean isOnlinePayment,final String action) {
        Log.e("PaymentObject", "" + jsonObject);
        String url = getResources().getString(R.string.base_url) + "user/save-transactions";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");
                        Log.e("register", response.toString());
                        res = response.toString();
                        checkCompleteResponce(res, isOnlinePayment);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));
                    params.put("_id", pref.getString("userId", "_id Wrong"));
                    params.put("shipment_id", shipmentId);
                    params.put("action", action);
                    params.put("total_amount", ""+CommonUtils.roundFigTwo(pendingPaymentResponse.getTotal_payment()));
                    params.put("payments", ""+jsonObject.toString());
                    params.put("service_for", "");
                    params.put("completePayment","true");

                    Log.e("Completeparams", " " + params.toString());

                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "29");


        return null;
    }



    public void checkCompleteResponce(String response,final boolean isOnlinePayment) {
        pDialog.dismiss();
        try {
            Log.e("register", response);
            Gson gson = new Gson();
            PrePaymentResponse pre = gson.fromJson(response,PrePaymentResponse.class);
            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);
                Log.e("register", "reader");
                status_code = reader.getString("status_code");
                Log.e("register", "status_code " + status_code);
                if (status_code.equalsIgnoreCase("200")) {
                    if(isOnlinePayment){
                        //-------go to citres payment
                        try {
                            JSONObject responseObject=reader.getJSONObject("response");
                            if(responseObject.has("txn_id")){
                                String txn_id=responseObject.getString("txn_id");

                                gotoCitrusPayment(txn_id,pendingPaymentResponse.getTotal_payment(),pre.getResponse().getUserDetails().getEmail(),pre.getResponse().getUserDetails().getPhone());

                            }else {
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text),Toast.LENGTH_SHORT).show();
                        }
                    }else {

                    }



                } else if (status_code.equalsIgnoreCase("406")) {

                    CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });


                } else {
                    CommonUtils.initiatePopupWindow(PendingPayments.this, getResources().getString(R.string.error_msg),
                            true, getResources().getString(R.string.error) + status_code, getResources().getString(R.string.ok), new CommonListners.AlertCallBackWithOk() {
                                @Override
                                public void positiveClick() {

                                }
                            });

                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
        }
    }


    public void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final  Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                Log.d("ResponseInstaPay",response);
                String[] alpha = response.split(":");
                String[] alp = alpha[3].split("=");
                callAfterPayAPI(alp[1]);

            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }
    private void callAfterPayAPI(String response){
        NetworkService networkService =  NetworkService.INSTANCE;
        APIService api = networkService.create();

        Map<String,String> map = new HashMap<>();
        map.put("TxId",$txd_id);
        map.put("TxStatus","SUCCESS");
        map.put("amount",$amount);
        map.put("pgTxnNo","");
        map.put("paymentMode","Online");
        map.put("currency","INR");
        map.put("service_request","complete-payment");
        map.put("processed_paymentid",response);

        Call<PaymentResponse> ab =  api.sendDataPayment(map);
        ab.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, retrofit2.Response<PaymentResponse> response) {
                Log.d("###OnSuccess Print","Response");
                Log.d("Print",response.body().getResponse().getTxnId());
                goToActiveShipments(true);
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                Log.d("###Onfailure Print","Response");
                Log.d("Error",t.toString());
                t.printStackTrace();
                Log.d("Error",t.getMessage());
            }
        });

    }

    public void goToActiveShipments(boolean isFrompandingPayment){
//        if(isFrompandingPayment){
//            Intent BackIntent = new Intent();
//            BackIntent.putExtra("orderId", orderId);
//            BackIntent.putExtra("shipmentId", shipment_id);
//            setResult(101,BackIntent);
//            finish();
//        }else {
            editor1.clear();
            editor1.commit();
            editor.putBoolean("hitActiveShipmentService", true);
            editor.commit();
            Intent i5 = new Intent(PendingPayments.this, MyShipments.class);
            i5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            i5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i5);
//        }

    }
}
