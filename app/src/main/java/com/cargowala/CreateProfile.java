package com.cargowala;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cargowala.ImageLib.PickerBuilder;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by Akash on 2/10/2016.
 */


@RuntimePermissions
public class CreateProfile extends Activity implements View.OnClickListener {

    int deviceWidth, deviceHeight;
    File mediaStorageDir;
    RelativeLayout actionBar;
    public static Uri uri;
    public static int CAMERA_REQUEST = 2, GALLERY_REQUEST = 4, CROP_FROM_CAMERA = 3;
    Dialog dialogBox;

    ImageView select_company, select_individual, select_broker;
    CircleImageView profile_image;
    MyTextView emailIdText;
    MaterialEditText first_name_et, last_name_et;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    MyTextViewSemi logoutText;
    LinearLayout change_picture;
    private String path;
    private String filepath;
    private String base64 = "";
    private String croppath;
    private String mImageFileLocation = "";

    boolean isCamera = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_profile);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;


        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        logoutText = (MyTextViewSemi) findViewById(R.id.logoutText);
        logoutText.setOnClickListener(this);

        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        profile_image.setOnClickListener(this);
        emailIdText = (MyTextView) findViewById(R.id.emailIdText);
        first_name_et = (MaterialEditText) findViewById(R.id.first_name_et);
        last_name_et = (MaterialEditText) findViewById(R.id.last_name_et);

        change_picture = (LinearLayout) findViewById(R.id.change_picture);
        change_picture.setOnClickListener(this);

        if (!pref.getString("userFirstName", "NA").equalsIgnoreCase("NA")) {
            first_name_et.setText(pref.getString("userFirstName", "NA"));
        }
        if (!pref.getString("userLastName", "NA").equalsIgnoreCase("NA")) {
            last_name_et.setText(pref.getString("userLastName", "NA"));
        }
        if (!pref.getString("userEmail", "NA").equalsIgnoreCase("NA")) {
            emailIdText.setText(pref.getString("userEmail", "NA"));
        }
        if (!pref.getString("userImage", "").equalsIgnoreCase("")) {

            Picasso.with(getApplicationContext())
                    .load(pref.getString("userImage", ""))
                    .placeholder(R.drawable.default_user)
                    .error(R.drawable.default_user).into(profile_image);

        } else {
            if (!pref.getString("imagebase64", "NA").equalsIgnoreCase("NA")) {
                byte[] decodedString = Base64.decode(pref.getString("imagebase64", "NA"), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                profile_image.setImageBitmap(decodedByte);
            }
        }


        select_company = (ImageView) findViewById(R.id.select_company);
        select_company.setOnClickListener(this);
        select_individual = (ImageView) findViewById(R.id.select_individual);
        select_individual.setOnClickListener(this);
        select_broker = (ImageView) findViewById(R.id.select_broker);
        select_broker.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.popup_button_yes2:
                dialog1.dismiss();

                boolean isGoogle = false;
                if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook")) {
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    LoginManager.getInstance().logOut();
                } else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google")) {
                    isGoogle = true;
                }



             /*   editor.putBoolean("isLoggedIn", false);
                editor.commit();*/

              /*  ((ActivityManager)getApplicationContext().getSystemService(ACTIVITY_SERVICE))
                        .clearApplicationUserData();*/


              /*  editor.putBoolean("isLoggedIn", false);

                editor.putString("userFirstName",first_name);
                editor.putString("userLastName",last_name);
                editor.putString("userEmail",email);
                editor.putString("userImage", url);
                editor.putString("logged_in_with","");*/


                editor.clear();

                editor.commit();

                Intent i2 = new Intent(CreateProfile.this, Login.class);
                i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                i2.putExtra("wasGoogleLoggedIn", isGoogle);
                startActivity(i2);


                finish();

                break;

            case R.id.popup_button_no2:
                dialog1.dismiss();


                break;


            case R.id.change_picture:

                Log.d("yo", "123");

                //selectImageFromGallery();
                DialogImage();
                break;


            case R.id.profile_image:


                DialogImage();
                break;


            case R.id.select_company:

                String fName1 = first_name_et.getText().toString().trim();
                String lName1 = last_name_et.getText().toString().trim();

                if (fName1.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_first_name), Toast.LENGTH_SHORT)
                            .show();
                } else if (lName1.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_last_name), Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Intent i1 = new Intent(CreateProfile.this, CompanyBusiness.class);

                    i1.putExtra("first_name", fName1);
                    i1.putExtra("last_name", lName1);
                    i1.putExtra("email_id", pref.getString("userEmail", "NA"));

                    i1.putExtra("image_url", pref.getString("userImage", ""));
                    //   i1.putExtra("image_base64",base64);
                    i1.putExtra("account_type", "22");


                    i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i1);
                }
                break;

            case R.id.select_individual:

                String fName = first_name_et.getText().toString().trim();
                String lName = last_name_et.getText().toString().trim();

                if (fName.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_first_name), Toast.LENGTH_SHORT)
                            .show();
                } else if (lName.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_last_name), Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Intent i1 = new Intent(CreateProfile.this, IndividualMobileNumber.class);

                    i1.putExtra("first_name", fName);
                    i1.putExtra("last_name", lName);
                    i1.putExtra("email_id", pref.getString("userEmail", "NA"));

                    i1.putExtra("image_url", pref.getString("userImage", ""));
                    //  i1.putExtra("image_base64",base64);
                    i1.putExtra("account_type", "21");


                    i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i1);
                }


                break;


            case R.id.select_broker:

                String fName2 = first_name_et.getText().toString().trim();
                String lName2 = last_name_et.getText().toString().trim();

                if (fName2.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_first_name), Toast.LENGTH_SHORT)
                            .show();
                } else if (lName2.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.please_enter_last_name), Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Intent i1 = new Intent(CreateProfile.this, AgentAddress.class);


                    Log.d("tt", "fName2 " + fName2);
                    Log.d("tt", "lName2 " + lName2);
                    Log.d("tt", "email_id " + pref.getString("userEmail", "NA"));
                    Log.d("tt", "image_url " + pref.getString("userImage", ""));
                    //       Log.d("tt","base64 "+base64);
                    Log.d("tt", "account_type " + 3);

                    i1.putExtra("first_name", fName2);
                    i1.putExtra("last_name", lName2);
                    i1.putExtra("email_id", pref.getString("userEmail", "NA"));

                    i1.putExtra("image_url", pref.getString("userImage", ""));

                    i1.putExtra("account_type", "3");


                    Log.d("tt", "fName2 " + fName2);
                    Log.d("tt", "lName2 " + lName2);
                    Log.d("tt", "email_id " + pref.getString("userEmail", "NA"));
                    Log.d("tt", "image_url " + pref.getString("userImage", ""));
                    //     Log.d("tt", "base64 " + base64);
                    Log.d("tt", "account_type " + 3);

                    i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i1);
                }


                break;


            case R.id.logoutText:


                initiateyYesNoWindow(getResources().getString(R.string.are_you_sure_you_want_to_log_out),
                        true, getResources().getString(R.string.log_out), getResources().getString(R.string.yes), getResources().getString(R.string.no));


                break;


        }


    }


    Dialog dialog1;

    private void initiateyYesNoWindow(String message, Boolean isAlert, String heading, String buttonTextYes, String buttonTextNo) {
        try {

            dialog1 = new Dialog(CreateProfile.this);
            dialog1.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.yesnopopup);
            dialog1.setCanceledOnTouchOutside(false);
            MyTextView popup_message2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_message2);
            popup_message2.setText(message);


            ImageView popup_image2 = (ImageView) dialog1
                    .findViewById(R.id.popup_image2);
            if (isAlert) {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image2.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_heading2);
            popup_heading2.setText(heading);


            MyTextView popup_button_yes2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_yes2);
            popup_button_yes2.setText(buttonTextYes);
            popup_button_yes2.setOnClickListener(this);

            MyTextView popup_button_no2 = (MyTextView) dialog1
                    .findViewById(R.id.popup_button_no2);
            popup_button_no2.setText(buttonTextNo);
            popup_button_no2.setOnClickListener(this);

            dialog1.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    TextView camera_text, open_gallery_text, cancel_text;

    private void DialogImage() {
        dialogBox = new Dialog(CreateProfile.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogBox.setContentView(R.layout.camera_gallery_popup);

        // Animation animation = AnimationUtils.loadAnimation(context, R.anim.bottom_up_anim);
        dialogBox.show();

        camera_text = (TextView) dialogBox.findViewById(R.id.camera_text);

        // RelativeLayout dialog_rel = (RelativeLayout) dialogBox.findViewById(R.id.dialog_rel);

        open_gallery_text = (TextView) dialogBox.findViewById(R.id.open_gallery_text);
        //remove_photo_text= (TextView) dialogBox.findViewById(R.id.remove_photo_text);
        cancel_text = (TextView) dialogBox.findViewById(R.id.cancel_text);

        cancel_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                dialogBox.dismiss();
            }
        });

        //remove_photo_text.setVisibility(View.GONE);
        camera_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();
                    isCamera = true;
        CreateProfilePermissionsDispatcher.takephotoWithPermissionCheck(CreateProfile.this);


            }
        });

        open_gallery_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();

                isCamera = false;

                //takephotoFromGallery();
            CreateProfilePermissionsDispatcher.openGalleryWithPermissionCheck(CreateProfile.this);


            }
        });

    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    public void openGallery() {
//        mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "CargoWala");
//        File file = new File(mediaStorageDir.getAbsolutePath(),
//                "CargoWala.jpg");
//
//        filepath = file.getAbsolutePath().toString().trim();
    //Crop.pickImage(CreateProfile.this);


        new PickerBuilder(CreateProfile.this, PickerBuilder.SELECT_FROM_GALLERY)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {

                        Picasso.with(CreateProfile.this).load(imageUri).into(profile_image);

                        final InputStream imageStream;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            String encodedImage = encodeImage(selectedImage);

                            Log.d("base%64", "onImageReceived: "+encodedImage);

                            editor.putString("imagebase64", encodedImage);

                            editor.putString("userImage", "");
                            editor.commit();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();



    }

    public void takephotoFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);

        /*Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image*//*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);*/

    }
    @NeedsPermission({Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void takephoto() {
/*
        mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "CargoWala");
        // Create the storage directory(MyCameraVideo) if it
        // does not
        // exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.failed_to_create_directory_cargowala),
                        Toast.LENGTH_LONG).show();
                Log.d("MyCameraVideo",
                        "Failed to create directory CargoWala.");
            }
        }

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(mediaStorageDir.getAbsolutePath(),
                "CargoWala.jpg");



        path = file.getAbsolutePath();
        filepath = path;
        uri = Uri.fromFile(file);

//        File photoFile = null;
//        try {
//            photoFile = createImageFile();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }



        String authorties = getApplicationContext().getPackageName().concat(".provider");
        Uri imageUri = FileProvider.getUriForFile(this,authorties,file);

    //    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, CAMERA_REQUEST);
//        uri = null;
    }


    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();

        return image;
*/

        new PickerBuilder(CreateProfile.this, PickerBuilder.SELECT_FROM_CAMERA)
                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                    @Override
                    public void onImageReceived(Uri imageUri) {

                        Picasso.with(CreateProfile.this).load(imageUri).into(profile_image);

                        final InputStream imageStream;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            String encodedImage = encodeImage(selectedImage);

                            Log.d("base%64", "onImageReceived: "+encodedImage);

                            editor.putString("imagebase64", encodedImage);

                            editor.putString("userImage", "");
                            editor.commit();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();

    }


    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG,75,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_REQUEST
                && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(
                    selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = decodeSampledBitmapFromFile(filepath, 1600, 1000);
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(filepath);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
            }

            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
            ByteArrayOutputStream ful_stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, ful_stream);
            byte[] ful_bytes = ful_stream.toByteArray();
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "Title", null);
            uri = Uri.parse(path);


            try {
                beginCrop(data.getData());
                // doCrop();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // decode
            // byte[] imageAsBytes = Base64.decode(Image.getBytes(),0);
            // user_image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes,
            // 0, imageAsBytes.length));

        } else if ((requestCode == CAMERA_REQUEST)
                && (resultCode == RESULT_OK)) {
            if (resultCode == RESULT_OK) {
                beginCrop(uri);
            }
        } else if ((requestCode == CROP_FROM_CAMERA)) {
            // Bitmap bitmap = decodeSampledBitmapFromFile(croppath, 600, 600);
            Bitmap bitmap = BitmapFactory.decodeFile(croppath);
            //  ByteArrayOutputStream thumb_stream = new ByteArrayOutputStream();
            if (bitmap != null) {
                //  bitmap.compress(Bitmap.CompressFormat.PNG, 100, thumb_stream);
                profile_image.setImageBitmap(bitmap);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);

                base64 = Base64.encodeToString(stream.toByteArray(),
                        Base64.NO_WRAP);

                Log.d("im", "base64 " + base64);

                editor.putString("imagebase64", base64);

                editor.putString("userImage", "");
                editor.commit();


                Log.d("base64", base64);

            }
            // byte[] ful_bytes = thumb_stream.toByteArray();
            // base64 = Base64.encodeBytes(ful_bytes);


            //hit service here aakash nlayk


            try {
                File f = new File(croppath);
                f.delete();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        } else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
//            .setImageURI(Crop.getOutput(result));

            Uri selectedImage = Crop.getOutput(result);
            Bitmap bmp = null, bmp_new = null;
            try {
                bmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(filepath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Log.d("orientation ", "" + orientation);


                Matrix matrix = new Matrix();
                if (orientation == 6 && isCamera) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }

                bmp_new = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            } catch (IOException e) {
                e.printStackTrace();
            }

            // Bitmap bitmap = BitmapFactory.decodeFile(filepath);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp_new.compress(Bitmap.CompressFormat.JPEG, 70, stream);

            profile_image.setImageBitmap(bmp_new);

            base64 = Base64.encodeToString(stream.toByteArray(),
                    Base64.NO_WRAP);
            Log.i("kjbnjhbase64 ", base64);


            Log.d("im", "base64 " + base64);

            editor.putString("imagebase64", base64);

            editor.putString("userImage", "");
            editor.commit();


            /*if (CommonUtils.isNetworkAvailable(context)) {

                updateUserInfoReq();
            } else {
                CommonUtils.showCustomErrorDialog1(context, getResources().getString(R.string.bad_connection));
            }*/

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    //.....................................Crop...................................................

    private void doCrop() throws IOException {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list = getPackageManager()
                .queryIntentActivities(intent, 0);
        int size = list.size();

        if (size == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.can_not_find_image_crop_app),
                    Toast.LENGTH_SHORT).show();
            return;
        } else {
            intent.setData(uri);
            intent.putExtra("crop", true);
            intent.putExtra("outputX", 600);
            intent.putExtra("outputY", 600);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);

            mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    "CargoWala");


            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.failed_to_create_directory_uchallenge),
                            Toast.LENGTH_LONG).show();
                    Log.d("MyCameraVideo",
                            "Failed to create directory uChallenge.");
                }
            }


            File file = new File(mediaStorageDir.getAbsolutePath(),
                    "imagetempone.jpg");
            if (file.exists()) {
                file.delete();
            } else {
                file.createNewFile();
            }

            croppath = file.getAbsolutePath();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            intent.putExtra("output", Uri.fromFile(file));
            // --------------------------------------------------------------------
            // -----------> When changing this to false it worked
            // <----------------
            // --------------------------------------------------------------------
            intent.putExtra("return-data", false);

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);
                i.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));
                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager()
                            .getApplicationLabel(
                                    res.activityInfo.applicationInfo);
                    co.icon = getPackageManager()
                            .getApplicationIcon(
                                    res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);
                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));
                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(
                        getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        getApplicationContext());
                builder.setTitle(getResources().getString(R.string.choose_crop_app));
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                startActivityForResult(
                                        cropOptions.get(item).appIntent,
                                        CROP_FROM_CAMERA);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        try {
                            if (uri != null) {
                                getContentResolver().delete(uri,
                                        null, null);
                                uri = null;
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    public Bitmap decodeSampledBitmapFromFile(String path, int reqWidth,
                                              int reqHeight) { // BEST QUALITY MATCH
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }

        int expectedWidth = width / inSampleSize;
        if (expectedWidth > reqWidth) {
            // if(Math.round((float)width / (float)reqWidth) > inSampleSize) //
            // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }


    boolean exitApp = false;

    @Override
    public void onBackPressed() {
        if (exitApp) {
            //super.onBackPressed();
            moveTaskToBack(true);
            //return;
        }

        Toast.makeText(this, getResources().getString(R.string.press_again_to_exit), Toast.LENGTH_SHORT).show();
        this.exitApp = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                exitApp = false;
            }
        }, 2000);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }


    @SuppressLint("NeedOnRequestPermissionsResult")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method

        CreateProfilePermissionsDispatcher.onRequestPermissionsResult(this,requestCode,grantResults);
    }

}
