package com.cargowala;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Akash on 2/17/2016.
 */
public class MyTextViewSemi extends TextView {

    public MyTextViewSemi(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewSemi(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewSemi(Context context) {
        super(context);
        init();
    }

    public void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/avenirnextltpro_semi.otf");
        setTypeface(tf/* ,1*/);

    }
}