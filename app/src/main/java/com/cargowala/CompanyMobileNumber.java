package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class CompanyMobileNumber extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;


    RelativeLayout actionBar;
    ImageView backbutton,company_progress;
    MyTextView next;
    SweetAlertDialog pDialog;
    String company_type,company_business,comp_buss_name,comp_landlin_no,
            register_address,register_city,register_state,register_pin,
            office_address,office_city,office_state,office_pin,first_name,last_name,email_id,image_url,account_type ;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    MaterialEditText phone_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_mob_no);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();


        Intent intent = getIntent();

        first_name = intent.getExtras().getString("first_name");
        last_name = intent.getExtras().getString("last_name");
        email_id = intent.getExtras().getString("email_id");
        image_url = intent.getExtras().getString("image_url");

        account_type = intent.getExtras().getString("account_type");

        company_type = intent.getExtras().getString("company_type");
        company_business = intent.getExtras().getString("company_business");
        comp_buss_name = intent.getExtras().getString("comp_buss_name");
        comp_landlin_no = intent.getExtras().getString("comp_landlin_no");
        Log.d("mmm","comp_landlin_no "+comp_landlin_no);
        register_address = intent.getExtras().getString("register_address");
        register_city = intent.getExtras().getString("register_city");
        register_state = intent.getExtras().getString("register_state");
        register_pin = intent.getExtras().getString("register_pin");
        office_address = intent.getExtras().getString("office_address");
        office_city = intent.getExtras().getString("office_city");
        office_state = intent.getExtras().getString("office_state");
        office_pin = intent.getExtras().getString("office_pin");

        phone_num = (MaterialEditText) findViewById(R.id.phone_num);

        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);
        backbutton.requestLayout();
        backbutton.setOnClickListener(this);

        company_progress = (ImageView) findViewById(R.id.company_progress);
        company_progress.getLayoutParams().width =  (deviceWidth /2);
        company_progress.requestLayout();

        next = (MyTextView) findViewById(R.id.next);
        next.setOnClickListener(this);

    }
    String phone_number;
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.backbutton:
                finish();


                break;


            case R.id.popup_button:

                if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }



                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(CompanyMobileNumber.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i2);


                    finish();

                }
                else
                {
                    dialog.dismiss();
                }


                break;


            case R.id.next:
               /* Intent i = new Intent(CompanyMobileNumber.this, CompanyOtp.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);*/

                 phone_number = phone_num.getText().toString();

                if(phone_number.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_phone_number), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(phone_number.length()<10)
                    {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_enter_valid_number), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {

                      /*  Intent i = new Intent(CompanyMobileNumber.this, CompanyOtp.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                        i.putExtra("first_name", first_name);
                        i.putExtra("last_name", last_name);
                        i.putExtra("email_id", email_id);
                        i.putExtra("image_url", image_url);
                        i.putExtra("image_base64", image_base64);
                        i.putExtra("account_type", account_type);

                        i.putExtra("company_business", company_business);
                        i.putExtra("company_buss_name", comp_buss_name);
                        i.putExtra("company_landline_no", comp_landlin_no);
                        i.putExtra("company_type", company_type);

                        i.putExtra("register_address", register_address);
                        i.putExtra("register_city", register_city);
                        i.putExtra("register_state", register_state);
                        i.putExtra("register_pin", register_pin);

                        i.putExtra("office_address", register_address);
                        i.putExtra("office_city", register_city);
                        i.putExtra("office_state", register_state);
                        i.putExtra("office_pin", register_pin);
                        i.putExtra("mobile_no", phone_number);

                        startActivity(i);*/


                        if(isOnline())
                        {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makeMobileOtpReq();
                        }
                        else {
                            Toast.makeText(CompanyMobileNumber.this,
                                    getResources().getString(R.string.check_your_network),
                                    Toast.LENGTH_SHORT).show();
                        }



                    }
                }


                break;

        }


    }





    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }








    String res;
    StringRequest strReq;

    public String makeMobileOtpReq() {

        String url = getResources().getString(R.string.base_url)+"user/get-otp";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkMobileOtpResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                    Log.d("_id ",   pref.getString("userId", "invalid_user_id"));
                    Log.d("mobile_no ", phone_number);


                    params.put("_id", pref.getString("userId", "invalid_user_id"));
                    params.put("mobile_no", phone_number);
                    params.put("security_token", pref.getString("security_token","Something Wrong"));


                    Log.e("register", params.toString());



                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "15");



        return null;
    }


    String status_code = "0";

    public void checkMobileOtpResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {



                 /*   initiatePopupWindow("Your email has been registered. Please check your mail and click the link to confirm your mail and login.",
                            false, "Email Registerd", "OK");*/


                    Intent i = new Intent(CompanyMobileNumber.this, CompanyOtp.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                    i.putExtra("first_name", first_name);
                    i.putExtra("last_name", last_name);
                    i.putExtra("email_id", email_id);
                    i.putExtra("image_url", image_url);

                    i.putExtra("account_type", account_type);

                    i.putExtra("company_business", company_business);
                    i.putExtra("comp_buss_name", comp_buss_name);
                    i.putExtra("comp_landlin_no", comp_landlin_no);
                    i.putExtra("company_type", company_type);

                    i.putExtra("register_address", register_address);
                    i.putExtra("register_city", register_city);
                    i.putExtra("register_state", register_state);
                    i.putExtra("register_pin", register_pin);



                    i.putExtra("office_address", office_address);
                    i.putExtra("office_city", office_city);
                    i.putExtra("office_state", office_state);
                    i.putExtra("office_pin", office_pin);

                    i.putExtra("mobile_no", phone_number);

                    startActivity(i);
                }
                else if(status_code.equalsIgnoreCase("409"))
                {



                    initiatePopupWindow(getResources().getString(R.string.this_mobile_number_has_already_been_registered),
                            true, getResources().getString(R.string.error),getResources().getString(R.string.ok));

                }
                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error),getResources().getString(R.string.ok));


                }
                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(CompanyMobileNumber.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }












    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }

}
