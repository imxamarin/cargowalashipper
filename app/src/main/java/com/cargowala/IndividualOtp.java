package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.DefaultRetryPolicy;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class IndividualOtp extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton;

    MaterialEditText edittext_email;
    MyTextView confirm,resend_otp;
    SweetAlertDialog pDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String first_name,last_name,email_id,image_url,account_type,phone_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.individual_otp);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;



        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        Intent intent = getIntent();

        first_name = intent.getExtras().getString("first_name");
        last_name = intent.getExtras().getString("last_name");
        email_id = intent.getExtras().getString("email_id");
        image_url = intent.getExtras().getString("image_url");

        account_type = intent.getExtras().getString("account_type");
        phone_number = intent.getExtras().getString("phone_number");



        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);

        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();
        backbutton.setOnClickListener(this);

        edittext_email = (MaterialEditText) findViewById(R.id.edittext_email);

        confirm = (MyTextView) findViewById(R.id.confirm);

        confirm.setOnClickListener(this);


        resend_otp = (MyTextView) findViewById(R.id.resend_otp);
        resend_otp.setOnClickListener(this);
        resend_otp.setText(getResources().getString(R.string.resend_otp_on)+" "+phone_number);


    }

    String otp;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.backbutton:

                finish();

                break;

            case R.id.resend_otp:
                if(isOnline())
                {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText(getResources().getString(R.string.loading));
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeMobileOtpReq();
                }
                else {
                    Toast.makeText(IndividualOtp.this,
                            getResources().getString(R.string.check_your_network),
                            Toast.LENGTH_SHORT).show();
                }



                break;


            case R.id.confirm:
               /* Intent i = new Intent(IndividualOtp.this, Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);*/


                //aakash

           //     confirmOtp();

                otp = edittext_email.getText().toString().trim();

                if(otp.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.please_enter_otp), Toast.LENGTH_LONG).show();
                }

                else if(otp.length()!=6)
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.otp_should_be_of_6_characters), Toast.LENGTH_LONG).show();
                }
                else if(isOnline())
                {
                    pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                    pDialog.setTitleText(getResources().getString(R.string.loading));
                    pDialog.setCancelable(false);
                    pDialog.show();
                    makeVerifyOtpReq();
                }
                else {
                    Toast.makeText(IndividualOtp.this,
                            getResources().getString(R.string.check_your_network),
                            Toast.LENGTH_SHORT).show();
                }


                break;

            case R.id.popup_button:


                if(status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910"))
                {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook"))
                    {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    }

                    else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google"))
                    {
                        isGoogle = true;
                    }





                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(IndividualOtp.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn",isGoogle);
                    startActivity(i2);


                    finish();

                }
                else
                {
                    dialog.dismiss();
                }


                break;


        }


    }


    public void confirmOtp()
    {
        if(isOnline()) {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
        pDialog.setTitleText(getResources().getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.show();
        makeCreateProfileReq();
        }
        else
        {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.check_your_network),
                    Toast.LENGTH_SHORT).show();
        }

    }






    String res;
    StringRequest strReq;
    public String makeCreateProfileReq() {
        String url = getResources().getString(R.string.base_url)+"user/create-profile";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkCreateProfileResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    Log.d("_id ",  pref.getString("userId", "invalid_user_id"));
                    Log.d("firstname ",  first_name);
                    Log.d("lastname ",  last_name);
                    Log.d("mobile_no ",  phone_number);
                    Log.d("image_url ",  image_url);
                    Log.d("account_type ", account_type);
                    params.put("_id", pref.getString("userId", "invalid_user_id"));
                    params.put("firstname", first_name);
                    params.put("lastname", last_name);
                    params.put("mobile_no", phone_number);

                //    params.put("image_base64",   "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAAEACAYAAACqHnrXAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDQ3QzYzNzhENTQwMTFFNUJDRTA4M0UwNjFCRDlBRkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDQ3QzYzNzlENTQwMTFFNUJDRTA4M0UwNjFCRDlBRkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowNDdDNjM3NkQ1NDAxMUU1QkNFMDgzRTA2MUJEOUFGQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowNDdDNjM3N0Q1NDAxMUU1QkNFMDgzRTA2MUJEOUFGQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuZC+AoAABViSURBVHja7J0LlF1Vece/uRkhLxJuIIBkFYcbIiArELiBGkgh6p0mgYIFmeHRBVLUGRE0dNk60yoBKwsn0q4aQWxGxNcqygy2NUrBzq0LMYKlc+URBV3CkFpUSgg3YEBtCNPvy9lncXO557XPc5/z/631rclj7v3O3vv7n7Mf396na3p6mqJgZf9GAlp0sb2R7Ui2ClsP26FsB7EdrGyeshnK5qnP7mLbzSaNuJPtJbYdyrazPcv2FNs2ZU+zvYIqD86WsXWxfG83qjZRFrCdwrZM2THK9tf8vrlt3+2FiG+KbSvbT9gayn6JpkkHCDBeFrP9kbIVSmxdKbf3m5W9q+Xff8H2fbb75WavBDqN5oMATWM22yq2tWxr2I4y5LqPYPszZcIzbPcom2B7Hk0LAWaVOWxns/Ur4c3MQZkOY7tM2R62e9nuYPtnNb4EEVFCFWghEyFnsY2TNdnxNbZzcyK+TmV9B9toy5NRnpSzEAZ4AibNm9jew3Y526KCxstqZdItvV0JcytCA0/AOJEnwLfImkG8pqDia0dmXa9ie5TtPrZzEE8QYNRdrwvZHmKrs/0J6ssRmeX9JttP2a5A9xQCDFsnF7D9WI3tlqFKfLOE7Ra2J9TTcT9UCQToF1mfO4/tYbavk7VmB/Q4nO0mJcQBzDVAgF6cqMYx32BbiuqIjD9g28T2CFnrogAC3AfJwfwC2yTbSoRDbLyF7W5lx6I6IMCSmix4nKwlBfQEkmGN6uJ/gvTzXyFAw5Gx3ffUZMF8hEDiyMTMx1S3dCUEWKyyDqs7MLqb6XO0GnffTFYOLQSYYyTZ+Ltsn0TXJ1PIzPOVbP9FBV3uKYIAL1TdnTMQ75lFJml+yPaXlO52LQgwQt6gujeymH4gYjzzSM/kRrIyag6EAM1GcjXvVd0bYBaytetBKsh6bB4FKBMscszCqYhlY5GUtgfISgmEAA0b78kO7kMRw8YzRw0f/hoCNANpKNmfNhOxmxtkQuYGts+rMT0EmEFk29DnVEN1IWZzyXvZNlMO1wtNF6Bk2X+F7f2I0dwjaWzfodfORIUAU0ZSmu5kuxixWRhkgu0/yDq0GAJMWXzSJXknYrJwLCcrq2khBJhet1OOyFuNWCwsx5O1tWk+BJgsMuHyZbY/RQwWnirbXWT4xIxpArwJYz7QwmlqKLIfBBg/spXoCsQcaEOOjLyNDF2CMkWAchLzDYg1kLf4MEGAp5t8hwOJ9pAugQCjRU7VupNwviTwx61sfwgBRoPkdP4L5WS9BySC3KjlaMlDIcDwyIxnFTEFAiJ7QeVg5RkQoD6y1PBexBLQZBXbeghQjx62zyKGQEg+Sgacfpc1AUq34auEM1xAdLE0HwL0zzDhzE5QoN5UlgQoR9Ndg5gBESOL9OdCgN7XIccO4NBcEAe3ZHVYkxUBfoBwihmIj8PYRiDAzsjLHJHnCeLmfWxvhQBfj4jvAMQHSCDWb6KMTTymfTEns12K2AAJIcdZXAYBWsjuhs8QdjmAZLmebS4ESHR+FvvkIPfIa8mvLroAxe91iAWQEh+mjCxLpCXAi8haeAcgDQ5UIiykAOVYwWsRAyBl1lEGDvhNQ4Ay9luC9gcpc0AWxoJpCPAjaHuQEeSdIrOKJMBVbCei3UFGOJjt3WleQHfC/obQ5tGzcMFcWnFSDy1fegS9aVGZDj9kPs2aGfx1eiv7Nxax+mQyZpTt1bwLsEJ4n0OkHLfkMHr3eafQW1l8pS7kM2hylIrLu/MuQDnjBVESAQfOm0V/cfkqesepb0ZlRBebuRag+PlztHN4lh27iK7/8Fl7RQgi42yytiw9k7TjUsIFBCF424ol9On150F80SMD5svScJyUAC9BG4djxYk9dO2H1lD3jBIqIx4uzasA5Z3ea9G++sjM5sevXgvxxcuxbEvzKMBzyDpmHmgwg0W3/oNraPYsvB4jAfrzKMAL0K4hIuLMZXR05RBURDJckDcBysbHXrSrZuXN2Z8uOfdkVERyLEm6Gxq3AGuEowa1Ofvtx9G8uei9J8yZeRLgH6M99Tlv9QmohORZnScBnoX21EPGfW88ZB4qInnk1QiJvU8izkyYY9iOQHvqcepJR2p/9tfbX6Rb73iAnt2xCxUZHFmUP4Nts+kCXIW21Gfp0Ydrfe6F3/yOrlw/DvGF4/SkBFiKuRBAk6N6Dtb63O2bJyG+8JyWhzHgCrSjHjP376YF82drffYHk0+hAsNzEiWUPBKXACXxugftqEd53mztzz638yVUYHgk7egUkwW4DG2oz+zZSDvLACeaLMClaL8QjYLd7VngeJMFeDzaDxjOCSYLECkcwHTk5PYZJgpQLvoYtB8wHDl2oMdEAS4iK5sAANOpmCjAHrQbyAk9ECAAORZgNwQYPzdfd36wwcdM/R78jcPn0J4901qfveq6O9FYCXdB4xAgjh9sY9lbFiXmSzeJG3Qk9rNA4uiCHox2AznhIBMFuADtBnLCQhMFeBDaDeAJiCcgAGGRA8XmmCbAuWg3kCO6TRMg9tKAPDHbNAHiCQjyxBtMEyAAECAECED2gQABcGd3nF8exwzPLowD9+Xhx34Z6PclF1T3jUhbf/Yr7VxQkA8B/h/abF+CJjkv6VlIX/zUxVq+/mpkM+166feo9Oh42bQuKE6FBXniFdME+DzaDOQE6Uq8BAECkA474nZQMvGiAUiI7SYK8Dm0G8ATMD0BPoN2AznhWRMFuA3tBnLCUxAgABAgBAgKyTYTBSh5V7vRdiAHTJkowD1sP0XbAcP5ralPQOFRtB8wnMfUw8RIAT6C9gOGk8hDJC4BbkX7AcN5xGQBPoz2A4bzkMkCfIawHAHMRfa0PmiyAIUH0I7AUH7E9jvTBXgf2hEYyv1JOYpTgPeiHYGhfC8PApTF+F+gLYFh7M6LAIW70J7AMLawvZAXAf472hMYRqIxG7cA62QdbAOAKdyVJwHKEYUTaFNgCD+nhLO4kjiafgztCgzhjqQdJiHAb6IbCgxhLI8CfJHtbrQtyDiPUwqbCJJ6O9JX0b4g43wlDadJCXAz4bhCkF1k8f1LeRbgK2kVEAAffCutB0SSL+j8PBteXAeyyBfScpykAOWEKWTGgKzxBNs9RRCgMIL2BhnjH9heLYoA76WEtvoD4AN5kdAX07yAUgo+/w7tDjLCP5J1/mehBDim+t0ApMlv2DamfRHdKfiUJYmPExbnHfn5tu20sn8jKiJePkMZeJdlKSW/t5N18jAAabCT7e+zcCFpCfBV9RQEIA1EfM0iC1AYZ/shYgEkjGS8fDorF5OmACUr5mpCdgxIlo+RtVG88AIU/pNSykIHhWSSUl73y5oAhY9m6Y4EcovMO3yQUsx6yaoA5Y26f4P4ADEji+6Zm3MoZeQ6Pkt4lwSIj6fZhrN4YVkRoHQL3kfWW2kAiJoryMp8gQBd+Anb9YgVEDH/xPbtrF5cKWPXcwNZR4MDEAXb2K7K8gVmTYB72C6hBM/mB7nFjqWdEGDwu9YHED8gJJ80oTdVyuh1SbL2rYghoIm8Xuw6Ey60lOFrk0XTHyGWQEB+xXah6oJCgCGQd3Sfy7YdMQV8IstY7yKDzqAtZfz65A27fYT1QeCPATJsh03JgGuU/vzlhF0TwB05ce/Lpl10yZDrlMVU5IuC3MVHyaBrlTvc5xBroI3vmtxDKhl2vTIz+jXEHFDcz3a2yXMEpglQppYvZftXxF7habCdyfayyYUoGXjNcqzhBWzfQQwWlkfZ1lIOUhZLhl63dDnOIev116BYyLESb6ecrA+XDL52EeH5GBMWCsntrLHtyEuBSoZfv3RHJeN9E2Iz98ir7VZTznbKlHJQBpmYeT9ZhzthsT6fyMl5Mtv5ct4KVspRWWQz78Vsv0e85ga5of4t22WU03TEUs7K83U1RvhfxK7xyNPuIrZr89yzKeWwTDJQrxJOWTMZeX3dCrY78l7QUk7LJWeNrmK7BbFsHHKA0slkrfURBGguMma4UnVjdiKujWivj5C1vluY9ioVoIwyLjyB7fuI8czyuOpy3kgFm8kuFaScsrH3bWRtWcHm3uwgYpNT0ZdTQY8fKRWorLJeKCdlLSOcPZoFfsZ2Blnndr5c1EooFbDMj6uGl/Hhi9BB4uwm6wT0ZRgWFFOAgryLQmZIj2G7jTL2yqocc48S3jVkHbpFEGCx+TXbe9QYBK/Ljo/HyNo+tFb9GUCA+/AQ26lk7a74MaojMv6HrDzdE9TTD0CAjsiM3DdUsFykJgmAfs/iQ2xLyNqp8gqqBAIMMj6UtcPjyErufhhV4htJIZP3eixmu4mQGA8BhkCWLWSz70lsvWSlSGGypjMymymnmB9N1sl1v0WVQIBRdk3rZO1Hkzv7J8h6/0DRabLdzHY82+lkHZSFGxQEGCvb2NazHaEEeWfB7vYylpPDsOQUgkVkHRO5FWGhTzeqQLt7+m1lc5UY5aQ2OTJhZg7Leh9ZW4Nkkuo5ND8EmCV2qbGi2GyytkHJetcatqMMLZO8XegeZRNsz6OZIUATkJzGf1NGSoAyPjqNrGx/ybzpyuB1S7L6D8g6afo+1a3E+ToQoPE8oew29feFZO3WX6ZMBHks234JjuGmyEo2EJMllgfJ2sAMIMDcs72la2cjE2GHs/WwHal+Hsp2ENsh6ucBbGX1+yLWOerPMgFk51S+oLrDO9Q4TXw9y/bfSnTb2J4mLIpDgGAfXlXCEMM2qYLRNT2Nrj4AECAAECAAAAIEAAIEAECAAECAAAAIEAAIEAAAAQIAAQIAIEAAIEAAAAQIAAQIAIAAAYAAAQAQIAAQIAAAAgQAAgQAQIAAQIAAQIAAAAgQAAgQAAABAgABAgAgQAAgQAAABAgABAgAgAABgAABABAgABAgAAACBCAHAlzZvxG1lB0qytppsDVRPa9ny9i6UJ/n+A9V517+u9FERtHHNtLh33vZ6qge8+q8hPoFID26+RFb458TMXz3MNsGVDEAeAICAAECACBAACBAAID7MoSscwyH+O4pVC8A+gJsbhlbV8dCPADoggIAAQIAIEAACjEGTAJJcq22/Czb40+yJoFkIqdO0Scai6+a8leNyWcSPtzqtY/2Td62fdfVzzh81lra0maqpbyJ+13Zv7G+ZWxdAwLclwEVIDWX3+lr+fMoWWltUxH4HaLO2e1R+RxSfioJlqs1GEfafHTyPaX8jkbgs6bKXPPxu6n4ZRHu9ctCHE04xjc5TGLKzbBXbgxJd0Glsp6UC/PZYK2FmXQJLD9Po0nltxLA55PUORPerWwjAX1Mqp9RNLjfOqqoupho6XUERT43pr6jFuAGsUldZzVpvyyGSbZqAnFeVeV0FV/SY8AhVWmVkBVf0wjMiRANPuRSme0+Kprl2hRShH3qO4KKqaYpwmoEN8QJjTJH4pdFOBCz+Cb8iM+rC1pWOyV0aGr09+stAekmFgm05T7HTzUf4vFTlrqP4HcrV73DeKVTuaZIb4/ZQMiAkRtbb4AbRpgnZ/uNx2+ZI/Ur3VJZ545YfG7X+DrxeT0BbSXrWKduW6e+v1RAP1uXCoBeJa4FZGXhNF0mGPw+Md2E0a98if/FyudUe6WxjXsEETmUd7H6/AZlgy1+nG4uYZlSfuxytZat6TGe8sOYiwha29O25R5jvjGfoorcL4uwnKb4ku6CkgqMRktgOwV3UwVsr4+JDCdGXBpsuMV3s22CwG64hvpzQ8PHYAcxt4uz3+HmEuZpNtpy/c0Okx9u5fEjQLeJs2GH9myo+uh1uAGUffhOy29Q8Tn13AadZmLTWAe0hefn8d+gzpt6az4qZMClwTZ4dDnthpvS8OF3lm/coQ50u/11dd1Nj6ejW0B6iX9Is05bn1JO31uOw6/qZjr6jegpOOIhPqceVCoCDDo+rLt0kSngE3KK/O/S9xpj9rk8hcKUTXeCYTBAuTa4dEWdqDjUeZA6rbvUT19cfpUIg/r1i9sE2qDX0kd3xBMprU+usDNJZQ+hlT0mX8KKw88ET6dyT0VQT7WAkzGjAf2OOjx1ajHfcOwn/4BDmUcz5NcPQ27i8/O9rtuRWL29Me6GKNNr2Rp2Nkwlou+uxHRj8PIh5ZiOqG6Cdj91eiG1Dn7LLl3UKHzXA7ZZJH5ddvboxlxfWPGl1QWtqRmt59Xj285mqETooxpRsOj4iPMGEmWPI2jXvhqz76z59WJAo5udqgBbMxj6CEQJNj8nz6jLA8b3THaSuaBe2Sh2t6j9xOGwU/NFoAIRJs64y5PQd4JBUgIcchHfBo9JhJqGABsO/oJObgT1EfYYD90nms5EQi1g167u8BmdOq0F6BKn5dcPwyoGOsWanV3UyIoAO+G10K0zIWEHcNWhvx+VADv5qFI672kIOo4pOwRj0+Xa3bJogtRpX8CbTiR+V/ZvDOrXD/aacacMmL1DLva7fMvYumaaY8Cqg4hGfQ6kdQbJDZeBc1TpR42AARYnAwHraSDAJIXX//UFrNM+jSdvGn6DxMGgy9BgIu1JmHKIO4+f7IwgA+Qox5NO2Q0jAcRgpzBFIVq/uyGq5LzFqu7xxK871OlIAOH3OTxJxuPyq3Y/BPUbNBackgKq7H9TmgJsBhyDtAanzhYb2+eoi0CGfATKpIfvKQcfXnmB+3RR6LVlmbB5iXbyfMVjDDQRsDzt43Wn+vKTzzni8r3NOPyqrqejX7fuocZ40OkGNsDXMZSWAJ3GRPZWoYpDxYZ9Mrhl/4+0fH+5RRB96t83tQR02SMwnBat7Q3AtQ5CkcZ4su3/Rij8bojWjcd2ArM9iTXmUR4/aV11jye/vbm40lanY+S8m8GP8NPyG5R+t5jrtA8xqUmYDQ53oQFl9rkhXnsBdQbIYy5Pg5qPgJ4g5wTmKSX0TS536CBd3r4AY+P2spbbuu1B/AZZPB4k5/zMasCbSNMjaGP1G+HTr/V7e9UNwUmEjdadEUktxHsFlb1RtRpgPOe3bz4Y8tq9Gnc0Ah+tNwydSYEw/hvkvFvALcgaEQVrIw2/MR7U5LYUtXd4ot66m6gA7cqrawTWeATiX0760811HwE+GuBO7jT+6g1RVvtG09T4XK/G5+z23BCiTpeT3pM+tN8ETklz25JmL0+UkxRga+V53eXtCZTFEfbRG+r7BgMI0WtDZ6dg7rSr3kt4w5rB6HSjGffptz/kTaNJzpth/dTpVNJ+ZXMBW1IZQ8Mubbq3VyUi7Jqedk/ej3k3hL0LoqwaxOv8lahoPdai1naHtK8hbEPZ54K27/Kwx7uNGMva6azMuM/nLLf5LMdQp1p+w4gubPyzb9f//38BBgBk4Cp0uGDguwAAAABJRU5ErkJggg==");
                    params.put("account_type", account_type);
                    params.put("security_token", pref.getString("security_token","Something Wrong"));
                    params.put("business[office_city]", "");
                    params.put("company_type[c_type]", "");
                    params.put("business_type[b_type]", "");


                    params.put("image_url", image_url);
                    params.put("image_base64",   pref.getString("imagebase64", ""));

                  //  params.put("security_token", );




                    Log.e("register", params.toString());



                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


//        strReq.setRetryPolicy(new DefaultRetryPolicy(
//                5000000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        int x=2;// retry count
        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 48,
                x, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, "100");



        return null;
    }

    String status_code = "0";

    public void checkCreateProfileResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {

                    String response1 = reader.getString("response");
                    JSONObject jsonobj = new JSONObject(response1);
                    String id = jsonobj.getString("_id");
                    Log.d("userId ",id);
                    Log.d("firstname ",jsonobj.getString("firstname"));

                   /* if(jsonobj.has("firstname")){*/
                    String firstname = jsonobj.getString("firstname");
                    /*}*/


                    String lastname = jsonobj.getString("lastname");
                    String email = jsonobj.getString("email");
                    String mobile_no = jsonobj.getString("mobile_no");
                    //String image = jsonobj.getString("image");



                    Log.d("userId ",id);
                    Log.d("firstname ", firstname);
                    Log.d("lastname ", lastname);
                    Log.d("email ", email);
                    Log.d("mobile_no ", mobile_no);
                   // Log.d("image ", image);


                    editor.putBoolean("isLoggedIn", true);


                    editor.putString("userId", id);

                    editor.putString("user_firstname", firstname);
                    editor.putString("user_lastname", lastname);
                    editor.putString("user_email", email);
                    editor.putString("user_mobile_no", mobile_no);

                    editor.putString("user_image_url", image_url);
                    editor.putString("user_image_base64", pref.getString("imagebase64", ""));
                    editor.putString("user_account_type", account_type);


                   // editor.putString("user_image", image);


                    editor.putBoolean("isProfileCreated", true);

                    editor.commit();


                    Intent i1 = new Intent(IndividualOtp.this, Home.class);
                    i1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i1);
                    finish();

                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

             /*   else if(status_code.equalsIgnoreCase("204"))
                {

                    initiatePopupWindow("This email id is not registerd with us.",
                            true, "Error", "OK");

                }*/
                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(IndividualOtp.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }



    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }












    String res1;
    StringRequest strReq1;

    public String makeVerifyOtpReq() {

        String url = getResources().getString(R.string.base_url)+"user/verify-otp";
        Log.d("regg", "2");
        strReq1 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res1 = response.toString();
                        checkVerifyOtpResponse(res1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res1 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    Log.d("_id ",   pref.getString("userId", "invalid_user_id"));
                    Log.d("otp ",otp );
                    params.put("security_token", pref.getString("security_token", "Something Wrong"));
                    params.put("_id",   pref.getString("userId", "invalid_user_id"));
                    params.put("otp",otp );

                    Log.e("register", params.toString());



                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };

        AppController.getInstance().addToRequestQueue(strReq1, "26");

        return null;
    }

    public void checkVerifyOtpResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register", response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {



                 /*   initiatePopupWindow("Your email has been registered. Please check your mail and click the link to confirm your mail and login.",
                            false, "Email Registerd", "OK");*/


               confirmOtp();

                }
                else if(status_code.equalsIgnoreCase("204") || status_code.equalsIgnoreCase("400"))
                {

                    initiatePopupWindow(getResources().getString(R.string.incorrect_otp),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }


                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true,getResources().getString(R.string.error)+ " " + status_code,getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }



    String res2;
    StringRequest strReq2;

    public String makeMobileOtpReq() {

        String url = getResources().getString(R.string.base_url)+"user/get-otp";
        Log.d("regg", "2");
        strReq2 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res2 = response.toString();
                        checkMobileOtpResponse(res2);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res2 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {


                    Log.d("_id ",   pref.getString("userId", "invalid_user_id"));
                    Log.d("mobile_no ", phone_number);


                    params.put("_id", pref.getString("userId", "invalid_user_id"));
                    params.put("mobile_no", phone_number);
                    params.put("security_token", pref.getString("security_token","Something Wrong"));

                    Log.e("register", params.toString());



                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq2, "27");



        return null;
    }

    public void checkMobileOtpResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                 status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {


                    initiatePopupWindow(getResources().getString(R.string.an_otp_has_successfully_been_sent)+phone_number,
                            false, getResources().getString(R.string.otp_sent), getResources().getString(R.string.ok));


              /*      Intent i = new Intent(IndividualOtp.this, IndividualOtp.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i.putExtra("first_name", first_name);
                    i.putExtra("last_name", last_name);
                    i.putExtra("email_id", email_id);
                    i.putExtra("image_url", image_url);
                    i.putExtra("image_base64", image_base64);

                    i.putExtra("account_type", account_type);
                    i.putExtra("phone_number", phone_number);


                    startActivity(i);*/

                }
                else if(status_code.equalsIgnoreCase("409"))
                {



                    initiatePopupWindow(getResources().getString(R.string.this_mobile_number_has_already_been_registered),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }

                else if(status_code.equalsIgnoreCase("406"))
                {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }

                else if(status_code.equalsIgnoreCase("910"))
                {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true,getResources().getString(R.string.error), getResources().getString(R.string.ok));


                }


                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true,getResources().getString(R.string.error)+ " " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }









}
