package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

/**
 * Created by Akash on 2/10/2016.
 */
public class MyShipments extends Activity implements View.OnClickListener {

    private static final String TAG = MyShipments.class.getSimpleName();
    int deviceWidth, deviceHeight;


    RelativeLayout actionBar;
    ImageView backbutton;

    SweetAlertDialog pDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences pref1;
    SharedPreferences.Editor editor1;

    ImageView active_image, history_image;
    LinearLayout active_bar, history_bar, active_bar_main, history_bar_main;

    private List<String> orderIdArray = new ArrayList<String>();
    private List<String> orderIdHistoryArray = new ArrayList<String>();
    int record_no1 = 0;
    boolean scroll1 = true;
    boolean headernotcreated1 = true;
    MyTextView loadingText;
    boolean doPagination1 = true;

    int record_no2 = 0;
    boolean scroll2 = true;
    boolean headernotcreated2 = true;
    boolean doPagination2 = true;


    ImageView no_connection_image1, no_connection_image2, blank_active_shipment, blank_history_shipment;

    ExampleScrollView complete_scroll_bar, complete_scroll_bar2;

    boolean createHistory = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_shipment);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        pref1 = getApplicationContext().getSharedPreferences("PendingShipment", MODE_PRIVATE);
        editor1 = pref1.edit();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height = (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height = (deviceHeight / 20);
        backbutton.getLayoutParams().width = (deviceHeight / 20);


        backbutton.requestLayout();

        active_image = (ImageView) findViewById(R.id.active_image);
        active_image.setOnClickListener(this);
        history_image = (ImageView) findViewById(R.id.history_image);
        history_image.setOnClickListener(this);


        active_bar_main = (LinearLayout) findViewById(R.id.active_bar_main);
        history_bar_main = (LinearLayout) findViewById(R.id.history_bar_main);


        active_bar = (LinearLayout) findViewById(R.id.active_bar);
        history_bar = (LinearLayout) findViewById(R.id.history_bar);
        no_connection_image1 = (ImageView) findViewById(R.id.no_connection_image1);
        no_connection_image2 = (ImageView) findViewById(R.id.no_connection_image2);
        blank_active_shipment = (ImageView) findViewById(R.id.blank_active_shipment);
        blank_history_shipment = (ImageView) findViewById(R.id.blank_history_shipment);


        complete_scroll_bar = (ExampleScrollView) findViewById(R.id.complete_scroll_bar);


        complete_scroll_bar.setOnScrollViewListener(new ExampleScrollView.OnScrollViewListener() {
            public void onScrollChanged(ExampleScrollView v, int l, int t,
                                        int oldl, int oldt) {

                View view = (View) v.getChildAt(v.getChildCount() - 1);
                int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));

                if (diff == 0) {

                    if (scroll1) {

                        if (doPagination1) {

                            scroll1 = false;


                            if (isOnline()) {
                          /*  pDialog = new SweetAlertDialog(BidBoard.this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText("Loading");
                            pDialog.setCancelable(false);
                            pDialog.show();*/

                                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                                loadingText = new MyTextView(
                                        MyShipments.this);

                                params4.setMargins(0, 10, 0, 20);

                                loadingText.setLayoutParams(params4);
                                loadingText.setText(getResources().getString(R.string.loading_dots));
                                loadingText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                                loadingText.setTypeface(null, Typeface.BOLD);

                                loadingText.setGravity(Gravity.CENTER);
                                active_bar_main.addView(loadingText);

                                complete_scroll_bar.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        complete_scroll_bar.fullScroll(ExampleScrollView.FOCUS_DOWN);
                                    }
                                });


                                makeActiveShipmentReq();
                            } else {
                                Toast.makeText(MyShipments.this,
                                        getResources().getString(R.string.check_your_network),
                                        Toast.LENGTH_SHORT).show();

                                complete_scroll_bar.scrollBy(0, -20);
                                scroll1 = true;
                            }

                        }
                    }

                }
            }
        });


        complete_scroll_bar2 = (ExampleScrollView) findViewById(R.id.complete_scroll_bar2);


        complete_scroll_bar2.setOnScrollViewListener(new ExampleScrollView.OnScrollViewListener() {
            public void onScrollChanged(ExampleScrollView v, int l, int t,
                                        int oldl, int oldt) {

                View view = (View) v.getChildAt(v.getChildCount() - 1);
                int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));

                if (diff == 0) {

                    if (scroll2) {

                        if (doPagination2) {

                            scroll2 = false;


                            if (isOnline()) {
                          /*  pDialog = new SweetAlertDialog(BidBoard.this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText("Loading");
                            pDialog.setCancelable(false);
                            pDialog.show();*/

                                LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                                loadingText = new MyTextView(
                                        MyShipments.this);

                                params4.setMargins(0, 10, 0, 20);

                                loadingText.setLayoutParams(params4);
                                loadingText.setText(getResources().getString(R.string.loading_dots));
                                loadingText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                                loadingText.setTypeface(null, Typeface.BOLD);

                                loadingText.setGravity(Gravity.CENTER);
                                history_bar_main.addView(loadingText);

                                complete_scroll_bar2.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        complete_scroll_bar2.fullScroll(ExampleScrollView.FOCUS_DOWN);
                                    }
                                });


                                makeHistoryShipmentReq();
                            } else {
                                Toast.makeText(MyShipments.this,
                                        getResources().getString(R.string.check_your_network),
                                        Toast.LENGTH_SHORT).show();

                                complete_scroll_bar2.scrollBy(0, -20);
                                scroll2 = true;
                            }

                        }
                    }

                }
            }
        });






/*


        for(int i = 0; i<7; i++)
        {

            CardView card_view = new CardView(
                    getApplicationContext());
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                   */
/* (int)(deviceHeight/1.5)*//*

                    );
          //  card_view.setRadius(10f);
            //left top right bottom
            params1.setMargins(20, 20, 20, 20);


            card_view.setLayoutParams(params1);


            orderIdArray.add("abc " + i);



            LinearLayout ll1 = new LinearLayout(
                    MyShipments.this);
            ll1.setOrientation(LinearLayout.VERTICAL);

            ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.MATCH_PARENT);

            ll1.setId(i + 100);
            ll1.setOnClickListener(MyShipments.this);
            ll1.setLayoutParams(params2);

            card_view.addView(ll1);



            RelativeLayout map_layout= new RelativeLayout(MyShipments.this);
            LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,*/
/* ViewGroup.LayoutParams.WRAP_CONTENT*//*
(int)(deviceWidth / 1.1));

            map_layout.setLayoutParams(params5);



            ImageView map_image = new ImageView(MyShipments.this);

            LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT*/
/*(int)(deviceHeight / 1.8)*//*
);
            //params3.weight = 0.82f;
            map_image.setLayoutParams(params3);



              int width  = (int)(deviceWidth/1.1)*2;
              int height = (int)(deviceWidth/1.1)*2;

            String url = "";
            url = url+"https://maps.googleapis.com/maps/api/staticmap?size="+width+"x"+height+"&markers=shadow:true|";
            url = url+pref.getString("book_origin_lat","NA")+","+pref.getString("book_origin_long","NA");
            url = url+ "&markers=shadow:true|";
            url = url+pref.getString("book_dest_lat","NA")+","+pref.getString("book_dest_long","NA");
            url = url+ "&path=weight:3%7Ccolor:blue%7Cenc:";
            url = url+ pref.getString("book_encoded_polyline","NA");
            url = url+ "&key=AIzaSyAvYeajGEsqSHc41kbSEh8hqb3URlt4J4E";


          //  map_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.splashbg));


            Picasso.with(getApplicationContext())
                    .load(url)
				*/
/*.placeholder(R.drawable.loading_screenshot)
                .error(R.drawable.no_image_screenshot)*//*

				*/
/*.resize(deviceWidth, deviceHeight).centerInside()*//*
.into(map_image);


            map_image.setScaleType(ImageView.ScaleType.FIT_XY);



            map_layout.addView(map_image);


            MyTextView status_text = new MyTextView(MyShipments.this);

            RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT , LinearLayout.LayoutParams.WRAP_CONTENT);

            params6.addRule(
                    RelativeLayout.ALIGN_PARENT_RIGHT,
                    RelativeLayout.TRUE);

            status_text.setPadding(10,5,10,5);
            status_text.setLayoutParams(params6);
            status_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            status_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            status_text.setText("Confirmed");


            map_layout.addView(status_text);
            ll1.addView(map_layout);



            MyTextView order_id = new MyTextView(
                    getApplicationContext());
            order_id.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

            order_id.setGravity(Gravity.CENTER);
            // heading.setTypeface(null, Typeface.BOLD);

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

           // params4.weight = 0.06f;
            //params4.gravity = Gravity.CENTER;

            order_id.setPadding(5,4,5,2);
            order_id.setLayoutParams(params4);
            order_id.setText("Order id : #4H7RJ5" );

            ll1.addView(order_id);






            MyTextView route_text = new MyTextView(
                    getApplicationContext());
            route_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

            route_text.setGravity(Gravity.CENTER);
           // heading.setTypeface(null, Typeface.BOLD);

         */
/*   LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                   0);
            params4.weight = 0.10f;*//*

            //params4.gravity = Gravity.CENTER;

            route_text.setLayoutParams(params4);
            route_text.setText("Chandigarh to Delhi");
            route_text.setPadding(5, 2, 5,2);
            ll1.addView(route_text);


            MyTextView schedule_text = new MyTextView(
                    getApplicationContext());
            schedule_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

            schedule_text.setGravity(Gravity.CENTER);
            // heading.setTypeface(null, Typeface.BOLD);

         */
/*   LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    0);
            params4.weight = 0.10f;*//*

            //params4.gravity = Gravity.CENTER;

            schedule_text.setLayoutParams(params4);
            schedule_text.setText("24 March 2016 at 10:00 AM");
            schedule_text.setPadding(5, 2, 5,4);
            ll1.addView(schedule_text);


*/
/*

            RelativeLayout rl1 = new RelativeLayout(
                    MyShipments.this);

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,0);
            params4.weight = 0.20f;
            rl1.setLayoutParams(params4);

            ll1.addView(rl1);




            ImageView delete_image = new ImageView(MyShipments.this);

            RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT , LinearLayout.LayoutParams.MATCH_PARENT);

            params5.addRule(
                    RelativeLayout.ALIGN_PARENT_RIGHT,
                    RelativeLayout.TRUE);

            delete_image.setLayoutParams(params5);
            delete_image.setId(i + 1000);
            delete_image.setOnClickListener(MyShipments.this);
            delete_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.delete_orange_icon));
            delete_image.setAdjustViewBounds(true);
            delete_image.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            rl1.addView(delete_image);



*//*



            active_bar_main.addView(card_view);




        }






*/




/*


        for(int i = 0; i<3; i++)
        {

            CardView card_view = new CardView(
                    getApplicationContext());
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                   */
/* (int)(deviceHeight/1.5)*//*

            );
            //  card_view.setRadius(10f);
            //left top right bottom
            params1.setMargins(20, 20, 20, 20);


            card_view.setLayoutParams(params1);


            orderIdHistoryArray.add("abc " + i);



            LinearLayout ll1 = new LinearLayout(
                    MyShipments.this);
            ll1.setOrientation(LinearLayout.VERTICAL);

            ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.MATCH_PARENT);

            ll1.setId(i + 1000);
            ll1.setOnClickListener(MyShipments.this);
            ll1.setLayoutParams(params2);

            card_view.addView(ll1);



            RelativeLayout map_layout= new RelativeLayout(MyShipments.this);
            LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,*/
/* ViewGroup.LayoutParams.WRAP_CONTENT*//*
(int)(deviceHeight / 1.8));

            map_layout.setLayoutParams(params5);



            ImageView map_image = new ImageView(MyShipments.this);

            LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT*/
/*(int)(deviceHeight / 1.8)*//*
);
            //params3.weight = 0.82f;
            map_image.setLayoutParams(params3);



            int width  = (int)(deviceWidth/1.1)*2;
            int height = (int)(deviceHeight/1.8)*2;

            String url = "";
            url = url+"https://maps.googleapis.com/maps/api/staticmap?size="+width+"x"+height+"&markers=shadow:true|";
            url = url+pref.getString("book_origin_lat","NA")+","+pref.getString("book_origin_long","NA");
            url = url+ "&markers=shadow:true|";
            url = url+pref.getString("book_dest_lat","NA")+","+pref.getString("book_dest_long","NA");
            url = url+ "&path=weight:3%7Ccolor:blue%7Cenc:";
            url = url+ pref.getString("book_encoded_polyline","NA");
            url = url+ "&key=AIzaSyAvYeajGEsqSHc41kbSEh8hqb3URlt4J4E";


            //  map_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.splashbg));


            Picasso.with(getApplicationContext())
                    .load(url)
				*/
/*.placeholder(R.drawable.loading_screenshot)
                .error(R.drawable.no_image_screenshot)*//*

				*/
/*.resize(deviceWidth, deviceHeight).centerInside()*//*
.into(map_image);


            map_image.setScaleType(ImageView.ScaleType.FIT_XY);



            map_layout.addView(map_image);


            MyTextView status_text = new MyTextView(MyShipments.this);

            RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT , LinearLayout.LayoutParams.WRAP_CONTENT);

            params6.addRule(
                    RelativeLayout.ALIGN_PARENT_RIGHT,
                    RelativeLayout.TRUE);

            status_text.setPadding(10, 5, 10, 5);
            status_text.setLayoutParams(params6);
            status_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            status_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
            status_text.setText("Confirmed");


           // map_layout.addView(status_text);
            ll1.addView(map_layout);



            MyTextView order_id = new MyTextView(
                    getApplicationContext());
            order_id.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

            order_id.setGravity(Gravity.CENTER);

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            order_id.setPadding(5,4,5,2);
            order_id.setLayoutParams(params4);

            String text1 = "<font color=#585858  >Order id : </font> <font color=#ff5400><b>ISUDNV75TN43</b></font>";
            order_id.setText(Html.fromHtml(text1));

          //  order_id.setText("Order id : #4H7RJ5" );

            ll1.addView(order_id);






            MyTextView route_text = new MyTextView(
                    getApplicationContext());
            route_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_line_color));

            route_text.setGravity(Gravity.CENTER);

            route_text.setLayoutParams(params4);
            route_text.setText("Chandigarh to Delhi");
            route_text.setPadding(5, 2, 5,2);
            ll1.addView(route_text);


            MyTextView schedule_text = new MyTextView(
                    getApplicationContext());
            schedule_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

            schedule_text.setGravity(Gravity.CENTER);


            schedule_text.setLayoutParams(params4);

            String text2 = "<font color=#ff5400  >24 March 2016</font> <font color=#585858><b>at</b></font> " +
                    "<font color=#ff5400  >10:00 AM</font>";
            schedule_text.setText(Html.fromHtml(text2));

           // schedule_text.setText("24 March 2016 at 10:00 AM");
            schedule_text.setPadding(5, 2, 5,4);
            ll1.addView(schedule_text);





            history_bar_main.addView(card_view);





        }




*/


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.backbutton:
                Intent home = new Intent(MyShipments.this, Home.class);
                home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(home);
                finish();

                break;


            case R.id.active_image:

                active_bar.setVisibility(View.VISIBLE);
                history_bar.setVisibility(View.GONE);
                active_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.my_ship_active));
                history_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.my_ship_history_inactive));

                break;


            case R.id.history_image:

                active_bar.setVisibility(View.GONE);
                history_bar.setVisibility(View.VISIBLE);
                active_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.my_ship_inactive));
                history_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.my_ship_history_active));


                if (createHistory) {
                    no_connection_image2.setVisibility(View.GONE);
                    blank_history_shipment.setVisibility(View.GONE);
                    complete_scroll_bar2.setVisibility(View.GONE);

                    orderIdHistoryArray.clear();


                    record_no2 = 0;
                    scroll2 = true;
                    headernotcreated2 = true;
                    doPagination2 = true;

                    history_bar_main.removeAllViews();


                    if (isOnline()) {
                        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        pDialog.setTitleText(getResources().getString(R.string.loading));
                        pDialog.setCancelable(false);
                        pDialog.show();
                        makeHistoryShipmentReq();
                    } else {
                        //    no_connection_image.setVisibility(View.VISIBLE);
                        no_connection_image2.setVisibility(View.VISIBLE);

                        Toast.makeText(MyShipments.this,
                                getResources().getString(R.string.check_your_network),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {

                }


                break;


            case R.id.popup_button:


                if (status_code.equalsIgnoreCase("406") || status_code.equalsIgnoreCase("910")) {

                    boolean isGoogle = false;
                    if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("facebook")) {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                    } else if (pref.getString("logged_in_with", "skip").equalsIgnoreCase("google")) {
                        isGoogle = true;
                    }


                    editor1.clear();

                    editor1.commit();


                    editor.clear();

                    editor.commit();

                    Intent i2 = new Intent(MyShipments.this, Login.class);
                    //i2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i2.putExtra("wasGoogleLoggedIn", isGoogle);
                    startActivity(i2);


                    finish();

                } else {
                    dialog.dismiss();
                }


                break;


        }


        for (int a = 0; a < orderIdArray.size(); a++) {
            if (v.getId() == (100 + a)) {

                editor.putBoolean("timeChanged", false);
                editor.commit();

                Intent i = new Intent(MyShipments.this, ActiveShipment.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("orderId", orderIdArray.get(a));
                startActivity(i);


            }

        }


        for (int a = 0; a < orderIdHistoryArray.size(); a++) {
            if (v.getId() == (100000 + a)) {


                Intent i = new Intent(MyShipments.this, HistoryShipment.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("orderId", orderIdHistoryArray.get(a));
                startActivity(i);


            }

        }


        for (int a = 0; a < orderIdHistoryArray.size(); a++) {
            if (v.getId() == (200000 + a)) {


                Intent i = new Intent(MyShipments.this, RepeatOrder.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                i.putExtra("orderId", orderIdHistoryArray.get(a));
                startActivity(i);


            }

        }


    }


    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    ///////////  active shipment  //////////////


    String res;
    StringRequest strReq;

    public String makeActiveShipmentReq() {

        String url = getResources().getString(R.string.base_url) + "user/view-shipments";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkActiveShipmentResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                /*  InstanceID instanceID = InstanceID.getInstance(ChangePassword.this);
                    Log.d("token", "2");
                    String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.d("regg", "6");

                    Log.d("register ", email_id);
                    Log.d("register ",pass);
                    Log.d("token ",token);

                    params.put("username", email_id);
                    params.put("password", pass);
                    params.put("mobile_tokens", token);

                    Log.e("register", params.toString());*/


                    params.put("security_token", pref.getString("security_token", "Something Wrong"));

                    params.put("_id", pref.getString("userId", "Something Wrong"));

                    params.put("starts_from", "" + record_no1);

                    params.put("limit", "" + (record_no1 + 3));
                    params.put("type", "active");

                    Log.e("register", params.toString());


                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "35");


        return null;
    }

    String status_code = "0";

    public void checkActiveShipmentResponse(String response) {


        if (headernotcreated1) {
            pDialog.dismiss();
        }


        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);


                if (status_code.equalsIgnoreCase("200")) {

                    String response1 = reader.getString("response");

                    JSONArray jsonArr1 = new JSONArray(response1);

                    if (headernotcreated1) {

                        headernotcreated1 = false;

                        if (jsonArr1.length() == 0) {

                            blank_active_shipment.setVisibility(View.VISIBLE);

                            //     ll_layout.setVisibility(View.GONE);

                        } else {


                            //   blank_no_driver_image.setVisibility(View.GONE);
                            complete_scroll_bar.setVisibility(View.VISIBLE);
                            active_bar_main.removeAllViews();
                        }

                    } else {
                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll1 = true;
                    }


                    record_no1 = record_no1 + 3;


                    for (int i = 0; i < jsonArr1.length(); i++) {

                        if (jsonArr1.length() % 3 == 0) {
                            doPagination1 = true;
                        } else {

                            doPagination1 = false;
                        }


                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);


                        String id = jsonObjectPackageData1.getString("id");
                        String shipment_id = jsonObjectPackageData1.getString("shipment_id");


                        String from_city = jsonObjectPackageData1.getString("from_city");
                        String to_city = jsonObjectPackageData1.getString("to_city");

                        String shipment_url = jsonObjectPackageData1.getString("shipment_url");

                        String transit_date = jsonObjectPackageData1.getString("transit_date");
                        String transit_time = jsonObjectPackageData1.getString("transit_time");

                        String shipment_status = jsonObjectPackageData1.getString("shipment_status");


                        blank_active_shipment.setVisibility(View.GONE);
                        complete_scroll_bar.setVisibility(View.VISIBLE);

                        // orderIdBigArray.add(id);

                        orderIdArray.add(jsonObjectPackageData1.toString());

                     /*   loadTypeArray.add(load_type_text);
                        fromCityArray.add(from_address);
                        toCityArray.add(to_address);
                        priorityDeliveryArray.add(priority_delivery);
                        transitDateArray.add(transit_date);
                        truckNameArray.add(truck_name_text);
                        transitTimeArray.add(transit_time);
                        truckQuantityArray.add(truck_quantity);
                        bidsQuantityArray.add(bids_quantity);
                        truckCategoryArray.add(truck_category);*/


                        CardView card_view = new CardView(
                                getApplicationContext());
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                   /* (int)(deviceHeight/1.5)*/
                        );
                        //  card_view.setRadius(10f);
                        //left top right bottom
                        params1.setMargins(20, 20, 20, 20);


                        card_view.setLayoutParams(params1);


                        LinearLayout ll1 = new LinearLayout(
                                MyShipments.this);
                        ll1.setOrientation(LinearLayout.VERTICAL);

                        ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                        ll1.setId(orderIdArray.size() + 99);
                        ll1.setOnClickListener(MyShipments.this);
                        ll1.setLayoutParams(params2);

                        card_view.addView(ll1);


                        RelativeLayout map_layout = new RelativeLayout(MyShipments.this);
                        LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,/* ViewGroup.LayoutParams.WRAP_CONTENT*/(int) (deviceWidth / 1.1));

                        map_layout.setLayoutParams(params5);


                        ImageView map_image = new ImageView(MyShipments.this);

                        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT/*(int)(deviceHeight / 1.8)*/);
                        //params3.weight = 0.82f;
                        map_image.setLayoutParams(params3);



                       /* int width  = (int)(deviceWidth/1.1)*2;
                        int height = (int)(deviceWidth/1.1)*2;

                        String url = "";
                        url = url+"https://maps.googleapis.com/maps/api/staticmap?size="+width+"x"+height+"&markers=shadow:true|";
                        url = url+pref.getString("book_origin_lat","NA")+","+pref.getString("book_origin_long","NA");
                        url = url+ "&markers=shadow:true|";
                        url = url+pref.getString("book_dest_lat","NA")+","+pref.getString("book_dest_long","NA");
                        url = url+ "&path=weight:3%7Ccolor:blue%7Cenc:";
                        url = url+ pref.getString("book_encoded_polyline","NA");
                        url = url+ "&key=AIzaSyAvYeajGEsqSHc41kbSEh8hqb3URlt4J4E";*/


                        //  map_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.splashbg));


                        /*Picasso.with(getApplicationContext())
                                .load(shipment_url)
                                .placeholder(R.drawable.loading_screen_square)
                                .error(R.drawable.unable_upload_square)
                                .into(map_image);*/
                        Glide.with(MyShipments.this)
                                .load(shipment_url)
                                .centerCrop()
                                .placeholder(R.drawable.loading_screen_square)
                                .error(R.drawable.unable_upload_square)
                                .into(map_image);


                        map_image.setScaleType(ImageView.ScaleType.FIT_XY);


                        map_layout.addView(map_image);


                        MyTextView status_text = new MyTextView(MyShipments.this);

                        RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        params6.addRule(
                                RelativeLayout.ALIGN_PARENT_RIGHT,
                                RelativeLayout.TRUE);

                        status_text.setPadding(10, 5, 10, 5);
                        status_text.setLayoutParams(params6);
                        status_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        status_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        status_text.setText(shipment_status);


                        map_layout.addView(status_text);
                        ll1.addView(map_layout);


                        MyTextView order_id = new MyTextView(
                                getApplicationContext());
                        order_id.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                        order_id.setGravity(Gravity.CENTER);
                        // heading.setTypeface(null, Typeface.BOLD);

                        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        // params4.weight = 0.06f;
                        //params4.gravity = Gravity.CENTER;

                        order_id.setPadding(5, 4, 5, 2);
                        order_id.setLayoutParams(params4);

                        String text1 = "<font color=#ffffff  >"+getResources().getString(R.string.order_id)+"</font><font color=#ffffff  ><b>" + shipment_id + "</b></font>";
                        order_id.setText(Html.fromHtml(text1));

                        ll1.addView(order_id);


                        MyTextView route_text = new MyTextView(
                                getApplicationContext());
                        route_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                        route_text.setGravity(Gravity.CENTER);
                        // heading.setTypeface(null, Typeface.BOLD);

         /*   LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                   0);
            params4.weight = 0.10f;*/
                        //params4.gravity = Gravity.CENTER;

                        route_text.setLayoutParams(params4);

                        String text2 = "<font color=#ffffff  ><b>" + from_city + "</b></font><font color=#ffffff  > to </font> <font color=#ffffff  ><b>" + to_city + "</b></font>";

                        route_text.setText(Html.fromHtml(text2));
                        route_text.setPadding(5, 2, 5, 2);
                        ll1.addView(route_text);


                        MyTextView schedule_text = new MyTextView(
                                getApplicationContext());
                        schedule_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                        schedule_text.setGravity(Gravity.CENTER);
                        // heading.setTypeface(null, Typeface.BOLD);

         /*   LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    0);
            params4.weight = 0.10f;*/
                        //params4.gravity = Gravity.CENTER;

                        schedule_text.setLayoutParams(params4);


                        String text3 = "<font color=#ffffff  ><b>" + transit_date + "</b></font><font color=#ffffff  > at </font> <font color=#ffffff  ><b>" + transit_time + "</b></font>";

                        schedule_text.setText(Html.fromHtml(text3));
                        schedule_text.setPadding(5, 2, 5, 4);
                        ll1.addView(schedule_text);


/*

            RelativeLayout rl1 = new RelativeLayout(
                    MyShipments.this);

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,0);
            params4.weight = 0.20f;
            rl1.setLayoutParams(params4);

            ll1.addView(rl1);




            ImageView delete_image = new ImageView(MyShipments.this);

            RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT , LinearLayout.LayoutParams.MATCH_PARENT);

            params5.addRule(
                    RelativeLayout.ALIGN_PARENT_RIGHT,
                    RelativeLayout.TRUE);

            delete_image.setLayoutParams(params5);
            delete_image.setId(i + 1000);
            delete_image.setOnClickListener(MyShipments.this);
            delete_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.delete_orange_icon));
            delete_image.setAdjustViewBounds(true);
            delete_image.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

            rl1.addView(delete_image);



*/


                        active_bar_main.addView(card_view);


                    }


                 /*   initiatePopupWindow("Your password has been changed.",
                            false, "Success", "OK");*/


                } else if (status_code.equalsIgnoreCase("204")) {


                    //no more records

                    doPagination1 = false;
                    if (record_no1 > 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_more_active_shipments), Toast.LENGTH_SHORT).show();

                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll1 = true;

                    } else {


                        no_connection_image1.setVisibility(View.GONE);
                        blank_active_shipment.setVisibility(View.VISIBLE);
                        complete_scroll_bar.setVisibility(View.GONE);

                    }

                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
            Log.d("error ", e.getLocalizedMessage() + " " + e.getMessage());

        }
    }


    ////////////////////  history shipment


    String res2;
    StringRequest strReq2;

    public String makeHistoryShipmentReq() {

        String url = getResources().getString(R.string.base_url) + "user/view-shipments";
        Log.d("regg", "2");
        strReq2 = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res2 = response.toString();
                        checkHistoryShipmentResponse(res2);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 " + error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res2 = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {
                /*  InstanceID instanceID = InstanceID.getInstance(ChangePassword.this);
                    Log.d("token", "2");
                    String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.d("regg", "6");

                    Log.d("register ", email_id);
                    Log.d("register ",pass);
                    Log.d("token ",token);

                    params.put("username", email_id);
                    params.put("password", pass);
                    params.put("mobile_tokens", token);

                    Log.e("register", params.toString());*/


                    params.put("security_token", pref.getString("security_token", "Something Wrong"));

                    params.put("_id", pref.getString("userId", "Something Wrong"));

                    params.put("starts_from", "" + record_no2);

                    params.put("limit", "" + (record_no2 + 3));
                    params.put("type", "history");

                    Log.e("register", params.toString());


                } catch (Exception i) {
                    Log.d("regg", "7 " + i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq2, "36");
        return null;
    }


    public void checkHistoryShipmentResponse(String response) {


        if (headernotcreated2) {
            pDialog.dismiss();
        }


        try {
            Log.e("register", response);

            if (response != null) {

                Log.e("register", "a");

                JSONObject reader = new JSONObject(response);

                Log.e("register", "reader");

                status_code = reader.getString("status_code");

                Log.e("register", "status_code " + status_code);

                createHistory = false;

                if (status_code.equalsIgnoreCase("200")) {

                    String response1 = reader.getString("response");

                    JSONArray jsonArr1 = new JSONArray(response1);

                    if (headernotcreated2) {

                        headernotcreated2 = false;

                        if (jsonArr1.length() == 0) {

                            blank_history_shipment.setVisibility(View.VISIBLE);

                            //     ll_layout.setVisibility(View.GONE);

                        } else {


                            //   blank_no_driver_image.setVisibility(View.GONE);
                            complete_scroll_bar2.setVisibility(View.VISIBLE);
                            history_bar_main.removeAllViews();
                        }

                    } else {
                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll2 = true;
                    }


                    record_no2 = record_no2 + 3;


                    for (int i = 0; i < jsonArr1.length(); i++) {

                        if (jsonArr1.length() % 3 == 0) {
                            doPagination2 = true;
                        } else {

                            doPagination2 = false;
                        }


                        JSONObject jsonObjectPackageData1 = jsonArr1.getJSONObject(i);


                        String id = jsonObjectPackageData1.getString("id");
                        String shipment_id = jsonObjectPackageData1.getString("shipment_id");


                        String from_city = jsonObjectPackageData1.getString("from_city");
                        String to_city = jsonObjectPackageData1.getString("to_city");

                        String shipment_url = jsonObjectPackageData1.getString("shipment_url");

                        String transit_date = jsonObjectPackageData1.getString("transit_date");
                        String transit_time = jsonObjectPackageData1.getString("transit_time");

                        String shipment_status = jsonObjectPackageData1.getString("shipment_status");


                        blank_history_shipment.setVisibility(View.GONE);
                        complete_scroll_bar2.setVisibility(View.VISIBLE);

                        // orderIdBigArray.add(id);

                        orderIdHistoryArray.add(jsonObjectPackageData1.toString());

                     /*   loadTypeArray.add(load_type_text);
                        fromCityArray.add(from_address);
                        toCityArray.add(to_address);
                        priorityDeliveryArray.add(priority_delivery);
                        transitDateArray.add(transit_date);
                        truckNameArray.add(truck_name_text);
                        transitTimeArray.add(transit_time);
                        truckQuantityArray.add(truck_quantity);
                        bidsQuantityArray.add(bids_quantity);
                        truckCategoryArray.add(truck_category);*/


                        CardView card_view = new CardView(
                                getApplicationContext());
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                   /* (int)(deviceHeight/1.5)*/
                        );
                        //  card_view.setRadius(10f);
                        //left top right bottom
                        params1.setMargins(20, 20, 20, 20);


                        card_view.setLayoutParams(params1);


                        LinearLayout ll1 = new LinearLayout(
                                MyShipments.this);
                        ll1.setOrientation(LinearLayout.VERTICAL);

                        ll1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.light_grey));

                        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


                        ll1.setLayoutParams(params2);

                        card_view.addView(ll1);


                        RelativeLayout map_layout = new RelativeLayout(MyShipments.this);
                        LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,/* ViewGroup.LayoutParams.WRAP_CONTENT*/(int) (deviceWidth / 1.1));

                        map_layout.setLayoutParams(params5);


                        map_layout.setId(orderIdHistoryArray.size() + 100000 - 1);
                        map_layout.setOnClickListener(MyShipments.this);

                        ImageView map_image = new ImageView(MyShipments.this);

                        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT/*(int)(deviceHeight / 1.8)*/);
                        //params3.weight = 0.82f;
                        map_image.setLayoutParams(params3);



                       /* int width  = (int)(deviceWidth/1.1)*2;
                        int height = (int)(deviceWidth/1.1)*2;

                        String url = "";
                        url = url+"https://maps.googleapis.com/maps/api/staticmap?size="+width+"x"+height+"&markers=shadow:true|";
                        url = url+pref.getString("book_origin_lat","NA")+","+pref.getString("book_origin_long","NA");
                        url = url+ "&markers=shadow:true|";
                        url = url+pref.getString("book_dest_lat","NA")+","+pref.getString("book_dest_long","NA");
                        url = url+ "&path=weight:3%7Ccolor:blue%7Cenc:";
                        url = url+ pref.getString("book_encoded_polyline","NA");
                        url = url+ "&key=AIzaSyAvYeajGEsqSHc41kbSEh8hqb3URlt4J4E";*/


                        //  map_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.splashbg));


                      /*  Picasso.with(getApplicationContext())
                                .load(shipment_url)
                                .placeholder(R.drawable.loading_screen_square)
                                .error(R.drawable.unable_upload_square)
                *//*.resize(deviceWidth, deviceHeight).centerInside()*//*.into(map_image);*/

                        Glide.with(MyShipments.this)
                                .load(shipment_url)
                                .centerCrop()
                                .placeholder(R.drawable.loading_screen_square)
                                .error(R.drawable.unable_upload_square)
                                .into(map_image);


                        map_image.setScaleType(ImageView.ScaleType.FIT_XY);
                        map_layout.addView(map_image);

                        MyTextView status_text = new MyTextView(MyShipments.this);

                        RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        params6.addRule(
                                RelativeLayout.ALIGN_PARENT_RIGHT,
                                RelativeLayout.TRUE);

                        status_text.setPadding(10, 5, 10, 5);
                        status_text.setLayoutParams(params6);
                        status_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        status_text.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                        status_text.setText(shipment_status);


                        map_layout.addView(status_text);
                        ll1.addView(map_layout);


                        MyTextView order_id = new MyTextView(
                                getApplicationContext());
                        order_id.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                        order_id.setGravity(Gravity.CENTER);
                        // heading.setTypeface(null, Typeface.BOLD);

                        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        // params4.weight = 0.06f;
                        //params4.gravity = Gravity.CENTER;

                        order_id.setPadding(5, 4, 5, 2);
                        order_id.setLayoutParams(params4);

                        String text1 = "<font color=#585858  "+getResources().getString(R.string.order_id)+"</font><font color=#ff5400  ><b>" + shipment_id + "</b></font>";
                        order_id.setText(Html.fromHtml(text1));

                        ll1.addView(order_id);


                        MyTextView route_text = new MyTextView(
                                getApplicationContext());
                        route_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                        route_text.setGravity(Gravity.CENTER);
                        // heading.setTypeface(null, Typeface.BOLD);

         /*   LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                   0);
            params4.weight = 0.10f;*/
                        //params4.gravity = Gravity.CENTER;

                        route_text.setLayoutParams(params4);

                        String text2 = "<font color=#ff5400  ><b>" + from_city + "</b></font><font color=#585858  > to </font> <font color=#ff5400  ><b>" + to_city + "</b></font>";

                        route_text.setText(Html.fromHtml(text2));
                        route_text.setPadding(5, 2, 5, 2);
                        ll1.addView(route_text);


                        MyTextView schedule_text = new MyTextView(
                                getApplicationContext());
                        schedule_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                        schedule_text.setGravity(Gravity.CENTER);
                        // heading.setTypeface(null, Typeface.BOLD);


                        schedule_text.setLayoutParams(params4);


                        String text3 = "<font color=#ff5400  ><b>" + transit_date + "</b></font><font color=#585858  > at </font> <font color=#ff5400  ><b>" + transit_time + "</b></font>";

                        schedule_text.setText(Html.fromHtml(text3));
                        schedule_text.setPadding(5, 2, 5, 4);
                        ll1.addView(schedule_text);


                        MyTextView repeat_order = new MyTextView(
                                getApplicationContext());
                        repeat_order.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        repeat_order.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));

                        repeat_order.setGravity(Gravity.CENTER);
                        // heading.setTypeface(null, Typeface.BOLD);

                        repeat_order.setId(orderIdHistoryArray.size() + 200000 - 1);
                        repeat_order.setOnClickListener(MyShipments.this);


                        repeat_order.setLayoutParams(params4);


                        repeat_order.setText(getResources().getString(R.string.repeat_this_order));

                        // left top right bottom
                        repeat_order.setPadding(5, 20, 5, 20);
                        ll1.addView(repeat_order);


                        history_bar_main.addView(card_view);


                    }


                 /*   initiatePopupWindow("Your password has been changed.",
                            false, "Success", "OK");*/


                } else if (status_code.equalsIgnoreCase("204")) {


                    //no more records

                    doPagination2 = false;
                    if (record_no2 > 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_more_history_shipments), Toast.LENGTH_SHORT).show();


                        ((ViewGroup) loadingText.getParent()).removeView(loadingText);
                        scroll2 = true;

                    } else {


                        no_connection_image2.setVisibility(View.GONE);
                        blank_history_shipment.setVisibility(View.VISIBLE);
                        complete_scroll_bar2.setVisibility(View.GONE);

                    }

                } else if (status_code.equalsIgnoreCase("406")) {


                    //password changed
                    initiatePopupWindow(getResources().getString(R.string.pass_changed_error),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));


                } else if (status_code.equalsIgnoreCase("910")) {

                    //user_inactive
                    initiatePopupWindow(getResources().getString(R.string.inactive_user_error),
                            true, getResources().getString(R.string.error),getResources().getString(R.string.ok));


                } else {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
            Log.d("error ", e.getLocalizedMessage() + " " + e.getMessage());

        }
    }


    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText) {
        try {

            dialog = new Dialog(MyShipments.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if (isAlert) {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            } else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (pref.getBoolean("hitActiveShipmentService",false)) {

            editor.putBoolean("hitActiveShipmentService", false);
            editor.commit();



          /*  no_connection_image.setVisibility(View.GONE);
            blank_no_driver_image.setVisibility(View.GONE);
            ll_layout.setVisibility(View.GONE);*/

            no_connection_image1.setVisibility(View.GONE);
            blank_active_shipment.setVisibility(View.GONE);
            complete_scroll_bar.setVisibility(View.GONE);

            orderIdArray.clear();




            record_no1 = 0;
            scroll1 = true;
            headernotcreated1 = true;
            doPagination1 = true;

            active_bar_main.removeAllViews();




            //////////


            no_connection_image2.setVisibility(View.GONE);
            blank_history_shipment.setVisibility(View.GONE);
            complete_scroll_bar2.setVisibility(View.GONE);

            orderIdHistoryArray.clear();

            record_no2 = 0;
            scroll2 = true;
            headernotcreated2 = true;
            doPagination2 = true;

            history_bar_main.removeAllViews();

            createHistory = true;

            ///////////





            if(isOnline())
            {
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                pDialog.setTitleText(getResources().getString(R.string.loading));
                pDialog.setCancelable(false);
                pDialog.show();
                makeActiveShipmentReq();
            }
            else {
                //    no_connection_image.setVisibility(View.VISIBLE);
                no_connection_image1.setVisibility(View.VISIBLE);

                Toast.makeText(MyShipments.this,
                        getResources().getString(R.string.check_your_network),
                        Toast.LENGTH_SHORT).show();
            }
        }
        else
        {

            // remember aakash
            editor1.putBoolean("hitMyDriverService",true);
            editor1.commit();
        }



      /*  Networking networking = new Networking(this);
        networking.makeActiveShipmentReq();

        ModelCars modelCars = AppController.getInstance().getShipment();
        Log.d(TAG, "Amrit: " + modelCars.getId());
        Log.d(TAG, "Amrit: " + modelCars.getShipment_id());*/
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        Intent i2 = new Intent(MyShipments.this, Home.class);
        i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i2);
        finish();

    }
}
