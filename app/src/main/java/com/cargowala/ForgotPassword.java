package com.cargowala;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import volley.AuthFailureError;
import volley.Request;
import volley.Response;
import volley.VolleyError;
import volley.VolleyLog;
import volley.toolbox.StringRequest;

import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Akash on 2/10/2016.
 */
public class ForgotPassword extends Activity implements View.OnClickListener{

    int deviceWidth, deviceHeight;

    RelativeLayout actionBar;
    ImageView backbutton;
    SweetAlertDialog pDialog;
    MaterialEditText edittext_email;
    MyTextView login,forgot_pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pass);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        deviceHeight = size.y;

        actionBar = (RelativeLayout) findViewById(R.id.actionBar);

        actionBar.getLayoutParams().height =  (deviceHeight / 12);

        actionBar.requestLayout();

        backbutton = (ImageView) findViewById(R.id.backbutton);
        backbutton.setOnClickListener(this);
        backbutton.getLayoutParams().height =  (deviceHeight / 20);
        backbutton.getLayoutParams().width =  (deviceHeight / 20);


        backbutton.requestLayout();

        login = (MyTextView) findViewById(R.id.login);
        login.setOnClickListener(this);
        forgot_pass = (MyTextView) findViewById(R.id.forgot_pass);
        forgot_pass.setOnClickListener(this);

        edittext_email = (MaterialEditText) findViewById(R.id.edittext_email);



    }

    String email_text;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {




            case R.id.popup_button:
                dialog.dismiss();


                break;

            case R.id.backbutton:

                finish();

                break;

            case R.id.login:

                finish();

                break;

            case R.id.forgot_pass:

               email_text = edittext_email.getText().toString();
                email_text = email_text.trim();

                if(email_text.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.please_enter_the_email),Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (isValidEmail(email_text)) {


                        if(isOnline()) {
                            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
                            pDialog.setTitleText(getResources().getString(R.string.loading));
                            pDialog.setCancelable(false);
                            pDialog.show();
                            makeForgotPassReq();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.check_your_network),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.please_enter_valid_email),Toast.LENGTH_LONG).show();
                    }
                }
                break;

        }


    }



    String res;
    StringRequest strReq;
    public String makeForgotPassReq() {

        String url = getResources().getString(R.string.base_url)+"user/forgot-password";
        Log.d("regg", "2");
        strReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("regg", "3");

                        Log.d("register", response.toString());
                        res = response.toString();
                        checkForgotPassResponse(res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("regg", "4 "+error.getMessage());
                pDialog.dismiss();
                VolleyLog.d("register", "Error: " + error.getMessage());
                res = error.toString();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("regg", "5");
                try {

                    Log.d("email_text ",  email_text);

                    params.put("email", email_text);

                    Log.e("register", params.toString());



                }
                catch(Exception i)
                {
                    Log.d("regg", "7 "+i.toString());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            // Adding request to request queue
        };


        AppController.getInstance().addToRequestQueue(strReq, "21");



        return null;
    }

    public void checkForgotPassResponse(String response)
    {
        pDialog.dismiss();
        try
        {
            Log.e("register",response);

            if (response != null) {

                Log.e("register","a");

                JSONObject reader = new JSONObject(response);

                Log.e("register","reader");

                String status_code = reader.getString("status_code");

                Log.e("register","status_code "+status_code);

                if(status_code.equalsIgnoreCase("200"))
                {


                    initiatePopupWindow(getResources().getString(R.string.an_email_has_been_sent_to_reset_your_registered_email_id_to_reset_password),
                            false, getResources().getString(R.string.email_sent), getResources().getString(R.string.ok));

                }
                else if(status_code.equalsIgnoreCase("429"))
                {



                    initiatePopupWindow(getResources().getString(R.string.this_email_id_is_not_verified_by_the_user),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }
                else if(status_code.equalsIgnoreCase("204"))
                {



                    initiatePopupWindow(getResources().getString(R.string.this_email_id_is_not_registerd_with_us),
                            true, getResources().getString(R.string.error), getResources().getString(R.string.ok));

                }
                else
                {
                    initiatePopupWindow(getResources().getString(R.string.some_problem_try_again_text),
                            true, getResources().getString(R.string.error)+" " + status_code, getResources().getString(R.string.ok));
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.some_problem_try_again_text), Toast.LENGTH_LONG).show();
            }


        }
        catch(Exception e)
        {
        }
    }




    Dialog dialog;

    private void initiatePopupWindow(String message, Boolean isAlert, String heading, String buttonText ) {
        try {

            dialog = new Dialog(ForgotPassword.this);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.toastpopup);
            dialog.setCanceledOnTouchOutside(false);
            MyTextView popupmessage = (MyTextView) dialog
                    .findViewById(R.id.popup_message);
            popupmessage.setText(message);


            ImageView popup_image = (ImageView) dialog
                    .findViewById(R.id.popup_image);
            if(isAlert)
            {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_alert));
            }
            else {
                popup_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.popup_confirm));
            }


            MyTextView popup_heading = (MyTextView) dialog
                    .findViewById(R.id.popup_heading);
            popup_heading.setText(heading);


            MyTextView popup_button = (MyTextView) dialog
                    .findViewById(R.id.popup_button);
            popup_button.setText(buttonText);
            popup_button.setOnClickListener(this);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



















    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo == null)
            return false;
        if (!activeNetworkInfo.isConnected())
            return false;
        if (!activeNetworkInfo.isAvailable())
            return false;
        return true;
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {

        }
        return false;
    }

}
